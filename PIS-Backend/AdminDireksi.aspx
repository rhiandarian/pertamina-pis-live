﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminDireksi.aspx.cs" Inherits="PIS_Backend.AdminDireksi" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Data Direksi</h2>
                    <ol class="breadcrumb">
                         <li class="breadcrumb-item">
                            Tentang PIS
                        </li>
                        <li class="breadcrumb-item">
                            <a>Manajemen</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Direksi</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="AdminDireksi.aspx"><strong> Semua Direksi</strong></a>
                        </li>
                        
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row ml-2 pb-3">
            <a href="SaveDireksi.aspx" class="btn btn-primary"><i class="fa fa-plus"></i><span class="add_new_text">Buat Baru</span></a>
        </div>
        <div class="row">
            <asp:ListView ID="ListViewDireksi" runat="server">
                <ItemTemplate>
                    <div class="col-xs-12 col-sm-4 box-content">
                                                        <div class="ibox">
                                                            <div class="ibox-content product-box">
                                                                <div class="product-imitation">
                                                                    <img src='<%# Eval("img_file") %>' class="img-fluid" alt="...">
                                                                    
                                                                </div>
                                                                <div class="product-desc">
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            
                                                                            <p class="text-center">
                                                                               <b><%# Eval("name") %></b> <br />
                                                                                <%# Eval("division") %><br />
                                                                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="EditClick" runat="server"><i class="fa fa-pencil"></i> Edit</asp:LinkButton>
                                                                                <asp:LinkButton ID="LinkButton2" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="DeleteClick" runat="server"><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                                                                                
                                                                            </p>
                                                                        </div>

                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                </ItemTemplate>
            </asp:ListView>
            
             
        </div>
        <a href="Direksi.aspx" target="_blank"><i class="fa fa-eye"></i> Lihat hasil website</a>
    </div>
</asp:Content>
