﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Configuration;


namespace PIS_Backend
{
    public class General
    {
        ModelData Db = new ModelData();



        public int VideoCategoryID = Convert.ToInt32(ConfigurationManager.AppSettings["VideoCategoryID"]);
        public int FotoCategoryID  = Convert.ToInt32(ConfigurationManager.AppSettings["FotoCategoryID"]);
        public int InfografisCategoryID = Convert.ToInt32(ConfigurationManager.AppSettings["InfografisCategoryID"]);
        public int CategoryKeberlanjutanID = Convert.ToInt32(ConfigurationManager.AppSettings["CategoryKeberlanjutanID"]);
        public int videoProfileID = Convert.ToInt32(ConfigurationManager.AppSettings["VideoProfileID"]);

        public string IndexIndPage = "";
        public string IndexEnPage = "En";

        public string search = "Search.aspx?search=";

        public bool ImageFileType(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            switch (ext.ToLower())
            {
                case ".gif":
                    return true;
                case ".jpg":
                    return true;
                case ".jpeg":
                    return true;
                case ".png":
                    return true;
                default:
                    return false;
            }
        }
        public bool SuperAdminAcessLevel(string sessionLevel)
        {
            if(sessionLevel == "1")
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public bool UserAccessLevel(string sessionLevel)
        {
            string[] ListUserAccessId = { "1", "2", "3" };
            bool result = false;
            for(int i=0; i < ListUserAccessId.Length; i++)
            {
                if(sessionLevel == ListUserAccessId[i])
                {
                    result = true;
                }
            }
            return result;
            
        }
        public bool adminAccessLevel(string sessionLevel)
        {
            string[] adminAccessId = { "1", "2" };
            bool result = false;
            for (int i = 0; i < adminAccessId.Length; i++)
            {
                if (sessionLevel == adminAccessId[i])
                {
                    result = true;
                }
            }
            return result;

        }
        public string changeCurrentUrl(string newUrl)
        {
            return "<script>history.pushState({ }, null, "+stripped(newUrl)+");</script>"; 
        }
        public bool PdfFileType(string FileName)
        {
            string ext = Path.GetExtension(FileName);

            return ext == ".pdf" ? true : false;
        }
        public string addJsonFieldValue(string field, string value, bool end = false)
        {
            string coma = !end ? "," : "";
            return stripped(field) + ":" + stripped(value) + ""+coma;
        }
        public string addJsonFieldValueInt(string field, int value, bool end = false)
        {
            string coma = !end ? "," : "";
            return stripped(field) + ":" + value + ""+coma;
        }
        public string addJsonFieldValueObject(string field, string value, bool end = false)
        {
            string coma = !end ? "," : "";
            return stripped(field) + ":" + value + "" + coma;
        }

        public string stripped(string words)
        {
            
            return "\""+words+"\" ";
            
        }
        public string alert(string word)
        {
            return "<script>alert('"+word+"');</script>";
        }
        internal string getDateFull(string v1, string v2)
        {
            throw new NotImplementedException();
        }
        public string changeLangMobile(string lang, bool master = false)
        {
            string changeLang = master == true ? "changeLang('"+lang+"')" : "changeLang()";
            string attributeID = lang == "En" ? "onclick="+stripped(changeLang) : "class="+stripped("active");
            string atrributeEn = lang == "En" ? "class=" + stripped("active") : "onclick=" + stripped(changeLang);
            return "<span href=" + stripped("") + " " + attributeID + " >ID</span> | <span href=" + stripped("") + " "+atrributeEn+" >EN</span>";
        }
        public string getDetailPaging(string lang, int PageIndex, int totalCount)
        {
            string page = "";
            string dari = "";
            switch(lang)
            {
                case "En":
                    page = "Pages";
                    dari = "of";
                    break;
                case "Ind":
                    page = "Halaman";
                    dari = "dari";
                    break;
                default:
                    page = "Halaman";
                    dari = "dari";
                    break;

            }
            return page+": " + PageIndex + " "+dari+" "+ totalCount;
        }
        public string div(string content, string id = "", string classes = "", string attribute = null,string attributeval = null)
        {
            id = getId(id);
            classes = getClass(classes);
            attribute = setAttribute(attribute,attributeval);
            
            return "<div "+id+" "+classes+" "+attribute+" >"+content+"</div>";
        }
        public string paragraph(string content, string id = "", string classes = "", string attribute = null, string attributeval = null)
        {
            id = getId(id);
            classes = getClass(classes);
            attribute = setAttribute(attribute, attributeval);

            return "<p "+id+" "+classes+" "+attribute+">"+content+"</p>";
        }
        public string divRow(string content, string id = "", string otherClass = "", string attribute = null, string attributeval = null)
        {
            string classes = "row";
            classes += otherClass != "" ? " " + otherClass : "";

            return div(content, id, classes, attribute, attributeval);
        }
        public string divCol(string content, string id = "", string otherClass = "", string attribute = null, string attributeval = null)
        {
            string classes = "col";
            classes += otherClass != "" ? " " + otherClass : "";

            return div(content, id, classes, attribute, attributeval);
        }
        public string getId(string id = "")
        {
            return id != "" ? "id=" + stripped(id) : "";
        }
        public string getClass(string classes = "")
        {
            return classes != "" ? "class=" + stripped(classes) : "";
        }
        public string setAttribute(string attribute = null, string attributeval = null)
        {
            return attribute != null ? attribute + "=" + stripped(attributeval) : "";
        }
        public bool isCurrentLink(string[]currPages,string pages)
        {
            for(int i = 0; i < currPages.Length; i++)
            {
                if(pages == currPages[i])
                {
                    return true;
                }
            }
            return false;
        }
        public List<Menu> profComp(string lang)
        {
            
            string[] EnMenu = { "Company Overview", "Our Logo", "Milestone" };
            string[] InMenu = { "Sekilas Tentang PIS", "Makna Logo PIS", "Tonggak Sejarah" };
            string[] pages =  { "Tentang_PIS.aspx", "makna-logo-pis.aspx", "TonggakSejarah.aspx" };

            List<Menu> lm = addSubMenu(lang, EnMenu, InMenu, pages);

            return lm;
            
        }
        public List<Menu> addSubMenu(string lang,string[] EnMenu,string[]InMenu, string[]pages)
        {
            List<Menu> ps = new List<Menu>();
            for (int i = 0; i < EnMenu.Length; i++)
            {
                ps.Add(new Menu()
                {
                    Name = lang == "En" ? EnMenu[i] : InMenu[i],
                    Link = pages[i],
                });
            }
            return ps;
        }
        public List<Menu> mngmnt(string lang)
        {
            
           
            string[] EnMenu = { "Board of Commisioners", "Board of Directors", "Message from the CEO", "Organization Structure &<br> Company Group" };
            string[] InMenu = { "Dewan Komisaris", "Dewan Direksi", "Sambutan Direksi","Struktur Organisasi & <br> Kelompok Perusahaan" };
            string[] pages  = { "Dewan_Komisaris.aspx","Direksi.aspx","sambutan-direktur-utama.aspx","Struktur Organisasi.aspx"};

            List<Menu> lm = addSubMenu(lang,EnMenu,InMenu,pages);

            return lm;

        }
        public List<Menu> aboutpis(string lang, string[]pisPage)
        {
            List<Menu> pis = new List<Menu>();

            
            pis.Add(new Menu()
            {
                Name = lang == "En" ? "Vision & Mission" : "Visi dan Misi",
                Link = pisPage[0],
            });
            pis.Add(new Menu()
            {
                Name = lang == "En" ? "Corporate Values" : "Tata Nilai",
                Link = pisPage[1],
            });
            pis.Add(new Menu()
            {
                Name = lang == "En" ? "Company Profile" : "Profil Perusahaan",
                classMenu = "drop-down",
                Link = "",
                SubMenu = profComp(lang)
            });
            pis.Add(new Menu()
            {
                Name = lang == "En" ? "Management" : "Manajemen",
                classMenu = "drop-down",
                Link = "",
                SubMenu = mngmnt(lang)
            });
            pis.Add(new Menu()
            {
                Name = lang == "En" ? "International Standard" : "Standar Internasional",
                Link = lang == "En" ? pisPage[12] : pisPage[11],
            });
            return pis;
        }
        public List<Menu> Activity(string lang,string[]pages)
        {
            List<Menu> pis = new List<Menu>();
           
            pis.Add(new Menu()
            {
                Name = lang == "En" ? "Our Services" : "Layanan Kami",
                Link = pages[0]
            });
            pis.Add(new Menu()
            {
                Name = lang == "En" ? "Our Fleet" : "Armada Kami",
                Link = lang == "En" ? pages[3] : pages[1]
            });

            return pis;
        }
        public List<Menu> MediaInformasi(string lang, string[]pages)
        {
            List<Menu> pis = new List<Menu>();


            pis.Add(new Menu()
            {
                Name = lang == "En" ? "News" : "Berita",
                Link = pages[0]
            });
            pis.Add(new Menu()
            {
                Name = lang == "En" ? "Gallery" : "Galeri",
                Link = "AllGallery.aspx"
            });
            pis.Add(new Menu()
            {
                Name = lang == "En" ? "Presentation" : "Presentasi",
                Link = pages[4]
            });

            return pis;
        }
        public List<Menu> GenerateMenu(string lang, string UrlCurrent)
        {
            List<Menu> lm = new List<Menu>();
            string[] home = { "/", "Index_en.aspx", "Index.aspx" };
            lm.Add(new Menu()
            {
                Name = lang == "En" ? "Home" : "Beranda",
                Link = lang == "En" ? home[1] : home[0],
                classMenu = isCurrentLink(home,UrlCurrent) ? "active" : "",
                
            });
            string[] aboutPIS = { "VisidanMisi.aspx","Tata_Nilai_dan_Budaya.aspx","Tentang_PIS.aspx","makna-logo-pis.aspx","TonggakSejarah.aspx",
                    "Dewan_Komisaris.aspx","Board%20of%20Commissioners.aspx","Direksi.aspx","Directors.aspx","sambutan-direktur-utama.aspx","Struktur%20Organisasi.aspx",
                    "Standard%20International.aspx","International%20Standard.aspx","visi-misi.aspx","vision-mission.aspx","dewan-komisaris.aspx","commissioners.aspx",
                    "tentang-pis.aspx","company-overview.aspx","our-logo.aspx","corporate-value.aspx","tata-nilai-budaya.aspx","tonggak-sejarah.aspx","milestone.aspx","dewan-direksi.aspx","directors.aspx","message-from-the-ceo.aspx",
                    "struktur-organisasi.aspx","organization-structure.aspx","standar-internasional.aspx","international-standard.aspx"
            };
            
            lm.Add(new Menu()
            {
                Name = lang == "En" ? "About PIS" : "Tentang PIS",
                Link = "",
                classMenu = isCurrentLink(aboutPIS,UrlCurrent) ? "drop-down active" : "drop-down",
                SubMenu = aboutpis(lang,aboutPIS) 
            });
            string[] actBisnis = { "layanan-kami.aspx", "Armada-Kami.aspx", "detail-armada.aspx", "Our%20Fleet.aspx", "our-services.aspx","armada-kami.aspx","our-fleet.aspx" };
            lm.Add(new Menu()
            {
                Name = lang == "En" ? "Business Activity" : "Aktivitas Bisnis",
                Link = "",
                classMenu = isCurrentLink(actBisnis, UrlCurrent) ? "drop-down active" : "drop-down",
                SubMenu = Activity(lang,actBisnis)
            });
            string[] medinfo = { "AllNews.aspx","news-detail.aspx","Galleri.aspx", "Galleries.aspx", "Persentasi.aspx","presentasi.aspx","presentation.aspx","berita.aspx","news.aspx","berita-detail.aspx","galeri.aspx","gallery.aspx" };
            lm.Add(new Menu()
            {
                Name = lang == "En" ? "Media & Information" : "Media & Informasi",
                Link = "",
                classMenu = isCurrentLink(medinfo, UrlCurrent) ? "drop-down active" : "drop-down",
                SubMenu = MediaInformasi(lang,medinfo)
            });
            
            return lm;
        }
        public bool otherMenuCurrentUrl(string pages = null)
        {
            bool result = false;
            if(pages != null)
            {
                string[] otherMenu =
                {
                    "laporan-tahunan.aspx",
                    "laporan-ikhtisar-keuangan.aspx",
                    "komposisi-pemegang-saham.aspx",
                    "informasi-dividen.aspx",
                    //"R-U-P-S.aspx",
                    "tata-kelola-perusahaan.aspx",
                    "penghargaan.aspx",
                    "keberlanjutan.aspx",
                    "informasi-pengadaan.aspx",
                    "career.aspx",
                    "contacts.aspx",
                    "annual-report.aspx",
                    "financial-statement.aspx",
                    "shareholders-composition.aspx",
                    "dividend-information.aspx,",
                    "agms.aspx",
                    "good-corporate-governance.aspx",
                    "awards.aspx",
                    "sustainability.aspx",
                    "procurement-information.aspx",
                    "karier.aspx",
                    "kontak.aspx",
                    "Search.aspx"
                    

                };
                if(isCurrentLink(otherMenu,pages))
                {
                    result = true;
                }
            }
            return result;
        }
        public string getOtherMenuClass(string currClass,string pages)
        {
            return otherMenuCurrentUrl(pages) ? currClass + " active" : currClass;
        }

        public List<Menu> Investor(string lang)
        {
            List<Menu> lm = new List<Menu>();

            lm.Add(new Menu()
            {
                Name = lang == "En" ? "Annual Report" : "Laporan Tahunan",
                Link = "laporan-tahunan.aspx",

            });
            lm.Add(new Menu()
            {
                Name = lang == "En" ? "Financial Statement & Highlight" : "Laporan & Ikhtisar Keuangan",
                Link = "laporan-ikhtisar-keuangan.aspx",

            });
            List<Menu> ish = new List<Menu>();
            string[] En = { "Shareholder Composition", "Dividend Information" };
            string[] In = { "Komposisi Pemegang Saham", "Informasi Dividen" };
            string[] Pages = { "komposisi-pemegang-saham.aspx", "informasi-dividen.aspx" };
            for(int i=0; i<Pages.Length; i++)
            {
                ish.Add(new Menu()
                {
                    Name = lang == "En" ? En[i] : In[i],
                    Link = Pages[i],
                });
            }
            lm.Add(new Menu()
            {
                Name = lang == "En" ? "Stock Information" : "Informasi Saham",
                Link = "",
                classMenu = "drop-down",
                SubMenu = ish

            });
            //lm.Add(new Menu()
            //{
            //    Name = lang == "En" ? "AGMS" : "RUPS",
            //    Link = "RUPS.aspx",

            //});

            return lm;
        }
        public List<Menu> komit(string lang)
        {
            string[] En = { "Good Corporate Governance", "Awards", "Sustainability" };
            string[] ind = { "Tata Kelola Perusahaan", "Penghargaan", "Keberlanjutan" };
            string[] pages = { "tata-kelola-perusahaan.aspx", "penghargaan.aspx", "keberlanjutan.aspx" };
            List<Menu> lm = new List<Menu>();
            for(int i=0; i< En.Length; i++)
            {
                lm.Add(new Menu()
                {
                    Name = lang == "En" ? En[i] : ind[i],
                    Link = pages[i]
                });
            }
            return lm;
        }
        public List<Menu> ip(string lang)
        {
            string[] En = { "Procurement Information", "General Procurement & e-Procurement"};
            string[] ind = { "Informasi Pengadaan", "Pengadaan Umum & e-Procurement" };
            string[] pages = { "informasi-pengadaan.aspx", "https://smart.gep.com/publicrfx/pertamina?oloc=215#/" };
            List<Menu> lm = new List<Menu>();
            for (int i = 0; i < En.Length; i++)
            {
                lm.Add(new Menu()
                {
                    Name = lang == "En" ? En[i] : ind[i],
                    Link = pages[i]
                });
            }
            return lm;
        }
        public List<Menu> MenuOthers(string lang, string pages = null)
        {
            List<Menu> lm = new List<Menu>();
            string[] investorMenu =
               {
                    "laporan-tahunan.aspx",
                    "laporan-ikhtisar-keuangan.aspx",
                    "komposisi-pemegang-saham.aspx",
                    "informasi-dividen.aspx",
                    "R-U-P-S.aspx",
                     "financial-statement.aspx",
                    "shareholders-composition.aspx",
                    "dividend-information.aspx,",
                    "agms.aspx"

                };
            lm.Add(new Menu()
            {
                Name = lang == "En" ? "Investor Relations" : "Hubungan Investor",
                Link = "",
                classMenu = pages != null && isCurrentLink(investorMenu,pages) ? "drop-down active" : "drop-down",
                SubMenu = Investor(lang)
               
            });
            string[] komLanjutMenu =
                {
                    "tata-kelola-perusahaan.aspx",
                    "penghargaan.aspx",
                    "keberlanjutan.aspx",
                    "good-corporate-governance.aspx",
                    "awards.aspx",
                    "sustainability.aspx"
                };
            lm.Add(new Menu()
            {
                Name = lang == "En" ? "Commitment & Sustainability" : "Komitmen & Keberlanjutan",
                Link = "",
                classMenu = pages != null && isCurrentLink(komLanjutMenu,pages) ? "drop-down active" : "drop-down",
                SubMenu = komit(lang)

            });
            string[] pengadaanMenu =
                {
                    "informasi-pengadaan.aspx", "procurement-information.aspx",
                    
                };
            lm.Add(new Menu()
            {
                Name = lang == "En" ? "Procurement" : "Pengadaan",
                Link = "",
                classMenu = pages != null && isCurrentLink(pengadaanMenu,pages) ? "drop-down active" : "drop-down",
                SubMenu = ip(lang)

            });

            string[] karirMenu = { "career.aspx", "karier.aspx" }; 
            lm.Add(new Menu()
            {
                Name = lang == "En" ? "Career" : "Karier",
                Link = "career.aspx",
                classMenu = pages != null && isCurrentLink(karirMenu,pages) ? "active" : ""
            });
            string[] kontakMenu = { "contacts.aspx", "kontak.aspx" };
            lm.Add(new Menu()
            {
                Name = lang == "En" ? "Contact" : "Kontak",
                Link = "contacts.aspx",
                classMenu = pages != null && isCurrentLink(kontakMenu, pages) ? "active" : ""
            });
            return lm;
        }
        public int getTotalPaging(int recordCount, int CurrentPage, int PageSize)
        {
            double dblPageCount = (double)((decimal)recordCount / Convert.ToDecimal(PageSize));
            int pageCount = (int)Math.Ceiling(dblPageCount);
            return pageCount;
        }
        public List<Paging> PopulatePaging(int recordCount, int CurrentPage, int PageSize)
        {
            int index = 0;
            for (int a = 0; a <= recordCount; a += 3)
            {
                if (a >= CurrentPage)
                {
                    index = a;
                    break;
                }
            }
            index -= 2;
            List<Paging> pages = new List<Paging>();
            int limit = index + 2;
            int limitSize = getTotalPaging(recordCount, CurrentPage, PageSize);
            if (limitSize < limit)
            {
                limit = limitSize;
            }
            for (int i = index; i <= limit; i++)
            {

                pages.Add(new Paging() { Index = i, active = i == CurrentPage ? "page-item active" : "page-item" });

            }



            return pages;
        }

    }
}