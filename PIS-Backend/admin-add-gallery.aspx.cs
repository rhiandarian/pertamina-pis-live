﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using System.IO;

namespace PIS_Backend
{
    public partial class admin_add_gallery : System.Web.UI.Page
    {
        General all = new General();
        Bitmap bmpImg = null;
        ModelData db = new ModelData();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                
                LoadCategory();
                PanelVideo.Visible = false;
                PanelImage.Visible = false;
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                loaddata();
            }
        }
        public void loaddata()
        {
            
            string pageName = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
            Gallery g = new Gallery();
            int type = g.findCategoryPage(pageName);
            string pageID = g.findPageAdminID(pageName);
            Level l = new Level();
            if (!l.isValidAccessPage(Session["Level"].ToString(), pageID))
            {
                Response.Redirect("admin.aspx");
            }
            DdlCategory.SelectedValue = type.ToString();
            if (type == all.VideoCategoryID)
            {
                PanelVideo.Visible = true;
               
                

            }
            else
            {
                PanelImage.Visible = true;
             
            }
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void LoadCategory()
        {
            Gallery g = new Gallery();
            DataSet dt = g.LoadCategory(2);

            DdlCategory.DataTextField = dt.Tables[0].Columns["Category_name"].ToString();
            DdlCategory.DataValueField = dt.Tables[0].Columns["ID"].ToString();
            DdlCategory.DataSource = dt.Tables[0];
            DdlCategory.DataBind();
            DdlCategory.Items.Insert(0, "Pilih Category");
            DdlCategory.SelectedIndex = 0;
            DdlCategory.Enabled = false;

        }

        protected void DdlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DdlCategory.SelectedValue != "Pilih Category")
            {
                if (DdlCategory.SelectedValue == all.VideoCategoryID.ToString())
                {
                    PanelVideo.Visible = true;
                    PanelImage.Visible = false;
                }
                else
                {
                    PanelImage.Visible = true;
                    PanelVideo.Visible = false;
                }
            }

        }
        public string randomTxt()
        {
            ImageResize ir = new ImageResize();
            return ir.textrandom();
        }
       
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string random = randomTxt();
                Gallery g = new Gallery();
                g.Title = title.Text;
                g.TitleEn = title_en.Text;
                g.Date = Convert.ToDateTime(date.Text);
                
                bool fileValid = true;
                bool file1Exsist = false;
                bool file2exsist = false;
                if (DdlCategory.SelectedValue != "Pilih Category")
                {
                    g.Category = Convert.ToInt32(DdlCategory.SelectedValue);
                    if(g.Category != all.VideoCategoryID)
                    {
                        if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
                        {
                            fileValid = true;
                            file1Exsist = true;
                            g.Img_File = random + FileUpload1.FileName;
                            g.FileImageSize = FileUpload1.PostedFile.ContentLength;
                        }
                        else
                        {
                            fileValid = false;
                        }
                    }
                    
                }
               
                if (g.Category == all.VideoCategoryID)
                {
                    g.Content = content.InnerHtml;
                    g.ContentEn = content_en.InnerText;
                    g.Video_url = videoUrl.Text;
                    g.Img_url = random + FileUpload2.FileName;
                    if((FileUpload2.PostedFile != null) && (FileUpload2.PostedFile.ContentLength > 0))
                    {
                        file2exsist = true;
                        g.FileCoverSize = FileUpload2.PostedFile.ContentLength;
                    }
                    else
                    {
                        fileValid = false;
                    }

                }
                
                if (!fileValid)
                {
                    showValidationMessage("Image tidak Lengkap");
                }
                else
                {
                    
                    string insertStatus = g.Insert(getip(),GetBrowserDetails(), DdlCategory.SelectedValue);

                    if (insertStatus == "success")
                    {
                        if (file1Exsist)
                        {
                            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                            string SetFolderBeforeResize = Server.MapPath("Gallerry") + "\\" + "serize" + random + fn;
                            string SetFolderAfterResize = Server.MapPath("Gallerry") + "\\" + random + fn;
                            FileUpload1.PostedFile.SaveAs(SetFolderBeforeResize);
                            ImageResize Iz = new ImageResize();
                            Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "Gallery", "Cut");
                        }
                        if(file2exsist) {
                            string fn2 = System.IO.Path.GetFileName(FileUpload2.PostedFile.FileName);
                            string SetFolderBeforeResize2 = Server.MapPath("Gallerry") + "\\" + "serize" + random + fn2;
                            string SetFolderAfterResize2 = Server.MapPath("Gallerry") + "\\" + random + fn2;
                            FileUpload2.PostedFile.SaveAs(SetFolderBeforeResize2);
                            ImageResize Iz2 = new ImageResize();
                            Iz2.ResizeImage(SetFolderBeforeResize2, SetFolderAfterResize2, "Gallery", "Cut");

                        }
                        string pageName = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
                        Response.Redirect(g.ListPageGallery(pageName));
                        
                    }
                    else
                    {

                        showValidationMessage(insertStatus);
                    }
                }
            }
            catch(Exception ex)
            {
                Gallery g = new Gallery();
                g.ErrorLogHistory(getip(), GetBrowserDetails(), "Insert", g.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                showValidationMessage(ex.Message.ToString());
            }
            
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Gallery g = new Gallery();
            string pageName = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
            Response.Redirect(g.ListPageGallery(pageName));
        }
    }
}