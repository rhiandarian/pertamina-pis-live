﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace PIS_Backend
{
    public partial class ListBanner : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if(!l.isValidAccessPage(Session["Level"].ToString(),"2"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindGVBanner();
                
            }
        }
        public void BindGVBanner()
        {
            Banner bnr = new Banner();
            DataTable dt = bnr.Select("*");
            GridViewBanner.DataSource = dt;
            GridViewBanner.DataBind();
        }

        protected void GridViewBanner_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGVBanner();
            GridViewBanner.PageIndex = e.NewPageIndex;
            GridViewBanner.DataBind();
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Response.Redirect("SaveBanner.aspx?ID=" + id);
        }
        protected void Delete_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Banner bnr = new Banner();
            string cond = " ID = " + id;
            try
            {
                bnr.Delete(id, getip(), GetBrowserDetails(), cond);
                BindGVBanner();
            }
            catch(Exception ex)
            {
                General all = new General();
                bnr.ErrorLogHistory(getip(), GetBrowserDetails(), "Delete", bnr.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                Response.Write(all.alert("Error " + ex.Message.ToString()));
            }
            

        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
    }
}