﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class AdminListAlbum : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                
                LoadAlbum();
            }
        }
        public void LoadAlbum()
        {
            Level l = new Level();
            if (!l.isValidAccessPage(Session["Level"].ToString(), "6"))
            {
                Response.Redirect("admin.aspx");
            }
            if (Request.QueryString["GalleryID"] != null)
            {
                int GalleryID = Convert.ToInt32(Request.QueryString["GalleryID"].ToString());
                Image img = new Image();
                string conditions = " GalleryID =" + GalleryID;
                DataTable dt = img.Select("ID,title, img_file, CONVERT(varchar,addDate,106) as 'image_date'", conditions);
                ListViewALbum.DataSource = dt;
                ListViewALbum.DataBind();
            }
            else
            {
                Response.Redirect("admin-list-gallery.aspx");
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Image img = new Image();
            string cond = " ID = " + id;
            General all = new General();
            try
            {
                img.Delete(id,getip(),GetBrowserDetails(),cond);
                LoadAlbum();
            }
            catch(Exception ex)
            {
                img.ErrorLogHistory(getip(), GetBrowserDetails(), "Delete", img.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int GalleryID = Convert.ToInt32(Request.QueryString["GalleryID"].ToString());
            Response.Redirect("admin-tambah-album?GalleryID=" + GalleryID);
        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            Gallery g = new Gallery();
            string link = g.adminListPage + "?type=" + all.FotoCategoryID;
            Response.Redirect(link);
        }

        protected void LinkButtonBack_Click(object sender, EventArgs e)
        {
            int GalleryID = Convert.ToInt32(Request.QueryString["GalleryID"].ToString());
           
            Response.Redirect("EditFoto?ID=" + GalleryID);
        }
    }
}