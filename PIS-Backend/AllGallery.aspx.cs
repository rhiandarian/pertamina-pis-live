﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class AllGallery : System.Web.UI.Page
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["Lang"] == null || Session["Lang"].ToString() == "Ind")
            {
                Response.Redirect("Galleri.aspx");
            }
            else
            {
                Response.Redirect("Galleries.aspx");
            }
            
        }
      
    }
}