﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace PIS_Backend
{
    public class InfoPengadaan:ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName = "InfoPengadaan";

        string viewName = "View_InfoPengadaan";

        public int PageSize = 4;

        public int pageID = 25;

        public int maxSize = 60000;

        public string maxSizeInstring = "50 KB";

        General all = new General();

        public string folder = "I_P";

        public string listPage = "AdminPengadaan.aspx";

        public string savePage = "SavePengadaan.aspx";

        public string title;

        public string title_en;

        public string url;

        public string cover;

        public string orderBy;

        public int file_input_size;

        public DateTime dates;

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string TitleEn
        {
            get { return title_en; }
            set { title_en = value; }
        }
        public int FileInputSize
        {
            get { return file_input_size; }
            set { file_input_size = value; }
        }



        public string Url
        {
            get { return url; }
            set { url = value; }
        }


        public string Cover
        {
            get { return cover; }
            set { cover = value; }
        }

        public string OrderBy
        {
            get { return orderBy; }
            set { orderBy = value; }
        }
        public DateTime Dates
        {
            get { return dates; }
            set { dates = value; }
        }

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();
        public DataTable Select(string field, string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            if(orderBy != null)
            {
                Db.OrderBy = orderBy;
            }
            string Query = Db.SelectQuery(field, viewName, conditions);

            DataTable dt = Db.getData(Query);

            return dt;
        }
        public string checkQuery(string field, string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            Db.OrderBy = orderBy != null ? orderBy : "";
            string Query = Db.SelectQuery(field, viewName, conditions);

            return Query;
        }

        public PagesData getPage()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("Header, content, content_en", " ID = " + pageID);

            PagesData p = new PagesData();
            p.Header = dt.Rows[0]["Header"].ToString();
            p.Content = dt.Rows[0]["content"].ToString();
            p.Content_En = dt.Rows[0]["content_en"].ToString();

            return p;

        }
        public DataTable LoadData(int PageIndex, string lang)
        {
            SqlCommand cmd = new SqlCommand("GetAllInfoPengadaan", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PageIndex", PageIndex);
            cmd.Parameters.AddWithValue("@PageSize", PageSize);

            cmd.Parameters.AddWithValue("@lang", lang);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public string validate()
        {
            
            if (title == "")
            {
                return "Judul dalam bahasa Indonesia harus diisi";
            }
            if(title_en == "")
            {
                return "Judul dalam bahasa Inggris harus di isi";
            }
            if (url != "")
            {
                if (!all.PdfFileType(url))
                {
                    return "File Pengadaan harus pdf";
                }
            }
            if (cover != null)
            {
                if (!all.ImageFileType(cover))
                {
                    return "Masukan file foto";
                }
            }
            if (file_input_size > maxSize)
            {
                return "Ukuran Foto terlalu besar, Maximum Size " + maxSizeInstring;
            }
            return "Valid";
        }
        public string Insert(string mainIP,string browserDetail,string random)
        {

            string validation = validate();
            if (validation != "Valid")
            {
                return validation;
            }
            if(url == "")
            {
                return "File Pengadaan harus diUpload";
            }
            if (cover == "")
            {
                return "File Cover harus di Upload";
            }
            string field = "title, title_en, Dates, url, cover, ";
            string value = "'" + title + "', '" + title_en + "', '" + dates + "',  '" + url + "', '" + folder + "/"+random + cover + "', ";
            Db.Insert(tableName, field, value, dateNow, user_id,mainIP,browserDetail);

            return "success";
        }
        public string Update(string random,string mainIP, string browserDetail, int Id, string cond = null)
        {
            string validation = validate();
            if (validation != "Valid")
            {
                return validation;
            }
            string newRecord = " title = '" + title + "', ";
            newRecord += " title_en = '" + title_en + "', ";
            newRecord += " Dates = '" + dates + "', ";
            newRecord += url != "" ? " url = '" + url + "', " : "";
            newRecord += cover != null ? " cover = '" + folder + "/"+random + cover + "', " : "";

            cond = cond == null ? "" : cond;
            Db.Update(tableName, newRecord, dateNow, user_id, cond,mainIP,browserDetail,Id);

            return "success";
        }
        public void Delete(int Id,string mainIP, string browserDetail, string condition = null)
        {
            string conditions = condition != null ? condition : "";

            Db.Delete(tableName, dateNow, user_id, conditions,Id,mainIP,browserDetail);
        }
    }
}