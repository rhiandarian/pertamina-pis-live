﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="ViewNews.aspx.cs" Inherits="PIS_Backend.ViewNews" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid" style="background-color:ghostwhite">
        <div class="text-center pt-5">
            <h2>News</h2>
        </div><hr />
        <div class="row">
            <div class="col">
                <h4 id="title" runat="server"></h4>
                <asp:Label ID="LblDate" runat="server" Text="Label" Font-Bold="true"></asp:Label>
            </div>
        </div>
        
        <div class="row pt-3">
            <div class="col">
                <asp:Image ID="imgArticle" runat="server"  Width="95%"/>
            </div>
        </div>

        <div class="row pt-3">
            <div class="col">
                <p id="content" runat="server">

                </p>
            </div>
        </div>
        <div class="text-right">
            <a class="btn btn-info" href="News.aspx"><i class="fa fa-home"></i></a>
        </div>
    </div>
</asp:Content>
