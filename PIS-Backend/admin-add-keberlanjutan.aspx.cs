﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using System.IO;

namespace PIS_Backend
{
    public partial class admin_add_keberlanjutan : System.Web.UI.Page
    {
        General all = new General();
        Bitmap bmpImg = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "28"))
                {
                    Response.Redirect("admin.aspx");
                }
                LoadCategory(); 
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void LoadCategory()
        {
           
        }

         

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string randomtext = textrandom();
                General gn = new General();
                Gallery g = new Gallery();
                g.Title = title.Text;
                g.TitleEn = title_en.Text;
                //g.Dates = DateTime.Now.ToString();
                g.Date = Convert.ToDateTime(date.Text);
                bool fileValid = true;
                bool fileExsist = false;
               
                    g.Category = gn.CategoryKeberlanjutanID;
                    if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
                    {
                        fileValid = true;
                        fileExsist = true;
                        g.Img_File = randomtext + FileUpload1.FileName;
                    }
                    else
                    {
                        fileValid = false;
                    }
                  

                if (!fileValid)
                {
                    showValidationMessage("File harus di Upload");
                }
                else
                {
                    string insertStatus = g.Insert(getip(),GetBrowserDetails(), all.CategoryKeberlanjutanID.ToString());

                    if (insertStatus == "success")
                    {
                        if (fileExsist)
                        {
                            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                            string SetFolderBeforeResize = Server.MapPath("Gallerry") + "\\" + "serize" + fn;
                            string SetFolderAfterResize = Server.MapPath("Gallerry") + "\\" + randomtext + fn;
                            FileUpload1.PostedFile.SaveAs(SetFolderBeforeResize);
                            ImageResize Iz = new ImageResize();
                            Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "Gallery", "Cut");
                        }
                        Response.Redirect("admin_listkeberlanjutan.aspx");
                    }
                    else
                    {
                        showValidationMessage(insertStatus);
                    }
                }
            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
            
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public string textrandom()
        {
            Random RNG = new Random();
            int length = 6;
            var rString = "";
            for (var i = 0; i < length; i++)
            {
                rString += ((char)(RNG.Next(1, 26) + 64)).ToString().ToLower();
            }
            return rString;
        }
    }
}