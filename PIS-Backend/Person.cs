﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PIS_Backend
{
    public class Person:ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName = "Person";

        public string viewName = "View_Person";

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        General all = new General();

        public int ID
        {
            get;
            set;
        }

        public string name;

        public int personType;

        public string division;

        public string division_en;

        public string description;

        public string description_en;

        public string img_file;

        public string folderName;

        public string Name { get { return name; } set { name = value; } }

        public int PersonType { get { return personType; } set { personType = value; } }

        public string Division { get { return division; } set { division = value; } }

        public string DivisionEn { get { return division_en; } set { division_en = value; } }

        public string Description { get { return description; } set { description = value; } }

        public string DescriptionEn { get { return description_en; } set { description_en = value; } }

        public string Img_File { get { return img_file; } set { img_file = value; } }

        public string FolderName { get { return folderName; } set { folderName = value; } }

        public DataTable Select(string field, string cond = null)
        {
            cond = cond == null ? "" : cond;
            string Query = Db.SelectQuery(field, viewName, cond);
            DataTable dt = Db.getData(Query);

            return dt;
        }
        public DataTable  allKomisaris()
        {
            int ptid = 1;
            string cond = " personTypeID = " + ptid;
            DataTable dt = Select("*", cond);
            return dt;
        }
        public DataTable allDirectors()
        {
            int ptid = 2;
            string cond = " personTypeID = " + ptid;
            DataTable dt = Select("*", cond);
            return dt;
        }
        public List<Person> getUtama(DataTable dt, string lang)
        {
            List<Person> lp = new List<Person>();
            lp.Add(new Person() {
                ID = Convert.ToInt32(dt.Rows[0]["ID"].ToString()),
                Img_File = dt.Rows[0]["img_file"].ToString(),
                Name = dt.Rows[0]["name"].ToString(),
                Division = lang == "En" ? dt.Rows[0]["division_en"].ToString() : dt.Rows[0]["division"].ToString()
            });
            return lp;
        }
        public List<Person> getAll(DataTable dt, string lang)
        {
            List<Person> lp = new List<Person>();
            for(int i = 1; i < dt.Rows.Count; i++)
            {
                lp.Add(new Person()
                {
                    ID = Convert.ToInt32(dt.Rows[i]["ID"].ToString()),
                    Img_File = dt.Rows[i]["img_file"].ToString(),
                    Name = dt.Rows[i]["name"].ToString(),
                    Division = lang == "En" ? dt.Rows[i]["division_en"].ToString() : dt.Rows[i]["division"].ToString()
                });
            }
            
            return lp;
        }
        public string Validate()
        {
            if(name == "")
            {
                return "Name harus diisi";
            }
            if(division == "")
            {
                return "Divisi dalam bahasa Indonesia harus diisi";
            }
            if(division_en == "")
            {
                return "Divisi dalam bahasa inggris harus di isi";
            }
            if(description == "")
            {
                return "Deskripsi dalam bahasa Indonesia harus diisi";
            }
            if(description_en == "")
            {
                return "Deskripsi dalam bahasa inggris harus di isi";
            }
            if(personType == 0)
            {
                return "Tipe tidak diketahui";
            }

            if(img_file != null && img_file != "")
            {
                if(folderName == null || folderName == "")
                {
                    return "Folder Name tidak di ketahui";
                }
                if(!all.ImageFileType(img_file))
                {
                    return "Masukan file foto";
                }
            }
            return "Valid";
        }
        public string Insert(string mainIP,string browserDetail,string textRandom)
        {
            string validation = Validate();
            if(validation == "Valid")
            {
                if(img_file == "")
                {
                    return "File harus di Upload";
                }
                img_file = textRandom + "" + img_file;
                //
                string field = "name, personTypeID, division, division_en, description, description_en, img_file, ";
                string value = "'"+name+"', "+personType+", '"+division+ "', '" + division_en.Replace("'","`") + "', '" + description.Replace("'","`")+ "', '" + description_en.Replace("'","`") + "', '" + folderName+"/"+img_file+"', ";

                Db.Insert(tableName, field, value, dateNow, user_id,mainIP,browserDetail);

                return "success";

            }
            else
            {
                return validation;
            }
        }
        public string Update(int Id,string mainIP, string browserDetail,string conditions = null)
        {
            string validation = Validate();
            if(validation == "Valid")
            {
                string newRecord = "name = '" + name + "', ";
                newRecord += "personTypeID = " + personType + ", ";
                newRecord += "division = '" + division + "', ";
                newRecord += "division_en = '" + division_en.Replace("'","`") + "', ";
                newRecord += "description = '" + description.Replace("'","`")+ "', ";
                newRecord += "description_en = '" + description_en.Replace("'", "`") + "', ";
                newRecord += img_file != null ? "img_file = '" + folderName + "/" + img_file + "', " : "";

                conditions = conditions == null ? "" : conditions;
                Db.Updates(tableName, newRecord, conditions, dateNow, user_id,mainIP,browserDetail,Id);

                return "success";
            }
            else
            {
                return validation;
            }
        }
        public void Delete(string Id,string mainIP, string browserDetail,string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            Db.Delete(tableName, dateNow, user_id, conditions,Convert.ToInt32(Id),mainIP,browserDetail);
        }
    }
}