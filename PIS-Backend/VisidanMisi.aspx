﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="VisidanMisi.aspx.cs" Inherits="PIS_Backend.Visi_Misi" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
   <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx" id="home_text" runat="server">Home</a></li>
					    <li class="breadcrumb-item" id="tentang_text" runat="server"> Tentang PIS </li>
					    <li class="breadcrumb-item active" aria-current="page" id="vm_text" runat="server">Visi Misi</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="header_text" runat="server">VISI & MISI</h3> 
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <section id="visi-misi">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div class="col-12">
					 <%-- <div class="row">
						  <div class="box-left align-self-center bg-blue">
								<img src="images/icons/visi.svg" class="img-fluid" alt="...">
					  </div>
						  <div class="ml-2 box-left  align-self-center bg-red">
							<img src="images/icons/misi.svg" class="img-fluid" alt="...">     
						</div>
						  --%>
	  					<h5><strong id="content_text" runat="server">VISI & MISI </strong></h5>
					  </div>
	  				
	  			</div>
	  			 <div class="row">

	  				<div id="content" class="col-12" runat="server">
                          
	  				</div> 

						<%--<center class="mt-4"><a id="fileDownload" runat="server"  target="_blank" class="btn btn-default-pis"><span class="bi bi-file-pdf-fill"></span> Lihat versi pdf</a></center>--%>

	  			</div>
	  		</div>

	  		 
	  	</div>
  	</section>
    
    <script>
        var urlName = '';
        if ($("#LbInd").attr("class") == "active") {
            urlName = 'visi-misi';
        }
        if($("#LbEn").attr("class") == "active")
        {
            urlName = 'vision-mission';
        }
        history.pushState({}, null,urlName);
    </script>

</asp:Content>
