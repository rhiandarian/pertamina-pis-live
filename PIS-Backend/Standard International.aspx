﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Standard International.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.Standard_International1" %>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Pertamina PIS</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="images/favicon.png" rel="icon">
  <link href="images/favicon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="vendor/bootstrap-4.5.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/animate/animate.css" rel="stylesheet" />
  <link href="vendor/venobox/venobox.css" rel="stylesheet">
  <link href="vendor/aos/aos.css" rel="stylesheet">
  <!--
    <link href="vendor/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
  -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link href="css/v01/style.css" rel="stylesheet" />
  <!-- Template Main CSS File -->
  
</head>

<body>
    <form runat="server">
  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <asp:LinkButton ID="LbInd" CssClass="active" runat="server">ID</asp:LinkButton> | <asp:LinkButton ID="LbEnd" OnClick="ChangeLang" runat="server">EN</asp:LinkButton>
      </div>
      <div class="contact-info float-right">
      	<a href="contacts.aspx"><i class="bi bi-envelope"></i></a>
      	<a href="#search"><i class="bi bi-search"></i></a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
        <a href="Index.aspx"><img src="images/Logo_Pertamina_PIS.svg" alt="" class="img-fluid"></a>
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
        <a href="" id="nav-right-option" class="nav-right-option float-right mobile-nav-toggle d-lg-none"><i class="bi bi-x"></i></a>
        <ul>
          
            <asp:ListView ID="ListViewMenu" runat="server">
                <ItemTemplate>
                    <li class='<%# Eval("classMenu") %>'>
                        <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                        <ul>
                            <asp:ListView ID="ListViewSubMenu" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                <ItemTemplate>
                                    <li class='<%# Eval("classMenu") %>'>
                                        <a href='<%# Eval("Link") %>'><%# System.Web.HttpUtility.HtmlEncode((string)Eval("Name")) %></a>
                                        <ul>
                                        <asp:ListView ID="ListViewSubMenu2" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                            </ul>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                        </ul>
                    </li>
                </ItemTemplate>
            </asp:ListView>
		    <asp:ListView ID="ListViewResponsive" runat="server">
                            <ItemTemplate>
                                <li class='<%# Eval("classMenu") %> d-lg-none'>
                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                    <ul>
                                        <asp:ListView ID="ListViewSubOthers" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li class='<%# Eval("classMenu") %>'>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                    <ul>
                                                        <asp:ListView ID="ListViewSub2Othes" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                                            <ItemTemplate>
                                                                <li>
                                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </ul>
                                                    
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </ul>
                                </li>
                            </ItemTemplate>
                        </asp:ListView>  

          <li class="nav-right-option d-none d-lg-block" id="othermenu" runat="server"></li>
        </ul>
        <div id="langMobile" runat="server" class="text-center lang-mobile d-lg-none">
         
        </div>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- Nav Overlay -->
  <div id="navOverlay" class="nav-overlay d-none d-lg-block">
    <div class="bg-black-transparent">
      <div class="container">
        <div class="nav-overlay-content">
          <ul>
                        <asp:ListView ID="ListViewOthers" runat="server">
                            <ItemTemplate>
                                <li class='<%# Eval("classMenu") %>'>
                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                    <ul>
                                        <asp:ListView ID="ListViewSubOthers" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li class='<%# Eval("classMenu") %>'>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                    <ul>
                                                        <asp:ListView ID="ListViewSub2Othes" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                                            <ItemTemplate>
                                                                <li>
                                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </ul>
                                                    
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </ul>
                                </li>
                            </ItemTemplate>
                        </asp:ListView>
                        
		    		
		    	</ul>
        </div>
      </div>
    </div>
  </div><!-- End Nav Overlay -->

  <!-- Search -->
  <div id="search">
    <div class="bg-overlay">
      <button type="button" class="close"><i class="bi bi-x close"></i></button>
      <form action="pencarian.html">
      <input type="search" id="txtSearch" runat="server" value="" autocomplete="off" placeholder="Ketik kata kunci disini" />
            <asp:Button ID="BtnSearch" runat="server" OnClick="BtnSearch_Click" CssClass="btn bg-red" Text="Cari" />
      </form>
    </div>
  </div><!-- End Search -->

  <main id="main">

  	<!-- Top Main -->
  	<section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx">Beranda</a></li>
                          <li class="breadcrumb-item active" ><a href=""></a>Manajemen</li>
					    <li class="breadcrumb-item active" aria-current="page">Standar Internasional</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3>STANDAR INTERNASIONAL</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>


  	<!-- Section -->
  	<section id="standard-internasional">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div class="col-lg-12 mb-4 title">
	  				<h5><strong>Standar Internasional</strong></h5>
	  			</div>

	  			<div class="col-lg-12 mb-4 desc">
            <div class="row">
              <div id="content" runat="server" class="col-12">
	  				   
              </div>

              <div class="col-12 pb-3 mt-5">
                <div class="row list-iso">
                    <asp:ListView ID="ListViewSI" runat="server">
                        <ItemTemplate>
                            <div class="col-12 pb-3 <%# Eval("class") %>">
                    <div class="row">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 pt-3">
                        <a href='<%# Eval("img_file") %>' class="image-popup-vertical-fit">
                          <img src='<%# Eval("img_file") %>' class="img-fluid" alt="...">
                        </a>
                      </div>
                      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 align-self-center">
                        <table class="table table-borderless m-0">
                          <tr class="d-flex">
                            <td class="d-none d-sm-block">Nama Sertifikasi</td>
                            <td><strong><%# Eval("Nama") %></strong></td>
                          </tr>
                          <tr class="d-flex">
                            <td class="d-none d-sm-block">Validasi</td>
                            <td><strong><%# Eval("Validasi") %></strong></td>
                          </tr>
                          <tr class="d-flex">
                            <td class="d-none d-sm-block">Pemberi Sertifikasi</td>
                            <td><strong><%# Eval("Pemberi") %></strong></td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </div>
                        </ItemTemplate>
                    </asp:ListView>
                  
                  
                </div>
              </div>

            </div>
              

	  			</div>
	  		</div>

	  	</div>
  	</section>

  </main>

  <!-- ======= Footer ======= -->
	<div id="footer-top-border">
		<div class="col-lg-12 col-12" style="top:0">
			<div class="row">
				<div class="col-lg-6 col-6 footer-border-1" style="top:0"></div>
				<div class="col-lg-3 col-3 footer-border-2" style="top:0"></div>
				<div class="col-lg-3 col-3 footer-border-3" style="top:0"></div>
			</div>
		</div>
	</div>
	<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-4 col-12 footer-info">
            <h6><strong>KANTOR PUSAT</strong></h6>
            <p id="address" runat="server" class="mt-3">
              Patra Jasa Office Tower Lantai 3 & 14 
              <br>JJl. Jend. Gatot Subroto Kav 32–34
              <br>Setiabudi, Jakarta 12950 
              <br>Indonesia 
            </p>
            <div class="d-flex mt-2">
              <div class="align-self-center rounded-icon">
	  					  <span class="bi bi-telephone-fill phone-icon"></span>
              </div>
              <div class="align-self-center">
               &nbsp; <span id="phone" runat="server"></span>
              </div>
	  				</div>
	  				<div class="d-flex mt-2">
              <div class="align-self-center rounded-icon">
	  					  <span class="bi bi-envelope-fill phone-icon"></span>
              </div>
              <div class="align-self-center">
                &nbsp; <span id="email" runat="server"></span>
              </div>
	  				</div>
	  				<div class="d-flex mt-2">
              <div class="align-self-center">
	  					  <img src="images/logo-135.jpg" width="25px" alt="" class="img-fluid">
              </div>
              <div class="align-self-center">
                &nbsp; Call Center <strong>135</strong>
              </div>
	  				</div>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>


          <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong id="perusahaan" runat="server">TAUTAN </strong></h6>
            <ul class="mt-3">
              <li><i class="bx bx-chevron-right"></i> <a href="https://pertamina.com" id="pt1" runat="server">PT Pertamina (Persero)</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://bumn.go.id" id="pt2" runat="server">'Kementerian BUMN</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://www.esdm.go.id" id="pt3" runat="server">Kementerian ESDM RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="http://www.dephub.go.id" id="pt4" runat="server">Kementerian Perhubungan RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://www.bkpm.go.id" id="pt5" runat="server">BKPM</a></li>
            </ul>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>

          <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong>TEMUKAN KAMI</strong></h6>
            <ul class="mt-3 social-links">
              <li >
              	<a id="link_fb" runat="server" class="d-flex">
	              	<div class="align-self-center">
	              		<span class="bi bi-facebook facebook"></span>
	              	</div>
	              	<div class="align-self-center">Facebook<br><em>Pertamina International Shipping</em></div>
	              </a>
	            </li>
              <li>
              	<a id="link_ig" runat="server" class="d-flex">
	              	<div class="align-self-center">
	              		<span class="bi bi-instagram instagram"></span>
	              		</div>
	              	<div class="align-self-center">Instagram<br><em>@pertamina_pis</em></div> 
	              </a>
	            </li>
              <li>
                <a id="link_tw" runat="server" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-twitter twitter"></span>
                    </div>
                  <div class="align-self-center">Twitter<br><em>@Pertamina_PIS</em></div> 
                </a>
              </li>
              <li>
                <a id="link_yt" runat="server" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-youtube twitter"></span>
                    </div>
                  <div class="align-self-center">Youtube<br><em>PT Pertamina International Shipping</em></div> 
                </a>
              </li>
            </ul>
          </div>
          <div class="col-12 d-lg-none separate"><hr></div>

        </div>
      </div>
    </div>

    <div class="container">
    	<div class="row d-flex">
	      <div class="col-lg-8 col-12 align-self-center part-of">
	        <span>PART OF</span>
	        <img src="images/logo-bumn.svg" height="30px" class="ml-3" alt="...">
	        <img src="images/logo-pertamina.svg" height="30px" class="ml-3" alt="...">
	      </div>
    		<div class="col-lg-4 col-12">
    			<a href="" class="btn btn-whistle">
            	<div class="row">
            		<div class="col pr-0 d-flex mr-2">
          				<img src="images/icons/wbs.svg">
            		</div>
            		<div class="col text-left pl-0">
		          		Whistle Blowing System
		          		<br><em>https://pertaminaclean.tipoffs.info/</em>
            		</div>
            	</div>
          	</a>
    		</div>
    		<div class="col-12 mt-3">
    			&copy; Copyright 2021 <strong><span>Pertamina International Shipping</span></strong>. All Rights Reserved
    		</div>
    	</div>
    </div>
  </footer><!-- End Footer -->
    

   

  <!-- Vendor JS Files -->
  <script src="vendor/jquery.min.js"></script>
  <script src="vendor/bootstrap-4.5.3-dist/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="vendor/venobox/venobox.min.js"></script>
  <script src="vendor/waypoints/lib/jquery.waypoints.min.js"></script>
  <script src="vendor/counterup/counterup.min.js"></script>
  <script src="vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="vendor/aos/aos.js"></script>
  <script src="vendor/fontawesome/js/all.min.js"></script>
  <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Template Main JS File -->
  <script src="js/main.js?v=0.1"></script>

  <script type="text/javascript">
      function changeLang()
      {
           var button = document.getElementById("<%= LbEnd.ClientID %>");
           button.click();
      }
      history.pushState({}, null, "standar-internasional");
    $(document).ready(function() {

      $('.image-popup-vertical-fit').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-img-mobile',
        image: {
          verticalFit: true
        },
        zoom: {
          enabled: true,
          duration: 300 
        }
      });

    });
  </script>
</form>
</body>

</html>
