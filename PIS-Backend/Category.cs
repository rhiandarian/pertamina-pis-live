﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;


namespace PIS_Backend
{
    public class Category
    {
        SqlConnection con = Connection.conn();

        public string tableName = "Category";
        string viewName = "ViewListCategory";
        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        private string category_name;

        private int category_type_id;

        public string Category_name
        {
            get { return category_name; }
            set { category_name = value; }
        }
        public int Category_Type_Id
        {
            get { return category_type_id; }
            set { category_type_id = value; }
        }
        public void Insert()
        {
            string field = "";
            string value = "";

            if (category_name != null)
            {
                field += "Category_name, ";
                value += "'" + category_name + "', ";
            }
            if (category_type_id != 0)
            {
                field += "Category_type_id, ";
                value += "'" + category_type_id + "', ";
            }

            field += "addDate, addUser";


            value += "'" + dateNow + "', " + user_id;

            string query = "INSERT INTO " + tableName + " (" + field + ") VALUES (" + value + ")";

            Db.setData(query);
        }
        public DataTable Select(string field, string conditions = null)
        {
            conditions = conditions != null ? conditions : "";

            string query = Db.SelectQuery(field, viewName, conditions);

            DataTable dt = Db.getData(query);

            return dt;
        }
        public DataSet LoadCategory(int Category_type_id)
        {
            string conditions = "Category_type_id = " + Category_type_id;
            string field = "ID, Category_name";

            string Query = Db.SelectQuery(field, tableName, conditions);
            
            DataSet dt = Db.getDataSet(Query);

            return dt;
        }
        public DataSet LoadCategoryType()
        {
            string field = "ID, Category_type_name";

            string Query = Db.SelectQuery(field, "CategoryType","");

            DataSet dt = Db.getDataSet(Query);

            return dt;
        }
        public void Delete(string conditions = null)
        {
            string query = "Update " + tableName + " set fl_deleted = 1, deleteDate = '" + dateNow + "', deleteUser = " + user_id;
            query += conditions != null ? "Where " + conditions : "";
            Db.setData(query);

        }
        public DataSet SelectList(string field, string type = null)
        {
            string conditions = "Where ";
            type = type == null ? "" : type;

            if (category_name != null)
            {
                conditions += "Category_name = '" + category_name + "' " + type;
            }
            if(category_type_id != 0)
            {
                conditions += "Category_type_id = " +category_type_id+ " " + type;
            }

            conditions += " (fl_deleted != 1 or fl_deleted IS NULL)";

            string query = "Select " + field + " from " + tableName + " " + conditions;

            DataSet dt = Db.getDataSet(query);

            return dt;
        }
    }
}