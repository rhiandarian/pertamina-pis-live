﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PIS_Backend
{
    public class Kapal:ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName = "Kapal";

        string viewName = "View_Kapal";

        public string IndPage = "Armada-Kami.aspx";

        public string EnPage = "Our Fleet.aspx";

        public string listPage = "AdminKapalMilikArmada.aspx";

        public int maxSize = 60000;


        public string maxSizeInString = "50KB";


        DateTime dateNow = DateTime.Now;

        public int PageSizes = 12;

        public int pageID = 1;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        private string name;

        private string oil;

        private string detail;

        private string type;

        private string built;

        private string img_file;

        private int file_size_input;

        private int size;

        private int capacity;

        private string builtOn;

        private string flag;

        private string callsign;

        private string class_dnv_gl;

        private string particulars;

        private string loa;

        private string lbp;

        private string parallel_body_loaded;

        private string breadth_moulded;

        private string height_from_keel_to_mask;

        private string depth_moulded;

        private string draft_summer;

        private string draft_ballast;

        private string tpc;

        private string airdraft_in_ballast__loaded_condition;

        private string total_size_manifolds;

        private string distance_form_bow_to_centre_manifold;

        private string distance_form_stern_to_centre_manifold;

        private string distance_between_manifolds;

        private string distance_form_ship_side_to_centre_manifold;

        private string distance_from_deck_tray_to_manifold;

        private string cargo_tank_capacity;

        private string slop_tank_capacity;

        private string ballast_tank_capacity;

        private string deadweight;

        private string tonnage_international;

        private string coating;

        private string main_engine;

        private string bow_thruster;

        private string cranes;

        private string cargo_pumps;

        private string stripping_pump;

        private string ballast_pumps;

        private string max_load_rate_homogenous_cargo_per_manifold;

        private string max_load_rate_homogenous_cargothrough_all_manifold;

        private string no_of_cargo_tanks;

        private string no_of_segregations;

        private string orderBy;

        public string NAME { get { return name; } set { name = value; } }
        public string TYPE { get { return type; } set { type = value; } }
        public string OIL { get { return oil; } set { oil = value; } }
        public string DETAIL { get { return detail; } set { detail = value; } }
        public string BUILT { get { return built; } set { built = value; } }
        public string IMG_FILE { get { return img_file; } set { img_file = value; } }
        public int SIZE { get { return size; } set { size = value; } }
        public int FileSizeInput { get { return file_size_input; } set { file_size_input = value; } }
        public int CAPACITY { get { return capacity; } set { capacity = value; } }
        public string BUILTON { get { return builtOn; } set { builtOn = value; } }
        public string FLAG { get { return flag; } set { flag = value; } }
        public string CALLSIGN { get { return callsign; } set { callsign = value; } }
        public string CLASS_DNV_GL { get { return class_dnv_gl; } set { class_dnv_gl = value; } }
        public string PARTICULARS { get { return particulars; } set { particulars = value; } }
        public string LOA { get { return loa; } set { loa = value; } }
        public string LBP { get { return lbp; } set { lbp = value; } }
        public string PARALLEL_BODY_LOADED { get { return parallel_body_loaded; } set { parallel_body_loaded = value; } }
        public string BREADTH_MOULDED { get { return breadth_moulded; } set { breadth_moulded = value; } }
        public string HEIGHT_FROM_KEEL_TO_MASK { get { return height_from_keel_to_mask; } set { height_from_keel_to_mask = value; } }
        public string DEPTH_MOULDED { get { return depth_moulded; } set { depth_moulded = value; } }
        public string DRAFT_SUMMER { get { return draft_summer; } set { draft_summer = value; } }
        public string DRAFT_BALLAST { get { return draft_ballast; } set { draft_ballast = value; } }
        public string TPC { get { return tpc; } set { tpc = value; } }
        public string AIRDRAFT_IN_BALLAST__LOADED_CONDITION { get { return airdraft_in_ballast__loaded_condition; } set { airdraft_in_ballast__loaded_condition = value; } }
        public string TOTAL_SIZE_MANIFOLDS { get { return total_size_manifolds; } set { total_size_manifolds = value; } }
        public string DISTANCE_FORM_BOW_TO_CENTRE_MANIFOLD { get { return distance_form_bow_to_centre_manifold; } set { distance_form_bow_to_centre_manifold = value; } }
        public string DISTANCE_FORM_STERN_TO_CENTRE_MANIFOLD { get { return distance_form_stern_to_centre_manifold; } set { distance_form_stern_to_centre_manifold = value; } }
        public string DISTANCE_BETWEEN_MANIFOLDS { get { return distance_between_manifolds; } set { distance_between_manifolds = value; } }
        public string DISTANCE_FORM_SHIP_SIDE_TO_CENTRE_MANIFOLD { get { return distance_form_ship_side_to_centre_manifold; } set { distance_form_ship_side_to_centre_manifold = value; } }
        public string DISTANCE_FROM_DECK_TRAY_TO_MANIFOLD { get { return distance_from_deck_tray_to_manifold; } set { distance_from_deck_tray_to_manifold = value; } }
        public string CARGO_TANK_CAPACITY { get { return cargo_tank_capacity; } set { cargo_tank_capacity = value; } }
        public string SLOP_TANK_CAPACITY { get { return slop_tank_capacity; } set { slop_tank_capacity = value; } }
        public string BALLAST_TANK_CAPACITY { get { return ballast_tank_capacity; } set { ballast_tank_capacity = value; } }
        public string DEADWEIGHT { get { return deadweight; } set { deadweight = value; } }
        public string TONNAGE_INTERNATIONAL { get { return tonnage_international; } set { tonnage_international = value; } }
        public string COATING { get { return coating; } set { coating = value; } }
        public string MAIN_ENGINE { get { return main_engine; } set { main_engine = value; } }
        public string BOW_THRUSTER { get { return bow_thruster; } set { bow_thruster = value; } }
        public string CRANES { get { return cranes; } set { cranes = value; } }
        public string CARGO_PUMPS { get { return cargo_pumps; } set { cargo_pumps = value; } }
        public string STRIPPING_PUMP { get { return stripping_pump; } set { stripping_pump = value; } }
        public string BALLAST_PUMPS { get { return ballast_pumps; } set { ballast_pumps = value; } }
        public string MAX_LOAD_RATE_HOMOGENOUS_CARGO_PER_MANIFOLD { get { return max_load_rate_homogenous_cargo_per_manifold; } set { max_load_rate_homogenous_cargo_per_manifold = value; } }
        public string MAX_LOAD_RATE_HOMOGENOUS_CARGOTHROUGH_ALL_MANIFOLD { get { return max_load_rate_homogenous_cargothrough_all_manifold; } set { max_load_rate_homogenous_cargothrough_all_manifold = value; } }
        public string NO_OF_CARGO_TANKS { get { return no_of_cargo_tanks; } set { no_of_cargo_tanks = value; } }
        public string NO_OF_SEGREGATIONS { get { return no_of_segregations; } set { no_of_segregations = value; } }
        public string OrderBy
        {
            get { return orderBy; }
            set { orderBy = value; }
        }


        public string validate()
        {
            if(file_size_input > maxSize)
            {
                return "Ukuran file terlalu besar, maximum Size adalah " + maxSizeInString;
            }
            if(name == "")
            {
                return "nama harus diisi!";
            }

            if (oil == "")
            {
                return "oil harus diisi";
            }
            if (detail == "")
            {
                return "detail harus diisi";
            }
            
            if (type == "")
            {
                return "type harus diisi";
            }
            
            if (built == "")
            {
                return "built harus diisi";
            }
            
            return "valid";

        }
        
        public DataTable Select(string field,string conditions = null)
        {
            string condition = conditions == null ? "" : conditions;
            if(orderBy != null)
            {
                Db.OrderBy = orderBy;
            }
            string Query = Db.SelectQuery(field, viewName, condition);
            DataTable dt = Db.getData(Query);
            return dt;
        }
        public string Insert(string mainIP,string browserDetail)
        {
            try
            {
                string valid = validate();
                if(valid != "valid")
                {
                    return valid;
                }
                

                
                string value = "'" + name + "', ";

                value += "'" + oil + "', ";

                value += "'" + detail + "', ";

                value += "'" + type + "', ";

                value += "'" + built + "', ";

                value += "'Kapal/" + img_file + "', ";

                value += size + ", ";

                value += capacity + ", ";

                value += "'" + builtOn + "', ";

                value += "'" + flag + "', ";

                value += "'" + callsign + "', ";

                value += "'" + class_dnv_gl + "', ";

                value += "'" + particulars + "', ";

                value += "'" + loa + "', ";

                value += "'" + lbp + "', ";

                value += "'" + parallel_body_loaded + "', ";

                value += "'" + breadth_moulded + "', ";

                value += "'" + height_from_keel_to_mask + "', ";

                value += "'" + depth_moulded + "', ";

                value += "'" + draft_summer + "', ";

                value += "'" + draft_ballast + "', ";

                value += "'" + tpc + "', ";

                value += "'" + airdraft_in_ballast__loaded_condition + "', ";

                value += "'" + total_size_manifolds + "', ";

                value += "'" + distance_form_bow_to_centre_manifold + "', ";

                value += "'" + distance_form_stern_to_centre_manifold + "', ";

                value += "'" + distance_between_manifolds + "', ";

                value += "'" + distance_form_ship_side_to_centre_manifold + "', ";

                value += "'" + distance_from_deck_tray_to_manifold + "', ";

                value += "'" + cargo_tank_capacity + "', ";

                value += "'" + slop_tank_capacity + "', ";

                value += "'" + ballast_tank_capacity + "', ";

                value += "'" + deadweight + "', ";

                value += "'" + tonnage_international + "', ";

                value += "'" + coating + "', ";

                value += "'" + main_engine + "', ";

                value += "'" + bow_thruster + "', ";

                value += "'" + cranes + "', ";

                value += "'" + cargo_pumps + "', ";

                value += "'" + stripping_pump + "', ";

                value += "'" + ballast_pumps + "', ";

                value += "'" + max_load_rate_homogenous_cargo_per_manifold + "', ";

                value += "'" + max_load_rate_homogenous_cargothrough_all_manifold + "', ";

                value += "'" + no_of_cargo_tanks + "', ";

                value += "'" + no_of_segregations + "', ";

                value += user_id+", '"+mainIP+"', '"+browserDetail+"', '"+ DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'";

                DataTable dt = Db.Sp_Result("SaveKapal",value);

                if(dt.Rows[0]["Status"].ToString() == "success")
                {
                    return dt.Rows[0]["Status"].ToString();
                }
                else
                {
                    return dt.Rows[0]["Message"].ToString();
                }
            }
            catch(Exception e)
            {
                return e.Message.ToString();
            }

            

        }
        public PagesData getPages()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("Header,content,content_en,others,others2", " ID = "+pageID);
            pd.Header = dt.Rows[0]["Header"].ToString();
            pd.Content = dt.Rows[0]["content"].ToString();
            pd.Content_En = dt.Rows[0]["content_en"].ToString();
            pd.Others = dt.Rows[0]["others"].ToString();
            pd.Others2 = dt.Rows[0]["others2"].ToString();

            return pd;

        }
        public string Update(int ID,string mainIP,string browserDetail)
        {
            try
            {
                string valid = validate();
                if(valid != "valid")
                {
                    return valid;
                }
                
                string newRecord = "'" + name + "', ";
                newRecord += "'" + oil + "', ";
                newRecord += "'" + detail + "', ";
                newRecord += "'" + type + "', ";
                newRecord += "'" + built + "', ";
                newRecord += "'Kapal/" + img_file + "', ";
                newRecord += size + ", ";
                newRecord += capacity + ", ";
                newRecord += "'" + builtOn + "', " ;
                newRecord += "'" + flag + "', " ;
                newRecord += "'" + callsign + "', " ;
                newRecord += " '" + class_dnv_gl + "', " ;
                newRecord += "'" + particulars + "', " ;
                newRecord += "'" + loa + "', " ;
                newRecord += "'" + lbp + "', " ;
                newRecord += "'" + parallel_body_loaded + "', " ;
                newRecord += "'" + breadth_moulded + "', " ;
                newRecord += "'" + height_from_keel_to_mask + "', " ;
                newRecord += "'" + depth_moulded + "', " ;
                newRecord += "'" + draft_summer + "', " ;
                newRecord += "'" + draft_ballast + "', " ;
                newRecord += "'" + tpc + "', " ;
                newRecord += "'" + airdraft_in_ballast__loaded_condition + "', " ;
                newRecord += "'" + total_size_manifolds + "', " ;
                newRecord += "'" + distance_form_bow_to_centre_manifold + "', " ;
                newRecord += "'" + distance_form_stern_to_centre_manifold + "', " ;
                newRecord += "'" + distance_between_manifolds + "', " ;
                newRecord += "'" + distance_form_ship_side_to_centre_manifold + "', " ;
                newRecord += "'" + distance_from_deck_tray_to_manifold + "', " ;
                newRecord += "'" + cargo_tank_capacity + "', " ;
                newRecord += "'" + slop_tank_capacity + "', " ;
                newRecord += "'" + ballast_tank_capacity + "', " ;
                newRecord += "'" + deadweight + "', " ;
                newRecord += "'" + tonnage_international + "', " ;
                newRecord += "'" + coating + "', " ;
                newRecord += "'" + main_engine + "', " ;
                newRecord += "'" + bow_thruster + "', " ;
                newRecord += "'" + cranes + "', " ;
                newRecord += "'" + cargo_pumps + "', " ;
                newRecord += "'" + stripping_pump + "', " ;
                newRecord += "'" + ballast_pumps + "', " ;
                newRecord += "'" + max_load_rate_homogenous_cargo_per_manifold + "', " ;
                newRecord += "'" + max_load_rate_homogenous_cargothrough_all_manifold + "', " ;
                newRecord += "'" + no_of_cargo_tanks + "', " ;
                newRecord += "'" + no_of_segregations + "', " ;
                newRecord += user_id+", '"+mainIP+"', '"+browserDetail+"', '"+ DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
                newRecord += ID;
                
                DataTable dt = Db.Sp_Result("SaveKapal", newRecord);

                if (dt.Rows[0]["Status"].ToString() == "success")
                {
                    return dt.Rows[0]["Status"].ToString();
                }
                else
                {
                    return dt.Rows[0]["Message"].ToString();
                }





            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }
        public DataTable GetAll(int PageIndex, int pageSize = 0)
        {
            SqlCommand cmd = new SqlCommand("GetAllKapal", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PageIndex",PageIndex);
            pageSize = pageSize != 0 ? pageSize : PageSizes;
            cmd.Parameters.AddWithValue("@PageSize", pageSize);

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public DataTable getLatest()
        {
            SqlCommand cmd = new SqlCommand("GetLatestKapal", con);
            cmd.CommandType = CommandType.StoredProcedure;
            

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public void Delete(string id,string mainIP,string browserDetail,string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            Db.Delete(tableName, dateNow, user_id, conditions,Convert.ToInt32(id),mainIP,browserDetail);
        }


    }
}