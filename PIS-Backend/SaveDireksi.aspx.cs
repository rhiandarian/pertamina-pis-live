﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class SaveDireksi : System.Web.UI.Page
    {
        string folderName = "DewanDireksi"; string listPage = "AdminDireksi.aspx"; int person_type = 2;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "17"))
                {
                    Response.Redirect("admin.aspx");
                }

                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;

                LoadData();
            }
        }
        public void LoadData()
        {
            if (Request.QueryString["ID"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                Person p = new Person();
                string cond = " ID = " + Id;
                DataTable dt = p.Select("*", cond);
                nama.Text = dt.Rows[0]["name"].ToString();
                division.Text = dt.Rows[0]["division"].ToString();
                division_en.Text = dt.Rows[0]["division_en"].ToString() != null ? dt.Rows[0]["division_en"].ToString() : "";
                description.InnerHtml = dt.Rows[0]["description"].ToString();
                description_en.InnerHtml = dt.Rows[0]["description_en"].ToString() != null ? dt.Rows[0]["description_en"].ToString() : "";
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string randomtext = textrandom();
                Person p = new Person();
                p.Name = nama.Text;
                p.PersonType = person_type;
                p.Division = division.Text;
                p.Description = description.InnerText;
                p.DivisionEn = division_en.Text;
                p.DescriptionEn = description_en.InnerText;
                p.FolderName = folderName;

                if (Request.QueryString["ID"] == null)
                {
                    p.Img_File = FileUpload1.FileName;
                    string insertStatus = p.Insert(getip(),GetBrowserDetails(),randomtext);

                    if (insertStatus == "success")
                    {
                        UploadDireksi(randomtext);
                        Response.Redirect(listPage);
                    }
                    else
                    {
                        showValidationMessage(insertStatus);
                    }
                }
                else
                {
                    int Id = Convert.ToInt32(Request.QueryString["ID"]);
                    string conditions = " ID = " + Id;
                    if (FileUpload1.FileName != "")
                    {
                        p.Img_File = randomtext + "" + FileUpload1.FileName;
                    }
                    string updateStatus = p.Update(Id,getip(),GetBrowserDetails(),conditions);
                    
                    if (updateStatus == "success")
                    {
                        if (FileUpload1.FileName != "")
                        {
                            UploadDireksi(randomtext);
                        }
                        Response.Redirect(listPage);
                    }
                    else
                    {
                        showValidationMessage(updateStatus);
                    }
                }
            }
            catch(Exception ex)
            {
                Person p = new Person();
                string action = Request.QueryString["ID"] == null ? "Insert" : "Update";
                p.ErrorLogHistory(getip(), GetBrowserDetails(), action, p.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                showValidationMessage(ex.Message.ToString());
            }
        }
        public void UploadDireksi(string randomtext)
        { 
            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string SetFolderBeforeResize = Server.MapPath(folderName) + "\\" + "serize" + randomtext + fn; //gambar yang akan diresize sesudah direze gambar akan dihapus
            string SetFolderAfterResize = Server.MapPath(folderName) + "\\" + randomtext + fn;
            FileUpload1.PostedFile.SaveAs(SetFolderBeforeResize);
            ImageResize Iz = new ImageResize();
            Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "Direksi", "Cut");
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public string textrandom()
        {
            Random RNG = new Random();
            int length = 6;
            var rString = "";
            for (var i = 0; i < length; i++)
            {
                rString += ((char)(RNG.Next(1, 26) + 64)).ToString().ToLower();
            }
            return rString;
        }


    }
}