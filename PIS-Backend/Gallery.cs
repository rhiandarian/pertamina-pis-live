﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace PIS_Backend
{
    public class Gallery : ModelData
    {
        SqlConnection con = Connection.conn();

       public string tableName = "Gallery";

        string viewName = "ViewGalleryFull";

        

        public int PageSize = 7;

        public int imageMaxSize = 300000;

        public string imageMaxSizeString = "150KB";

        public string adminListPage = "admin-list-gallery.aspx";

        public string adminSavePage = "admin-add-gallery.aspx";

        Category c = new Category();

        General all = new General();

        DateTime dateNow = DateTime.Now;

        
        

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        private string title;

        private string title_en;

        private string content;

        private string content_en;

        private string img_file;

        private string video_url;

        private string img_url;

        private string embed_url;

        private int category_id;

        private int file_image_Size;

        private int file_cover_Size;

        public DateTime date;

        public int No
        {
            get;
            set;
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string TitleEn
        {
            get { return title_en; }
            set { title_en = value; }
        }
        public string Img_File
        {
            get { return img_file; }
            set { img_file = value; }
        }
        public string Video_url
        {
            get { return video_url; }
            set { video_url = value; }
        }
        public string Img_url
        {
            get { return img_url; }
            set { img_url = value; }
        }
        public string Embed_url
        {
            get { return embed_url; }
            set { embed_url = value; }
        }
        public string Content
        {
            get { return content; }
            set { content = value; }
        }
        public string ContentEn
        {
            get { return content_en; }
            set { content_en = value; }
        }
        public int Category
        {
            get { return category_id; }
            set { category_id = value; }
        }

        public int FileImageSize
        {
            get { return file_image_Size; }
            set { file_image_Size = value; }
        }

        public int FileCoverSize
        {
            get { return file_cover_Size; }
            set { file_cover_Size = value; }
        }
        public string CategoryName
        {
            get;
            set;
        }
        public string Dates
        {
            get;
            set;
        }
        public List<Image> ImageDetail
        {
            get;
            set;
        }
        public string detindex
        {
            get;
            set;
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }
        public DataTable Select(string field, string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            
            string query = Db.SelectQuery(field, viewName, conditions);

            
            DataTable dt = Db.getData(query);

            return dt;

        }
        public DataTable GetDetailImage(int GalleryID)
        {
            Image img = new Image();
            string field = "title, title_en, img_file, CONVERT(varchar,addDate,106) as 'image_date'";
            string conditions = " GalleryID = " + GalleryID;
            DataTable dt = img.Select(field, conditions);

            return dt;
        }
        public PageGallery FotoPage()
        {
            PageGallery pg = setPageName("ListFoto", "AddFoto", "EditFoto");
            
            return pg;
        }
        public PageGallery InfografisPage()
        {
            PageGallery pg = setPageName("admin-semua-infografis", "AddInfografis", "EditInfografis");

            return pg;
        }
        public PageGallery VideoPage()
        {
            PageGallery pg = setPageName("admin-semua-video", "admin-tambah-video", "EditVideo");

            return pg;
        }
        public string AddPageGallery(string listPage)
        {
            string resultPage = "";
            List<PageGallery> pg = GalleryPage();
            for(int i = 0; i < pg.Count; i++)
            {
                if(pg[i].ListPage == listPage)
                {
                    resultPage = pg[i].AddPage;
                }
            }
            return resultPage;
        }
        public int findCategoryPage(string page)
        {
            int result = 0;
            List<PageGallery> pg = GalleryPage();
            for (int i = 0; i < pg.Count; i++)
            {
                if (pg[i].ListPage == page || pg[i].AddPage == page)
                {
                    result = pg[i].ID;
                }
            }
            return result;
        }
        public string findPageAdminID(string page)
        {
            string result = "";
            List<PageGallery> pg = GalleryPage();
            for (int i = 0; i < pg.Count; i++)
            {
                if (pg[i].ListPage == page || pg[i].AddPage == page || pg[i].EditPage == page)
                {
                    result = pg[i].PageAdminID;
                }
            }
            return result;
        }
        public string ListPageGallery(string page)
        {
            string resultPage = "";
            List<PageGallery> pg = GalleryPage();
            for (int i = 0; i < pg.Count; i++)
            {
                if (pg[i].AddPage == page || pg[i].EditPage == page)
                {
                    resultPage = pg[i].ListPage;
                }
            }
            return resultPage;
        }

        public List<PageGallery> GalleryPage()
        {
            List<PageGallery> pg = new List<PageGallery>();
            pg.Add(new PageGallery() {
                ID = all.FotoCategoryID,
                ListPage = "ListFoto",
                AddPage = "AddFoto",
                EditPage = "EditFoto",
                PageAdminID = "6"
            });
            pg.Add(new PageGallery()
            {
                ID = all.InfografisCategoryID,
                ListPage = "admin-semua-infografis",
                AddPage = "AddInfografis",
                EditPage = "EditInfografis",
                PageAdminID = "7"
            });
            pg.Add(new PageGallery() {
                ID = all.VideoCategoryID,
                ListPage = "admin-semua-video",
                AddPage = "admin-tambah-video",
                EditPage = "EditVideo",
                PageAdminID = "5"
            });
            return pg;
        }
        public PageGallery setPageName(string listPage, string addpage, string editpage)
        {
            PageGallery pg = new PageGallery();
            pg.ListPage = listPage;
            pg.AddPage = addpage;
            pg.EditPage = editpage;
            return pg;
        }
        public string ValidationVideoProfile()
        {
            if(title == "")
            {
               return "Judul harus dalam bahasa Indonesia di isi";
            }
            if (title_en == "")
            {
                return "Judul dalam bahasa Inggris harus di isi";
            }
            if (content == "")
            {
                return "Konten dalam bahasa Indonesia harus di isi";
            }
            if (content_en == "")
            {
                return "Konten dalam bahasa Inggris harus di isi";
            }
            if (video_url == "")
            {
                return "Video Url harus di isi";
            }
            if (!Uri.IsWellFormedUriString(video_url, UriKind.RelativeOrAbsolute))
            {
                return "Video Url tidak Valid";
            }
            if (img_url == "")
            {
                return "Thumbnail Youtube harus di isi";
            }
            if (!Uri.IsWellFormedUriString(img_url, UriKind.RelativeOrAbsolute))
            {
                return "Thumbnail Youtube Tidak Valid";
            }
            return "Valid";
        }
        public void SaveVideoProfile(string mainIP, string getBrowser)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("SaveVideoProfile", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@videoProfileID", all.videoProfileID);
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@title_en", title_en);
            cmd.Parameters.AddWithValue("@content", content);
            cmd.Parameters.AddWithValue("@content_en", content_en);
            cmd.Parameters.AddWithValue("@Dates", date);
            cmd.Parameters.AddWithValue("@videoUrl", video_url);
            cmd.Parameters.AddWithValue("@imgUrl", img_url);
            cmd.Parameters.AddWithValue("@user_id", user_id);
            cmd.Parameters.AddWithValue("@mainIP", mainIP);
            cmd.Parameters.AddWithValue("@browserDetail", getBrowser);
            cmd.Parameters.AddWithValue("@createdAt", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public  DataSet LoadCategory(int Category_type_id)
        {
            Category c = new Category();
            
            return c.LoadCategory(Category_type_id);
        }
        public DataTable AdminGallery(int CategoryID = 0)
        {
            SqlCommand cmd = new SqlCommand("AdminGallery", con);
            cmd.CommandType = CommandType.StoredProcedure;
            if(CategoryID != 0)
            {
                cmd.Parameters.AddWithValue("@category",CategoryID);
            }
            



            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public DataTable GetLatestGallery()
        {
            SqlCommand cmd = new SqlCommand("GetLatestGallery", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@videoCategoryID", all.VideoCategoryID);
            


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public string Validate(string categoryValue)
        {
            
            if (title == null || title == "")
            {
                return "judul dalam bahasa Indonesia harus di isi";
            }
            if(title_en == null || title_en == "")
            {
                return "judul dalam bahasa Inggris harus di isi";
            }
            if (categoryValue == "Pilih Category")
            {
                return "kategori harus di pilih";
            }
            else
            {
               if(category_id == all.VideoCategoryID)
               {
                    if (content == null || content == "")
                    {
                        return "konten harus di isi";
                    }
                    if(content_en == null || content_en == "")
                    {
                        return "konten dalam bahasa inggris harus di isi";
                    }
                    if (video_url == null || video_url == "")
                    {
                        return "Video Url harus di isi";
                    }
                    if (!Uri.IsWellFormedUriString(video_url, UriKind.RelativeOrAbsolute))
                    {
                        return "Video Url harus Url";
                    }
                    if(img_url != null)
                    {
                        if (!all.ImageFileType(img_url))
                        {
                            return "Masukan file foto";
                        }
                    }
                    if(FileCoverSize > imageMaxSize)
                    {
                        return "Ukuran Foto terlalu besar, Ukuran Maximum " + imageMaxSizeString;
                    }
                    
                }
                else
                {
                    if(img_file != null)
                    {
                        if(!all.ImageFileType(img_file))
                        {
                            return "Upload harus Image";
                        }
                    }
                    if(file_image_Size > imageMaxSize)
                    {
                        return "Ukuran File terlalu besar, Ukuran Maximum " + imageMaxSizeString;
                    }
                }
            }
            
            return "valid";
        }
        public string Insert(string mainIP, string browserDetail,string ddlVal)
        {
            try
            {
                if (Validate(ddlVal) != "valid")
                {
                    return Validate(ddlVal);
                }
                string field = "title, ";
                field += "title_en, ";
                field += "Dates, ";
                
                string value = "'" + title.Replace("'", "") + "', ";
                value += "'" + title_en.Replace("'", "") + "', ";
                value += "'" + date + "', ";

                if (category_id == all.VideoCategoryID)
                {
                    field += "content, ";
                    field += "content_en, ";
                    field += "video_url, ";
                    field += "img_url, ";

                    value += "'" + content.Replace("'", "") + "', ";
                    value += "'" + content_en.Replace("'","") + "', ";
                    value += "'" + video_url + "', ";
                    value += "'Gallerry/" +img_url+ "', ";
                }
                else
                { 
                    field += "img_file, ";
                    value += "'Gallerry/" + img_file + "', ";
                }
                field += "category_id, ";

                field += "addDate, addUser";

                
                value += category_id + ", ";

                value += "'" + dateNow + "', " + user_id;

                string query = "INSERT INTO " + tableName + " (" + field + ") VALUES (" + value + ")";
                string action = getUserName(user_id) + " Insert Gallery ('+(Select Category_name from Category Where ID = " + ddlVal + ")+') with Id = '+CAST("+getLastIDQuery(tableName,"ID")+" AS VARCHAR(10))+' Field = " + field + ") Value " + value.Replace("'", "");
                query += logHistoryQuery(mainIP, browserDetail,action,Convert.ToInt32(user_id),"Success");
                
                Db.setData(query);
               

                return "success";
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }

        }
        public string Update(string ddlValue,int id,string mainIP,string browserDetail, string conditions = null)
        {
            try
            {
                if (Validate(ddlValue) != "valid")
                {
                    return Validate(ddlValue);
                }

                string newRecord = "title = '" + title.Replace("'","") + "', ";
                newRecord += "title_en = '" + title_en.Replace("'", "") + "', ";
                newRecord += "Dates = '" + date + "', ";
                
                if (category_id == all.VideoCategoryID)
                {
                    newRecord += "content = '" + content.Replace("'", "") + "', ";
                    newRecord += "content_en = '" + content_en.Replace("'", "") + "', ";
                    newRecord += "video_url = '" + video_url + "', ";
                    if(img_url != null)
                    {
                        newRecord += "img_url = 'Gallerry/" + img_url + "', ";
                    }
                    
                    
                }
                else
                {
                    if(img_file != null)
                    {
                        newRecord += "img_file = 'Gallerry/" + img_file + "', ";
                    }
                     
                }

                newRecord += "category_id = " + category_id + ", ";

                conditions = conditions != null ? conditions : "";

                Db.Update(tableName, newRecord, dateNow, user_id, conditions,mainIP,browserDetail,id);

                return "success";
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }
        public void Delete(string id,string mainIP,string browseDetail,string condition = null)
        {
            string conditions = condition != null ? condition : "";

            Db.Delete(tableName, dateNow, user_id, conditions,Convert.ToInt32(id),mainIP,browseDetail);
        }
        public DataTable GetAll(int PageIndex,int Category_id)
        {
            SqlCommand cmd = new SqlCommand("GetAllGallery", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PageIndex", PageIndex);
            cmd.Parameters.AddWithValue("@PageSize", PageSize);
            cmd.Parameters.AddWithValue("@VideoCategory", all.VideoCategoryID);
            cmd.Parameters.AddWithValue("@Category_id", Category_id);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public DataTable ShowVideoProfile(string lang)
        {
            int id = all.videoProfileID;
            string cond = " category_id = " + id;
            string content = lang == "En" ? "content_en" : "content";
            string field = "top 1 title, title_en, video_url,img_url, SUBSTRING("+content+",1,500) as content";
            DataTable dt = Select(field,cond);
            return dt;
        }
        public DataTable GetLatestVideo()
        {
            SqlCommand cmd = new SqlCommand("GetLatestVideo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@videoCategoryID", all.VideoCategoryID);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
    }
}