﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="adminSaveKapal.aspx.cs" Inherits="PIS_Backend.adminSaveKapal" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Kapal</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                             Aktivitas Bisnis
                        </li>
                        </li>
                        <li class="breadcrumb-item">
                           Armada Kami
                        </li>
                        <li class="breadcrumb-item">
                           <a href="adminSaveKapal.aspx"><strong>Buat Baru</strong></a> 
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Data Kapal Milik</small></h5>
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                <form>
                    <div class="row">
                                <div class="col-md-6">
                             <div class="form-group">
                                <label for="name"><b>Name</b></label>
                                <asp:TextBox ID="name" cssClass="form-control" placeholder="Input name" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="oil"><b>Oil</b></label>
                                <asp:TextBox ID="oil" cssClass="form-control" placeholder="Input oil" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="detail"><b>detail</b></label>
                                <textarea class="form-control" id="detail" placeholder="Input detail" runat="server" rows="10"></textarea>
                              </div>
                             <div class="form-group">
                                <label for="type"><b>type</b></label>
                                <asp:TextBox ID="type" cssClass="form-control" placeholder="Input type" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="built"><b>built</b></label>
                                <asp:TextBox ID="built" cssClass="form-control" placeholder="Input built" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="FileUpload1"><b>img file</b></label>
                                <asp:FileUpload ID="FileUpload1" runat="server" />
                              </div>
                            <div class="form-group">
                                <asp:Image ID="Image1" runat="server" />
                            </div>
                             <div class="form-group">
                                <label for="size"><b>size</b></label>
                                <asp:TextBox ID="size" cssClass="form-control" placeholder="Input size" runat="server"></asp:TextBox>
                              </div>
                              <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                    ControlToValidate="size" runat="server"
                                    ErrorMessage="Only Numbers allowed"
                                    ValidationExpression="\d+">
                                </asp:RegularExpressionValidator>
                             <div class="form-group">
                                <label for="capacity"><b>capacity</b></label>
                                <asp:TextBox ID="capacity" cssClass="form-control" placeholder="Input capacity" runat="server"></asp:TextBox>
                              </div>
                             <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                                    ControlToValidate="capacity" runat="server"
                                    ErrorMessage="Only Numbers allowed"
                                    ValidationExpression="\d+">
                                </asp:RegularExpressionValidator>
                             <div class="form-group">
                                <label for="builtOn"><b>built On</b></label>
                                <asp:TextBox ID="builtOn" cssClass="form-control" placeholder="Input builtOn" runat="server"></asp:TextBox>
                              </div> 
                             <div class="form-group">
                                <label for="flag"><b>flag</b></label>
                                <asp:TextBox ID="flag" cssClass="form-control" placeholder="Input flag" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="callsign"><b>callsign</b></label>
                                <asp:TextBox ID="callsign" cssClass="form-control" placeholder="Input callsign" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="class_dnv_gl"><b>class dnv gl</b></label>
                                <asp:TextBox ID="class_dnv_gl" cssClass="form-control" placeholder="Input class dnv gl" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="particulars"><b>particulars</b></label>
                                <asp:TextBox ID="particulars" cssClass="form-control" placeholder="Input particulars" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="loa"><b>loa</b></label>
                                <asp:TextBox ID="loa" cssClass="form-control" placeholder="Input loa" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="lbp"><b>lbp</b></label>
                                <asp:TextBox ID="lbp" cssClass="form-control" placeholder="Input lbp" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="parallel_body_loaded"><b>parallel body loaded</b></label>
                                <asp:TextBox ID="parallel_body_loaded" cssClass="form-control" placeholder="Input parallel body loaded" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="breadth_moulded"><b>breadth moulded</b></label>
                                <asp:TextBox ID="breadth_moulded" cssClass="form-control" placeholder="Input breadth moulded" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="height_from_keel_to_mask"><b>height from keel to mask</b></label>
                                <asp:TextBox ID="height_from_keel_to_mask" cssClass="form-control" placeholder="Input height from keel to mask" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="depth_moulded"><b>depth moulded</b></label>
                                <asp:TextBox ID="depth_moulded" cssClass="form-control" placeholder="Input depth moulded" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="draft_summer"><b>draft summer</b></label>
                                <asp:TextBox ID="draft_summer" cssClass="form-control" placeholder="Input draft summer" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="draft_ballast"><b>draft ballast</b></label>
                                <asp:TextBox ID="draft_ballast" cssClass="form-control" placeholder="Input draft ballast" runat="server"></asp:TextBox>
                              </div>  
                         </div> 
                         <div class="col-md-6">
                             <div class="form-group">
                                <label for="tpc"><b>tpc</b></label>
                                <asp:TextBox ID="tpc" cssClass="form-control" placeholder="Input tpc" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="airdraft_in_ballast__loaded_condition"><b>airdraft in ballast  loaded condition</b></label>
                                <asp:TextBox ID="airdraft_in_ballast__loaded_condition" cssClass="form-control" placeholder="Input airdraft in ballast  loaded condition" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="total_size_manifolds"><b>total size manifolds</b></label>
                                <asp:TextBox ID="total_size_manifolds" cssClass="form-control" placeholder="Input total size manifolds" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="distance_form_bow_to_centre_manifold"><b>distance form bow to centre manifold</b></label>
                                <asp:TextBox ID="distance_form_bow_to_centre_manifold" cssClass="form-control" placeholder="Input distance form bow to centre manifold" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="distance_form_stern_to_centre_manifold"><b>distance form stern to centre manifold</b></label>
                                <asp:TextBox ID="distance_form_stern_to_centre_manifold" cssClass="form-control" placeholder="Input distance form stern to centre manifold" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="distance_between_manifolds"><b>distance between manifolds</b></label>
                                <asp:TextBox ID="distance_between_manifolds" cssClass="form-control" placeholder="Input distance between manifolds" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="distance_form_ship_side_to_centre_manifold"><b>distance form ship side to centre manifold</b></label>
                                <asp:TextBox ID="distance_form_ship_side_to_centre_manifold" cssClass="form-control" placeholder="Input distance form ship side to centre manifold" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="distance_from_deck_tray_to_manifold"><b>distance from deck tray to manifold</b></label>
                                <asp:TextBox ID="distance_from_deck_tray_to_manifold" cssClass="form-control" placeholder="Input distance from deck tray to manifold" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="cargo_tank_capacity"><b>cargo tank capacity</b></label>
                                <asp:TextBox ID="cargo_tank_capacity" cssClass="form-control" placeholder="Input cargo tank capacity" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="slop_tank_capacity"><b>slop tank capacity</b></label>
                                <asp:TextBox ID="slop_tank_capacity" cssClass="form-control" placeholder="Input slop tank capacity" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="ballast_tank_capacity"><b>ballast tank capacity</b></label>
                                <asp:TextBox ID="ballast_tank_capacity" cssClass="form-control" placeholder="Input ballast tank capacity" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="deadweight"><b>deadweight</b></label>
                                <asp:TextBox ID="deadweight" cssClass="form-control" placeholder="Input deadweight" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="tonnage_international"><b>tonnage international</b></label>
                                <asp:TextBox ID="tonnage_international" cssClass="form-control" placeholder="Input tonnage international" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="coating"><b>coating</b></label>
                                <asp:TextBox ID="coating" cssClass="form-control" placeholder="Input coating" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="main_engine"><b>main engine</b></label>
                                <asp:TextBox ID="main_engine" cssClass="form-control" placeholder="Input main engine" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="bow_thruster"><b>bow thruster</b></label>
                                <asp:TextBox ID="bow_thruster" cssClass="form-control" placeholder="Input bow thruster" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="cranes"><b>cranes</b></label>
                                <asp:TextBox ID="cranes" cssClass="form-control" placeholder="Input cranes" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="cargo_pumps"><b>cargo pumps</b></label>
                                <asp:TextBox ID="cargo_pumps" cssClass="form-control" placeholder="Input cargo pumps" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="stripping_pump"><b>stripping pump</b></label>
                                <asp:TextBox ID="stripping_pump" cssClass="form-control" placeholder="Input stripping pump" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="ballast_pumps"><b>ballast pumps</b></label>
                                <asp:TextBox ID="ballast_pumps" cssClass="form-control" placeholder="Input ballast pumps" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="max_load_rate_homogenous_cargo_per_manifold"><b>max load rate homogenous cargo per manifold</b></label>
                                <asp:TextBox ID="max_load_rate_homogenous_cargo_per_manifold" cssClass="form-control" placeholder="Input max load rate homogenous cargo per manifold" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="max_load_rate_homogenous_cargothrough_all_manifold"><b>max load rate homogenous cargothrough all manifold</b></label>
                                <asp:TextBox ID="max_load_rate_homogenous_cargothrough_all_manifold" cssClass="form-control" placeholder="Input max load rate homogenous cargothrough all manifold" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="no_of_cargo_tanks"><b>no of cargo tanks</b></label>
                                <asp:TextBox ID="no_of_cargo_tanks" cssClass="form-control" placeholder="Input no of cargo tanks" runat="server"></asp:TextBox>
                              </div>
                             <div class="form-group">
                                <label for="no_of_segregations"><b>no of segregations</b></label>
                                <asp:TextBox ID="no_of_segregations" cssClass="form-control" placeholder="Input no of segregations" runat="server"></asp:TextBox>
                              </div>
                         </div>
                      </div>
                      <div class="form-group">
                          <asp:Button ID="BtnSave" CssClass="btn btn-primary" runat="server" Text="Simpan" OnClick="BtnSave_Click"  />
                      </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $('b').each(function () {
            var newLabel = $(this).text().charAt(0).toUpperCase() + '' + $(this).text().substring(1, $(this).text().length);
            $(this).text(newLabel);
        });
    </script>
</asp:Content>
