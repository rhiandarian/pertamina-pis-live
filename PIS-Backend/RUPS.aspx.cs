﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class RUPS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("R-U-P-S.aspx",true);
        }

        public void LoadData()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 22 ");
            header.Src = dt.Rows[0]["Header"].ToString();
            content.InnerHtml = dt.Rows[0]["content"].ToString();
        }
        public void BindLaporanRUPS(int PageIndex)
        {
            modelRUPS lt = new modelRUPS();
            DataTable dt = lt.LoadlAll(PageIndex);
            int totalRecord = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());
            ListViewLt.DataSource = dt;
            ListViewLt.DataBind(); 

        }
    }
}