﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="Struktur Organisasi.aspx.cs" Inherits="PIS_Backend.Struktur_Organisasi" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx">Home</a></li>
					    <li class="breadcrumb-item"><a href="Struktur Organisasi.aspx" id="link1" runat="server"></a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="link2" runat="server"></li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="hTitle" runat="server"></h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <section class="kelompok-perusahaan">
  		<div class="container">
	  		<div class="row mb-5" >
	  			

	  			<div class="col-lg-12 mb-4 desc">

	  				<div id="content" runat="server">
                          
	  				</div>
                    

						<%--<center class="mt-4"><a id="A1" runat="server"  target="_blank" class="btn btn-default-pis"><span class="bi bi-file-pdf-fill"></span> Lihat versi pdf</a></center>--%>
						<center class="mt-4"><a id="fileDownload" runat="server" class="btn btn-default-pis"></a></center>

	  			</div>
	  		</div>

	  	</div>
  	</section>
    
    <script>
        var urlName = '';
        if ($("#LbInd").attr("class") == "active") {
            urlName = "struktur-organisasi";
        }
        if($("#LbEn").attr("class") == "active")
        {
            urlName = "organization-structure";
        }
        history.pushState({}, null, urlName);
        $("#ContentPlaceHolder1_fileDownload").hide();
        $("#btnDownload").attr("href", $("#ContentPlaceHolder1_fileDownload").attr("href"));
    </script>
</asp:Content>
