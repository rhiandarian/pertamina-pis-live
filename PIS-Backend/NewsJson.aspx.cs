﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class NewsJson : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Response.Write(data());
            }
        }
        public string data()
        {
            General all = new General();
            News n = new News();
            DataTable dt = n.LoadNews();
            string print = "[";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string coma = i < dt.Rows.Count - 1 ? ", " : "";
                print += "{" + all.addJsonFieldValue("uuid", dt.Rows[i]["uuid"].ToString());
                print += all.addJsonFieldValue("img_file", dt.Rows[i]["img_file"].ToString());
                print += all.addJsonFieldValue("news_date", dt.Rows[i]["news_date"].ToString());
                print += all.addJsonFieldValue("Category_name", dt.Rows[i]["Category_name"].ToString());
                print += all.addJsonFieldValue("Category_en", dt.Rows[i]["Category_en"].ToString());
                print += all.addJsonFieldValue("title", dt.Rows[i]["title"].ToString());
                print += all.addJsonFieldValue("title_en", dt.Rows[i]["title_en"].ToString());
                print += all.addJsonFieldValue("key", dt.Rows[i]["key"].ToString(),true)+"}"+coma;
            }
            print += "]";
            return print;
        }
    }
}