﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace PIS_Backend
{
    public class Connection
    {
        public static SqlConnection conn()
        {
            string config = ConfigurationManager.ConnectionStrings["PicDb"].ToString();
            SqlConnection con = new SqlConnection(config);
            return con;
        }
    }
}