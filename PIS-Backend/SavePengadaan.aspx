﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="SavePengadaan.aspx.cs" Inherits="PIS_Backend.SavePengadaan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Informasi Pengadaan</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Menu Lainnya 
                        </li>
                        <li class="breadcrumb-item">
                            Pengadaan
                        </li> 
                        <li class="breadcrumb-item active">
                           <a href="SavePengadaan.aspx"><strong>Buat Baru</strong></a> 
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Buat Baru</small></h5>
                              <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label for="title"><b>Title</b></label>
                             <asp:TextBox ID="title" cssClass="form-control"  placeholder="Masukkan judul Pengadaan dalam bahasa Indonesia" runat="server"></asp:TextBox>
                        </div>
                        <div class="col">
                            <label for="title_en"><b>Title English</b></label>
                             <asp:TextBox ID="title_en" cssClass="form-control"  placeholder="Masukkan judul Pengadaan dalam bahasa Inggris" runat="server"></asp:TextBox>
                        </div>
                    </div>
                        <div class="form-group">
                            <label for="date"><b>add date</b></label>
                            <asp:TextBox ID="date" runat="server" CssClass="form-control" placeholder="Masukkan tanggal pengadaan"></asp:TextBox>
                         </div>
                         
                      </div>
                     <div class="form-group">
                            <label for="FileUpload1"><b>PDF pengadaan</b></label>
                            <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                         </div>
                       <div class="form-group">
                            <label for="FileUpload2"><b>Cover Image</b></label>
                            <asp:FileUpload ID="FileUpload2" cssClass="form-control-file" runat="server" />
                         </div>
                     <div class="form-group">
                         <asp:Button ID="Save" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="BtnAdd_Click"    />
                      </div>
            </div>
        </div>
    </div>
</asp:Content>
