﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tesvideo.aspx.cs" Inherits="PIS_Backend.tesvideo" %>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Pertamina PIS</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="images/favicon.png" rel="icon">
  <link href="images/favicon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="vendor/bootstrap-4.5.3-dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/animate/animate.css" rel="stylesheet" />
  <link href="vendor/venobox/venobox.css" rel="stylesheet">
  <link href="vendor/aos/aos.css" rel="stylesheet">
  <link href="vendor/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Template Main CSS File -->
    <link href="css/v01/style.css" rel="stylesheet" />
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <a href="" class="active">ID</a> | <a href="">EN</a>
      </div>
      <div class="contact-info float-right">
      	<a href="contact.html"><i class="bi bi-envelope"></i></a>
      	<a href="#search"><i class="bi bi-search"></i></a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
        <a href="index.html"><img src="images/Logo-Pertamina-PIS.svg" alt="" class="img-fluid"></a>
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
      	<a href="" id="nav-right-option" class="nav-right-option float-right mobile-nav-toggle d-lg-none"><i class="bi bi-x"></i></a>
        <ul>
          <li class="active"><a href="index.html">Beranda</a></li>
          <li class="drop-down"><a href="">Tentang PIS</a>
          	<ul>
              <li><a href="visi-misi.html">Visi & Misi</a></li>
              <li><a href="tata-nilai.html">Tata Nilai</a></li>
              <li class="drop-down"><a href="">Profil Perusahaan</a>
              	<ul>
                  <li><a href="sekilas-tentang-pis.html">Sekilas Tentang PIS</a></li>
                  <li><a href="makna-logo-pis.html">Makna Logo PIS</a></li>
                  <li><a href="tonggak-sejarah.html">Tonggak Sejarah</a></li>
                </ul>
              </li>
              <li class="drop-down"><a href="">Manajemen</a>
              	<ul>
                  <li><a href="dewan-komisaris.html">Dewan Komisaris</a></li>
                  <li><a href="direksi.html">Direksi</a></li>
                  <li><a href="sambutan-direktur-utama.html">Sambutan Direksi</a></li>
                  <li><a href="struktur-organisasi.html">Struktur Organisasi</a></li>
                </ul>
              </li>
              <li><a href="kelompok-perusahaan.html">Kelompok Perusahaan</a></li>
              <li><a href="standard-internasional.html">Standar Internasional</a></li>
            </ul>
          </li>
          <li class="drop-down"><a href="">Aktivitas Bisnis</a>
          	<ul>
              <li><a href="layanan-kami.html">Layanan Kami</a></li>
              <li class="drop-down"><a href="">Armada Kami</a>
              	<ul>
              		<li><a href="">Kapal Milik</a></li>
              		<li><a href="">Time Charter</a></li>
              		<li><a href="">Spot Charter</a></li>
              	</ul>
              </li>
            </ul>
          </li>
          <li class="drop-down"><a href="">Media & Informasi</a>
          	<ul>
          		<li><a href="berita.html">Berita</a></li>
          		<li><a href="gallery.html">Galeri</a></li>
          		</li>
          	</ul>
          </li>

		      <li class="d-lg-none"><a href="">Hubungan Investor</a></li>
		      <li class="d-lg-none"><a href="">Komitmen & Keberlanjutan</a></li>
		      <li class="d-lg-none"><a href="">Pengadaan</a></li>
		      <li class="d-lg-none"><a href="career.html">Karir</a></li>
		      <li class="d-lg-none"><a href="contact.html">Kontak</a></li>

          <li class="nav-right-option d-none d-lg-block"><a href="" onclick="openNavOverlay()" id="nav-right-option">Menu Lainnya <span class=""></span></a></li>
        </ul>
        <div class="text-center lang-mobile d-lg-none">
		      <span href="" class="active">ID</span> | <span href="">EN</span>
		    </div>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- Nav Overlay -->
  <div id="navOverlay" class="nav-overlay d-none d-lg-block">
  	<div class="bg-black-transparent">
	  	<div class="container">
		    <div class="nav-overlay-content">
		      <a href="">Hubungan Investor</a>
		      <a href="">Komitmen & Keberlanjutan</a>
		      <a href="">Pengadaan</a>
		      <a href="career.html">Karir</a>
		      <a href="contact.html">Kontak</a>
		    </div>
	  	</div>
  	</div>
  </div><!-- End Nav Overlay -->

  <!-- Search -->
  <div id="search">
  	<div class="bg-overlay">
	    <button type="button" class="close"><i class="bi bi-x close"></i></button>
	    <form>
	    <input type="search" value="" placeholder="Type keyword(s) here" />
	    <button type="submit" class="btn bg-red">Cari</button>
	    </form>
  	</div>
	</div><!-- End Search -->

  <main id="main">

  	<!-- Top Main -->
  	<section id="breadcrumbs" class="pt-3">
			<div class="container">
				<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
					    <li class="breadcrumb-item"><a href="">Media & Informasi</a></li>
					    <li class="breadcrumb-item active" aria-current="page">Berita</li>
					  </ol>
					</nav>
					<div class="col-lg-12 page-title">
						<h3>Media & Informasi, Galeri</h3>
					</div>
				</div>
					
				</div>
  	</section>


  	<!-- ======= Media Information Section ======= -->
    <section class="gallery">
      <div class="container">
				<div class="row no-gutters">
					<div class="col-lg-12 mb-3">
						<ul class="nav nav-tabs row-eq-height" id="myTab" role="tablist">
							<li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5 active" id="video-tab" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="true">
							   Video
							  </a>
						  </li>
						  <li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5" id="photo-tab" data-toggle="tab" href="#photo" role="tab" aria-controls="photo" aria-selected="false">
						    	Foto
						    </a>
						  </li>
						  <li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5" id="infografis-tab" data-toggle="tab" href="#infografis" role="tab" aria-controls="infografis" aria-selected="false">
						    	Infografis
						    </a>
						  </li>
						</ul>
						<div class="tab-content bg-white pt-5 pb-5" id="myTabContent">
						  <div class="tab-pane fade show active" id="video" role="tabpanel" aria-labelledby="video-tab">
						  	<div class="row">
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<!--
						  			<div class="embed-responsive embed-responsive-16by9">
										  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/URHBX4oK3Wc" allowfullscreen></iframe>
										</div>
										-->
										<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  	</div>

						  	<!---Pagination-->
						  	<div class="row mt-4">
				        	<div class="col-12">
				        		<div class="row">
				        			<div class="col-6">
				        				<p>Pages: 1 of 52</p>
				        			</div>
				        			<div class="col-6 text-right">
				        				<nav aria-label="...">
												  <ul class="pagination  justify-content-end">
												    <li class="page-item disabled">
												      <span class="page-link">Prev</span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">1</a></li>
												    <li class="page-item active">
												      <span class="page-link">
												        2 <span class="sr-only">(current)</span>
												      </span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">3</a></li>
												    <li class="page-item">
												      <a class="page-link" href="#">Next</a>
												    </li>
												  </ul>
												</nav>
				        			</div>
				        		</div>
				        	</div>
				        </div>

						  </div>

						  <div class="tab-pane fade" id="photo" role="tabpanel" aria-labelledby="photo-tab">
						  	<div class="row">
						  		<div class="popup-gallery gallery-1 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
						  		</div>
						  		<div class="popup-gallery gallery-2 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
							  	</div>
							  	<div class="popup-gallery gallery-3 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
							  	</div>
							  	<div class="popup-gallery gallery-4 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
							  	</div>
							  	<div class="popup-gallery gallery-5 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
							  	</div>
							  	<div class="popup-gallery gallery-6 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
							  	</div>
							  	<div class="popup-gallery gallery-7 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>25 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
							  	</div>
						  	</div>

						  	<!---Pagination-->
						  	<div class="row mt-4">
				        	<div class="col-12">
				        		<div class="row">
				        			<div class="col-6">
				        				<p>Pages: 1 of 52</p>
				        			</div>
				        			<div class="col-6 text-right">
				        				<nav aria-label="...">
												  <ul class="pagination  justify-content-end">
												    <li class="page-item disabled">
												      <span class="page-link">Prev</span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">1</a></li>
												    <li class="page-item active">
												      <span class="page-link">
												        2 <span class="sr-only">(current)</span>
												      </span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">3</a></li>
												    <li class="page-item">
												      <a class="page-link" href="#">Next</a>
												    </li>
												  </ul>
												</nav>
				        			</div>
				        		</div>
				        	</div>
				        </div>
						  </div>
						  <div class="tab-pane fade" id="infografis" role="tabpanel" aria-labelledby="infografis-tab">
						  	<div class="row">
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="image-link" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  	</div>

						  	<!---Pagination-->
						  	<div class="row mt-4">
				        	<div class="col-12">
				        		<div class="row">
				        			<div class="col-6">
				        				<p>Pages: 1 of 52</p>
				        			</div>
				        			<div class="col-6 text-right">
				        				<nav aria-label="...">
												  <ul class="pagination  justify-content-end">
												    <li class="page-item disabled">
												      <span class="page-link">Prev</span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">1</a></li>
												    <li class="page-item active">
												      <span class="page-link">
												        2 <span class="sr-only">(current)</span>
												      </span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">3</a></li>
												    <li class="page-item">
												      <a class="page-link" href="#">Next</a>
												    </li>
												  </ul>
												</nav>
				        			</div>
				        		</div>
				        	</div>
				        </div>

						  </div>
						</div>

					</div>
        </div>
        
      </div>
    </section><!-- Media Information Section -->

    

  </main>

  <!-- ======= Footer ======= -->
	<div id="footer-top-border">
		<div class="col-lg-12 col-12">
			<div class="row">
				<div class="col-lg-6 col-6 footer-border-1"></div>
				<div class="col-lg-3 col-3 footer-border-2"></div>
				<div class="col-lg-3 col-3 footer-border-3"></div>
			</div>
		</div>
	</div>
	<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-4 col-12 footer-info">
            <h6><strong>KANTOR PUSAT</strong></h6>
            <p class="mt-3">
              Patra Jasa Office Tower Lantai 3 & 14 
              <br>Jl. Jend. Gatot Subroto Kav 32–34, 
              <br>Kuningan Timur, Setiabudi 
              <br>Jakarta 12950 
              <br>Indonesia 
            </p>
            <div class="d-flex mt-2">
              <div class="align-self-center rounded-icon">
	  					  <span class="bi bi-telephone-fill phone-icon"></span>
              </div>
              <div class="align-self-center">
                &nbsp; (021) 5290 0271 / 5290 0272
              </div>
	  				</div>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>


          <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong>ANAK PERUSAHAAN & AFILIASI</strong></h6>
            <ul class="mt-3">
              <li><i class="bx bx-chevron-right"></i> <a href="#">PT Pertamina (Persero)</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Kementrian ESDM RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Kementrian Perhubungan RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">BKPM</a></li>
            </ul>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>

          <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong>TEMUKAN KAMI</strong></h6>
            <ul class="mt-3 social-links">
              <li>
              	<a href="#" class="d-flex">
	              	<div class="align-self-center">
	              		<span class="bi bi-facebook facebook"></span>
	              	</div>
	              	<div class="align-self-center">Facebook<br><em>PT Pertamina International Shipping</em></div>
	              </a>
	            </li>
              <li>
              	<a href="#" class="d-flex">
	              	<div class="align-self-center">
	              		<span class="bi bi-instagram instagram"></span>
	              		</div>
	              	<div class="align-self-center">Instagram<br><em>@Pertamina_PIS</em></div> 
	              </a>
	            </li>
              <li>
              	<a href="#" class="d-flex">
	              	<div class="align-self-center">
	              		<span class="bi bi-twitter twitter"></span>
	              		</div>
	              	<div class="align-self-center">Twitter<br><em>@pertamina_pis</em></div> 
	              </a>
	            </li>
            </ul>
          </div>
          <div class="col-12 d-lg-none separate"><hr></div>

        </div>
      </div>
    </div>

    <div class="container">
    	<div class="row d-flex">
	      <div class="col-lg-8 col-12 align-self-center copyright">
	        &copy; Copyright 2021 <strong><span>Pertamina International Shipping</span></strong>. All Rights Reserved
	      </div>
    		<div class="col-lg-4 col-12">
    			<a href="" class="btn btn-whistle">
            	<div class="row">
            		<div class="col pr-0 d-flex mr-2">
          				<img src="images/icons/wbs.svg">
            		</div>
            		<div class="col text-left pl-0">
		          		Whistle Blowing System
		          		<br><em>https://pertaminaclean.tipoffs.info/</em>
            		</div>
            	</div>
          	</a>
    		</div>
    	</div>
    </div>
  </footer><!-- End Footer -->
    

   

  <!-- Vendor JS Files -->
  <script src="vendor/jquery.min.js"></script>
  <script src="vendor/bootstrap-4.5.3-dist/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="vendor/venobox/venobox.min.js"></script>
  <script src="vendor/waypoints/lib/jquery.waypoints.min.js"></script>
  <script src="vendor/counterup/counterup.min.js"></script>
  <script src="vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="vendor/aos/aos.js"></script>
  <script src="vendor/fontawesome/js/all.min.js"></script>
  <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Template Main JS File -->
  <script src="js/main.js?v=0.1"></script>

  <script type="text/javascript">
  	$(document).ready(function() {

		  $('.image-popup-vertical-fits').magnificPopup({
				type: 'image',
				closeOnContentClick: true,
				mainClass: 'mfp-img-mobile',
				image: {
					verticalFit: true
				},
				zoom: {
					enabled: true,
					duration: 300 
				}
			});

			$('.popup-youtube').magnificPopup({
				disableOn: 700,
				type: 'iframe',
				mainClass: 'mfp-fade',
				removalDelay: 160,
				preloader: false,
				fixedContentPos: false,
			});

			let countGallery = $(".popup-gallery").length;
			for (i=0; i<countGallery; i++) {
			  $(".gallery-"+i).magnificPopup({
					delegate: 'a',
					type: 'image',
					tLoading: 'Loading image #%curr%...',
					mainClass: 'mfp-img-mobile',
					gallery: {
						enabled: true,
						navigateByImgClick: true,
						preload: [0,1] // Will preload 0 - before current, and 1 after the current image
					},
					image: {
						tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
						titleSrc: function(item) {
							return item.el.attr('title') + '';
						}
					}
				});
			} 

		});
  </script>

</body>

</html>
