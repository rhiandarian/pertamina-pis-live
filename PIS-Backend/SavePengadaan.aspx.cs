﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class SavePengadaan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "29"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                Loaddata();
            }
        }
        public void Loaddata()
        {
            if (Request.QueryString["ID"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                string cond = " ID = " + Id;
                InfoPengadaan p = new InfoPengadaan();
                DataTable dt = p.Select("*", cond);
                title.Text = dt.Rows[0]["title"].ToString();
                title_en.Text = dt.Rows[0]["title_en"] != null ? dt.Rows[0]["title_en"].ToString() : "";
                if (dt.Rows[0]["Dates"].ToString() != "")
                {
                    DateTime dates = Convert.ToDateTime(dt.Rows[0]["Dates"].ToString());
                    date.Text = dates.ToString("MM/dd/yyyy");
                }
            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            { General all = new General();
                string random = randoms();
                InfoPengadaan ip = new InfoPengadaan();
                ip.Title = title.Text;
                ip.TitleEn = title_en.Text;
                ip.Dates = Convert.ToDateTime(date.Text);
                ip.Url = FileUpload1.FileName;
                
                ip.FileInputSize = FileUpload2.PostedFile.ContentLength;
                
                if (Request.QueryString["ID"] == null)
                {
                    ip.Cover = FileUpload2.FileName;
                    string insertStatus = ip.Insert(getip(),GetBrowserDetails(),random);
                    if (insertStatus == "success")
                    {
                        UploadPdf(ip.folder);
                        UploadCover(ip.folder,random);
                        Response.Redirect(ip.listPage);
                    }
                    else
                    {
                        showValidationMessage(insertStatus);
                    }
                }
                else
                {
                    int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                    string cond = " ID = " + Id;
                    if (FileUpload2.FileName != "")
                    {
                        ip.Cover = FileUpload2.FileName;
                    }
                    string updateStatus = ip.Update(random,getip(),GetBrowserDetails(),Id,cond);
                    if(updateStatus == "success")
                    {
                        if (FileUpload1.FileName != "")
                        {
                            UploadPdf(ip.folder);
                        }
                        if (FileUpload2.FileName != "")
                        {
                            UploadCover(ip.folder,random);
                        }
                        Response.Redirect(ip.listPage);
                    }
                    else
                    {
                        showValidationMessage(updateStatus);
                    }
                }
            }
            catch(Exception ex)
            {
                InfoPengadaan ip = new InfoPengadaan();
                string action = Request.QueryString["ID"] == null ? "Insert" : "Update";
                ip.ErrorLogHistory(getip(), GetBrowserDetails(), action, ip.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                showValidationMessage(ex.Message.ToString());
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void UploadPdf(string folder)
        {

            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string SaveLocation = Server.MapPath(folder) + "\\" + fn;

            FileUpload1.PostedFile.SaveAs(SaveLocation);
        }
        public string randoms()
        {
            ImageResize ir = new ImageResize();
            return ir.textrandom();
        }
        public void UploadCover(string folder, string random)
        {
            

            ImageResize Iz = new ImageResize();
            string fn = System.IO.Path.GetFileName(FileUpload2.PostedFile.FileName);
            string SetFolderBeforeResize = Server.MapPath(folder) + "\\" + "serize" + random + fn; //gambar yang akan diresize sesudah direze gambar akan dihapus
            string SetFolderAfterResize = Server.MapPath(folder) + "\\" + random + fn;
            FileUpload2.PostedFile.SaveAs(SetFolderBeforeResize);
            Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "Pengadaan", "Cut");

           

        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
    }
}