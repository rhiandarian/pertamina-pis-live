﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class SaveSO : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "20"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;

                LoadData();
            }
        }

        public void LoadData()
        {
            if (Request.QueryString["ID"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                StandardISO si = new StandardISO();
                string cond = " ID = " + Id;
                DataTable dt = si.Select("*", cond);
                nama.Text = dt.Rows[0]["Nama"].ToString();
                pemberi.Text = dt.Rows[0]["Pemberi"].ToString();
                nama_en.Text = dt.Rows[0]["Nama_En"].ToString();
                pemberi_en.Text = dt.Rows[0]["Pemberi_En"].ToString();
                DateTime dt1 = Convert.ToDateTime(dt.Rows[0]["ValidasiFrom"].ToString());
                DateTime dt2 = Convert.ToDateTime(dt.Rows[0]["ValidasiTo"].ToString());
                from.Text = dt1.ToString("MM/dd/yyyy");
                until.Text =  dt2.ToString("MM/dd/yyyy");
            }
        }
 
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                StandardISO si = new StandardISO();
                si.Nama = nama.Text;
                si.Nama_En = nama_en.Text;
                si.ValidasiFrom  = Convert.ToDateTime(from.Text);
                si.ValidasiUntil = Convert.ToDateTime(until.Text);
                si.Pemberi = pemberi.Text;
                si.Pemberi_En = pemberi_en.Text;
                si.Img_File = FileUpload1.FileName;

                if (Request.QueryString["ID"] == null)
                {
                    string insertStatus = si.Insert(getip(),GetBrowserDetails());
                    if (insertStatus == "success")
                    {
                        UploadSO();
                        Response.Redirect(si.listPage);
                    }
                    else
                    {
                        showValidationMessage(insertStatus);
                    }
                }
                else
                {
                    int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                    string cond = " ID = " + Id;
                    string updateStatus = si.Update(Id,getip(),GetBrowserDetails(),cond);
                    if (updateStatus == "success")
                    {
                        if (FileUpload1.FileName != "")
                        {
                            UploadSO();
                        }
                        Response.Redirect(si.listPage);
                    }
                    else
                    {
                        showValidationMessage(updateStatus);
                    }
                }
            }
            catch(Exception ex)
            {
                StandardISO si = new StandardISO();
                string action = Request.QueryString["ID"] == null ? "Insert" : "Update";
                si.ErrorLogHistory(getip(), GetBrowserDetails(), action, si.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                showValidationMessage(ex.Message.ToString());
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void UploadSO()
        {
            StandardISO si = new StandardISO();

            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string SaveLocation = Server.MapPath(si.folder) + "\\" + fn;

            FileUpload1.PostedFile.SaveAs(SaveLocation);
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
    }
}