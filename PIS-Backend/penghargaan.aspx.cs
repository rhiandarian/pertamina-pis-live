﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PIS_Backend
{
    public partial class penghargaan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {             
            }

            LoadData();
            loadPenghargaan(1);
        }

        public void LoadData()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 18 ");
            imgHeader.Src = dt.Rows[0]["Header"].ToString();
            //content.InnerHtml = dt.Rows[0]["content"].ToString();

            li_1.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Home" : "Beranda";
            li_2.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Commitment & Sustainibility" : "Komitmen & Keberlanjutan";
            li_3.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Awards" : "Penghargaan";
            header_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "AWARDS" : "PENGHARGAAN";
            content_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Awards" : "Penghargaan";
            //fileDownload.HRef = dt.Rows[0]["others"].ToString();
            editClassLang();
        }

        public void loadPenghargaan(int PageIndex)
        {
            Penghargaan lt = new Penghargaan();
            DataTable dt = lt.LoadData(PageIndex);
            int totalRecord = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());
            List<Penghargaan> img = new List<Penghargaan>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                img.Add(new Penghargaan()
                { 
                    Title = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[i]["title_en"].ToString() : dt.Rows[i]["title"].ToString(),
                    Desc = dt.Rows[i]["description"].ToString(),
                    Cover = dt.Rows[i]["cover"].ToString(),
                     
                });
            }
            ListViewLt.DataSource = img;
            ListViewLt.DataBind();
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            } 
        }
    }
}