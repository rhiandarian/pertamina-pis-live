﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace PIS_Backend
{
    public class Users:ModelData
    {
        SqlConnection con = Connection.conn();
        ModelData Db = new ModelData();
        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        public string listPage = "UserList.aspx";

        public string tableName = "Users";

        public string viewName = "View_UsersFull";

        public string tableLevel = "Level";

        private int emp_id;

        private string username;

        private string password;

        private string confirmPassword;

        private string email;

        private int level;

        

        public int EmpId
        {
            get { return emp_id; }
            set { emp_id = value; }
        }
        public string Username
        {
            get { return username; }
            set { username = value; }
        }


        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string ConfirmPassword
        {
            get { return confirmPassword; }
            set { confirmPassword = value; }
        }

        public int Level
        {
            get { return level; }
            set { level = value; }
        }
        


        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public DataSet SelectLevels()
        {
            string Query = Db.SelectQuery("Id,name", tableLevel,"");
            DataSet dt = Db.getDataSet(Query);
            return dt;
        }
        public bool ValidEmail(string email)
        {
           
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (match.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool validInputUsername(string input)
        {
            string[] notValid = { "~", "!", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "+", "=", "'", "{", "}", "[", "]", ":", ";", ",", "\"", "<", ">", ",", ".", "/", "?","Select","SELECT","INSERT","Insert","Update","UPDATE","Delete","DELETE" };
            for(int i=0; i<notValid.Length; i++)
            {
                if(input.Contains(notValid[i]))
                {
                    return false;
                }
            }
            return true;
        }
        public bool validInputRegistration(string input)
        {
            string[] notValid = { "~", "!", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "+", "=", "'", "{", "}", "[", "]", ":", ";", ",", "\"", "<", ">", ",", ".", "/", "?"};
            for (int i = 0; i < notValid.Length; i++)
            {
                if (input.Contains(notValid[i]))
                {
                    return false;
                }
            }
            return true;
        }
        public string validate()
        {
            if(emp_id == 0)
            {
                return "Employee Id Harus diisi";
            }
            if(username == "")
            {
                return "Username harus diisi";
            }
            if(!validInputRegistration(username))
            {
                return "Username hanya mengandung nama dan angka";
            }
            if(password == "")
            {
                return "Password harus diisi";
            }
            if(confirmPassword == "")
            {
                return "Confirm Password harus diisi";
            }
            if(password != confirmPassword)
            {
                return "Confirm Password harus sama dengan Password";
            }
            if(email == "")
            {
                return "Email harus diisi";
            }
            if(!ValidEmail(email))
            {
                return "Email tidak Valid";
            }
            if(level == 0)
            {
                return "Level User Harus di Pilih";
            }
            return "Valid";
        }
        public string EncryptPassword(string password)
        {
            byte[] encData_byte = new byte[password.Length];
            encData_byte = System.Text.Encoding.UTF8.GetBytes(password);
            string encodedData = Convert.ToBase64String(encData_byte);
            return encodedData;
        }
        public string DecryptPassword(string password)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(password);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char);
            return result;
        }
        public DataTable SaveUser(string mainIP,string browserDetail)
        {
            
            SqlCommand cmd = new SqlCommand("RegistrationUser", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@emp_id", emp_id);
            cmd.Parameters.AddWithValue("@User_name", username);
            cmd.Parameters.AddWithValue("@Password", EncryptPassword(password));
            cmd.Parameters.AddWithValue("@Email", email);
            cmd.Parameters.AddWithValue("@Level", level);
            cmd.Parameters.AddWithValue("@mainIP", mainIP);
            cmd.Parameters.AddWithValue("@browserDetail", browserDetail);
            cmd.Parameters.AddWithValue("@createdAt", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            cmd.Parameters.AddWithValue("@user_id",Convert.ToInt32(user_id));

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;


        }
        public DataTable LoginUser()
        {
            string cond = "User_name = '" + username.Replace("'", "''").Replace("--", "") + "' AND Password = '" + EncryptPassword(password) + "'";

            DataTable dt = Db.getData(Db.SelectQuery("*", viewName, cond));

            return dt;
        }
        public bool AuthUserLogin()
        {
            DataTable dt = LoginUser();
            
            
            if (dt.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}