﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Net;

namespace PIS_Backend
{
    public partial class Login : System.Web.UI.Page
    {
        SqlConnection con = Connection.conn();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LblErrorEmail.ForeColor = System.Drawing.Color.Red;
                LblErrorMessage.Visible = false;
                LblErrorEmail.Visible = false;
            }
        }
        protected bool AuthFirst()
        {
            Users u = new Users();
            u.Email = Email.Text;
            u.Password = Password.Text;

            return u.AuthUserLogin();
        }

        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            if (!AuthFirst())
            {
                LblErrorMessage.Visible = true;
            }
            else
            {
                Users u = new Users();
                u.Email = Email.Text;
                u.Password = Password.Text;
                if (!u.validInputUsername(u.Username))
                {
                    LblErrorMessage.Visible = true;
                    LblErrorMessage.Text = "Username tidak Valid";
                }
                else
                {
                    DataTable dt = u.LoginUser();

                string email = Email.Text;
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(email);
                if (match.Success)
                {

                    DataTable dt = u.LoginUser();

                    if (dt.Rows[0]["Password"].ToString() == u.EncryptPassword(Password.Text))
                    {
                        getip(); 
                        Session.Add("UserID", dt.Rows[0]["Id"].ToString());
                        Session.Add("Username", dt.Rows[0]["User_name"].ToString());
                        Session.Add("Level", dt.Rows[0]["Level_Id"].ToString());
                        Session.Add("LevelType", dt.Rows[0]["Level"].ToString());

                        Response.Redirect("admin.aspx");
                    }
                    else
                    {
                        LblErrorMessage.Visible = true;
                        LblErrorMessage.ForeColor = System.Drawing.Color.Red;


                    }
                }

                LblErrorEmail.Visible = true;
                LblErrorEmail.ForeColor = System.Drawing.Color.Red;

            }
        }

        public void getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            string mainIP = ipAddress[0];

            ModelData Db = new ModelData();
            string query = $@"insert into current_log (ip_address,browser,created_at) values('{mainIP}','{GetBrowserDetails()}','{DateNow()}')";
            Db.setData(query);
        }

        public static string DateNow()
        {
            TimeZoneInfo timeZoneInfo;
            DateTime dateTime;
            //Set the time zone information to US Mountain Standard Time 
            timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            //Get date and time in US Mountain Standard Time 
            dateTime = TimeZoneInfo.ConvertTime(DateTime.Now, timeZoneInfo);
            //Print out the date and time
            return dateTime.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
    }
}