﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class MenuAdminJson : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Response.Write(data(Request.QueryString["LevelId"].ToString()));
            }

        }
        public string data(string levelid)
        {
            Level l = new Level();
            General all = new General();
            DataTable dt =  l.SelectLevelPages("id_html,id_parent,id_parent_2,id_parent_3","Level_Id = "+ Request.QueryString["LevelId"].ToString());
            string print = "[";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string coma = i < dt.Rows.Count - 1 ? ", " : "";
                print += "{" + all.addJsonFieldValue("id_html", dt.Rows[i]["id_html"].ToString());
                print += all.addJsonFieldValue("id_parent", dt.Rows[i]["id_parent"].ToString());
                print += all.addJsonFieldValue("id_parent_2", dt.Rows[i]["id_parent_2"].ToString());
                print += all.addJsonFieldValue("id_parent_3", dt.Rows[i]["id_parent_3"].ToString(), true) + " }" + coma;
            }
            print += "]";
            return print;
        }
    }
}