﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PIS_Backend
{
    public class Layanan:ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName  = "Layanan";

        public string viewName   = "View_Layanan";

        public string IndexPage  = "AdminLayanan.aspx";

        public string SavePage   = "SaveLayanan.aspx";

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        General all = new General();

        public string title;

        public string title_en;

        public string description;

        public string description_en;

        public string img_file;

        public string Title { get { return title; } set { title = value; } }

        public string TitleEn { get { return title_en; } set { title_en = value; } }

        public string Description { get { return description; } set { description = value; } }

        public string DescriptionEn { get { return description_en; } set { description_en = value; } }

        public string Img_File { get { return img_file; } set { img_file = value; } }

        public DataTable Select(string field,string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            string Query = Db.SelectQuery(field, viewName, conditions);
            DataTable dt = Db.getData(Query);

            return dt;
        }
        public string Validate()
        {
            if(title == "")
            {
                return "judul dalam bahasa Indonesia harus di isi";
            }
            if (title_en == "")
            {
                return "judul dalam bahasa Inggris harus di isi";
            }
            if (description == "")
            {
                return "deskripsi dalam bahasa Indonesia harus di isi";
            }
            if (description_en == "")
            {
                return "deskripsi dalam bahasa Inggris harus di isi";
            }
            if (img_file != null)
            {
                if(!all.ImageFileType(img_file))
                {
                    return "Masukan file foto";
                }
            }
            return "Valid";
        }
        public DataTable GetPageData()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 9 ");
            return dt;
        }
        public string Insert(string mainIP, string browserDetail)
        {
            string validation = Validate();
            if(validation != "Valid")
            {
                return validation;
            }
            string field = "title, title_en, description, description_en, img_file, ";
            string value = "'" + title + "', '" + title_en + "', '" + description+ "', '" + description_en + "', 'Layanan/" + img_file + "', ";

            Db.Insert(tableName, field, value,dateNow,user_id,mainIP,browserDetail);
            return "success";
        }
        public string Update(int id,string mainIP, string browserDetail,string conditions = null)
        {
            string validation = Validate();
            if(validation != "Valid")
            {
                return validation;
            }
            string newRecord = "";
            newRecord += "title = '" + title + "', ";
            newRecord += "title_en = '" + title_en + "', ";
            newRecord += "description = '" + description + "', ";
            newRecord += "description_en = '" + description_en + "', ";
            newRecord += img_file != null ? "img_file = 'Layanan/" + img_file + "', " : "";
            
            conditions = conditions == null ? "" : conditions;

            Db.Updates(tableName, newRecord, conditions,dateNow,user_id,mainIP,browserDetail,id);

          return "success";
        }
        public void Delete(string Id, string mainIP, string browserDetail, string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            Db.Delete(tableName, dateNow, user_id, conditions, Convert.ToInt32(Id), mainIP, browserDetail);
        }

        public void Deleted(string Id, string mainIP, string browserDetail,string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            Db.Delete(tableName, dateNow, user_id, conditions,Convert.ToInt32(Id),mainIP,browserDetail);
        }
    }
}