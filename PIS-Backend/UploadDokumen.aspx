﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="UploadDokumen.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.UploadDokumen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Unggah Dokumen</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Pengaturan Pengguna
                        </li>
                        <li class="breadcrumb-item">
                            Unggah Dokumen
                        </li>
                        <li class="breadcrumb-item">
                            <a href="UploadDokumen.aspx"><strong>Buat Baru</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Unggah Dokumen</small></h5>
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                 <form>
                      <div class="form-group">
                          <label for="keterangan"><b>Keterangan</b></label>
                          <asp:TextBox ID="keterangan" cssClass="form-control" placeholder="Masukkan keterangan tentang file" runat="server"></asp:TextBox>
                        
                      </div>
                     <div class="form-group">
                            <label for="FileUpload1"><b>File</b></label>
                            <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                         </div>
                     
                     
                    <div class="form-group">
                        <asp:Button ID="Save" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="BtnSave_Click"    />
                    </div>
                      
                </form>
            </div>
        </div>
    </div>
</asp:Content>
