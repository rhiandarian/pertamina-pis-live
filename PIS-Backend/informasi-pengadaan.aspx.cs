﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class informasi_pengadaan : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadPengadaan(1);
                BindPage();
            }
            LblIndex.Visible = true;
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"].ToString() == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
        public void BindPage()
        {
            editClassLang();

            string titles = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Procurement Information" : link2.InnerText;
            link1.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Procurement" : link2.InnerText;
            link2.InnerText = titles;
            headerTitle.InnerText = titles.ToUpper();
            title.InnerText = titles;

            InfoPengadaan ip = new InfoPengadaan();
            PagesData pd = ip.getPage();
            imgHeader.Src = pd.header;
            content.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? pd.content_en : pd.content;
        }
        public void LoadPengadaan(int Index)
        {
            string lang = Session["Lang"] != null ? Session["Lang"].ToString() : "Ind";
            InfoPengadaan ip = new InfoPengadaan();
            LblIndex.Text = Index.ToString();
            DataTable dt = ip.LoadData(Index,lang);
            ListViewPengadaan.DataSource = dt;
            ListViewPengadaan.DataBind();
            int totalRecord = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());
            populatePaging(totalRecord, Index);
            
        }
        public void Download(object sender, EventArgs e)
        {
            InfoPengadaan p = new InfoPengadaan();
            LinkButton lb = (LinkButton)sender;
            string filename = lb.CommandArgument;
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.TransmitFile(Server.MapPath("~/" + p.folder + "/" + filename));
            Response.End();
        }
        public void populatePaging(int totalCount, int indexPage)
        {
            string lang = Session["Lang"] != null ? Session["Lang"].ToString() : "Ind";
            InfoPengadaan ip = new InfoPengadaan();
            List<Paging> lp = all.PopulatePaging(totalCount, indexPage, ip.PageSize);
            RptPaging.DataSource = lp;
            RptPaging.DataBind();
            int totalPages = all.getTotalPaging(totalCount, indexPage, ip.PageSize);
            detailPaging.InnerHtml = all.getDetailPaging(lang, indexPage, totalPages);
        }
        protected void Page_Changed(object sender, EventArgs e)
        {
            string commendArgument = (sender as LinkButton).CommandArgument.ToString();
            int pagesIndex = int.Parse(LblIndex.Text);
            if (commendArgument == "first")
            {
                if (pagesIndex > 1)
                {
                    pagesIndex--;
                    LoadPengadaan(pagesIndex);
                }

            }
            else if (commendArgument == "last")
            {
                string lang = Session["Lang"] != null ? Session["Lang"].ToString() : "Ind";
                InfoPengadaan p = new InfoPengadaan();
                DataTable dt = p.LoadData(1, lang);


                int totalCount = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());

                List<Paging> pages = all.PopulatePaging(totalCount, 1, p.PageSize);

                int totalPaging = pages.Count;
                int limit = all.getTotalPaging(totalCount, 1, p.PageSize);

                if (pagesIndex < limit)
                {
                    pagesIndex++;
                    LoadPengadaan(pagesIndex);
                }


            }
            else
            {

                int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
                LoadPengadaan(pageIndex);
            }

        }
    }
}