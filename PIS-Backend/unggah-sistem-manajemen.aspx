﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="unggah-sistem-manajemen.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.unggah_sistem_manajemen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Sistem Manajemen</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                           <a href="admin-sistem-manajemen.aspx">Semua Sistem Manajemen</a> 
                        </li>

                        <li class="breadcrumb-item">
                           <a href="unggah-sistem-manajemen.aspx"><strong>Buat Baru</strong></a> 
                        </li>
                        
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Sistem Manajemen Data.</small></h5>
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                 <form>
                      <div class="form-group">
                          <div class="row">
                              <div class="col">
                                  <label for="nama"><b>Nama</b></label>
                                  <asp:TextBox ID="nama" cssClass="form-control" placeholder="Masukan Nama Sertifikasi" runat="server"></asp:TextBox>
                              </div>
                              <div class="col">
                                  <label for="nama"><b>Nama (English)</b></label>
                                  <asp:TextBox ID="nama_en" cssClass="form-control" placeholder="Masukan Nama Sertifikasi dalam Bahasa Inggris" runat="server"></asp:TextBox>
                              </div>
                          </div>
                        
                      </div>
                     <div class="form-group">
                         <div class="row">
                             <div class="col">
                                 <label for="from"><b>Valid dari</b></label>
                                <asp:TextBox ID="from" CssClass="form-control" placeholder="Masukan Tanggal Valid" runat="server"></asp:TextBox>
                             </div>
                             <div class="col">
                                 <label for="until"><b>Valid sampai</b></label>
                                <asp:TextBox ID="until" CssClass="form-control" placeholder="Masukan Tanggal Valid" runat="server"></asp:TextBox>
                             </div>
                         </div>
                     </div>
                     <div class="form-group">
                         <div class="row">
                             <div class="col">
                                 <label for="pemberi"><b>Pemberi</b></label>
                                 <asp:TextBox ID="pemberi" cssClass="form-control" placeholder="Masukan Pemberi Sertifikasi" runat="server"></asp:TextBox>
                             </div>
                             <div class="col">
                                 <label for="pemberi_en"><b>Pemberi (English)</b></label>
                                 <asp:TextBox ID="pemberi_en" cssClass="form-control" placeholder="Masukan Pemberi Sertifikasi dalam Bahasa Inggris" runat="server"></asp:TextBox>
                             </div>
                         </div>
                        
                      </div>
                      <div class="form-group">
                        <label for="FileUpload1"><b>Image</b></label>
                        <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                     </div>
                     <div class="form-group">
                        <asp:Button ID="Save" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="Save_Click"      />
                    </div>
                      
                </form>
            </div>
        </div>
    </div>
</asp:Content>
