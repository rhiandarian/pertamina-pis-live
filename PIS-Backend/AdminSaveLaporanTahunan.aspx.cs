﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PIS_Backend
{
    public partial class AdminSaveLaporanTahunan : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "21"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                year.Text = "0";
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                LaporanTahunan lt = new LaporanTahunan();
                lt.Year = Convert.ToInt32(year.Text);
                lt.Url = FileUpload1.FileName;
                lt.Cover = FileUpload2.FileName;

                if (!all.PdfFileType(FileUpload1.PostedFile.FileName))
                {
                    showValidationMessage("File harus Pdf");
                }
                else
                {
                    string insertStatus = lt.Insert(getip(),GetBrowserDetails());

                    if (insertStatus == "Success")
                    {
                        string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                        string SaveLocation = Server.MapPath("Laporan Tahunan") + "\\" + fn;

                        FileUpload1.PostedFile.SaveAs(SaveLocation);

                        fn = System.IO.Path.GetFileName(FileUpload2.PostedFile.FileName);
                        ImageResize Iz = new ImageResize();
                        string SetFolderBeforeResize = Server.MapPath("Laporan Tahunan") + "\\" + "serize"  + fn; //gambar yang akan diresize sesudah direze gambar akan dihapus
                        string SetFolderAfterResize = Server.MapPath("Laporan Tahunan") + "\\"  + fn;

                        FileUpload2.PostedFile.SaveAs(SetFolderBeforeResize);
                        Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "laporan-tahunan", "cut");

                        Response.Redirect("AdminLaporanTahunan.aspx");
                    }
                    else
                    {
                        showValidationMessage(insertStatus);
                    }

                }


            }
            catch (Exception ex)
            {
                LaporanTahunan lt = new LaporanTahunan();
                lt.ErrorLogHistory(getip(), GetBrowserDetails(), "Save", lt.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                showValidationMessage(ex.ToString());
            }
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
    }
}