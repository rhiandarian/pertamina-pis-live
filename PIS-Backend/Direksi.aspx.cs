﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System;

namespace PIS_Backend
{
    public partial class Direksi1 : System.Web.UI.Page
    {
        int ptid = 2; string EnPage = "Directors.aspx";
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDireksi();
                BindPage();
                if (Session["Lang"] != null && Session["Lang"].ToString() == "En")
                {
                    Response.Redirect(EnPage);
                }
                bindMenu();
                loadfooter();
            }
        }
        public void bindMenu()
        {
            string fullPath = Request.Url.AbsolutePath;
            string pageName = System.IO.Path.GetFileName(fullPath);
            string currPage = pageName + ".aspx";
            
            string lang = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "En" : "Ind";

            List<Menu> mn = all.GenerateMenu(lang, currPage);
            ListViewMenu.DataSource = mn;
            ListViewMenu.DataBind();

            List<Menu> ot = all.MenuOthers(lang);
            ListViewOthers.DataSource = ot;
            ListViewOthers.DataBind();

            ListViewResponsive.DataSource = ot;
            ListViewResponsive.DataBind();

            string otmn = "Menu Lainnya";
            othermenu.Attributes["class"] = all.getOtherMenuClass(othermenu.Attributes["class"], currPage);
            othermenu.InnerHtml = "<a href=" + all.stripped("") + " onclick=" + all.stripped("openNavOverlay()") + " id=" + all.stripped("nav-right-option") + " >" + otmn + " <span class=" + all.stripped("") + "></span></a>";
            langMobile.InnerHtml = all.changeLangMobile("Ind");
        }
        protected void ChangeLang(object sender, EventArgs e)
        {
            if (Session["Lang"] == null)
            {
                Session.Add("Lang", "En");
            }
            else
            {
                Session["Lang"] = "En";
            }
            Response.Redirect(EnPage);
        }
        public void BindDireksi()
        {
            string lang = "Ind";
            Person p = new Person();
            DataTable dt = p.allDirectors();
            List<Person> dirut = p.getUtama(dt, lang);
            ListViewDirut.DataSource = dirut;
            ListViewDirut.DataBind();
            List<Person> alldirektur = p.getAll(dt, lang);
            ListViewDireksi.DataSource = alldirektur;
            ListViewDireksi.DataBind();
            ListViewModalDireksi.DataSource = dt;
            ListViewModalDireksi.DataBind();
        }
        public void BindPage()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 14 ");
            headerImage.Src = dt.Rows[0]["Header"].ToString();
            content.InnerHtml = dt.Rows[0]["content"].ToString();
        }
        public void loadfooter()
        {
            ModelData Db = new ModelData();
            string query = "Select * from  Contact  where ID = 1";
            DataTable dt = Db.getData(query);
            address.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["address_en"].ToString() : dt.Rows[0]["address"].ToString(); ;
            phone.InnerHtml = dt.Rows[0]["phone"].ToString();
            email.InnerHtml = dt.Rows[0]["email"].ToString();
            link_fb.HRef = dt.Rows[0]["link_fb"].ToString();
            link_tw.HRef = dt.Rows[0]["link_tw"].ToString();
            link_ig.HRef = dt.Rows[0]["link_ig"].ToString();
            link_yt.HRef = dt.Rows[0]["link_yt"].ToString();

            perusahaan.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "LINKS" : perusahaan.InnerHtml;
            pt2.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Ministry of State Owned Enterprises" : pt2.InnerHtml;
            pt3.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Ministry of Energy and Mineral Resources" : pt3.InnerHtml;
            pt4.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Ministry of Transportation " : pt4.InnerHtml;
            pt5.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "BKPM" : pt5.InnerHtml;
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(all.search + "" + txtSearch.Value);
        }
    }
}