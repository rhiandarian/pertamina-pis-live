﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="AlbumImages.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.AlbumImages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid" style="background-color:ghostwhite">
        <div class="row pt-5">
             <div class="col text-center">
                 <h1>Album</h1>
             </div>
         </div><hr />
        <div>
             
        </div>
        <div class="row pt-5">
            <div class="col">
                <div class="album py-5 bg-light">
                    <div class="container">
                        <asp:LinkButton ID="LinkButtonUploadAlbum" CssClass="btn btn-info" runat="server" OnClick="LinkButtonUploadAlbum_Click"><i class="fa fa-plus"></i> Add New</asp:LinkButton>
                       <div class="row pt-3">
                           <asp:ListView ID="ListViewAlbum" runat="server">
                                <ItemTemplate>
                                 <div class="col-md-4">
                                     <div class="card mb-4 box-shadow">
                                        <img class="card-img-top"  alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src='<%# Eval("img_file") %>' data-holder-rendered="true">
                                        <div class="card-body">
                                          <p class="card-text"><%# Eval("title") %></p>
                                          <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                              <asp:LinkButton ID="LinkButtonDelete" CssClass="btn btn-danger" OnClick="Delete" CommandArgument='<%# Eval("ID") %>' runat="server"><i class="fa fa-trash"></i></asp:LinkButton>
                                            </div>
                                            <small class="text-muted"><i>On <%# Eval("image_date") %></i></small>
                                          </div>
                                        </div>
                                  </div>
                                
                          
                        </div>
                                    </ItemTemplate>
                            </asp:ListView>
                           
                        
                        </div>
                        <asp:LinkButton ID="LinkButtonBack" CssClass="btn btn-info" OnClick="ReturnHome"   runat="server"><i class="fa fa-home"></i></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
