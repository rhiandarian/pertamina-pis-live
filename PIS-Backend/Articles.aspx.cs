﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class Articles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    dropdownMenuButton.Visible = false;
                }
                BindGVArticle();
               

            }
        }
        public void BindGVArticle(string txtSearch = null)
        {
            Article atc = new Article();

            
            DataTable dt = atc.LoadData(txtSearch);

            GVArticles.DataSource = dt;
            GVArticles.DataBind();
            
        }

        protected void GVArticles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (Session["UserID"] == null)
            {
                if (Session["Level"].ToString() != "1")
                {
                    e.Row.Cells[4].Visible = false;
                    e.Row.Cells[6].Visible = false;
                }
            }


        }

        protected void EditClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Response.Redirect("EditArticle.aspx?ID=" + id);
        }
        protected void ViewClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Response.Redirect("ViewArticle.aspx?ID=" + id);
        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            string WhereId = "ID = " + id;
            Article Db = new Article();
            Db.Delete(WhereId);
            BindGVArticle();
        }

        protected void GVArticles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGVArticle();
            GVArticles.PageIndex = e.NewPageIndex;
            GVArticles.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGVArticle(TxtSearch.Text);
        }
    }
}