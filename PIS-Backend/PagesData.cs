﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PIS_Backend
{
    public class PagesData:ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName = "Pages";



        public string validationHeaderImage = "Header harus foto";

        public string validationPdfMessage = "File harus pdf";

        public string successMessage = "Halaman berhasil diubah";

        DateTime dateNow = DateTime.Now;

        public int PageSizes = 3;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        public string header;

        public string content;

        public string content_en;

        public string others;

        public string others2;

        public string Header { get { return header; } set { header = value; } }

        public string Content { get { return content; } set { content = value; } }

        public string Content_En { get { return content_en; } set { content_en = value; } }

        public string Others { get { return others; } set { others = value; } }

        public string Others2 { get { return others2; } set { others2 = value; } }

        public DataTable Select(string field, string cond = null)
        {
            cond = cond == null ? "" : cond;
            string Query = Db.SelectQuery(field,tableName,cond);
            
            DataTable dt = Db.getData(Query);
            return dt;
        }
        public DataSet GetData(string field, string cond = null)
        {
            cond = cond == null ? "" : cond;
            string Query = Db.SelectQuery(field, tableName, cond);

            DataSet dt = Db.getDataSet(Query);
            return dt;
        }
        public string AddLogHistories(string mainIP,string browserDetail,int pageID,string newRecord)
        {
            string action = getUserName(user_id) + " Save Page '+(Select name from " + tableName + " Where ID = " + pageID + ")+' with " + newRecord.Replace("'", "");
            return logHistoryQuery(mainIP, browserDetail, action, Convert.ToInt32(user_id), "Success");
        }
        public void Save(string mainIP,string browserDetail,int pageID,string cond = null)
        {
            string newRecord = "set ";
            newRecord += header != null ? "Header = '" + header + "', " : "";
            newRecord += content != null ? "content = '" + content.Replace("'","") + "', " : "";
            newRecord += content_en != null ? "content_en = '" + content_en.Replace("'", "") + "', " : "";
            newRecord += others != null ? "others = '" + others + "', " : "";
            newRecord += others2 != null ? "others2 = '" + others2 + "', " : "";
            newRecord += "edit_date = '" + dateNow + "', edit_user = " + user_id;

            cond = cond != null ? " Where " + cond : "";

            string Query = "Update " + tableName + " " + newRecord + " " + cond+" "+AddLogHistories(mainIP,browserDetail,pageID,newRecord);
            Db.setData(Query);
        }
        public string tesSave(string mainIP, string browserDetail, int pageID, string cond = null)
        {
            string newRecord = "set ";
            newRecord += header != null ? "Header = '" + header + "', " : "";
            newRecord += content != null ? "content = '" + content.Replace("'", "") + "', " : "";
            newRecord += content_en != null ? "content_en = '" + content_en.Replace("'", "") + "', " : "";
            newRecord += others != null ? "others = '" + others + "', " : "";
            newRecord += others2 != null ? "others2 = '" + others2 + "', " : "";
            newRecord += "edit_date = '" + dateNow + "', edit_user = " + user_id;

            cond = cond != null ? " Where " + cond : "";

            string Query = "Update " + tableName + " " + newRecord + " " + cond + " " + AddLogHistories(mainIP, browserDetail, pageID, newRecord);
            return Query;
        }


    }
}