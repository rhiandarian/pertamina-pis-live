﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="admin_career.aspx.cs" Inherits="PIS_Backend.admin_career" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Karir</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Menu Lainnya
                        </li> 
                        <li class="breadcrumb-item ">
                            <a href="admin_career.aspx"><strong>Karir</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Halaman Karir</small></h5>
                              <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                <div id="successdiv" class="alert alert-success" runat="server"></div>
                <div class="row">
                </div>
                <div class="form-group">
                    <label for="FileUpload1"><b>Header Karir</b></label>
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </div>
                <div class="form-group">
                    <label for="content"><b>Konten atas (bahasa Indonesia)</b></label>
                   <textarea class="summernote" id="content" runat="server"  rows="5"></textarea>
                </div>
                <div class="form-group">
                    <label for="content_en"><b>Konten (bahasa Inggris)</b></label>
                   <textarea class="summernote" id="content_en" runat="server"  rows="5"></textarea>
                </div>
                <div class="form-group">
                    <label for="disclaimer"><b>Disclaimer (bahasa Indonesia) </b></label>
                   <textarea class="summernote" id="disclaimer" runat="server"  rows="5"></textarea>
                </div>
                <div class="form-group">
                    <label for="disclaimer_en"><b>Disclaimer (bahasa Inggris)</b></label>
                   <textarea class="summernote" id="disclaimer_en" runat="server"  rows="5"></textarea>
                </div>
                <div class="form-group">
                    <label for="magang"><b>Magang (bahasa Indonesia) </b></label>
                   <textarea class="summernote" id="magang" runat="server"  rows="5"></textarea>
                </div>
                <div class="form-group">
                    <label for="magang_en"><b>Magang (bahasa Inggris)</b></label>
                   <textarea class="summernote" id="magang_en" runat="server"  rows="5"></textarea>
                </div>
                <div class="col-lg-6">
                        <div class="form-group">
                            <label for="fb"><b>Mail Lamaran</b></label>
                            <asp:TextBox ID="maillamaran" cssClass="form-control" placeholder="Masukan Mail lamaran" runat="server"></asp:TextBox>
                         </div>
                    </div> 
                <div class="col-lg-6">
                        <div class="form-group">
                            <label for="fb"><b>Mail Magang</b></label>
                            <asp:TextBox ID="mailmagang" cssClass="form-control" placeholder="Masukan Mail Magang" runat="server"></asp:TextBox>
                         </div>
                    </div> 
                 <div class="form-group">
                    <label for="image_karier"><b>Foto Karir</b></label>
                    <asp:FileUpload ID="image_karier" runat="server" />
                </div>
               <%-- <div class="form-group">
                    <label for="FileUpload2"><b>Unduh PDF</b></label>
                    <asp:FileUpload ID="FileUpload2" runat="server" />
                </div>--%>
                <div class="form-group">
                    <asp:Button ID="Save" CssClass="btn btn-primary" runat="server" Text="Save" OnClick="Save_Click"       /> <a href="career.aspx" target="_blank"><i class="fa fa-eye"></i> Lihat hasil website </a>
                </div>
                
            </div>
            
        </div>
    </div>
</asp:Content>
