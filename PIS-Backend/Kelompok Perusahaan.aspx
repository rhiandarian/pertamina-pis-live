﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="Kelompok Perusahaan.aspx.cs" Inherits="PIS_Backend.Kelompok_Perusahaan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx">Home</a></li>
					    <li class="breadcrumb-item"><a href="">Tentang PIS</a></li>
					    <li class="breadcrumb-item active" aria-current="page">Kelompok Perusahaan</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3>Kelompok Perusahaan</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img src="images/Header/kapal_header_07.png" class="img-fluid" alt="...">
			</div>
  	</section>
    <section class="kelompok-perusahaan">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div class="col-lg-12 mb-4 title">
	  				<h5><strong>Kelompok Perusahaan</strong></h5>
	  			</div>

	  			<div class="col-lg-12 mb-4 desc">

	  				<img src="images/kelompok-perusahaan.png" class="img-fluid" alt="...">

            <center class="mt-4"><a href="documents/kelompok-perusahaan.pdf" class="btn btn-default-pis"><span class="bi bi-file-pdf-fill"></span> Lihat versi pdf</a></center>

            <h5 class="mt-5 mb-4 pt-3"><strong>Alamat Kelompok Perusahaan</strong></h5>
            <table class="table table-borderless table-striped mt-4">
              <tbody><tr>
                <td class="text-center" width="20%"><img src="images/Logo-Kelompok-Perusahaan/Logo-Pertamina.png" class="img-fluid" alt=""></td>
                <td>PT Pertamina Persero</td>
                <td><a href="">Kunjungi</a></td>
              </tr>
              <tr>
                <td class="text-center"><img src="images/Logo-Kelompok-Perusahaan/Logo-Pertamina-Trans.png" class="img-fluid" alt=""></td>
                <td>PT Pertamina Trans Kontinental</td>
                <td><a href="">Kunjungi</a></td>
              </tr>
              <tr>
                <td class="text-center"><img src="images/Logo-Kelompok-Perusahaan/Logo-Peteka-Karya-Samudra.png" class="img-fluid" alt=""></td>
                <td>PT Peteka Karya Samudra</td>
                <td><a href="">Kunjungi</a></td>
              </tr>
              <tr>
                <td class="text-center"><img src="images/Logo-Kelompok-Perusahaan/Logo-Peteka-Karya-Tirta.png" class="img-fluid" alt=""></td>
                <td>PT Peteka Karya Tirta</td>
                <td><a href="">Kunjungi</a></td>
              </tr>
              <tr>
                <td class="text-center"><img src="images/Logo-Kelompok-Perusahaan/Logo-Trans-Yeong-Maritime.png" class="img-fluid" alt=""></td>
                <td>PT Trans Yeong Maritime</td>
                <td><a href="">Kunjungi</a></td>
              </tr>
              <tr>
                <td class="text-center"><img src="images/Logo-Kelompok-Perusahaan/Logo-Peteka-Karya-Jala.png" class="img-fluid" alt=""></td>
                <td>PT Peteka Karya Jala</td>
                <td><a href="">Kunjungi</a></td>
              </tr>
              <tr>
                <td class="text-center"><img src="images/Logo-Kelompok-Perusahaan/Logo-Peteka-Karya-Gapura.png" class="img-fluid" alt=""></td>
                <td>PT Peteka Karya Gapura</td>
                <td><a href="">Kunjungi</a></td>
              </tr>
            </tbody></table>

	  			</div>
	  		</div>

	  	</div>
  	</section>
</asp:Content>
