﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class SavePresentasi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "8"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadData();
                
            }
        }
        public void LoadData()
        {
            if (Request.QueryString["ID"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                string cond = " ID = " + Id;
                Presentasi p = new Presentasi();
                DataTable dt = p.Select("*", cond);
                title.Text = dt.Rows[0]["title"].ToString();
                title_en.Text = dt.Rows[0]["title_en"].ToString() != null ? dt.Rows[0]["title_en"].ToString() : "";
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Presentasi p = new Presentasi();
                p.Title = title.Text;
                p.TitleEn = title_en.Text;
                p.Url = FileUpload1.FileName;
                p.Cover = FileUpload2.FileName;

                if (Request.QueryString["ID"] == null)
                {
                    string insertStatus = p.Insert(getip(),GetBrowserDetails());
                    if(insertStatus == "success")
                    {
                        UploadPdf(p.folder);
                        UploadCover(p.folder);
                        Response.Redirect(p.listPage);
                    }
                    else
                    {
                        showValidationMessage(insertStatus);
                    }

                }
                else
                {
                    int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                    string cond = " ID = " + Id;
                    string updateStatus = p.Update(Id,getip(), GetBrowserDetails(),cond);
                    if(updateStatus == "success")
                    {
                        if(FileUpload1.FileName != "")
                        {
                            UploadPdf(p.folder);
                        }
                        if(FileUpload2.FileName != "")
                        {
                            UploadCover(p.folder);
                        }
                        Response.Redirect(p.listPage);
                    }
                    else
                    {
                        showValidationMessage(updateStatus);
                    }
                }
            }
            catch(Exception ex)
            {
                Presentasi p = new Presentasi();
                General all = new General();
                string action = Request.QueryString["ID"] == null ? "Insert" : "Update";
                p.ErrorLogHistory(getip(), GetBrowserDetails(), action, p.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                showValidationMessage(ex.Message.ToString());
            }
        }

        public void UploadPdf(string folder)
        {
            
            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string SaveLocation = Server.MapPath(folder) + "\\" + fn;

            FileUpload1.PostedFile.SaveAs(SaveLocation);
        }
        public void UploadCover(string folder)
        {
            string fn = System.IO.Path.GetFileName(FileUpload2.PostedFile.FileName);
            string SaveLocation = Server.MapPath(folder) + "\\" + fn;

            FileUpload2.PostedFile.SaveAs(SaveLocation);

        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
    }
}