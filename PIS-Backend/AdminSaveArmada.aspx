﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminSaveArmada.aspx.cs" Inherits="PIS_Backend.AdminSaveArmada" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Kapal Milik & Armada</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="">Home</a>
                        </li>
                        </li>
                        <li class="breadcrumb-item">
                           <a href="AdminKapalMilikArmada.aspx">Kapal Milik & Armada</a> 
                        </li>
                        <li class="breadcrumb-item">
                           <a href="AdminSaveArmada.aspx"><strong>Kapal Milik</strong></a> 
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Armada Save Data.</small></h5>
                              <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                <div class="form-group">
                    <label for="title"><b>title</b></label>
                    <asp:TextBox ID="title" cssClass="form-control" placeholder="Input name" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="content"><b>content</b></label>
                    <asp:TextBox ID="content" cssClass="form-control" placeholder="Input name" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="content"><b>image</b></label>
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </div>
                <div class="form-group">
                    <asp:Button ID="Save" CssClass="btn btn-primary" runat="server" Text="Save" OnClick="Save_Click"  />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
