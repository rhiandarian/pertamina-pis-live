﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class AddCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadCategoryType();

            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            bool valid = true;
            string validationMessage = "";
            if(category_name.Text == "")
            {
                valid = false;
                validationMessage = "Category name must be Filled !";
            }
            if(DdlCategoryType.SelectedValue == "Select Category Type")
            {
                valid = false;
                validationMessage = "Category type must be Selected !";
            }
            if(valid)
            {
                Category ctg = new Category();
                ctg.Category_name = category_name.Text;
                ctg.Category_Type_Id = Convert.ToInt32(DdlCategoryType.SelectedValue);
                ctg.Insert();
                LblErrorMessage.Visible = false;
                Response.Redirect("ListCategory.aspx");
            }
            else
            {
                LblErrorMessage.Visible = true;
                LblErrorMessage.Text = validationMessage;
            }
        }
        public void LoadCategoryType()
        {
            Category c = new Category();
            DataSet dt = c.LoadCategoryType();

            DdlCategoryType.DataTextField = dt.Tables[0].Columns["Category_type_name"].ToString();
            DdlCategoryType.DataValueField = dt.Tables[0].Columns["ID"].ToString();
            DdlCategoryType.DataSource = dt.Tables[0];
            DdlCategoryType.DataBind();
            DdlCategoryType.Items.Insert(0, "Select Category Type");
            DdlCategoryType.SelectedIndex = 0;
        }
    }
}