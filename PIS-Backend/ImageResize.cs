﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using System.IO;

namespace PIS_Backend
{
    public class ImageResize : System.Web.UI.Page
    {
        public string textrandom()
        {
            Random RNG = new Random();
            int length = 6;
            var rString = "";
            for (var i = 0; i < length; i++)
            {
                rString += ((char)(RNG.Next(1, 26) + 64)).ToString().ToLower();
            }
            return rString;
        }

        public void ResizeImage(string originalImagePath, string SaveAfterResize, string for_, string mode)
        {
            int width = 0;
            int height = 0;
            bool deleted = true;
            switch (for_)
            {
                case "Layanan Kami":
                    width = 600;
                    height = 232;
                    break;
                case "Gallery":
                    width = 1230;
                    height = 691;
                    break;
                case "Direktur Utama":
                    width = 1612;
                    height = 2418;
                    break;
                case "Berita":
                    width = 444;
                    height = 250;
                    deleted = false;
                    break;
                case "Komisaris":
                    width = 860;
                    height = 1280;
                    deleted = false;
                    break;
                case "Armada-Kami":
                    width = 444;
                    height = 250;
                    break;
                case "laporan-tahunan":
                    width = 396;
                    height = 561;
                    break;
                case "Default-Thumbnail":
                    width = 444;
                    height = 250;
                    break;
                case "Banner":
                    width = 1920;
                    height = 1080;
                    break;
                case "Direksi":
                    width = 257;
                    height = 385;
                    break;
                case "Pengadaan":
                    width = 2480;
                    height = 3508;
                    break;
                default:
                    break;
            }

            System.Drawing.Image originalImage = System.Drawing.Image.FromFile(originalImagePath);

            int towidth = width;
            int toheight = height;

            int x = 0;
            int y = 0;
            int ow = originalImage.Width;
            int oh = originalImage.Height;

            switch (mode)
            {
                case "HW":
                    break;
                case "W":
                    toheight = originalImage.Height * width / originalImage.Width;
                    break;
                case "H":
                    towidth = originalImage.Width * height / originalImage.Height;
                    break;
                case "Cut":
                    if ((double)originalImage.Width / (double)originalImage.Height > (double)towidth / (double)toheight)
                    {
                        oh = originalImage.Height;
                        ow = originalImage.Height * towidth / toheight;
                        y = 0;
                        x = (originalImage.Width - ow) / 2;
                    }
                    else
                    {
                        ow = originalImage.Width;
                        oh = originalImage.Width * height / towidth;
                        x = 0;
                        y = (originalImage.Height - oh) / 2;
                    }
                    break;
                default:
                    break;
            }


            System.Drawing.Image bitmap = new System.Drawing.Bitmap(towidth, toheight);


            Graphics g = System.Drawing.Graphics.FromImage(bitmap);


            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;


            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;


            g.Clear(Color.Transparent);


            g.DrawImage(originalImage, new Rectangle(0, 0, towidth, toheight),
             new Rectangle(x, y, ow, oh),
             GraphicsUnit.Pixel);

            try
            { 
                bitmap.Save(SaveAfterResize);
            }
            catch (System.Exception e)
            {
                throw e;
            }
            finally
            {
                originalImage.Dispose();
                bitmap.Dispose();
                g.Dispose();
                if (deleted == true)
                {
                    File.Delete(originalImagePath);
                }
            }
        }

    }
}