﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="admin_listpenghargaan.aspx.cs" Inherits="PIS_Backend.admin_listpenghargaan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Penghargaan</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="">Menu Lainnya</a>
                        </li> 
                        <li class="breadcrumb-item">
                            <a href="">Komitmen dan Keberlanjutan</a>
                        </li>
                        <li class="breadcrumb-item">
                           <a href="admin_listpenghargaan.aspx"><strong> Penghargaan </strong></a> 
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <a href="SavePenghargaan.aspx" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                                        <div class="row mt-2 mb-2">
                                            <asp:ListView ID="ListViewPenghargaan" runat="server" ItemPlaceholderID="itemPlaceHolder1" OnPagePropertiesChanging="ListViewPenghargaan_PagePropertiesChanging" GroupPlaceholderID="groupPlaceHolder1">
                                                <GroupTemplate>
                                                    <tr>
                                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                                                    </tr>
                                                </GroupTemplate>
                                                <ItemTemplate>
                                                    <div class="col-xs-12  col-sm-4 box-content">
                                                        <div class="col pt-3 pl-0 pr-0">
                                                          <a class="image-popup-vertical-fit" href="images/default-photo.png">
                                                            <img src='<%#  Eval("cover") %>' class="img-fluid mt-2 mb-2" alt="...">
                                                          </a>
                                                          <p class="text-center"><strong><%#  Eval("title") %></strong><br><%#  Eval("description") %> </p>
                                                            <p class="text-center">
                                                                <asp:LinkButton ID="LinkButton2" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="EditClick" runat="server">Edit <i class="fa fa-pencil"></i></asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton4" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="DeleteClick" runat="server">Delete <i class="fa fa-trash"></i></asp:LinkButton>
                                                             </p>
                                                        </div>
                                                      </div> 
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
                                                    <asp:DataPager ID="DataPager1" runat="server" PagedControlID="ListViewpenghargaan" PageSize="6">
                                                        <Fields>
                                                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
                                                                ShowNextPageButton="false" />
                                                            <asp:NumericPagerField ButtonType="Link" />
                                                            <asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton = "false" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </LayoutTemplate>
                                            </asp:ListView>
                                            <%--<asp:ListView ID="ListViewArmada" runat="server" ItemPlaceholderID="itemPlaceHolder1" OnPagePropertiesChanging="OnPagePropertiesChanging" GroupPlaceholderID="groupPlaceHolder1">
                                                
                                                
                                                
                                            </asp:ListView>--%>
                                                
                                         </div>
        <%--<a href="Penghargaan.aspx"><i class="fa fa-eye"> See Result on Website</i></a>--%>
                                     </div>
</asp:Content>
