﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class Index : System.Web.UI.Page
    {
        General all = new General();
        string EnPage = "En";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Lang"] != null && Session["Lang"].ToString() == "En")
            {
                Response.Redirect(EnPage);
            }

            if (!IsPostBack)
            {
                bindMenu();
                Kapal k = new Kapal();
                PagesData pd = k.getPages();
                totalKapal.InnerText = pd.others;
                kapalmilik.InnerText = pd.others2;

                

                loadfooter();





            }
        }
        public void bindMenu()
        {
            string currPage = "Index.aspx";
            string lang = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "En" : "Ind";

            List<Menu> mn = all.GenerateMenu(lang, currPage);
            ListViewMenu.DataSource = mn;
            ListViewMenu.DataBind();

            List<Menu> ot = all.MenuOthers(lang);
            ListViewOthers.DataSource = ot;
            ListViewOthers.DataBind();

            ListViewResponsive.DataSource = ot;
            ListViewResponsive.DataBind();

            string otmn = lang == "En" ? "Others" : "Menu Lainnya";
            othermenu.Attributes["class"] = all.getOtherMenuClass(othermenu.Attributes["class"], currPage);
            othermenu.InnerHtml = "<a href=" + all.stripped("") + " onclick=" + all.stripped("openNavOverlay()") + " id=" + all.stripped("nav-right-option") + " >" + otmn + " <span class=" + all.stripped("") + "></span></a>";
            langMobile.InnerHtml = all.changeLangMobile("Ind");

            Carier pd = new Carier();
            DataTable dt = pd.Select("*", " ID = 1 ");
            AttributeCollection myAttributes = magang_section.Attributes;
            myAttributes.CssStyle.Add("background-image", dt.Rows[0]["others"].ToString());
        }
        protected void ChangeLang(object sender, EventArgs e)
        {
            if (Session["Lang"] == null)
            {
                Session.Add("Lang", "En");
            }
            else
            {
                Session["Lang"] = "En";
            }
            Response.Redirect(all.IndexEnPage);
        }
       
       

        public void DownloadPengadaan(object sender, EventArgs e)
        {
            InfoPengadaan p = new InfoPengadaan();
            LinkButton lb = (LinkButton)sender;
            string filename = lb.CommandArgument;
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.TransmitFile(Server.MapPath("~/" + p.folder + "/" + filename));
            Response.End();
        }
        


        public void loadfooter()
        {
            ModelData Db = new ModelData();
            string query = "Select * from  Contact  where ID = 1";
            DataTable dt = Db.getData(query);
            address.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["address_en"].ToString() : dt.Rows[0]["address"].ToString(); ;
            phone.InnerHtml = dt.Rows[0]["phone"].ToString();
            email.InnerHtml = dt.Rows[0]["email"].ToString();
            link_fb.HRef = dt.Rows[0]["link_fb"].ToString();
            link_tw.HRef = dt.Rows[0]["link_tw"].ToString();
            link_ig.HRef = dt.Rows[0]["link_ig"].ToString();
            link_yt.HRef = dt.Rows[0]["link_yt"].ToString();


        }



        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(all.search + "" + txtSearch.Value);
        }
    }
}