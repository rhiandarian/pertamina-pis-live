﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="admin-add-gallery.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.admin_add_gallery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Galeri Data</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a>Galeri</a>
                        </li>
                        <li class="breadcrumb-item">
                            <asp:LinkButton ID="LinkButton1" OnClick="LinkButton1_Click" runat="server">Daftar Galeri</asp:LinkButton>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="admin-add-gallery.aspx"><strong>Buat Baru</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Buat Baru</small></h5>
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                 <form>
                      <div class="form-group">
                          <div class="row">
                              <div class="col">
                                  <label for="title"><b>judul</b></label>
                                 <asp:TextBox ID="title" cssClass="form-control" placeholder="Masukkan judul foto/video/infografis dalam bahasa Indonesia" runat="server"></asp:TextBox>
                              </div>
                              <div class="col">
                                  <label for="title_en"><b>judul bahasa inggris</b></label>
                                 <asp:TextBox ID="title_en" cssClass="form-control" placeholder="Masukkan judul foto/video/infografis dalam bahasa Inggris" runat="server"></asp:TextBox>
                              </div>
                          </div>
                        
                      </div>
                     <div class="form-group">
                        <label for="ddlCategory"><b>kategori</b></label>
                         <asp:DropDownList ID="DdlCategory" runat="server" CssClass="form-control"  AutoPostBack="true" OnSelectedIndexChanged="DdlCategory_SelectedIndexChanged"></asp:DropDownList>
                     </div>
                     <div class="form-group">
                        <label for="date"><b>tanggal</b></label>
                        <asp:TextBox ID="date" cssClass="form-control" placeholder="Masukkan tanggal" runat="server"></asp:TextBox>
                      </div>
                     <asp:Panel ID="PanelVideo" runat="server">
                         <div class="form-group">
                        <label for="content"><b>konten</b></label>
                        <textarea id="content" class="summernote"   runat="server" rows="10"></textarea>
                      </div>
                         <div class="form-group">
                        <label for="content_en"><b>konten bahasa inggris</b></label>
                        <textarea id="content_en" class="summernote"   runat="server" rows="10"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="videoUrl"><b>video Url</b></label>
                          <asp:TextBox ID="videoUrl" CssClass="form-control" placeholder="Masukkan tautan video Youtube" runat="server"></asp:TextBox>
                     </div>
                     <div class="form-group">
                        <label for="FileUpload2"><b>URL Thumbnail</b></label>
                         <asp:FileUpload ID="FileUpload2" runat="server" />
                     </div>
                     
                     </asp:Panel>
                     <asp:Panel ID="PanelImage" runat="server">
                         <div class="form-group">
                            <label for="FileUpload1"><b>Foto</b></label>
                            <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                         </div>
                         <div class="form-group">
                             <asp:Image ID="Image1" runat="server" />
                         </div>
                     </asp:Panel>
                     
                    <div class="form-group">
                        <asp:Button ID="BtnSave" runat="server" CssClass="btn btn-primary" Text="Simpan"   OnClick="BtnSave_Click"   />
                    </div>
                      
                </form>
            </div>
        </div>
    </div>
</asp:Content>
