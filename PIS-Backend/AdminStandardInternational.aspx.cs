﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace PIS_Backend
{
    public partial class AdminStandardInternational : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "20"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindSO();
            }
        }
        public void BindSO()
        {
            StandardISO si = new StandardISO();
            DataTable dt = si.Select("*");
            ListViewSO.DataSource = dt;
            ListViewSO.DataBind();
        }
        protected void Edit_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            StandardISO si = new StandardISO();
            Response.Redirect(si.savePage + "?ID=" + id);
        }
        protected void Delete_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            StandardISO si = new StandardISO();
            string cond = " ID = " + id;
            try
            {
                si.Delete(Convert.ToInt32(id),getip(),GetBrowserDetails(),cond);
            }
            catch(Exception ex)
            {
                General all = new General();
                si.ErrorLogHistory(getip(), GetBrowserDetails(), "Delete", si.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                Response.Write("Error " + ex.ToString());
            }
            
            BindSO();
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
    }
}