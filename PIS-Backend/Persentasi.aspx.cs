﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class Persentasi : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadPersentasi(1);
                bindPage();
            }
            LblIndex.Visible = false;
        }
        public void bindPage()
        {
            editClassLang();
            bool langEn = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? true : false;
            link1.InnerText = langEn ? "Media & Information" : link1.InnerText;
            link2.InnerText = langEn ? "Persentation" : link2.InnerText;

            htitle.InnerText = link2.InnerText.ToUpper();
            //contenttitle.InnerText = link2.InnerText;
            Presentasi p = new Presentasi();
            PagesData pd = p.getPage();
            imgHeader.Src = pd.header;
            content.InnerHtml = langEn ? pd.content_en : pd.content;
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
        public void LoadPersentasi(int Index)
        {
            bool langEn = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? true : false;
            Presentasi p = new Presentasi();
            LblIndex.Text = Index.ToString();
            DataTable dt = p.LoadData(Index,langEn);
            ListViewPersentasi.DataSource = dt;
            ListViewPersentasi.DataBind();
            int totalRecord = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());
            populatePaging(totalRecord, Index);
            
        }
        public void populatePaging(int totalCount, int pageIndex)
        {
            bool langEn = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? true : false;
            string lang = langEn ? "En" : "Ind";

            Presentasi p = new Presentasi();
            List<Paging> lp = all.PopulatePaging(totalCount, pageIndex, p.PageSize);
            RptPaging.DataSource = lp;
            RptPaging.DataBind();
            int total = all.getTotalPaging(totalCount, pageIndex, p.PageSize);
            detailPaging.InnerHtml = all.getDetailPaging(lang, pageIndex, lp.Count);
        }
        protected void Page_Changed(object sender, EventArgs e)
        {
            string commendArgument = (sender as LinkButton).CommandArgument.ToString();
            int pagesIndex = int.Parse(LblIndex.Text);
            if (commendArgument == "first")
            {
                if (pagesIndex > 1)
                {
                    pagesIndex--;
                    LoadPersentasi(pagesIndex);
                }

            }
            else if (commendArgument == "last")
            {
                bool langEn = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? true : false;
                Presentasi p = new Presentasi();
                DataTable dt = p.LoadData(1, langEn);


                int totalCount = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());

                int limit = all.getTotalPaging(totalCount, 1, p.PageSize);
                if (pagesIndex < limit)
                {
                    pagesIndex++;
                    LoadPersentasi(pagesIndex);
                }




            }
            else
            {

                int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
                LoadPersentasi(pageIndex);
            }

        }
        public void Download(object sender, EventArgs e)
        {
            Presentasi p = new Presentasi();
            LinkButton lb = (LinkButton)sender;
            string filename = lb.CommandArgument;
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.TransmitFile(Server.MapPath("~/"+p.folder+"/" + filename));
            Response.End();
        }
        protected void lnkPage_Click(object sender, EventArgs e)
        {

        }
    }
}