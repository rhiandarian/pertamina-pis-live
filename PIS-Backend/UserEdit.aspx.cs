﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class UserEdit : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "35"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadData();
            }
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void LoadData()
        {
            ModelData Db = new ModelData();

            if (Request.QueryString["ID"] != null)
            {
               
            int Id = Convert.ToInt32(Request.QueryString["ID"]);
            string field = "select * from View_UsersFull where Id = " + Id; 

            DataTable dt = Db.getData(field);
            Username.Text = dt.Rows[0]["User_name"].ToString(); 
            Email.Text = dt.Rows[0]["Email"].ToString(); 
            Password.Value = dt.Rows[0]["password"].ToString();

            string query = Db.SelectQuery("Id,name", "ViewLevelUser", "");
            DataTable DL = Db.getData(query);

            level.DataSource = DL;
            level.DataBind();
            level.DataTextField = "name";
            level.DataValueField = "Id";
            level.DataBind();
            level.SelectedIndex = Convert.ToInt32(dt.Rows[0]["Level_Id"].ToString()) - 1;
                //Response.Redirect("LevelList.aspx");
            }
            else
            {
                string query = Db.SelectQuery("Id,name", "ViewLevelUser", "");
                DataTable DL = Db.getData(query);

                level.DataSource = DL;
                level.DataBind();
                level.DataTextField = "name";
                level.DataValueField = "Id";
                level.DataBind();
            }

        }
        public string QueryLogHistories(string action)
        {
            Users u = new Users();

            return u.logHistoryQuery(getip(), GetBrowserDetails(), action, Convert.ToInt32(Session["UserID"].ToString()), "Success");
        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
            ModelData Db = new ModelData();


                if (Request.QueryString["ID"] != null)
                {
                    if (Username.Text != "")
                    {
                        int Id = Convert.ToInt32(Request.QueryString["ID"]);
                        string newRecord = "User_name = '" + Username.Text + "', ";
                        newRecord += "Email = '" + Email.Text + "', ";
                        if (Password.Value != "")
                        {
                            newRecord += "Password = '" + EncryptPassword(Password.Value) + "', ";
                        }
                        newRecord += "Level_id = '" + level.Value + "' ";

                        Db.setData("update Users set " + newRecord + "  where Id =" + Id+" "+QueryLogHistories(Session["Username"].ToString()+" Update Users Id "+Id+" with "+newRecord.Replace("'","")));
                        Response.Redirect("UserList.aspx");
                    }
                    else
                    {
                        showValidationMessage("Nama Level Tidak Boleh Kosong");
                        LoadData();
                    }
                }
                else
                {


                    string field = "(User_name,Password,Email,Level_id)";
                    string value = $"( '{Username.Text}','{EncryptPassword(Password.Value)}','{Email.Text}','{level.Value}')";
                    string Query = "INSERT INTO Users " + field + " VALUES " + value;
                    Db.setData(Query);
                    Response.Redirect("UserList.aspx");
                }
            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
            
        }

        public string EncryptPassword(string password)
        {
            byte[] encData_byte = new byte[password.Length];
            encData_byte = System.Text.Encoding.UTF8.GetBytes(password);
            string encodedData = Convert.ToBase64String(encData_byte);
            return encodedData;
        }
    }
}