﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace PIS_Backend
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            RegisterRoutes(RouteTable.Routes);
        }
        static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute("", "", "~/Index.aspx");
            routes.MapPageRoute("loginAdmin", "login", "~/Login.aspx");
            routes.MapPageRoute("En", "En", "~/Index_en.aspx");
            routes.MapPageRoute("visi-misi", "visi-misi", "~/VisidanMisi.aspx");
            routes.MapPageRoute("vision-mission", "vision-mission", "~/VisidanMisi.aspx");
            routes.MapPageRoute("tentang-pis", "tentang-pis", "~/Tentang_PIS.aspx");
            routes.MapPageRoute("tonggak-sejarah", "tonggak-sejarah", "~/TonggakSejarah.aspx");
            routes.MapPageRoute("milestone", "milestone", "~/TonggakSejarah.aspx");
            routes.MapPageRoute("tata-nilai-budaya", "tata-nilai-budaya", "~/Tata_Nilai_dan_Budaya.aspx");
            routes.MapPageRoute("corporate-value", "corporate-value", "~/Tata_Nilai_dan_Budaya.aspx");
            routes.MapPageRoute("company-overview", "company-overview", "~/Tentang_PIS.aspx");
            routes.MapPageRoute("our-logo", "our-logo", "~/makna-logo-pis.aspx");
            routes.MapPageRoute("dewan-komisaris", "dewan-komisaris", "~/Dewan_Komisaris.aspx");
            routes.MapPageRoute("commissioners", "commissioners", "~/Board of Commissioners.aspx");
            routes.MapPageRoute("dewan-direksi", "dewan-direksi", "~/Direksi.aspx");
            routes.MapPageRoute("directors", "directors", "~/Directors.aspx");
            routes.MapPageRoute("message-from-the-ceo", "message-from-the-ceo", "~/sambutan-direktur-utama.aspx");
            routes.MapPageRoute("struktur-organisasi", "struktur-organisasi", "~/Struktur Organisasi.aspx");
            routes.MapPageRoute("organization-structure", "organization-structure", "~/Struktur Organisasi.aspx");
            routes.MapPageRoute("standar-internasional", "standar-internasional", "~/Standard International.aspx");
            routes.MapPageRoute("international-standard", "international-standard", "~/International Standard.aspx");
            routes.MapPageRoute("armada-kami", "armada-kami", "~/Armada-Kami.aspx");
            routes.MapPageRoute("our-fleet", "our-fleet", "~/Our Fleet.aspx");
            routes.MapPageRoute("presentasi", "presentasi", "~/Persentasi.aspx");
            routes.MapPageRoute("presentation", "presentation", "~/Persentasi.aspx");
            routes.MapPageRoute("berita", "berita", "~/AllNews.aspx");
            routes.MapPageRoute("berita-detail", "berita-detail", "~/news-detail.aspx");
            routes.MapPageRoute("galeri", "galeri", "~/Galleri.aspx");
            routes.MapPageRoute("gallery", "gallery", "~/Galleries.aspx");
            routes.MapPageRoute("news", "news", "~/AllNews.aspx");
            routes.MapPageRoute("our-services", "our-services", "~/layanan-kami.aspx");
            routes.MapPageRoute("annual-report", "annual-report", "~/laporan-tahunan.aspx");
            routes.MapPageRoute("financial-statement", "financial-statement", "~/laporan-ikhtisar-keuangan.aspx");
            routes.MapPageRoute("shareholders-composition", "shareholders-composition", "~/komposisi-pemegang-saham.aspx");
            routes.MapPageRoute("agms", "agms", "~/R-U-P-S.aspx");
            routes.MapPageRoute("good-corporate-governance", "good-corporate-governance", "~/tata-kelola-perusahaan.aspx");
            routes.MapPageRoute("awards", "awards", "~/penghargaan.aspx");
            routes.MapPageRoute("dividend-information", "dividend-information", "~/informasi-dividen.aspx");
            routes.MapPageRoute("sustainability", "sustainability", "~/keberlanjutan.aspx");
            routes.MapPageRoute("procurement-information", "procurement-information", "~/informasi-pengadaan.aspx");
            routes.MapPageRoute("karier", "karier", "~/career.aspx");
            routes.MapPageRoute("kontak", "kontak", "~/contacts.aspx");
            routes.MapPageRoute("ListFoto", "ListFoto", "~/admin-list-gallery.aspx");
            routes.MapPageRoute("admin-semua-infografis", "admin-semua-infografis", "~/admin-list-gallery.aspx");
            routes.MapPageRoute("admin-semua-video", "admin-semua-video", "~/admin-list-gallery.aspx");
            routes.MapPageRoute("admin-semua-berita", "admin-semua-berita", "~/ListNews.aspx");
            routes.MapPageRoute("admin-tambah-berita", "admin-tambah-berita", "~/admin-add-news.aspx");
            routes.MapPageRoute("AddFoto", "AddFoto", "~/admin-add-gallery.aspx");
            routes.MapPageRoute("AddInfografis", "AddInfografis", "~/admin-add-gallery.aspx");
            routes.MapPageRoute("admin-tambah-video", "admin-tambah-video", "~/admin-add-gallery.aspx");
            routes.MapPageRoute("EditFoto", "EditFoto", "~/admin-edit-gallery.aspx");
            routes.MapPageRoute("EditInfografis", "EditInfografis", "~/admin-edit-gallery.aspx");
            routes.MapPageRoute("EditVideo", "EditVideo", "~/admin-edit-gallery.aspx");
            routes.MapPageRoute("admin-admin-list-album", "admin-admin-list-album", "~/AdminListAlbum.aspx");
            routes.MapPageRoute("admin-tambah-album", "admin-tambah-album", "~/SaveAlbum.aspx");
            routes.MapPageRoute("semua-berita", "semua-berita", "~/ListNews.aspx");
            routes.MapPageRoute("tambah-berita", "tambah-berita", "~/admin-add-news.aspx");
            routes.MapPageRoute("ubah-berita", "ubah-berita", "~/admin-edit-news.aspx");

            routes.MapPageRoute("admin-slider", "admin-slider", "~/ListBanner.aspx");
            routes.MapPageRoute("admin-ubah-slider", "admin-ubah-slider", "~/SaveBanner.aspx");
            routes.MapPageRoute("admin-video-profil", "admin-video-profil", "~/VideoProfile.aspx");
            routes.MapPageRoute("admin-presentasi", "admin-presentasi", "~/AdminPresentasi.aspx");
            routes.MapPageRoute("admin-tambah-presentasi", "admin-tambah-presentasi", "~/SavePresentasi.aspx");
            routes.MapPageRoute("admin-layanan", "admin-layanan", "~/AdminLayanan.aspx");
            routes.MapPageRoute("admin-tambah-layanan", "admin-tambah-layanan", "~/SaveLayanan.aspx");
            routes.MapPageRoute("admin-ubah-layanan", "admin-ubah-layanan", "~/editpagelayanan.aspx");
            routes.MapPageRoute("admin-kapal-milik-armada", "admin-kapal-milik-armada", "~/AdminKapalMilikArmada.aspx");
            routes.MapPageRoute("admin-ubah-halaman-kapal", "admin-ubah-halaman-kapal", "~/EditPageKapal.aspx");
            routes.MapPageRoute("admin-tambah-kapal", "admin-tambah-kapal", "~/adminSaveKapal.aspx");
            routes.MapPageRoute("admin-visi-misi", "admin-visi-misi", "~/admin_visimisi.aspx");
            routes.MapPageRoute("admin-tata-nilai", "admin-tata-nilai", "~/admin_tatanilai.aspx");
            routes.MapPageRoute("admin-tentang-pis", "admin-tentang-pis", "~/admin_tentangPIS.aspx");
            routes.MapPageRoute("admin-tonggak-sejarah", "admin-tonggak-sejarah", "~/admin_tonggakSejarah.aspx");
            routes.MapPageRoute("admin-makna-logo-pis", "admin-makna-logo-pis", "~/admin_maknalogoPIS.aspx");
            routes.MapPageRoute("admin-komisaris", "admin-komisaris", "~/AdminKomisaris.aspx");
            routes.MapPageRoute("admin-tambah-komisaris", "admin-tambah-komisaris", "~/SaveKomisaris.aspx");
            routes.MapPageRoute("admin-ubah-halaman-komisaris", "admin-ubah-halaman-komisaris", "~/EditPageKomisaris.aspx");
            routes.MapPageRoute("admin-direksi", "admin-direksi", "~/AdminDireksi.aspx");
            routes.MapPageRoute("admin-tambah-direksi", "admin-tambah-direksi", "~/SaveDireksi.aspx");
            routes.MapPageRoute("admin-halaman-direksi", "admin-halaman-direksi", "~/DireksiPage.aspx");
            routes.MapPageRoute("admin-ubah-sambutan-direksi", "admin-ubah-sambutan-direksi", "~/EditSambutanDirut.aspx");
            routes.MapPageRoute("admin-ubah-struktur-organisasi", "admin-ubah-struktur-organisasi", "~/Edit Struktur organisasi.aspx");
            routes.MapPageRoute("admin-halaman-standar-internasional", "admin-halaman-standar-internasional", "~/StandardInternationalPage.aspx");
            routes.MapPageRoute("admin-standar-internasional", "admin-standar-internasional", "~/AdminStandardInternational.aspx");
            routes.MapPageRoute("simpan-standar-internasional", "simpan-standar-internasional", "~/SaveSO.aspx");
            routes.MapPageRoute("admin-laporan-tahunan", "admin-laporan-tahunan", "~/AdminLaporanTahunan.aspx");
            routes.MapPageRoute("admin-tambah-laporan-tahunan", "admin-tambah-laporan-tahunan", "~/AdminSaveLaporanTahunan.aspx");
            routes.MapPageRoute("admin-halaman-laporan-tahunan", "admin-halaman-laporan-tahunan", "~/editPageLaporanTahunan.aspx");
            routes.MapPageRoute("admin-laporan-keuangan", "admin-laporan-keuangan", "~/AdminLaporanKeuangan.aspx");
            routes.MapPageRoute("admin-tambah-laporan-keuangan", "admin-tambah-laporan-keuangan", "~/SaveLaporanKeuangan.aspx");
            routes.MapPageRoute("admin-halaman-laporan-keuangan", "admin-halaman-laporan-keuangan", "~/LaporanKeuanganPages.aspx");
            routes.MapPageRoute("admin-halaman-komposisi-pemegang-saham", "admin-halaman-komposisi-pemegang-saham", "~/KomposisiPemegangSahamPages.aspx");
            routes.MapPageRoute("admin-informasi-dividen", "admin-informasi-dividen", "~/admin_informasidividen.aspx");
            routes.MapPageRoute("admin-halaman-rups", "admin-halaman-rups", "~/admin_pagerRUPS.aspx");
            routes.MapPageRoute("admin-data-rups", "admin-data-rups", "~/admin_dataallRUPS.aspx");
            routes.MapPageRoute("admin-tambah-rups", "admin-tambah-rups", "~/admin_addRUPS.aspx");
            routes.MapPageRoute("admin-ubah-tata-kelola-perusahaan", "admin-ubah-tata-kelola-perusahaan", "~/EditTataKelolaPerusahaan.aspx");
            routes.MapPageRoute("admin-penghargaan", "admin-penghargaan", "~/admin_penghargaan.aspx");
            routes.MapPageRoute("admin-data-penghargaan", "admin-data-penghargaan", "~/admin_listpenghargaan.aspx");
            routes.MapPageRoute("admin-tambah-penghargaan", "admin-tambah-penghargaan", "~/SavePenghargaan.aspx");
            routes.MapPageRoute("admin-keberlanjutan", "admin-keberlanjutan", "~/admin_berkelanjutan.aspx");
            routes.MapPageRoute("admin-detail-keberlanjutan", "admin-detail-keberlanjutan", "~/admin_listkeberlanjutan.aspx");
            routes.MapPageRoute("admin-tambah-keberlanjutan", "admin-tambah-keberlanjutan", "~/admin-add-keberlanjutan.aspx");
            routes.MapPageRoute("admin-ubah-keberlanjutan", "admin-ubah-keberlanjutan", "~/admin-edit-keberlanjutan.aspx");
            routes.MapPageRoute("admin-album-keberlanjutan", "admin-album-keberlanjutan", "~/AlbumImagesKeberlanjutan.aspx");
            routes.MapPageRoute("admin-upload-album-keberlanjutan", "admin-upload-album-keberlanjutan", "~/admin_UploadAlbumKeberlanjutan.aspx");
            routes.MapPageRoute("admin-pengadaan", "admin-pengadaan", "~/AdminPengadaan.aspx");
            routes.MapPageRoute("admin-tambah-pengadaan", "admin-tambah-pengadaan", "~/SavePengadaan.aspx");
            routes.MapPageRoute("admin-ubah-pengadaan", "admin-ubah-pengadaan", "~/EditInfoPengadaan.aspx");
            routes.MapPageRoute("admin-karier", "admin-karier", "~/admin_career.aspx");
            routes.MapPageRoute("admin-kontak", "admin-kontak", "~/admin_contact.aspx");
            routes.MapPageRoute("admin-data-dokumen", "admin-data-dokumen", "~/DokumentList.aspx");
            routes.MapPageRoute("admin-tambah-dokumen", "admin-tambah-dokumen", "~/UploadDokumen.aspx");
            routes.MapPageRoute("admin-data-level-pengguna", "admin-data-level-pengguna", "~/LevelList.aspx");
            routes.MapPageRoute("admin-tambah-level-pengguna", "admin-tambah-level-pengguna", "~/LevelUserEdit.aspx");
            routes.MapPageRoute("admin-data-pengguna", "admin-data-pengguna", "~/UserList.aspx");
            routes.MapPageRoute("admin-tambah-pengguna", "admin-tambah-pengguna", "~/Register.aspx"); 
            routes.MapPageRoute("admin-pesan-masuk", "admin-pesan-masuk", "~/AdminInbox.aspx");

        }
    }
}