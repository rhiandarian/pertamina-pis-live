﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class LevelList : System.Web.UI.Page
    {
        ModelData Db = new ModelData();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "34"))
                {
                    Response.Redirect("admin.aspx");
                }
                LoadLevelUser();
            }
        }
        public void LoadLevelUser()
        {
            ModelData Db = new ModelData();

            string query = Db.SelectQuery("Id,name", "ViewLevelUser", ""); 
            DataTable dt = Db.getData(query);
            GridViewGallery.DataSource = dt;
            GridViewGallery.DataBind();
        }
        protected void EditClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            //Response.Redirect("admin-edit-keberlanjutan.aspx?ID=" + id);
            Response.Redirect("LevelUserEdit.aspx?ID=" + id);
        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Db.setData("update Level set isdeleted = 1 where id="+id);
            LoadLevelUser();
        }
        public void GVGalleryPageindexChanging(object sender, GridViewPageEventArgs e)
        {
            LoadLevelUser();
            GridViewGallery.PageIndex = e.NewPageIndex;
            GridViewGallery.DataBind();
        }
    }
}