﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class SaveArmada : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadData();

            }
        }
        public void LoadData()
        {
            if (Request.QueryString["ID"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                string conditions = " ID = " + Id;
                Armada am = new Armada();
                DataTable dt = am.Select("*", conditions);
                title.Text = dt.Rows[0]["title"].ToString();
                content.Text = dt.Rows[0]["content"].ToString();
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                Armada amd = new Armada();
                amd.Title = title.Text;
                amd.Content = content.Text;
                amd.Img_File = FileUpload1.FileName;

                if (Request.QueryString["ID"] == null)
                {
                    string validationmessage = "";

                    if(!all.ImageFileType(FileUpload1.FileName))
                    {
                        validationmessage = "File Must be Image";
                    }
                    string insertStatus = amd.Insert();
                    if(insertStatus != "success")
                    {
                        validationmessage = insertStatus;
                    }
                    if(validationmessage != "")
                    {
                        showValidationMessage(validationmessage);
                    }
                    else
                    {
                        UploadImageArmada();
                        Response.Redirect("Armada-Kami.aspx");
                    }
                }
                else
                {
                    int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                    string UpdateStatus = "not used";//amd.Update(" ID = " + Id);
                    if(UpdateStatus != "success")
                    {
                        showValidationMessage(UpdateStatus);
                    }
                    else
                    {
                        if(amd.img_file != "")
                        {
                            UploadImageArmada();
                        }
                        Response.Redirect("Armada-Kami.aspx");

                    }
                }
            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void UploadImageArmada()
        {
            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string SaveLocation = Server.MapPath("Armada") + "\\" + fn;

            FileUpload1.PostedFile.SaveAs(SaveLocation);
        }
    }
}