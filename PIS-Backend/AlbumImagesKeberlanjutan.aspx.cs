﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class AlbumImagesKeberlanjutan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "28"))
                {
                    Response.Redirect("admin.aspx");
                }
                LoadAlbum();
            }
        }
        public void LoadAlbum()
        {
            if (Request.QueryString["GalleryID"] != null)
            {
                int GalleryID = Convert.ToInt32(Request.QueryString["GalleryID"].ToString());
                Image img = new Image();
                string conditions = " GalleryID =" + GalleryID;
                DataTable dt = img.Select("ID,title, img_file, CONVERT(varchar,addDate,106) as 'image_date'", conditions);
                ListViewAlbum.DataSource = dt;
                ListViewAlbum.DataBind();
            }
            else
            {
                Response.Redirect("admin_listkeberlanjutan.aspx");
            }
        }
        public void ReturnHome(object sender, EventArgs e)
        {
            int GalleryID = Convert.ToInt32(Request.QueryString["GalleryID"].ToString());
            Response.Redirect("admin-edit-keberlanjutan.aspx?ID=" + GalleryID);
        }

        protected void LinkButtonUploadAlbum_Click(object sender, EventArgs e)
        {
            int GalleryID = Convert.ToInt32(Request.QueryString["GalleryID"].ToString());
            Response.Redirect("admin_UploadAlbumKeberlanjutan.aspx?GalleryID=" + GalleryID);
        }
        public void Delete(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            int imageId = Convert.ToInt32(lb.CommandArgument);
            General all = new General();
            Image img = new Image();
            string conditions = " ID =" + imageId;
            try
            {
                img.Delete(imageId.ToString(), getip(), GetBrowserDetails(), conditions);
                LoadAlbum();
            }
            catch(Exception ex)
            {
                img.ErrorLogHistory(getip(),GetBrowserDetails(),"Delete",img.tableName,Session["Username"].ToString(), Session["UserID"].ToString(),ex.Message.ToString());
                Response.Write(all.alert("Error with " + ex.Message.ToString()));
            }


            
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
    }
}