﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="admin_UploadAlbumKeberlanjutan.aspx.cs" Inherits="PIS_Backend.admin_UploadAlbumKeberlanjutan" EnableEventValidation="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
                        <h2>Keberlanjutan</h2>
                        <ol class="breadcrumb">
                             <li class="breadcrumb-item">
                                <a>Menu Lainnya</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="admin_listkeberlanjutan.aspx">Keberlanjutan</a>
                            </li>
                            <li class="breadcrumb-item active">
                                 <strong>Unggah Album keberlanjutan</strong> 
                            </li>
                        </ol>
                    </div> 
            </div> 
         <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
       
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Unggah Album Keberlanjutan</h5> 
                        </div>
                        <div class="ibox-content"> 
                             <form>
                                   <div class="form-group">
                                        <label for="title"><b>title</b></label>
                                        <asp:TextBox ID="title" cssClass="form-control" placeholder="Masukan Judul Foto" runat="server"></asp:TextBox>
                                      </div>
                                     <div class="form-group">
                                        <label for="title_en"><b>title</b></label>
                                        <asp:TextBox ID="title_en" cssClass="form-control" placeholder="Masukan Judul Foto dalam Bahasa Inggris" runat="server"></asp:TextBox>
                                      </div>
                                     <div class="form-group">
                                         <label for="FileUpload1"><b>Image</b></label>
                                         <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                                      </div>
                                      <div class="form-group">
                                        <asp:Button ID="Save" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="BtnSave_Click"     />
                                    </div>
                       <div class="text-right">
             <asp:LinkButton ID="LinkButtonHome" runat="server" CssClass="btn btn-info" OnClick="returnHome">Kembali</asp:LinkButton>
         </div> 
                            </form>
                        </div>
                    </div>
                </div>
       
</asp:Content>
