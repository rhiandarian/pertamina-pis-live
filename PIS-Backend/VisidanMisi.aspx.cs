﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PIS_Backend
{
    public partial class Visi_Misi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
            }

            Loadpage();
        }
        public void Loadpage()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 7 ");
            imgHeader.Src = dt.Rows[0]["Header"].ToString();
            content.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content"].ToString();
            header_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Vision & Mission" : "Visi & Misi";
            content_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Vision & Mission" : "Visi & Misi";
            home_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Home" : "Beranda";
            tentang_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "About PIS" : "Tentang PIS";
            vm_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Vision & Mission" : "Visi & Misi";
            //fileDownload.HRef = dt.Rows[0]["others"].ToString();
            editClassLang();
        }

        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }

    }
}