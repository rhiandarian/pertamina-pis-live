﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="Articles.aspx.cs" Inherits="PIS_Backend.Articles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function confirmDelete(btnDelete) {
            if (btnDelete.dataset.confirmed) {
                // The action was already confirmed by the user, proceed with server event
                btnDelete.dataset.confirmed = false;
                return true;
            } else {
                // Ask the user to confirm/cancel the action
                event.preventDefault();
                swal({
                    title: 'Are you sure?',
                    text: "This Article will be Removed!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                })
                .then(function (isConfirm) {
                    if (isConfirm.value) {
                        // Set data-confirmed attribute to indicate that the action was confirmed
                        btnDelete.dataset.confirmed = true;
                        // Trigger button click programmatically
                        btnDelete.click();
                    }
                    
                }).catch(function (reason) {
                    // The action was canceled by the user
                });
            }
        }
    </script>
    <style>
        

       #ContentPlaceHolder1_GVArticles tr:nth-child(even){background-color: white;}

       #ContentPlaceHolder1_GVArticles tr:nth-child(odd){background-color: gainsboro;}

       #ContentPlaceHolder1_GVArticles tr:hover {background-color: #ddd;}

       #ContentPlaceHolder1_GVArticles th {
          text-align: center;
          background-color: #d82f2f;
          color: black;
       }
      
    </style>
    <div class="container-fluid" style="background-color:ghostwhite">
        <div class="row pt-5">
             <div class="col text-center">
                 <h1>Articles</h1>
             </div>
         </div><hr />
        <div class="row pb-3">
            <div class="col-4">
                <div class="dropdown">
                  <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" runat="server" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-plus"></i> Add New
                  </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="AddArticle.aspx">Article</a>
                        <a class="dropdown-item" href="AddCategory.aspx">Category</a>
                
                      </div>
                </div>
            </div>
            <div class="col-7">
             <asp:TextBox ID="TxtSearch" placeholder="Search Article" CssClass="form-control" runat="server"></asp:TextBox>  
               
            </div>     
           <div class="col">
             
             <asp:LinkButton ID="LinkButton4" CssClass="btn btn-dark" OnClick="btnSearch_Click" runat="server"><i class="fa fa-search"></i></asp:LinkButton>     
            </div>       
         </div>
        
        <div class="row">
            <div class="col">
                
                <asp:GridView ID="GVArticles" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered" Width="100%" OnRowDataBound="GVArticles_RowDataBound" AllowPaging="true" OnPageIndexChanging="GVArticles_PageIndexChanging"  >
                    <Columns>
                        <asp:TemplateField HeaderText="No" >   
                         <ItemTemplate>
                                <b> <%# Container.DataItemIndex + 1 %> </b>  
                         </ItemTemplate>
                     </asp:TemplateField>
                        <asp:TemplateField HeaderText="Title">
                            <ItemTemplate>
                                <asp:Label ID="Label1" ForeColor="#3568ff" runat="server" Text='<%#Bind("title") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Content">
                            <ItemTemplate>
                                <asp:Label ID="Label2" ForeColor="#3568ff" runat="server" Text='<%#Bind("content") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <asp:Label ID="Label3" ForeColor="#3568ff" runat="server" Text='<%#Bind("Category_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Bind("ID") %>' CssClass="btn btn-success" OnClick="EditClick"  ><i class="fa fa-edit"></i></asp:LinkButton>
                            </ItemTemplate>
                       </asp:TemplateField>
                        <asp:TemplateField HeaderText="View">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandArgument='<%#Bind("ID") %>' CssClass="btn btn-primary" OnClick="ViewClick" ><i class="fa fa-eye"></i></asp:LinkButton>
                              </ItemTemplate>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandArgument='<%#Bind("ID") %>' CssClass="btn btn-danger" OnClientClick="return confirmDelete(this);" OnClick="DeleteClick" ><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                       </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <div align="center">No Article found.</div>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
    </div>
</asp:Content>
