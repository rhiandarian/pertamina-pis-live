﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Galleri.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.Galleri" %>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Pertamina PIS</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="images/favicon.png" rel="icon">
  <link href="images/favicon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="vendor/bootstrap-4.5.3-dist/css/bootstrap.min.css" rel="stylesheet">

    <link href="vendor/animate/animate.css" rel="stylesheet" />
  <link href="vendor/venobox/venobox.css" rel="stylesheet">
  <link href="vendor/aos/aos.css" rel="stylesheet">
  <!--
    <link href="vendor/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
  -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="css/v01/style.css" rel="stylesheet" />
</head>

<body>
    <form runat="server">
  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <asp:LinkButton ID="LbInd" CssClass="active" runat="server">ID</asp:LinkButton> | <asp:LinkButton ID="LbEnd" OnClick="ChangeLang" runat="server">EN</asp:LinkButton>
      </div>
      <div class="contact-info float-right">
      	<a href="contacts.aspx"><i class="bi bi-envelope"></i></a>
      	<a href="#search"><i class="bi bi-search"></i></a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
       <a href="Index.aspx"><img src="images/Logo_Pertamina_PIS.svg" alt="" class="img-fluid"></a>
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
      	<a href="" id="nav-right-option" class="nav-right-option float-right mobile-nav-toggle d-lg-none"><i class="bi bi-x"></i></a>
        <ul>
          
            <asp:ListView ID="ListViewMenu" runat="server">
                <ItemTemplate>
                    <li class='<%# Eval("classMenu") %>'>
                        <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                        <ul>
                            <asp:ListView ID="ListViewSubMenu" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                <ItemTemplate>
                                    <li class='<%# Eval("classMenu") %>'>
                                        <a href='<%# Eval("Link") %>'><%# System.Web.HttpUtility.HtmlEncode((string)Eval("Name")) %></a>
                                        <ul>
                                        <asp:ListView ID="ListViewSubMenu2" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                            </ul>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                        </ul>
                    </li>
                </ItemTemplate>
            </asp:ListView>
            <asp:ListView ID="ListViewResponsive" runat="server">
                            <ItemTemplate>
                                <li class='<%# Eval("classMenu") %> d-lg-none'>
                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                    <ul>
                                        <asp:ListView ID="ListViewSubOthers" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li class='<%# Eval("classMenu") %>'>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                    <ul>
                                                        <asp:ListView ID="ListViewSub2Othes" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                                            <ItemTemplate>
                                                                <li>
                                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </ul>
                                                    
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </ul>
                                </li>
                            </ItemTemplate>
                        </asp:ListView>
		      

          <li class="nav-right-option d-none d-lg-block" id="othermenu" runat="server"></li>
        </ul>
        <div id="langMobile" runat="server" class="text-center lang-mobile d-lg-none">
		     
		    </div>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- Nav Overlay -->
  <div id="navOverlay" class="nav-overlay d-none d-lg-block">
  	<div class="bg-black-transparent">
	  	<div class="container">
		    <div class="nav-overlay-content">
		    	<ul>
                        <asp:ListView ID="ListViewOthers" runat="server">
                            <ItemTemplate>
                                <li class='<%# Eval("classMenu") %>'>
                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                    <ul>
                                        <asp:ListView ID="ListViewSubOthers" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li class='<%# Eval("classMenu") %>'>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                    <ul>
                                                        <asp:ListView ID="ListViewSub2Othes" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                                            <ItemTemplate>
                                                                <li>
                                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </ul>
                                                    
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </ul>
                                </li>
                            </ItemTemplate>
                        </asp:ListView>
                        
		    		
		    	</ul>
		    </div>
	  	</div>
  	</div>
  </div><!-- End Nav Overlay -->

  <!-- Search -->
  <div id="search">
  	<div class="bg-overlay">
	    <button type="button" class="close"><i class="bi bi-x close"></i></button>
	    <form >
	    <input type="search" id="txtSearch" runat="server" value="" autocomplete="off" placeholder="Ketik kata kunci disini" />
            <asp:Button ID="BtnSearch" runat="server" OnClick="BtnSearch_Click" CssClass="btn bg-red" Text="Cari" />
	    </form>
  	</div>
	</div><!-- End Search -->

  <main id="main">

  	<!-- Top Main -->
  	<section id="breadcrumbs" class="pt-3">
			<div class="container">
				<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx">Beranda</a></li>
					    <li class="breadcrumb-item"><a href="">Media & Informasi</a></li>
					    <li class="breadcrumb-item active" aria-current="page">Galleri</li>
					  </ol>
					</nav>
					<div class="col-lg-12 page-title">
						<h3 class="mb-3">GALERI</h3>
					</div>
				</div>
					
				</div>
  	</section>
       <asp:Label ID="LblIndexFoto" runat="server" Text="Label"></asp:Label>
      <asp:Label ID="LblIndexVideo" runat="server" Text="Label"></asp:Label>
      <asp:Label ID="LblIndexInfografis" runat="server" Text="Label"></asp:Label>

  	<!-- ======= Media Information Section ======= -->
    <section class="gallery">
      <div class="container">
				<div class="row no-gutters">
					<div class="col-lg-12 mb-3">
						<ul class="nav nav-tabs row-eq-height" id="myTab" role="tablist">
							<li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5 active" id="video-tab" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="true">
							   Video
							  </a>
						  </li>
						  <li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5" id="photo-tab" data-toggle="tab" href="#photo" role="tab" aria-controls="photo" aria-selected="false">
						    	Foto
						    </a>
						  </li>
						  <li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5" id="infografis-tab" data-toggle="tab" href="#infografis" role="tab" aria-controls="infografis" aria-selected="false">
						    	Infografis
						    </a>
						  </li>
						</ul>
						<div class="tab-content bg-white pt-5 pb-5" id="myTabContent">
						  <div class="tab-pane fade show active" id="video" role="tabpanel" aria-labelledby="video-tab">
						  	<div class="row">
						  		<asp:ListView ID="ListViewVideo" runat="server">
                                      <ItemTemplate>
                                          <div class="col-lg-4 col-md-6 mb-3 col-12 box-video">
						  			<!--
						  			<div class="embed-responsive embed-responsive-16by9">
										  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/URHBX4oK3Wc" allowfullscreen></iframe>
										</div>
										-->
										<div class="image-video">
											<a href='<%# Eval("url") %>' class="popup-youtube">
					                <img src='<%# Eval("Thumbnail") %>' class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="">
											<p class="mb-0"><em><%# Eval("news_date") %></em> &nbsp;</p>
          						<p><strong><%# Eval("title") %></strong></p>
										</div>
						  		</div>
                                      </ItemTemplate>
                                  </asp:ListView>
						  	</div>

						  	<!---Pagination-->
						  	<div class="row mt-4">
				        	<div class="col-12 pagination-box">
				        		<div class="row">
				        			<div class="col-xs-12 col-sm-6">
				        				<p id="detailpagingVideo" runat="server"></p>
				        			</div>
				        			<div class="col-xs-12 col-sm-6 text-right">
				        				<asp:Repeater ID="RptVideo" runat="server">
                                  <HeaderTemplate>
                      
										 <ul class="pagination justify-content-end">
                                               <li class="page-item">
                                                    <asp:LinkButton  runat="server" Text="<<" CommandArgument="first"
                                                        CssClass="page-link" category="video" OnClick="Page_Changed"></asp:LinkButton>
                                                </li>
										    
                                  </HeaderTemplate>
                                  <ItemTemplate>
                                      <li class='<%#Eval("active")%>'>
                                        <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Index") %>' category="video" CommandArgument='<%# Eval("Index") %>'
                                        CssClass="page-link" OnClick="Page_Changed"></asp:LinkButton>
                                      </li>
                                  </ItemTemplate>
                                  <FooterTemplate>
                                      <li class="page-item">
                                              <asp:LinkButton  runat="server" Text=">>" category="video" CommandArgument="last"
                                            CssClass="page-link" OnClick="Page_Changed"></asp:LinkButton>
                                       </li>
                                     </ul>
                      
                                  </FooterTemplate>
                              </asp:Repeater>
				        			</div>
				        		</div>
				        	</div>
				        </div>

						  </div>

						  <div class="tab-pane fade" id="photo" role="tabpanel" aria-labelledby="photo-tab">
						  	<div class="row">
						  		<asp:ListView ID="ListViewPhoto" runat="server">
                                      <ItemTemplate>
                                          <div class="popup-gallery gallery-<%# Eval("No") %> col-xs-12 col-sm-4 mb-3">
							  		        <a class="image-popup-vertical-fit" href='<%# Eval("Img_File") %>' title='<%# Eval("Title") %>'>
							  			        <div class="box-image">
						  					        <img src='<%# Eval("Img_File") %>' class="" alt="...">
						  				        </div>
											    <div class="">
												    <p class="mb-0"><em><%# Eval("Dates") %></em> <em class="float-right"><%# Eval("detindex") %></em></p>
	          						                <p><strong><%# Eval("Title") %></strong></p>
											    </div>
							  		        </a>
                                              <asp:ListView ID="ListViewDetailImage" runat="server" DataSource='<%# Eval("ImageDetail") %>'>
                                                  <ItemTemplate>
                                                      <a class="image-popup-vertical-fit" href='<%# Eval("Img_File") %>' title='<%# Eval("Title") %>'></a>
                                                  </ItemTemplate>
                                              </asp:ListView>
							  		        
						  		        </div>
                                      </ItemTemplate>
                                  </asp:ListView>
						  	</div>

						  	<!---Pagination-->
						  	<div class="row mt-4">
				        	<div class="col-12 pagination-box">
				        		<div class="row">
				        			<div class="col-xs-12 col-sm-6">
				        				<p id="detailPagingPhoto" runat="server"></p>
				        			</div>
				        			<div class="col-xs-12 col-sm-6 text-right">
				        				<asp:Repeater ID="RptPaging" runat="server">
                                  <HeaderTemplate>
                      
										 <ul class="pagination justify-content-end">
                                               <li class="page-item">
                                                    <asp:LinkButton  runat="server" Text="<<" CommandArgument="first"
                                                        CssClass="page-link" category="photo" OnClick="Page_Changed" ></asp:LinkButton>
                                                </li>
										    
                                  </HeaderTemplate>
                                  <ItemTemplate>
                                      <li class='<%#Eval("active")%>'>
                                        <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Index") %>' category="photo" CommandArgument='<%# Eval("Index") %>'
                                        CssClass="page-link" OnClick="Page_Changed" ></asp:LinkButton>
                                      </li>
                                  </ItemTemplate>
                                  <FooterTemplate>
                                      <li class="page-item">
                                              <asp:LinkButton  runat="server" Text=">>" category="photo" CommandArgument="last"
                                            CssClass="page-link" OnClick="Page_Changed" ></asp:LinkButton>
                                       </li>
                                     </ul>
                      
                                  </FooterTemplate>
                            </asp:Repeater>
												</nav>
				        			</div>
				        		</div>
				        	</div>
				        </div>
						  </div>
						  <div class="tab-pane fade" id="infografis" role="tabpanel" aria-labelledby="infografis-tab">
						  	<div class="row">
                                  <asp:ListView ID="ListViewInfografis" runat="server">
                                      <ItemTemplate>
                                          <div class="col-xs-12 col-sm-4 mb-3">
						  			        <div class="box-image">
					  					        <a href="<%# Eval("Img_File") %>" class="image-popup-vertical-fits">
					  						        <img src="<%# Eval("Img_File") %>" class="" alt="...">
						  				        </a>
					  				        </div>
										        <div class="">
											        <p class="mb-0"><em><%# Eval("Dates") %></em> </p>
          						        <p class="mb-1"><strong><%# Eval("Title") %></strong></p>
          						        <p><a href='<%# Eval("Img_File") %>' class="red_pis">Download</a></p>
										        </div>
						  		        </div>
                                      </ItemTemplate>
                                  </asp:ListView>
						  		
						  		
						  		
						  	</div>

						  	<!---Pagination-->
						  	<div class="row mt-4">
				        	<div class="col-12 pagination-box">
				        		<div class="row">
				        			<div class="col-xs-12 col-sm-6">
				        				<p id="detailInfografisPaging" runat="server"></p>
				        			</div>
				        			<div class="col-xs-12 col-sm-6 text-right">
				        				<nav aria-label="...">
												  <ul class="pagination  justify-content-end">
												    <asp:Repeater ID="RptInfg" runat="server">
                                  <HeaderTemplate>
                      
										 <ul class="pagination justify-content-end">
                                               <li class="page-item">
                                                    <asp:LinkButton  runat="server" Text="<<" CommandArgument="first"
                                                        CssClass="page-link" category="infografis" OnClick="Page_Changed"></asp:LinkButton>
                                                </li>
										    
                                  </HeaderTemplate>
                                  <ItemTemplate>
                                      <li class='<%#Eval("active")%>'>
                                        <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Index") %>' category="infografis" CommandArgument='<%# Eval("Index") %>'
                                        CssClass="page-link" OnClick="Page_Changed"></asp:LinkButton>
                                      </li>
                                  </ItemTemplate>
                                  <FooterTemplate>
                                      <li class="page-item">
                                              <asp:LinkButton  runat="server" Text=">>" category="infografis" CommandArgument="last"
                                            CssClass="page-link" OnClick="Page_Changed"></asp:LinkButton>
                                       </li>
                                     </ul>
                      
                                  </FooterTemplate>
                              </asp:Repeater>
												  </ul>
												</nav>
				        			</div>
				        		</div>
				        	</div>
				        </div>

						  </div>
						</div>

					</div>
        </div>

        <%--<div class="row no-gutters">
        	<div class="col-12">
        		<p>Instagram Feed</p>
                <a href="https://www.instagram.com/pertamina_pis/ " > 
        		<img src="images/feed/Instagram_vertical.png" class="d-none d-sm-block img-fluid" alt="...">
        		<img src="images/feed/Instagram_horizontal.png" class="d-sm-none img-fluid" alt="...">
                </a>
        	</div>
        </div>--%>
        <asp:TextBox ID="DataShow" runat="server"></asp:TextBox>
      </div>
    </section><!-- Media Information Section -->

    

  </main>

   <!-- ======= Footer ======= -->
	<div id="footer-top-border">
		<div class="col-lg-12 col-12" style="top:0">
			<div class="row">
				<div class="col-lg-6 col-6 footer-border-1" style="top:0"></div>
				<div class="col-lg-3 col-3 footer-border-2" style="top:0"></div>
				<div class="col-lg-3 col-3 footer-border-3" style="top:0"></div>
			</div>
		</div>
	</div>
	<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-4 col-12 footer-info">
            <h6><strong>KANTOR PUSAT</strong></h6>
            <p id="address" runat="server" class="mt-3">
              Patra Jasa Office Tower Lantai 3 & 14 
              <br>JJl. Jend. Gatot Subroto Kav 32–34
              <br>Setiabudi, Jakarta 12950 
              <br>Indonesia 
            </p>
            <div class="d-flex mt-2">
              <div class="align-self-center rounded-icon">
	  					  <span class="bi bi-telephone-fill phone-icon"></span>
              </div>
              <div class="align-self-center">
               &nbsp; <span id="phone" runat="server"></span>
              </div>
	  				</div>
	  				<div class="d-flex mt-2">
              <div class="align-self-center rounded-icon">
	  					  <span class="bi bi-envelope-fill phone-icon"></span>
              </div>
              <div class="align-self-center">
                &nbsp; <span id="email" runat="server"></span>
              </div>
	  				</div>
	  				<div class="d-flex mt-2">
              <div class="align-self-center">
	  					  <img src="images/logo-135.jpg" width="25px" alt="" class="img-fluid">
              </div>
              <div class="align-self-center">
                &nbsp; Call Center <strong>135</strong>
              </div>
	  				</div>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>


          <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong id="perusahaan" runat="server">TAUTAN </strong></h6>
            <ul class="mt-3">
              <li><i class="bx bx-chevron-right"></i> <a href="https://pertamina.com" id="pt1" runat="server">PT Pertamina (Persero)</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://bumn.go.id" id="pt2" runat="server">'Kementerian BUMN</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://www.esdm.go.id" id="pt3" runat="server">Kementerian ESDM RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="http://www.dephub.go.id" id="pt4" runat="server">Kementerian Perhubungan RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://www.bkpm.go.id" id="pt5" runat="server">BKPM</a></li>
            </ul>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>

          <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong>TEMUKAN KAMI</strong></h6>
            <ul class="mt-3 social-links">
              <li >
              	<a id="link_fb" runat="server" class="d-flex">
	              	<div class="align-self-center">
	              		<span class="bi bi-facebook facebook"></span>
	              	</div>
	              	<div class="align-self-center">Facebook<br><em>Pertamina International Shipping</em></div>
	              </a>
	            </li>
              <li>
              	<a id="link_ig" runat="server" class="d-flex">
	              	<div class="align-self-center">
	              		<span class="bi bi-instagram instagram"></span>
	              		</div>
	              	<div class="align-self-center">Instagram<br><em>@pertamina_pis</em></div> 
	              </a>
	            </li>
              <li>
                <a id="link_tw" runat="server" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-twitter twitter"></span>
                    </div>
                  <div class="align-self-center">Twitter<br><em>@Pertamina_PIS</em></div> 
                </a>
              </li>
              <li>
                <a id="link_yt" runat="server" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-youtube twitter"></span>
                    </div>
                  <div class="align-self-center">Youtube<br><em>PT Pertamina International Shipping</em></div> 
                </a>
              </li>
            </ul>
          </div>
          <div class="col-12 d-lg-none separate"><hr></div>

        </div>
      </div>
    </div>

    <div class="container">
    	<div class="row d-flex">
	      <div class="col-lg-8 col-12 align-self-center part-of">
	        <span>PART OF</span>
	        <img src="images/logo-bumn.svg" height="30px" class="ml-3" alt="...">
	        <img src="images/logo-pertamina.svg" height="30px" class="ml-3" alt="...">
	      </div>
    		<div class="col-lg-4 col-12">
    			<a href="" class="btn btn-whistle">
            	<div class="row">
            		<div class="col pr-0 d-flex mr-2">
          				<img src="images/icons/wbs.svg">
            		</div>
            		<div class="col text-left pl-0">
		          		Whistle Blowing System
		          		<br><em>https://pertaminaclean.tipoffs.info/</em>
            		</div>
            	</div>
          	</a>
    		</div>
    		<div class="col-12 mt-3">
    			&copy; Copyright 2021 <strong><span>Pertamina International Shipping</span></strong>. All Rights Reserved
    		</div>
    	</div>
    </div>
  </footer><!-- End Footer -->
    

   

  <!-- Vendor JS Files -->
  <script src="vendor/jquery.min.js"></script>
  <script src="vendor/bootstrap-4.5.3-dist/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="vendor/venobox/venobox.min.js"></script>
  <script src="vendor/waypoints/lib/jquery.waypoints.min.js"></script>
  <script src="vendor/counterup/counterup.min.js"></script>
  <script src="vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="vendor/aos/aos.js"></script>
  <script src="vendor/fontawesome/js/all.min.js"></script>
  <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Template Main JS File -->
  <script src="js/main.js?v=0.1"></script>

  <script type="text/javascript">
      history.pushState({}, null, "galeri");
      function changeLang()
      {
           var button = document.getElementById("<%= LbEnd.ClientID %>");
           button.click();
      }
  	$(document).ready(function() {
  	    $("#DataShow").hide();
  	    if ($("#DataShow").val() == 'photo') {
  	        $('.nav-link').removeClass('active');
  	        $('.tab-pane').removeClass('active show');
  	        $('#photo-tab').addClass('active');
  	        $('#photo').addClass('active show');
  	    }
  	    if ($("#DataShow").val() == 'infografis') {
  	        $('.nav-link').removeClass('active');
  	        $('.tab-pane').removeClass('active show');
  	        $('#infografis-tab').addClass('active');
  	        $('#infografis').addClass('active show');
  	    }
  	    if ($("#DataShow").val() == 'videos') {
  	        $('.nav-link').removeClass('active');
  	        $('.tab-pane').removeClass('active show');
  	        $("#video-tab").addClass('active');
  	        $("#video").addClass('active show');
  	    }
		  $('.image-popup-vertical-fits').magnificPopup({
				type: 'image',
				closeOnContentClick: true,
				mainClass: 'mfp-img-mobile',
				image: {
					verticalFit: true
				},
				zoom: {
					enabled: true,
					duration: 300 
				}
			});

			$('.popup-youtube').magnificPopup({
				disableOn: 700,
				type: 'iframe',
				mainClass: 'mfp-fade',
				removalDelay: 160,
				preloader: false,
				fixedContentPos: false,
			});

			let countGallery = $(".popup-gallery").length;
			for (i=1; i<=countGallery; i++) {
			  $(".gallery-"+i).magnificPopup({
					delegate: 'a',
					type: 'image',
					tLoading: 'Loading image #%curr%...',
					mainClass: 'mfp-img-mobile',
					gallery: {
						enabled: true,
						navigateByImgClick: true,
						preload: [0,1] // Will preload 0 - before current, and 1 after the current image
					},
					image: {
						tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
						titleSrc: function(item) {
							return item.el.attr('title') + '';
						}
					}
				});
			} 

			let infografisImg = $("#infografis").find('img');
			$.each(infografisImg, function(key, value){
				let widthImage = value.width;
				let heightImage = value.height;
				if(widthImage>heightImage){
					$(this).css({'width': '100%', 'height': 'auto'});
				} else {
					$(this).css({'height': '100%', 'width': 'auto'});
				}

			})

			let shipImg = $(".popup-gallery").find('img');
			$.each(shipImg, function(key, value){
				let widthImage = value.width;
				let heightImage = value.height;
				console.log(value);
				console.log(heightImage);
				if(heightImage>250){
						$(this).css({'width': '100%', 'height': 'auto'});
					} else {
						$(this).css({'height': '100%', 'width': 'auto'});
					}
			});
			

		});

  </script>
</form>
</body>

</html>