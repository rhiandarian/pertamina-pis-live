﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="TonggakSejarah.aspx.cs" Inherits="PIS_Backend.TonggakSejarah" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx" id="li_1" runat="server">Home</a></li>
					    <li class="breadcrumb-item" id="li_2" runat="server">Profil Perusahaan</li>
					    <li class="breadcrumb-item active" aria-current="page" id="li_3" runat="server">Tonggak Sejarah</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="header_text" runat="server">Tonggak Sejarah</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <section id="tonggak-sejarah">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div class="col-lg-12 content">
	  				<%--<div class="row">
	  					<div class="col-12">
	  						<h5>Tonggak Sejarah</h5>
	  					</div>
	  				</div>--%>

            <div class="row">
              <div id="content" runat="server" class="col-12">
                
              </div>
            </div>
        

	  			</div>
	  		</div>
	  	</div>
  	</section>
    <script>
        var urlName = '';
        if ($("#LbInd").attr("class") == "active") {
            urlName = 'tonggak-sejarah';
        }
        if($("#LbEn").attr("class") == "active")
        {
            urlName = 'milestone';
        }
        history.pushState({}, null,urlName);
    </script>
</asp:Content>
