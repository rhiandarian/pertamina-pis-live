﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PIS_Backend
{
    public partial class UploadAlbum : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                LblErrorMessage.Visible = false;
                if (Request.QueryString["GalleryID"] == null)
                {
                    Response.Redirect("AdminGallery.aspx");
                }
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["GalleryID"] != null)
            {
                int GalleryID = Convert.ToInt32(Request.QueryString["GalleryID"].ToString());
                Image img = new Image();
                img.Gallery_Id = GalleryID;
                img.Title = title.Text;
                img.Img_File = FileUpload1.FileName;

                string insertStatus = img.Insert(getip(),GetBrowserDetails());
                if(insertStatus == "success")
                {
                    string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                    string SaveLocation = Server.MapPath("Gallery") + "\\" + fn;

                    FileUpload1.PostedFile.SaveAs(SaveLocation);
                    Response.Redirect("AlbumImages.aspx?GalleryID=" + GalleryID);
                }
                else
                {
                    showValidationMessage(insertStatus);
                }
            }
            else
            {
                showValidationMessage("Undefined Album");
            }
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void returnHome(object sender, EventArgs e)
        {
            int GalleryID = Convert.ToInt32(Request.QueryString["GalleryID"].ToString());
            Response.Redirect("AlbumImages.aspx?GalleryID=" + GalleryID);
        }
    }
}