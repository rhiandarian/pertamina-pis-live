﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminListAlbum.aspx.cs" Inherits="PIS_Backend.AdminListAlbum" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Album</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="ListFoto">Semua Foto</a>
                        </li>
                        <li class="breadcrumb-item">
                           <a href="AdminListAlbum.aspx"><strong>Semua Album</strong></a> 
                        </li>
                        
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="pb-3">
        <asp:LinkButton CssClass="btn btn-primary" OnClick="LinkButton1_Click" ID="LinkButton1" runat="server">Foto Baru</asp:LinkButton>
            </div>

                                        <div class="row">
                                            <asp:ListView ID="ListViewALbum" runat="server">
                <ItemTemplate>
                    <div class="col-xs-12 col-sm-4 box-content">
                                                        <div class="ibox">
                                                            <div class="ibox-content product-box">
                                                                <div class="product-imitation">
                                                                    <img src='<%# Eval("img_file") %>' class="img-fluid" alt="...">
                                                                    
                                                                </div>
                                                                <div class="product-desc">
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            
                                                                            <p class="text-center">
                                                                               <b><%# Eval("title") %></b> <br />
                                                                               
                                                                                <asp:LinkButton ID="LinkButton2" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="DeleteClick" runat="server"><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                                                                                
                                                                            </p>
                                                                        </div>

                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                </ItemTemplate>
            </asp:ListView>
                                            <%--<asp:ListView ID="ListViewArmada" runat="server" ItemPlaceholderID="itemPlaceHolder1" OnPagePropertiesChanging="OnPagePropertiesChanging" GroupPlaceholderID="groupPlaceHolder1">
                                                
                                                
                                                
                                            </asp:ListView>--%>
                                                
                                         </div>
        <div>
            <asp:LinkButton ID="LinkButtonBack" OnClick="LinkButtonBack_Click" CssClass="btn btn-info" runat="server"><span class="glyphicon glyphicon-arrow-left"></span> Kembali</asp:LinkButton>
        </div>
        
                                     </div>
</asp:Content>
