﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminKapalMilikArmada.aspx.cs" Inherits="PIS_Backend.AdminKapalMilikArmada" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Kapal</h2>
                    <ol class="breadcrumb">
                         <li class="breadcrumb-item">
                            Aktifitas Bisnis
                        </li>
                         <li class="breadcrumb-item">
                            Armada Kami
                        </li>

                        </li>
                        <li class="breadcrumb-item">
                           <a href="AdminKapalMilikArmada.aspx"><strong>Semua Kapal</strong></a> 
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="wrapper wrapper-content animated fadeIn">

            <div class="row">
                <div class="col-lg-12">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs" role="tablist">
                            <li><a id="kapalMilik" class="nav-link active" data-toggle="tab" href="#tab-1"> Kapal Milik</a></li>
                            <li><a id="armada" class="nav-link" data-toggle="tab" href="#tab-2" style="display:none">Armada</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="pb-3">
                                        <a href="adminSaveKapal.aspx" class="btn btn-primary"><i class="fa fa-plus"></i><span class="add_new_text">Buat Baru</span></a>
                                    </div>
                                    <asp:GridView ID="GridViewKapal" runat="server" AutoGenerateColumns="false" AllowPaging="true" CssClass="table table-bordered table-striped" OnPageIndexChanging="GridViewKapal_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="No">   
                                                 <ItemTemplate>
                                                       <b>  <%# Container.DataItemIndex + 1 %>  </b>  
                                                 </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Nama">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" ForeColor="#3568ff" runat="server" Text='<%#Bind("name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Oil">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" ForeColor="#3568ff" runat="server" Text='<%#Bind("oil") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" ForeColor="#3568ff" runat="server" Text='<%#Bind("type") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Built">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" ForeColor="#3568ff" runat="server" Text='<%#Bind("built") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Ubah">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Bind("ID") %>'  CssClass="btn btn-success" OnClick="EditClick" ><i class="fa fa-edit"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Hapus">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandArgument='<%#Bind("ID") %>'  CssClass="btn btn-danger" OnClick="DeleteClick" ><i class="fa fa-edit"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                   
                                </div>
                            </div>
                            <div role="tabpanel" id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ml-2">
                                        <a href="AdminSaveArmada.aspx" class="btn btn-primary"><i class="fa fa-plus"></i><span class="add_new_text">Buat Baru</span></a>
                                    </div>
                                    
                                    <div class="wrapper wrapper-content animated fadeInRight">
                                        <div class="row">
                                            <asp:ListView ID="ListViewArmada" runat="server" ItemPlaceholderID="itemPlaceHolder1" OnPagePropertiesChanging="OnPagePropertiesChanging" GroupPlaceholderID="groupPlaceHolder1">
                                               
                                                <GroupTemplate>
                                                    <tr>
                                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                                                    </tr>
                                                </GroupTemplate>
                                                <ItemTemplate>
                                                    <div class="col-xs-12 col-sm-4 box-content">
                                                        <div class="ibox">
                                                            <div class="ibox-content product-box">
                                                                <div class="product-imitation">
                                                                    <img src="Armada/<%#  Eval("img_file") %>" class="img-fluid mt-2 mb-2" alt="..." height="10%">
                                                                </div>
                                                                <div class="product-desc">
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            <p class="text-center"><%#  Eval("title") %></p>
                                                                            <p class="text-center">
                                                                                <asp:LinkButton ID="LinkButton3" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="DeleteArmada" runat="server">Delete <i class="fa fa-trash"></i></asp:LinkButton>
                                                                            </p>
                                                                        </div>

                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
                                                    <asp:DataPager ID="DataPager1" runat="server" PagedControlID="ListViewArmada" PageSize="3">
                                                        <Fields>
                                                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
                                                                ShowNextPageButton="false" />
                                                            <asp:NumericPagerField ButtonType="Link" />
                                                            <asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton = "false" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </LayoutTemplate>
                                                
                                                
                                            </asp:ListView>
                                                
                                         </div>
                                     </div>
                                    <div>

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                
            </div>
           

            <div class="row">
                <div class="col">
                    <a href="Armada-Kami.aspx" target="_blank"><i class="fa fa-eye"></i> Lihat hasil website</a>
                </div>
            </div>
            

            



        </div>
</asp:Content>
