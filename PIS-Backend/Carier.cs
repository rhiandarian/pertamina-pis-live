﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PIS_Backend
{
    public class Carier
    {
        SqlConnection con = Connection.conn();

        public string tableName = "Carier";

        public string validationHeaderImage = "Header harus foto";

        public string validationImageKarier = "Header harus foto";

        public string successMessage = "Halaman Berhasil di Ubah";

        DateTime dateNow = DateTime.Now;

        public int PageSizes = 3;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        public string header;

        public string content;

        public string content_en;

        public string disclaimer;

        public string disclaimer_en;

        public string magang;

        public string magang_en;

        public string others;
        public string mail_lamaran_kerja;
        public string mail_permohonan_magang;

        public string Header { get { return header; } set { header = value; } }

        public string Content { get { return content; } set { content = value; } }

        public string Content_En { get { return content_en; } set { content_en = value; } }

        public string Disclaimer { get { return disclaimer; } set { disclaimer = value; } }

        public string Disclaimer_en { get { return disclaimer_en; } set { disclaimer_en = value; } }

        public string Magang { get { return magang; } set { magang = value; } }

        public string Magang_en { get { return magang_en; } set { magang_en = value; } }

        public string Others { get { return others; } set { others = value; } }

        public string Mail_lamaran_kerja { get { return mail_lamaran_kerja; } set { mail_lamaran_kerja = value; } }

        public string Mail_permohonan_magang { get { return mail_permohonan_magang; } set { mail_permohonan_magang = value; } }

        public DataTable Select(string field, string cond = null)
        {
            cond = cond == null ? "" : cond;
            string Query = Db.SelectQuery(field, tableName, cond);

            DataTable dt = Db.getData(Query);
            return dt;
        }
        public DataSet GetData(string field, string cond = null)
        {
            cond = cond == null ? "" : cond;
            string Query = Db.SelectQuery(field, tableName, cond);

            DataSet dt = Db.getDataSet(Query);
            return dt;
        }
        public void Save(string cond = null)
        {
            string newRecord = "set ";
            newRecord += header != null ? "Header = '" + header + "', " : "";
            newRecord += content != null ? "content = '" + content.Replace("'", "") + "', " : "";
            newRecord += content_en != null ? "content_en = '" + content_en.Replace("'", "") + "', " : "";

            newRecord += disclaimer != null ? "disclaimer = '" + disclaimer.Replace("'", "") + "', " : "";
            newRecord += disclaimer_en != null ? "disclaimer_en = '" + disclaimer_en.Replace("'", "") + "', " : "";

            newRecord += magang != null ? "magang = '" + magang.Replace("'", "") + "', " : "";
            newRecord += magang_en != null ? "magang_en = '" + magang_en.Replace("'", "") + "', " : "";

            newRecord += others != null ? "others = '" + others + "', " : "";

            newRecord += mail_lamaran_kerja != null ? "mail_lamaran_kerja = '" + mail_lamaran_kerja + "', " : "";
            newRecord += mail_permohonan_magang != null ? "mail_permohonan_magang = '" + mail_permohonan_magang + "', " : "";

            newRecord += "edit_date = '" + dateNow + "', edit_user = " + user_id;

            cond = cond != null ? " Where " + cond : "";

            string Query = "Update " + tableName + " " + newRecord + " " + cond;
            Db.setData(Query);
        }


    }
}