﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace PIS_Backend
{
    public partial class AdminInbox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "32"))
                {
                    Response.Redirect("admin.aspx");
                }

                BindGVLayanan();

            }
        }
        public void BindGVLayanan()
        {
            ModelData md = new ModelData();
            DataTable dt = md.getData("select id,nama,judul,email,no_telp,institusi,isi_pesan,convert(varchar, created_at, 106) as created_at from Inbox where is_deleted = 0 or is_deleted is null order by created_at asc");
            GridViewLayanan.DataSource = dt;
            GridViewLayanan.DataBind();
        }
        protected void Edit_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Layanan ly = new Layanan();
            Response.Redirect(ly.SavePage+"?ID="+id);
        }
        protected void Delete_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender; 
            string id = lb.CommandArgument; 
            ModelData md = new ModelData();
             md.setData("update inbox set is_deleted = 1 where id ='" + id+"'"); 
            BindGVLayanan();
        }

        protected void GridViewLayanan_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGVLayanan();
            GridViewLayanan.PageIndex = e.NewPageIndex;
            GridViewLayanan.DataBind();
        }
    }
}