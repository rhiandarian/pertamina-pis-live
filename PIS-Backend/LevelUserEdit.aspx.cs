﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace PIS_Backend
{
    public partial class LevelUserEdit : System.Web.UI.Page
    {
        General all = new General();
        ModelData db = new ModelData();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "34"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadData();
            }
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        } 
        public void LoadData()
        {
            LoadDataHalaman();
            if (Request.QueryString["ID"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["ID"]);
                ModelData Db = new ModelData();
                string field = "select name from Level where ID = " + Id;
                DataTable dt = Db.getData(field);
                levelName.Text = dt.Rows[0]["name"].ToString();
                Level l = new Level();
                DataTable ds = l.SelectLevelPages("LevelPagesId, namapages", "Level_Id = " + Id);

                if (ds.Rows.Count > 0)
                {
                    GVListPage.DataSource = ds;
                    GVListPage.DataBind();
                }


            }
             
        }
        public void LoadDataHalaman()
        {
            DataSet ds = db.getDataSet("Select * from PagesAdmin");
            DdlHalaman.DataTextField = ds.Tables[0].Columns["nama"].ToString();
            DdlHalaman.DataValueField = ds.Tables[0].Columns["id"].ToString();
            DdlHalaman.DataSource = ds.Tables[0];
            DdlHalaman.DataBind();
            DdlHalaman.Items.Insert(0, "Pilih halaman untuk Level Pengguna");
            DdlHalaman.SelectedIndex = 0;
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Level l = new Level();
                l.LevelName = levelName.Text;
                l.PageID = DdlHalaman.SelectedValue == "Pilih halaman untuk Level Pengguna" ? 0 : Convert.ToInt32(DdlHalaman.Text);
                if(Request.QueryString["ID"] != null)
                {
                    l.ID = Request.QueryString["ID"].ToString();
                }
                if(l.validation() == "Success")
                {
                    DataTable dt = l.SaveLevel(getip(), GetBrowserDetails());
                    if (dt.Rows[0]["Status"].ToString() != "Success")
                    {
                        showValidationMessage(dt.Rows[0]["Message"].ToString());
                    }
                    else
                    {
                        if (Request.QueryString["ID"] == null)
                        {
                            Response.Redirect("LevelUserEdit.aspx?ID=" + dt.Rows[0]["ID"].ToString());
                        }
                        LblErrorMessage.Visible = false;
                        LoadData();
                    }
                 
                }
                else
                {
                    showValidationMessage(l.validation());
                }
            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
            
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            Level l = new Level();
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            string query = "Delete from LevelPages Where ID = " + id+" "+l.logHistoryQuery(getip(),GetBrowserDetails(),Session["Username"].ToString()+" Delete Pages from Level",Convert.ToInt32(Session["UserID"].ToString()),"Success");
            l.setData(query);
            LoadData();
            GVListPage.DataBind();

        }

        protected void GVListPage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            LoadData();
            GVListPage.PageIndex = e.NewPageIndex;
            GVListPage.DataBind();
        }
    }
}