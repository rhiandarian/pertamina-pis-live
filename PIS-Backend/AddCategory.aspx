﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="AddCategory.aspx.cs" Inherits="PIS_Backend.AddCategory" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid" style="background-color:ghostwhite">
        <div class="row pt-5">
             <div class="col text-center">
                 <h1>Add Category</h1>
             </div>
         </div><hr />
        <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
        <div class="row pt-5 pb-5">
             <div class="col">
                 <form>
                     <div class="form-group">
                        <label for="category_name"><b>Category Name</b></label>
                        <asp:TextBox ID="category_name" cssClass="form-control" placeholder="Add Category Name" runat="server"></asp:TextBox>
                      </div>
                     <div class="form-group">
                        <label for="DdlCategoryType"><b>Category Type</b></label>
                         <asp:DropDownList ID="DdlCategoryType" CssClass="form-control" runat="server"></asp:DropDownList>
                      </div>
                     <div class="form-group">
                         <asp:Button ID="BtnAdd" runat="server" Text="Add" CssClass="btn btn-primary" OnClick="BtnAdd_Click" />
                      </div>
                 </form>
             </div>
        </div>
        <div class="text-right">
           <a href="ListCategory.aspx" class="btn btn-info"><i class="fa fa-home"></i></a>
        </div>
     </div>
</asp:Content>
