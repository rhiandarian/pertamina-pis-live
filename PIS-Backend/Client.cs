﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
namespace PIS_Backend
{
    public class Client
    {
        SqlConnection con = Connection.conn();

        public string tableName = "Client";

        //string viewName = "View_Armada";

        DateTime dateNow = DateTime.Now;

        public int PageSizes = 3;

        string user_id = HttpContext.Current.Session["UserID"].ToString();

        ModelData Db = new ModelData();

        public string name;

        public string email;

        public string Name { get { return name; } set { name = value; } }

        public string Email { get { return email; } set { email = value; } }

        public string Validation()
        {
            if(name == "")
            {
                return "Name must be Filled";
            }
            if (email == "")
            {
                return "Email must be Filled";
            }
            return "Valid";
        }
        public string Insert()
        {
            try
            {
                string valid = Validation();
                if (valid != "Valid")
                {
                    return valid;
                }

                string field = "(Name,Email,addDate,addUser)";
                string value = "('"+name+"', '"+email+"', '"+dateNow+"', "+user_id+")";

                string Query = "INSERT INTO " + tableName + " " + field + " VALUES " + value;

                Db.setData(Query);

                return "Success";
            }
            catch(Exception e)
            {
                return e.Message.ToString();
            }
            
        }

    }
}