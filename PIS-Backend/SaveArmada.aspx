﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="SaveArmada.aspx.cs" Inherits="PIS_Backend.SaveArmada" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid" style="background-color:ghostwhite">
        <div class="row pt-5">
             <div class="col text-center">
                 <h1>ARMADA</h1>
             </div>
         </div><hr />
        <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
        <div class="form-group">
            <label for="title"><b>title</b></label>
            <asp:TextBox ID="title" cssClass="form-control" placeholder="Input name" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <label for="content"><b>content</b></label>
            <asp:TextBox ID="content" cssClass="form-control" placeholder="Input name" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <label for="content"><b>image</b></label>
            <asp:FileUpload ID="FileUpload1" runat="server" />
        </div>
        <div class="form-group">
            <asp:Button ID="Save" CssClass="btn btn-radius-pis bg-red mt-4" runat="server" Text="Save" OnClick="Save_Click" />
        </div>
    </div>
</asp:Content>
