﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

namespace PIS_Backend
{
    public partial class admin_edit_news : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"]  == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "4"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindCategory();
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadData();


            }
        }
        public void LoadData()
        {
            if (Request.QueryString["ID"] == null)
            {
                Response.Redirect("ListNews.aspx");
            }
            int Id = Convert.ToInt32(Request.QueryString["ID"]);
            string select = "title, content, title_en, content_en, [key], key_en, Dates, city, img_file, status, category_id";
            string conditions = "ID = " + Id;
            News atc = new News();
            DataTable dt = atc.Select(select, conditions);

            title.Text = dt.Rows[0]["title"].ToString();
            title_en.Text = dt.Rows[0]["title_en"].ToString();
            content.InnerHtml = dt.Rows[0]["content"].ToString();
            content_en.InnerHtml = dt.Rows[0]["content_en"].ToString();
            Image1.Attributes["src"] = dt.Rows[0]["img_file"].ToString();
            key.Text = dt.Rows[0]["key"].ToString();
            key_en.Text = dt.Rows[0]["key_en"].ToString();
            if (dt.Rows[0]["Dates"].ToString() != "")
            {
                DateTime dates = Convert.ToDateTime(dt.Rows[0]["Dates"].ToString());
                date.Text = dates.ToString("MM/dd/yyyy");
            }
            ddlCategory.SelectedValue = dt.Rows[0]["category_id"].ToString();
            
        }
        public void BindCategory()
        {
            Category ctg = new Category();

            ctg.Category_Type_Id = 1;

            string selectCategory = "ID, Category_name";

            DataSet dt = ctg.SelectList(selectCategory, "and");

            ddlCategory.DataTextField = dt.Tables[0].Columns["Category_name"].ToString();
            ddlCategory.DataValueField = dt.Tables[0].Columns["ID"].ToString();
            ddlCategory.DataSource = dt.Tables[0];
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, "Pilih Kategori");
            ddlCategory.SelectedIndex = 0;



        }
        public bool ImageFileType(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            switch (ext.ToLower())
            {
                case ".gif":
                    return true;
                case ".jpg":
                    return true;
                case ".jpeg":
                    return true;
                case ".png":
                    return true;
                default:
                    return false;
            }
        }
        public string randomTxt()
        {
            ImageResize ir = new ImageResize();
            return ir.textrandom();
        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                EditNews("Draft");
            }
            catch(Exception ex)
            {
                News n = new News();
                n.ErrorLogHistory(getip(), GetBrowserDetails(), "Update", n.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.ToString());
                LblErrorMessage.Visible = true;
                LblErrorMessage.Text = ex.Message.ToString();
                LblErrorMessage.Visible = true;
                LblErrorMessage.Text = ex.Message.ToString();
            }
        }

        protected void BtnPublished_Click(object sender, EventArgs e)
        {
            try
            {
                EditNews("Published");
            }
            catch(Exception ex)
            {
                News n = new News();
                n.ErrorLogHistory(getip(), GetBrowserDetails(), "Update", n.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.ToString());
                LblErrorMessage.Visible = true;
                LblErrorMessage.Text = ex.Message.ToString();
            }
            
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }

        public void EditNews(string status)
        {
            string random = randomTxt();
            bool exsist = false;
            bool validUpload = true;
            News atc = new News();
            if (title.Text != "")
            {
                atc.Title = title.Text;
                exsist = true;
            }
            if (title_en.Text != "")
            {
                atc.TitleEn = title_en.Text;
                exsist = true;
            }
            if (content.InnerHtml != "")
            {
                atc.Content = content.InnerText.Replace("'", "");
                exsist = true;
            }
            if(key.Text != "")
            {
                atc.Key = key.Text;
                exsist = true;
            }
            if(key_en.Text != "")
            {
                atc.KeyEn = key_en.Text;
                exsist = true;
            }
            if (content_en.InnerHtml != "")
            {
                atc.ContentEn = content_en.InnerText.Replace("'", "");
                exsist = true;
            }
            if (date.Text != "")
            {
                atc.Date = Convert.ToDateTime(date.Text);
                exsist = true;
            }
            atc.Img_File = Image1.Attributes["src"];
            if (FileUpload1.FileName != "")
            {
                atc.Img_File = "Beritaa/" + random + FileUpload1.FileName;
                exsist = true;
                string SaveLocation = "";
                if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
                {
                    string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                    string SetFolderBeforeResize = Server.MapPath("Beritaa") + "\\" + "serize" + random + fn;
                    string SetFolderAfterResize = Server.MapPath("Beritaa") + "\\" + random + fn;

                    try
                    {
                        if (ImageFileType(FileUpload1.FileName))
                        {
                            FileUpload1.PostedFile.SaveAs(SetFolderBeforeResize);
                            ImageResize Iz = new ImageResize();
                            Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "Berita", "Cut");

                        }
                        else
                        {
                            validUpload = false;
                            LblErrorMessage.Visible = true;
                            LblErrorMessage.Text = "Masukan file foto";
                        }





                    }
                    catch (Exception ex)
                    {
                        validUpload = false;
                        LblErrorMessage.Visible = true;
                        LblErrorMessage.Text = ex.Message;
                    }
                }
            }
            if (ddlCategory.SelectedValue != "Pilih Kategori")
            {
                exsist = true;
                atc.Category_Id = Convert.ToInt32(ddlCategory.SelectedValue);
            }
            if (exsist)
            {
                if (validUpload)
                {
                    atc.Status = status;
                    int Id = Convert.ToInt32(Request.QueryString["ID"]);
                    string conditions = "ID = " + Id;
                    DataTable dt = atc.Update(Id,getip(),GetBrowserDetails());
                    if(dt.Rows[0]["Status"].ToString() == "Success")
                    {
                        Response.Redirect("ListNews.aspx");
                    }
                    else
                    {
                        LblErrorMessage.Visible = true;
                        LblErrorMessage.Text = dt.Rows[0]["Message"].ToString();
                    }

                    
                }
            }
            else
            {
                Response.Redirect("ListNews.aspx");
            }
            LoadData();
        }
    }
}