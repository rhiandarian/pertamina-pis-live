﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="DokumentList.aspx.cs" Inherits="PIS_Backend.DokumentList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Unggah Dokumen</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Pengaturan Pengguna
                        </li>
                        <li class="breadcrumb-item">
                            Unggah Dokumen
                        </li>
                        <li class="breadcrumb-item">
                            <a href="DokumentList.aspx"><strong>Semua Dokumen</strong></a>
                        </li>
                        
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <h5>Semua Dokumen </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                </div>
                <div class="ibox-content">
                    <div>
                        <div id="errordiv" class="alert alert-danger" runat="server"></div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <a href="UploadDokumen.aspx" class="btn btn-primary"><i class="fa fa-plus"></i> <span class="add_new_text">Buat Baru</span></a>
                        </div>
                        
                    </div>
                    <div class="row pt-3">
                        <div class="col-lg-12">
                            <asp:GridView ID="GridViewDokumen" runat="server" AutoGenerateColumns="false" AllowPaging="true" OnPageIndexChanging="GridViewDokumen_PageIndexChanging" CssClass="table table-bordered table-striped">
                                <Columns>
                                    <asp:TemplateField HeaderText="No">   
                                         <ItemTemplate>
                                               <b>  <%# Container.DataItemIndex + 1 %>  </b>  
                                         </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="URL File">   
                                        <ItemTemplate>
                                            <a href='<%#Eval("file_url") %>' target="_blank"><%#Eval("file_url") %></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Keterangan">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" ForeColor="#3568ff" runat="server" Text='<%#Bind("Keterangan") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Hapus">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn btn-danger" CommandArgument='<%# Eval("ID") %>' OnClick="DeleteDocument" runat="server"><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>
