﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
namespace PIS_Backend
{
    public class Armada
    {
        SqlConnection con = Connection.conn();

        public string tableName = "Armada";

        string viewName = "View_Armada";

        DateTime dateNow = DateTime.Now;

        public int PageSizes = 11;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        public string title;

        public string content;

        public string img_file;

        public string Title { get { return title; } set { title = value; } }

        public string Content { get { return content; } set { content = value; } }

        public string Img_File { get { return img_file; } set { img_file = value; } }

        public string validate()
        {
            if(title == "")
            {
                return "title must be filled";
            }
            return "valid";
        }
        public string Insert()
        {
            try
            {
                string valid = validate();

                if (valid != "valid")
                {
                    return valid;
                }

                string field = "(title, content, uuid, img_file, addDate, addUser)";
                string value = "( '" + title + "','" + content + "',NEWID(),'" + img_file + "','" + dateNow + "'," + user_id + ")";

                string Query = "INSERT INTO " + tableName + " " + field + " VALUES " + value;

                Db.setData(Query);

                return "success";
            }
            catch(Exception ex)
            {
                return ex.Message.ToString();
            }
            

            
        }
        public DataTable Select(string field, string conditions = null)
        {
            string cond = conditions != null ? conditions : "";
            string Query = Db.SelectQuery(field, viewName, cond);
            DataTable dt = Db.getData(Query);

            return dt;
        }
        //public string Update(string conditions = null)
        //{
        //    try
        //    {
        //        string valid = validate();
        //        if(valid != "valid")
        //        {
        //            return valid;
        //        }
        //        string newRecord = "title = '" + title + "', ";
        //        newRecord += "content = '" + content + "', ";
        //        newRecord += img_file != "" ? "img_file = '" + img_file + "', " : "";
        //        conditions = conditions != null ? conditions : "";

        //        Db.Update(tableName, newRecord, dateNow, "'" + user_id + "'", conditions);

        //        return "success";
        //    }
        //    catch(Exception ex)
        //    {
        //        return ex.Message.ToString();
        //    }
        //}
        public DataTable GetAll(int PageIndex, int pageSize = 0)
        {
            SqlCommand cmd = new SqlCommand("GetAllArmada", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PageIndex", PageIndex);
            pageSize = pageSize != 0 ? pageSize : PageSizes;
            cmd.Parameters.AddWithValue("@PageSize", pageSize);

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public DataTable getLatest()
        {
            SqlCommand cmd = new SqlCommand("GetLatestArmada", con);
            cmd.CommandType = CommandType.StoredProcedure;


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        //public void Delete(string conditions = null)
        //{
        //    conditions = conditions == null ? "" : conditions;
        //    Db.Delete(tableName, dateNow, user_id, conditions);
        //}
    }
}