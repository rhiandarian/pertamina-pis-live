﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class admin_edit_gallery : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                //if (Session["Level"].ToString() != "1")
                //{
                //    Response.Redirect("Index.aspx");
                //}
                LoadCategory();
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadData();
            }
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void LoadCategory()
        {
            Gallery g = new Gallery();

            DataSet dt = g.LoadCategory(2);

            DdlCategory.DataTextField = dt.Tables[0].Columns["Category_name"].ToString();
            DdlCategory.DataValueField = dt.Tables[0].Columns["ID"].ToString();
            DdlCategory.DataSource = dt.Tables[0];
            DdlCategory.DataBind();
            DdlCategory.Items.Insert(0, "Pilih Category");
            DdlCategory.SelectedIndex = 0;
        }
        public void LoadData()
        {
            Gallery g = new Gallery();
            if (Request.QueryString["ID"] == null)
            {
                Response.Redirect(g.adminListPage);
            }
            string pageName = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
            string pageID = g.findPageAdminID(pageName);
            Level l = new Level();
            if (!l.isValidAccessPage(Session["Level"].ToString(), pageID))
            {
                Response.Redirect("admin.aspx");
            }
            int Id = Convert.ToInt32(Request.QueryString["ID"]);
            string field = "title, title_en, content, content_en, video_url, Dates, img_url, img_file, embed_url, category_id";
            string conditions = " ID = " + Id;

            DataTable dt = g.Select(field, conditions);
            title.Text = dt.Rows[0]["title"].ToString();
            title_en.Text = dt.Rows[0]["title_en"] != null ? dt.Rows[0]["title_en"].ToString() : dt.Rows[0]["title_en"].ToString();
            if (dt.Rows[0]["Dates"].ToString() != "")
            {
                DateTime dates = Convert.ToDateTime(dt.Rows[0]["Dates"].ToString());
                date.Text = dates.ToString("MM/dd/yyyy");
            }

            DdlCategory.SelectedValue = dt.Rows[0]["category_id"].ToString();
            if (Convert.ToInt32(dt.Rows[0]["category_id"].ToString()) != all.FotoCategoryID)
            {
                LinkButtonUploadAlbum.Visible = false;
            }

            DdlCategory.Enabled = false;
            if (dt.Rows[0]["category_id"].ToString() == all.VideoCategoryID.ToString())
            {
                PanelVideo.Visible = true;
                PanelImage.Visible = false;
                content.InnerHtml = dt.Rows[0]["content"].ToString();
                content_en.InnerHtml = dt.Rows[0]["content_en"] != null ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content_en"].ToString();
                videoUrl.Text = dt.Rows[0]["video_url"].ToString();
            

            }
            else
            {
                PanelImage.Visible = true;
                PanelVideo.Visible = false;
                Image1.Attributes["src"] = dt.Rows[0]["img_file"].ToString();
            }
        }
        public string randomTxt()
        {
            ImageResize ir = new ImageResize();
            return ir.textrandom();
        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string random = randomTxt();
                Gallery g = new Gallery();
                g.Title = title.Text;
                g.TitleEn = title_en.Text;
                g.Date = Convert.ToDateTime(date.Text);
                if (DdlCategory.SelectedValue != "Pilih Category")
                {

                    g.Category = Convert.ToInt32(DdlCategory.SelectedValue);
                }


                bool fileExsist = false;
                bool file2Exsist = false;

                if (g.Category == all.VideoCategoryID)
                {
                    g.Content = content.InnerText;
                    g.ContentEn = content_en.InnerText;
                    g.Video_url = videoUrl.Text;
                    if ((FileUpload2.PostedFile != null) && (FileUpload2.PostedFile.ContentLength > 0))
                    {
                        g.Img_url =  random + FileUpload2.FileName;
                        file2Exsist = true;
                        g.FileCoverSize = FileUpload2.PostedFile.ContentLength;
                    }

                }
                else
                {
                    if (DdlCategory.SelectedValue != "Pilih Category")
                    {
                        if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
                        {
                            g.Img_File = random + FileUpload1.FileName;
                            g.FileImageSize = FileUpload1.PostedFile.ContentLength;
                            fileExsist = true;
                        }
                        
                    }
                }
                int Id = Convert.ToInt32(Request.QueryString["ID"]);
                string conditions = " ID = " + Id;
                string UpdateStatus = g.Update(DdlCategory.SelectedValue,Id,getip(),GetBrowserDetails(), conditions);
                if (UpdateStatus == "success")
                {
                    if (fileExsist)
                    {
                        string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                        string SetFolderBeforeResize = Server.MapPath("Gallerry") + "\\" + "serize" + random + fn;
                        string SetFolderAfterResize = Server.MapPath("Gallerry") + "\\" + random + fn;
                        FileUpload1.PostedFile.SaveAs(SetFolderBeforeResize);
                        ImageResize Iz = new ImageResize();
                        Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "Gallery", "Cut");
                    }
                    if(file2Exsist)
                    {
                        string fn2 = System.IO.Path.GetFileName(FileUpload2.PostedFile.FileName);
                        string SetFolderBeforeResize2 = Server.MapPath("Gallerry") + "\\" + "serize" + random + fn2;
                        string SetFolderAfterResize2 = Server.MapPath("Gallerry") + "\\" + random + fn2;
                        FileUpload2.PostedFile.SaveAs(SetFolderBeforeResize2);
                        ImageResize Iz2 = new ImageResize();
                        Iz2.ResizeImage(SetFolderBeforeResize2, SetFolderAfterResize2, "Gallery", "Cut");
                    }
                    redirectToListPage();
                }
                else
                {
                    showValidationMessage(UpdateStatus);
                    LoadData();
                }
            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
            
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = Convert.ToInt32(Request.QueryString["ID"]);
            Response.Redirect("admin-admin-list-album?GalleryID=" + Id);
        }

        protected void lbIndex_Click(object sender, EventArgs e)
        {
            redirectToListPage();
        }
        public void redirectToListPage()
        {
            string pageName = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
            Gallery g = new Gallery();
            Response.Redirect(g.ListPageGallery(pageName));
        }
    }
}