﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIS_Backend
{
    public class Product
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
    }
}