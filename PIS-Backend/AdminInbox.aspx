﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminInbox.aspx.cs" Inherits="PIS_Backend.AdminInbox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Pesan Masuk</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Menu Lainnya
                        </li> 
                        <li class="breadcrumb-item active">
                            <a href="AdminInbox.aspx"><strong>Semua Pesan Masuk</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Semua Pesan Masuk </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        
                        <div class="ibox-content">
                            <%--<div class="row">
                                <a href="SaveLayanan.aspx" class="btn btn-primary"><i class="fa fa-plus"></i><span class="add_new_text">Buat Baru</span></a>
                            </div>--%>
                            <div class="row pt-3">
                                <asp:GridView ID="GridViewLayanan" runat="server" AutoGenerateColumns="false" AllowPaging="true" OnPageIndexChanging="GridViewLayanan_PageIndexChanging" CssClass="table table-striped table-bordered table-hover dataTables-example">
                                   <Columns>
                                       <asp:TemplateField HeaderText="No" >   
                                             <ItemTemplate>
                                                    <b> <%# Container.DataItemIndex + 1 %> </b>  
                                             </ItemTemplate>
                                        </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Nama">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" ForeColor="#3568ff" runat="server" Text='<%#Bind("nama") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Judul">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" ForeColor="#3568ff" runat="server" Text='<%#Bind("judul") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                         <asp:TemplateField HeaderText="Email">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" ForeColor="#3568ff" runat="server" Text='<%#Bind("email") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Telepon">
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" ForeColor="#3568ff" runat="server" Text='<%#Bind("no_telp") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Institusi">
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" ForeColor="#3568ff" runat="server" Text='<%#Bind("institusi") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Isi Pesan">
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" ForeColor="#3568ff" runat="server" Text='<%#Bind("isi_pesan") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Tanggal">
                                            <ItemTemplate>
                                                <asp:Label ID="Label7" ForeColor="#3568ff" runat="server" Text='<%#Bind("created_at") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Aksi">
                                            <ItemTemplate> 
                                                <asp:LinkButton ID="Label8" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Bind("id") %>' OnClick="Delete_Click" runat="server">Delete <i class="fa fa-trash"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                   </Columns>
                                </asp:GridView>
                               
                                
                            </div>
                            <%--<a href="contacts.aspx" target="_blank"><i class="fa fa-eye"></i> Lihat hasil di Website </a>--%>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
</asp:Content>
