﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace PIS_Backend
{
    public class News:ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName = "Berita";
        public string viewName = "View_News";

        public string EnPage = "news";
        public string IndPage = "berita";

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        public int PageSize = 5;

        private int id;

        private string uuid;

        private string title;

        private string title_en;

        private string city;

        private string content;

        private string status;

        private string content_en;

        private string img_file;

        private string key;

        private string key_en;

        private string category_name;

        private int category_id;

        public DateTime date;

        private string dates;


        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string UUID
        {
            get { return uuid; }
            set { uuid = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string TitleEn
        {
            get { return title_en; }
            set { title_en = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }
        public string Categories
        {
            get { return category_name; }
            set { category_name = value; }
        }

        public string Content
        {
            get { return content; }
            set { content = value; }
        }
        public string Key
        {
            get { return key; }
            set { key = value; }
        }
        public string KeyEn
        {
            get { return key_en; }
            set { key_en = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        public string ContentEn
        {
            get { return content_en; }
            set { content_en = value; }
        }



        public string Img_File
        {
            get { return img_file; }
            set { img_file = value; }
        }
        public int Category_Id
        {
            get { return category_id; }
            set { category_id = value; }
        }
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }
        public string Dates
        {
            get { return dates; }
            set { dates = value; }
        }


        public DataTable Insert(string mainIP, string browserDetail)
        {
            string field = "";
            string value = "";

            if (title != null)
            {
                field += "title, ";
                value += "'" + title + "', ";
            }
            if (title_en != null)
            {
                field += "title_en, ";
                value += "'" + title_en + "', ";
            }
            if (content != null)
            {
                field += "content, ";
                value += "'" + content + "', ";
            }
            if (content_en != null)
            {
                field += "content_en, ";
                value += "'" + content_en + "', ";
            }
            if(key != null)
            {
                value += "'" + key.Replace(" ","-") + "', ";
            }
            if(key_en != null)
            {
                value += "'" + key_en.Replace(" ", "-") + "', ";
            }
            if (date != null)
            {
                field += "Dates, ";
                value += "'" + date + "', ";
            }
            
            if (img_file != null)
            {
                field += "img_file, ";
                value += "'" + img_file + "', ";
            }
            if (category_id != 0)
            {
                field += "category_id, ";
                value += category_id + ", ";
            }
            if(status != null)
            {
                field += "status, ";
                value += "'" + status + "',";
            }
             value += user_id+", '"+mainIP+"', '"+browserDetail+"', '"+ DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'";

            DataTable dt = Db.Sp_Result("SaveNews",value);

            return dt;

        }
        public DataTable Update(int ID,string mainIP, string getBrowser)
        {
           

            string newRecord = " '" + title + "',  " ;
            newRecord +=  " '" + title_en + "',  " ;
            newRecord +=  " '" + content + "',  " ;
            newRecord +=  " '" + content_en + "',  " ;
            newRecord += " '" + key.Replace(" ", "-") + "',  ";
            newRecord += " '" + key_en.Replace(" ", "-") + "',  ";
            newRecord += " '" + date + "', " ;
            newRecord +=  " '" + img_file + "',  " ;
            newRecord +=  " " + category_id + ",  " ;
            newRecord +=  " '" + status + "', ";
            newRecord += user_id + ", ";
            newRecord += " '" + mainIP + "', ";
            newRecord += " '" +getBrowser + "', ";
            newRecord += " '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
            newRecord += ID;


            DataTable dt = Db.Sp_Result("SaveNews", newRecord);

            return dt;
        }
        public DataTable Select(string field, string conditions = null)
        {

            conditions = conditions != null ? " Where " + conditions : "";


            string query = "Select " + field + " from " + viewName + " " + conditions;

            DataTable dt = Db.getData(query);

            return dt;
        }
        public DataTable LoadNews()
        {
            SqlCommand cmd = new SqlCommand("LoadNews", con);
            cmd.CommandType = CommandType.StoredProcedure;
            
            
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public DataTable GetAllNews(int PageIndex, string lang = null, int category = 0)
        {
            lang = lang == null ? "Ind" : lang;
            SqlCommand cmd = new SqlCommand("GetAllNews", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PageIndex", PageIndex);
            cmd.Parameters.AddWithValue("@PageSize", PageSize);
            cmd.Parameters.AddWithValue("@Lang", lang);
            if(category != 0)
            {
                cmd.Parameters.AddWithValue("@Category", category);
            }


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;

        }
        public DataTable CountAllCategory(string lang)
        {
            SqlCommand cmd = new SqlCommand("DetailNews", con);
            cmd.Parameters.AddWithValue("@lang", lang);
            cmd.CommandType = CommandType.StoredProcedure;
            


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public DataTable getLatestNews(string lang = null)
        {
            lang = lang == null ? "Ind" : lang;
            SqlCommand cmd = new SqlCommand("GetLatestNews", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@limit", 4);
            cmd.Parameters.AddWithValue("@lang", lang);

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public void Delete(string id,string mainIP,string browserDetail,string conditions = null)
        {
            
            string query = "Update " + tableName + " set fl_deleted = 1, deleteDate = '" + dateNow + "', deleteUser = " + user_id;
            query += conditions != null ? "Where " + conditions : "";
            query += logHistoryQuery(mainIP, browserDetail, getUserName(user_id)+" Delete " + tableName + " Id " + Convert.ToInt32(id), Convert.ToInt32(user_id), "Success");
            Db.setData(query);

        }
        public DataTable LoadData(string txtSearch = null)
        {


            SqlCommand cmd = new SqlCommand("LoadBerita", con);
            cmd.CommandType = CommandType.StoredProcedure;
            if (txtSearch != null)
            {
                cmd.Parameters.AddWithValue("@txtSearch", txtSearch);
            }
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
    }
}