﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class admin_pageRUPS : System.Web.UI.Page
    {
        string folderHeader = "Header";
        string folderDownload = "documents";
        public string PageUrl;
        General all = new General();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "25"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                successdiv.Visible = false;
                LoadData();

            }
        }
        public void LoadData()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 22 ");
            content.InnerHtml = dt.Rows[0]["content"].ToString();
            PageUrl = dt.Rows[0]["url"].ToString();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                PagesData pd = new PagesData();
                string validation = "";
                bool HeaderExsist = false;
                bool validheader = true; 
                if (FileUpload1.FileName != "")
                {
                    HeaderExsist = true;
                    pd.Header = folderHeader + "/RUPS-" + FileUpload1.FileName;
                    if (!all.ImageFileType(FileUpload1.FileName))
                    {
                        validheader = false;
                    }

                }
                pd.Content = content.InnerText;
                if (validheader == false)
                {
                    validation = "Header must be Image";
                }

                if (validation == "")
                {
                    if (HeaderExsist == true)
                    {
                        UploadFile(folderHeader, FileUpload1.PostedFile.FileName);
                    }
                    pd.Save(" ID = 22");
                    successMessage("Page RUPS Has Been updated");
                }
                else
                {
                    showValidationMessage(validation);
                }

            }
            catch (Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
        }
        public void showValidationMessage(string textMessage)
        {
            successdiv.Visible = false;
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void successMessage(string textMessage)
        {
            LblErrorMessage.Visible = false;
            successdiv.InnerHtml = textMessage;
            successdiv.Visible = true;
        }
        public void UploadFile(string folderName, string fileUpload)
        {

            string fn = System.IO.Path.GetFileName(fileUpload);
            string SaveLocation = Server.MapPath(folderName) + "\\" + "RUPS-" + fn;

            FileUpload1.PostedFile.SaveAs(SaveLocation);
        }
    }
}