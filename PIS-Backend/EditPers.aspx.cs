﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;
namespace PIS_Backend
{
    public partial class EditPers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadData();

            }
            
        }
        public bool ImageFileType(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            switch (ext.ToLower())
            {
                case ".gif":
                    return true;
                case ".jpg":
                    return true;
                case ".jpeg":
                    return true;
                case ".png":
                    return true;
                default:
                    return false;
            }
        }
        public void LoadData()
        {
            if (Request.QueryString["ID"] == null)
            {
                Response.Redirect("SiaranPers.aspx");
            }
            int Id = Convert.ToInt32(Request.QueryString["ID"]);
            string select = "title, content, img_file, city";
            string conditions = "ID = " + Id;
            Pers prs = new Pers();
            DataTable dt = prs.Select(select,conditions);

            title.Text = dt.Rows[0]["title"].ToString();
            content.InnerHtml = dt.Rows[0]["content"].ToString();
            city.Text = dt.Rows[0]["city"].ToString();
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            bool exsist = false;
            bool validUpload = true;
            Pers prs = new Pers();

            if (title.Text != "")
            {
                prs.Title = title.Text;
                exsist = true;
            }
            if (content.InnerHtml != "")
            {
                prs.Content = content.InnerHtml;
                exsist = true;
            }
            if(city.Text != "")
            {
                prs.City = city.Text;
                exsist = true;
            }
            if (FileUpload1.FileName != "")
            {
                prs.Img_File = "Pers/" + FileUpload1.FileName;
                exsist = true;
                string SaveLocation = "";
                if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
                {
                    string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                    SaveLocation = Server.MapPath("Pers") + "\\" + fn;
                    try
                    {
                        if (ImageFileType(FileUpload1.FileName))
                        {
                            FileUpload1.PostedFile.SaveAs(SaveLocation);
                        }
                        else
                        {
                            validUpload = false;
                            LblErrorMessage.Visible = true;
                            LblErrorMessage.Text = "File Must be Image";
                        }





                    }
                    catch (Exception ex)
                    {
                        validUpload = false;
                        LblErrorMessage.Visible = true;
                        LblErrorMessage.Text = ex.Message;
                    }
                }
            }
            if (exsist)
            {
                if (validUpload)
                {
                    int Id = Convert.ToInt32(Request.QueryString["ID"]);
                    string conditions = "ID = " + Id;
                    prs.Update(conditions);

                    Response.Redirect("SiaranPers.aspx");
                }
            }
            else
            {
                Response.Redirect("SiaranPers.aspx");
            }
        }
    }
}