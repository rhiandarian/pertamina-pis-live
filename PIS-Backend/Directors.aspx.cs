﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System;

namespace PIS_Backend
{
    public partial class Directors : System.Web.UI.Page
    {
        int ptid = 2;
        string IndPage = "Direksi.aspx";
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDireksi();
                BindPage();
                if (Session["Lang"] == null || Session["Lang"].ToString() == "Ind")
                {
                    Response.Redirect(IndPage);
                }
                bindMenu();
                loadfooter();
            }
        }
        public void bindMenu()
        {
            string fullPath = Request.Url.AbsolutePath;
            string pageName = System.IO.Path.GetFileName(fullPath);
            string currPage = pageName + ".aspx";
            string lang = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "En" : "Ind";

            List<Menu> mn = all.GenerateMenu(lang, currPage);
            ListViewMenu.DataSource = mn;
            ListViewMenu.DataBind();

            List<Menu> ot = all.MenuOthers(lang);
            ListViewOthers.DataSource = ot;
            ListViewOthers.DataBind();

            ListViewResponsive.DataSource = ot;
            ListViewResponsive.DataBind();

            string otmn = "Others";
            othermenu.Attributes["class"] = all.getOtherMenuClass(othermenu.Attributes["class"], currPage);
            othermenu.InnerHtml = "<a href=" + all.stripped("") + " onclick=" + all.stripped("openNavOverlay()") + " id=" + all.stripped("nav-right-option") + " >" + otmn + " <span class=" + all.stripped("") + "></span></a>";
            langMobile.InnerHtml = all.changeLangMobile("En");
        }
        public void loadfooter()
        {
            ModelData Db = new ModelData();
            string query = "Select * from  Contact  where ID = 1";
            DataTable dt = Db.getData(query);
            address.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["address_en"].ToString() : dt.Rows[0]["address"].ToString(); ;
            phone.InnerHtml = dt.Rows[0]["phone"].ToString();
            email.InnerHtml = dt.Rows[0]["email"].ToString();
            link_fb.HRef = dt.Rows[0]["link_fb"].ToString();
            link_tw.HRef = dt.Rows[0]["link_tw"].ToString();
            link_ig.HRef = dt.Rows[0]["link_ig"].ToString();
            link_yt.HRef = dt.Rows[0]["link_yt"].ToString();
        }
        protected void ChangeLang(object sender, EventArgs e)
        {
            if (Session["Lang"] == null)
            {
                Session.Add("Lang", "Ind");
            }
            else
            {
                Session["Lang"] = "Ind";
            }
            Response.Redirect(IndPage);
        }
        public void BindDireksi()
        {
            string lang = "En";
            Person p = new Person();
            DataTable dt = p.allDirectors();
            List<Person> dirut = p.getUtama(dt, lang);
            ListViewDirut.DataSource = dirut;
            ListViewDirut.DataBind();
            List<Person> alldirektur = p.getAll(dt, lang);
            ListViewDireksi.DataSource = alldirektur;
            ListViewDireksi.DataBind();
            ListViewModalDireksi.DataSource = dt;
            ListViewModalDireksi.DataBind();
        }
        public void BindPage()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 14 ");
            headerImage.Src = dt.Rows[0]["Header"].ToString();
            content.InnerHtml = dt.Rows[0]["content_en"].ToString();
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(all.search + "" + txtSearch.Value);
        }
    }
}