﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class tata_kelola_perusahaan : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindPage();
                bindMenu();
                loadfooter();
            }
        }
        public void bindMenu()
        {
            string fullPath = Request.Url.AbsolutePath;
            string pageName = System.IO.Path.GetFileName(fullPath);
            string currPage = pageName + ".aspx";
            //Response.Write(all.alert(currPage));
            string lang = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "En" : "Ind";

            List<Menu> mn = all.GenerateMenu(lang, currPage);
            ListViewMenu.DataSource = mn;
            ListViewMenu.DataBind();

            List<Menu> ot = all.MenuOthers(lang,currPage);
            ListViewOthers.DataSource = ot;
            ListViewOthers.DataBind();

            ListViewResponsive.DataSource = ot;
            ListViewResponsive.DataBind();

            string otmn = lang == "En" ? "Others" : "Menu Lainnya";
            othermenu.Attributes["class"] = all.getOtherMenuClass(othermenu.Attributes["class"], currPage);
            othermenu.InnerHtml = "<a href=" + all.stripped("") + " onclick=" + all.stripped("openNavOverlay()") + " id=" + all.stripped("nav-right-option") + " >" + otmn + " <span class=" + all.stripped("") + "></span></a>";
            langMobile.InnerHtml = all.changeLangMobile(lang, true);
            string nama = lang == "En" ? "Nama_En as Nama" : "Nama";
            
            SistemManajemen SI = new SistemManajemen();
            DataTable dt = SI.Select(nama+", img_file");
            ListViewSO.DataSource = dt;
            ListViewSO.DataBind();

        }
        public void loadfooter()
        {
          
            
            kantor.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "HEAD OFFICE" : kantor.InnerHtml; 
            findus.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "FIND US" : findus.InnerHtml;

            perusahaan.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "LINKS" : perusahaan.InnerHtml;
            pt2.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Ministry of State Owned Enterprises" : pt2.InnerHtml;
            pt3.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Ministry of Energy and Mineral Resources" : pt3.InnerHtml;
            pt4.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Ministry of Transportation " : pt4.InnerHtml;
            pt5.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "BKPM" : pt5.InnerHtml;

            ModelData Db = new ModelData();
            string query = "Select * from  Contact  where ID = 1";
            DataTable dt = Db.getData(query);
            address.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["address_en"].ToString() : dt.Rows[0]["address"].ToString(); ;
            phone.InnerHtml = dt.Rows[0]["phone"].ToString();
            email.InnerHtml = dt.Rows[0]["email"].ToString();
            link_fb.HRef = dt.Rows[0]["link_fb"].ToString();
            link_tw.HRef = dt.Rows[0]["link_tw"].ToString();
            link_ig.HRef = dt.Rows[0]["link_ig"].ToString();
            link_yt.HRef = dt.Rows[0]["link_yt"].ToString();
        }
        public void setLang(string lang)
        {
            if (Session["Lang"] == null)
            {
                Session.Add("Lang", lang);
            }
            else
            {
                Session["Lang"] = lang;
            }

            string url = "tata-kelola-perusahaan.aspx";
            Response.Redirect(url);

        }
        protected void LangInd_Click(object sender, EventArgs e)
        {
            setLang("Ind");
        }
        protected void LangEn_Click(object sender, EventArgs e)
        {
            setLang("En");
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"].ToString() == "En")
                {
                    LinkButton1.CssClass = LinkButton1.CssClass.Replace("active", "");
                    
                    LinkButton2.CssClass = "active";
                }
                else
                {
                    LinkButton2.CssClass = LinkButton2.CssClass.Replace("active", "");

                    LinkButton1.CssClass = "active";
                }

            }


        }
        public void BindPage()
        {
            editClassLang();

            link1.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Commitment & Sustainibility" : link1.InnerText;
            link2.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Good Corporate Governance (GCG)" : link2.InnerText;
            title.InnerText = link2.InnerText.ToUpper();

            BtnSearch.Text = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Search" : BtnSearch.Text;

            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 21 ");
            imgHeader.Src = dt.Rows[0]["Header"].ToString();
            content.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content"].ToString();
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(all.search + "" + txtSearch.Value);
        }
    }
}