﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class AdminDireksi : System.Web.UI.Page
    {
        string SavePage = "SaveDireksi.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "17"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindDireksi();
            }
        }
        public void BindDireksi()
        {
            Person p = new Person();
            string cond = "personTypeID = 2";
            DataTable dt = p.Select("*", cond);

            ListViewDireksi.DataSource = dt;
            ListViewDireksi.DataBind();
        }
        protected void EditClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Response.Redirect(SavePage + "?ID=" + id);
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }

        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Person p = new Person();
            string cond = " ID = " + id;
            try
            {
                p.Delete(id,getip(),GetBrowserDetails(),cond);
                BindDireksi();
            }
            catch(Exception ex)
            {
                General all = new General();
                p.ErrorLogHistory(getip(), GetBrowserDetails(),"Delete", p.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                Response.Write(all.alert("Error " + ex.Message.ToString()));
            }

        }
    }
}