﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class admin_edit_keberlanjutan : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "28"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadData();
            }
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        } 
        public void LoadData()
        {
            if (Request.QueryString["ID"] == null)
            {
                Response.Redirect("admin_listkeberlanjutan.aspx");
            }
            int Id = Convert.ToInt32(Request.QueryString["ID"]);
            Gallery g = new Gallery();
            string field = "title,title_en,Dates, content, video_url, img_url, embed_url, category_id";
            string conditions = " ID = " + Id;

            DataTable dt = g.Select(field, conditions);
            title.Text = dt.Rows[0]["title"].ToString();
            title_en.Text = dt.Rows[0]["title_en"].ToString();
            if (dt.Rows[0]["Dates"].ToString() != "")
            {   
                DateTime dates = Convert.ToDateTime(dt.Rows[0]["Dates"].ToString());
                date.Text = dates.ToString("MM/dd/yyyy");
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string randomtext = textrandom();
                General gn = new General();
                Gallery g = new Gallery();
                g.Title = title.Text;
                g.TitleEn = title_en.Text;
                //g.Date = DateTime.Now;
                g.Date = Convert.ToDateTime(date.Text);
                g.Category = gn.CategoryKeberlanjutanID;
                bool fileExsist = false;
                if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
                        {
                            g.Img_File = randomtext + FileUpload1.FileName;
                            fileExsist = true;
                        } 
                int Id = Convert.ToInt32(Request.QueryString["ID"]);
                string conditions = " ID = " + Id;
                string UpdateStatus = g.Update(gn.CategoryKeberlanjutanID.ToString(), Id,getip(),GetBrowserDetails(),conditions);
                if (UpdateStatus == "success")
                {
                    if (fileExsist)
                    {
                        string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                        string SetFolderBeforeResize = Server.MapPath("Gallerry") + "\\" + "serize" + fn;
                        string SetFolderAfterResize = Server.MapPath("Gallerry") + "\\" + randomtext + fn;
                        FileUpload1.PostedFile.SaveAs(SetFolderBeforeResize);
                        ImageResize Iz = new ImageResize();
                        Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "Gallery", "Cut");
                    }
                    Response.Redirect("admin_listkeberlanjutan.aspx");
                }
                else
                {
                    showValidationMessage(UpdateStatus);
                    LoadData();
                }
            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
            
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = Convert.ToInt32(Request.QueryString["ID"]);
            Response.Redirect("AlbumImagesKeberlanjutan.aspx?GalleryID=" + Id);
        }

        public string textrandom()
        {
            Random RNG = new Random();
            int length = 6;
            var rString = "";
            for (var i = 0; i < length; i++)
            {
                rString += ((char)(RNG.Next(1, 26) + 64)).ToString().ToLower();
            }
            return rString;
        }
    }
}