﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace PIS_Backend
{
    public partial class AdminPengadaan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "29"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindPengadaan();
            }
        }
        public void BindPengadaan()
        {
            InfoPengadaan ip = new InfoPengadaan();
            ip.OrderBy = " addDate Desc";
            DataTable dt = ip.Select("*");
            ListViewPengadaan.DataSource = dt;
            ListViewPengadaan.DataBind();
        }
        protected void EditClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            InfoPengadaan ip = new InfoPengadaan();
            Response.Redirect(ip.savePage + "?ID=" + id);
        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            int Idnum = Convert.ToInt32(id);
            InfoPengadaan ip = new InfoPengadaan();
            string cond = " ID = " + id;
            try
            {
                ip.Delete(Idnum, getip(), GetBrowserDetails(), cond);
            }
            catch(Exception ex)
            {
                General all = new General();
                ip.ErrorLogHistory(getip(),GetBrowserDetails(),"Delete",ip.tableName,Session["Username"].ToString(), Session["UserID"].ToString(),ex.Message.ToString());
                Response.Write(all.alert(ex.Message.ToString()));
            }
            
            BindPengadaan();
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void ListViewPengadaan_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (ListViewPengadaan.FindControl("DataPager1") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindPengadaan();
        }
    }
}