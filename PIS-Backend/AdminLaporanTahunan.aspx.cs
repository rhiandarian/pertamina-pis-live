﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace PIS_Backend
{
    public partial class AdminLaporanTahunan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "21"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindLaporan();
            }
        }
        public void BindLaporan()
        {
            LaporanTahunan lt = new LaporanTahunan();
            lt.OrderBy = " addDate Desc";
            DataTable dt = lt.Select("*");
            ListViewLaporanTahunan.DataSource = dt;
            ListViewLaporanTahunan.DataBind();
        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            LaporanTahunan lt = new LaporanTahunan();
            string cond = " ID = " + id;
            General all = new General();
            try
            {
                lt.Delete(id,getip(),GetBrowserDetails(),cond);
                BindLaporan();
            }
            catch(Exception ex)
            {
                lt.ErrorLogHistory(getip(), GetBrowserDetails(), "Delete", lt.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                Response.Write(all.alert("Error " + ex.Message.ToString()));
            }
            
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void ListViewLaporanTahunan_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (ListViewLaporanTahunan.FindControl("DataPager1") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindLaporan();
        }
    }
}