﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class IndexTemp : System.Web.UI.Page
    {
        General all = new General();
        string EnPage = "En";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Lang"] != null && Session["Lang"].ToString() == "En")
            {
                Response.Redirect(EnPage);
            }

            if (!IsPostBack)
            {
                bindMenu();
               
                //LoadLayanan();
                LoadNews();
                //LoadGallery();
                //LoadVideos();

            //    LoadKapal();
                //BindPengadaan();

                loadfooter();





            }
        }
        public void bindMenu()
        {
            string currPage = "Index.aspx";
            string lang = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "En" : "Ind";

            List<Menu> mn = all.GenerateMenu(lang, currPage);
            ListViewMenu.DataSource = mn;
            ListViewMenu.DataBind();

            List<Menu> ot = all.MenuOthers(lang);
            ListViewOthers.DataSource = ot;
            ListViewOthers.DataBind();

            ListViewResponsive.DataSource = ot;
            ListViewResponsive.DataBind();

            string otmn = lang == "En" ? "Others" : "Menu Lainnya";
            othermenu.Attributes["class"] = all.getOtherMenuClass(othermenu.Attributes["class"], currPage);
            othermenu.InnerHtml = "<a href=" + all.stripped("") + " onclick=" + all.stripped("openNavOverlay()") + " id=" + all.stripped("nav-right-option") + " >" + otmn + " <span class=" + all.stripped("") + "></span></a>";
            langMobile.InnerHtml = all.changeLangMobile("Ind");
        }
        public List<News> getNews(DataTable dt, int i)
        {
            List<News> n1 = new List<News>();
            n1.Add(new News()
            {
                UUID = dt.Rows[i]["uuid"].ToString(),
                Img_File = dt.Rows[i]["img_file"].ToString(),
                Dates = dt.Rows[i]["news_date"].ToString(),
                Categories = dt.Rows[i]["Category_name"].ToString(),
                Title = dt.Rows[i]["title"].ToString(),
                Key = dt.Rows[i]["Key"].ToString()
            });

            return n1;
        }
        protected void ChangeLang(object sender, EventArgs e)
        {
            if (Session["Lang"] == null)
            {
                Session.Add("Lang", "En");
            }
            else
            {
                Session["Lang"] = "En";
            }
            Response.Redirect(all.IndexEnPage);
        }
        //public void LoadLayanan()
        //{
        //    Layanan ly = new Layanan();
        //    DataTable dt = ly.Select(" TOP 3 *");
        //    ListViewLayanan.DataSource = dt;
        //    ListViewLayanan.DataBind();
        //}
        //public void LoadBanner()
        //{
        //    Banner bnr = new Banner();
        //    DataTable dt = bnr.Select("*");
        //    ListViewBanner.DataSource = dt;
        //    ListViewBanner.DataBind();
        //}
        public void LoadNews()
        {
            News n = new News();
            DataTable dt = n.LoadNews();

            List<News> n1 = getNews(dt, 0);

            ListViewNews1.DataSource = n1;
            ListViewNews1.DataBind();



            List<News> n2 = getNews(dt, 1);
            ListViewNews2.DataSource = n2;
            ListViewNews2.DataBind();




            List<News> n3 = getNews(dt, 2);
            ListViewNews3.DataSource = n3;
            ListViewNews3.DataBind();





        }
        //public string contentCard(string imgSrc, string newsDate, string Category, string title)
        //{
        //    string image = "<img src=" + all.stripped(imgSrc) + " class=" + all.stripped("card - img") + " alt=" + all.stripped("") + " width=" + all.stripped("100%") + ">";
        //    string dateandtitle = "<em>" + newsDate + "</em> &nbsp;<span class=" + all.stripped("badge badge-danger") + ">" + Category.ToUpper() + "</span><br>" + title;
        //    string paragraph = all.paragraph(dateandtitle, "", "card-content p-4");

        //    string cardimgOverlay = all.div(paragraph, "", "card-img-overlay bg-black-transparent");



        //    return all.div(image + " " + cardimgOverlay, "", "card text-white overflow-hidden media-information-box card-block");

        //}
        //public string mediaInfoRight(string imgSrc, string newsDate, string Category, string title)
        //{
        //    return all.div(contentCard(imgSrc, newsDate, Category, title), "", "col-12 pl-0 pr-0 mb-4 media-information-box-right");
        //}
        //public void LoadVideos()
        //{
        //    Gallery g = new Gallery();

        //    DataTable dt = g.ShowVideoProfile("Ind");
        //    ListViewVideo.DataSource = dt;
        //    ListViewVideo.DataBind();


        //}

        public void DownloadPengadaan(object sender, EventArgs e)
        {
            InfoPengadaan p = new InfoPengadaan();
            LinkButton lb = (LinkButton)sender;
            string filename = lb.CommandArgument;
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.TransmitFile(Server.MapPath("~/" + p.folder + "/" + filename));
            Response.End();
        }
        //public void LoadGallery()
        //{
        //    Gallery g = new Gallery();
        //    DataTable dt = g.GetLatestGallery();
        //    string content = "";
        //    int x = 0;
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        int ID = Convert.ToInt32(dt.Rows[i]["ID"].ToString());
        //        int CategoryID = Convert.ToInt32(dt.Rows[i]["category_id"].ToString());
        //        string card = "card";
        //        string aClass = "";
        //        string title = "";
        //        string album = "";
        //        if (CategoryID == all.FotoCategoryID)
        //        {
        //            x++;
        //            card += " popup-gallery gallery-" + x;
        //            aClass = "image-popup-vertical-fit";
        //            title = "title=" + all.stripped(dt.Rows[i]["title"].ToString());
        //            DataTable imageDetails = g.GetDetailImage(ID);
        //            if (imageDetails.Rows.Count > 0)
        //            {
        //                for (int a = 0; a < imageDetails.Rows.Count; a++)
        //                {
        //                    album += "<a class=" + all.stripped("image-popup-vertical-fit") + " href=" + all.stripped(imageDetails.Rows[a]["img_file"].ToString()) + " title=" + all.stripped(all.stripped(imageDetails.Rows[a]["title"].ToString())) + "></a>";
        //                }
        //            }
        //        }
        //        if (CategoryID == all.VideoCategoryID)
        //        {
        //            aClass = "popup-youtube";
        //        }
        //        if (CategoryID == all.InfografisCategoryID)
        //        {
        //            aClass = "image-popup-vertical-fits";
        //            title = "title=" + all.stripped(dt.Rows[i]["title"].ToString());
        //        }
        //        content += "<div class=" + all.stripped(card) + ">";
        //        content += "<a  href=" + all.stripped(dt.Rows[i]["Url"].ToString()) + " class=" + all.stripped(aClass) + " " + title + " >";
        //        content += "<img class=" + all.stripped("card-img-top") + " src=" + all.stripped(dt.Rows[i]["Thumbnail"].ToString()) + " alt=" + all.stripped("Card image cap") + ">";
        //        content += "<div class=" + all.stripped("card-body") + ">";
        //        content += "<p class=" + all.stripped("mb-0") + "><em>" + dt.Rows[i]["Gallery_Date"].ToString() + "</em> &nbsp;<span class=" + all.stripped("badge badge-danger") + ">" + dt.Rows[i]["Category_name"].ToString().ToUpper() + "</span></p>";
        //        content += "<p><strong>" + dt.Rows[i]["title"].ToString() + "</strong></p>";
        //        content += "</div></a>";
        //        content += album != "" ? album : "";
        //        content += "</div>";


        //    }
        //    glr.InnerHtml = content;
        //}
        //public void BindPengadaan()
        //{
        //    InfoPengadaan ip = new InfoPengadaan();
        //    PagesData pd = ip.getPage();
        //    pengadaandesc.InnerHtml = pd.content;
        //    ip.OrderBy = " Dates Desc ";
        //    DataTable dt = ip.Select("top 8 SUBSTRING(title,1,24) as title, url, cover, CONVERT(varchar,Dates,106) as 'date' ");
        //    ListViewPengadaan.DataSource = dt;
        //    ListViewPengadaan.DataBind();
        //}
        //public void LoadKapal()
        //{
        //    Kapal k = new Kapal();
        //    //DataTable dt = k.getLatest();
        //    PagesData pd = k.getPages();
        //    kapaldesc.InnerHtml = pd.content;
        //    //ListViewKapal.DataSource = dt;
        //    //ListViewKapal.DataBind();
        //}


        public void loadfooter()
        {
            ModelData Db = new ModelData();
            string query = "Select * from  Contact  where ID = 1";
            DataTable dt = Db.getData(query);
            address.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["address_en"].ToString() : dt.Rows[0]["address"].ToString(); ;
            phone.InnerHtml = dt.Rows[0]["phone"].ToString();
            email.InnerHtml = dt.Rows[0]["email"].ToString();
            link_fb.HRef = dt.Rows[0]["link_fb"].ToString();
            link_tw.HRef = dt.Rows[0]["link_tw"].ToString();
            link_ig.HRef = dt.Rows[0]["link_ig"].ToString();
            link_yt.HRef = dt.Rows[0]["link_yt"].ToString();
        }

        

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(all.search + "" + txtSearch.Value);
        }
    }
}