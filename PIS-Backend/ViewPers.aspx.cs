﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class ViewPers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int Id = Convert.ToInt16(Request.QueryString["ID"]);

                Pers prs = new Pers();
                string select = "title, content, img_file, city, CONVERT(varchar, addDate, 106) as 'Pers Date' ";
                string conditions = "ID = " + Id;

                DataTable dt = prs.Select(select, conditions);
                title.InnerText = dt.Rows[0]["title"].ToString();

                LblDate.Text = dt.Rows[0]["Pers Date"].ToString();

                imgArticle.Attributes.Add("src", dt.Rows[0]["img_file"].ToString());

                content.InnerHtml = "<b>"+ dt.Rows[0]["city"].ToString() +"</b> - "+dt.Rows[0]["content"].ToString();
            }
      }
    }
}