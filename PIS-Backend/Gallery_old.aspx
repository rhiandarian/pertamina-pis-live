﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="Gallery_old.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.Gallery1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <main id="main">

  	<!-- Top Main -->
  	<section id="breadcrumbs" class="pt-3">
			<div class="container">
				<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
					    <li class="breadcrumb-item"><a href="">Media & Informasi</a></li>
					    <li class="breadcrumb-item active" aria-current="page">Berita</li>
					  </ol>
					</nav>
					<div class="col-lg-12 page-title">
						<h3>Media & Informasi, Galeri</h3>
					</div>
				</div>
					
				</div>
  	</section>


  	<!-- ======= Media Information Section ======= -->
    <section class="gallery">
      <div class="container">
				<div class="row no-gutters">
					<div class="col-lg-12 mb-3">
						<ul class="nav nav-tabs row-eq-height" id="myTab" role="tablist">
							<li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5 active" id="video-tab" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="true">
							   Video
							  </a>
						  </li>
						  <li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5" id="photo-tab" data-toggle="tab" href="#photo" role="tab" aria-controls="photo" aria-selected="false">
						    	Foto
						    </a>
						  </li>
						  <li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5" id="infografis-tab" data-toggle="tab" href="#infografis" role="tab" aria-controls="infografis" aria-selected="false">
						    	Infografis
						    </a>
						  </li>
						</ul>
						<div class="tab-content bg-white pt-5 pb-5" id="myTabContent">
						  <div class="tab-pane fade show active" id="video" role="tabpanel" aria-labelledby="video-tab">
						  	<div class="row">
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<!--
						  			<div class="embed-responsive embed-responsive-16by9">
										  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/URHBX4oK3Wc" allowfullscreen></iframe>
										</div>
										-->
										<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  	</div>

						  	<!---Pagination-->
						  	<div class="row mt-4">
				        	<div class="col-12">
				        		<div class="row">
				        			<div class="col-6">
				        				<p>Pages: 1 of 52</p>
				        			</div>
				        			<div class="col-6 text-right">
				        				<nav aria-label="...">
												  <ul class="pagination  justify-content-end">
												    <li class="page-item disabled">
												      <span class="page-link">Prev</span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">1</a></li>
												    <li class="page-item active">
												      <span class="page-link">
												        2 <span class="sr-only">(current)</span>
												      </span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">3</a></li>
												    <li class="page-item">
												      <a class="page-link" href="#">Next</a>
												    </li>
												  </ul>
												</nav>
				        			</div>
				        		</div>
				        	</div>
				        </div>

						  </div>

						  <div class="tab-pane fade" id="photo" role="tabpanel" aria-labelledby="photo-tab">
						  	<div class="row">
						  		<div class="popup-gallery gallery-1 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
						  		</div>
						  		<div class="popup-gallery gallery-2 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
							  	</div>
							  	<div class="popup-gallery gallery-3 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
							  	</div>
							  	<div class="popup-gallery gallery-4 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
							  	</div>
							  	<div class="popup-gallery gallery-5 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
							  	</div>
							  	<div class="popup-gallery gallery-6 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
							  	</div>
							  	<div class="popup-gallery gallery-7 col-4 mb-5">
							  		<a class="image-popup-vertical-fit" href="images/image-holder1.jpg">
							  			<div class="box-image">
						  					<img src="images/image-holder1.jpg" class="" alt="...">
						  				</div>
											<div class="mt-3">
												<p class="mb-0"><em>25 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
	          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
											</div>
							  		</a>
							  		<a class="image-popup-vertical-fit" href="images/service-box-image-1.jpg" title="Lorem Ipsum"></a>
							  	</div>
						  	</div>

						  	<!---Pagination-->
						  	<div class="row mt-4">
				        	<div class="col-12">
				        		<div class="row">
				        			<div class="col-6">
				        				<p>Pages: 1 of 52</p>
				        			</div>
				        			<div class="col-6 text-right">
				        				<nav aria-label="...">
												  <ul class="pagination  justify-content-end">
												    <li class="page-item disabled">
												      <span class="page-link">Prev</span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">1</a></li>
												    <li class="page-item active">
												      <span class="page-link">
												        2 <span class="sr-only">(current)</span>
												      </span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">3</a></li>
												    <li class="page-item">
												      <a class="page-link" href="#">Next</a>
												    </li>
												  </ul>
												</nav>
				        			</div>
				        		</div>
				        	</div>
				        </div>
						  </div>
						  <div class="tab-pane fade" id="infografis" role="tabpanel" aria-labelledby="infografis-tab">
						  	<div class="row">
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="image-link" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  	</div>

						  	<!---Pagination-->
						  	<div class="row mt-4">
				        	<div class="col-12">
				        		<div class="row">
				        			<div class="col-6">
				        				<p>Pages: 1 of 52</p>
				        			</div>
				        			<div class="col-6 text-right">
				        				<nav aria-label="...">
												  <ul class="pagination  justify-content-end">
												    <li class="page-item disabled">
												      <span class="page-link">Prev</span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">1</a></li>
												    <li class="page-item active">
												      <span class="page-link">
												        2 <span class="sr-only">(current)</span>
												      </span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">3</a></li>
												    <li class="page-item">
												      <a class="page-link" href="#">Next</a>
												    </li>
												  </ul>
												</nav>
				        			</div>
				        		</div>
				        	</div>
				        </div>

						  </div>
						</div>

					</div>
        </div>
        
      </div>
    </section><!-- Media Information Section -->

    

  </main>

    <script type="text/javascript">
  	$(document).ready(function() {

		  $('.image-popup-vertical-fits').magnificPopup({
				type: 'image',
				closeOnContentClick: true,
				mainClass: 'mfp-img-mobile',
				image: {
					verticalFit: true
				},
				zoom: {
					enabled: true,
					duration: 300 
				}
			});

			$('.popup-youtube').magnificPopup({
				disableOn: 700,
				type: 'iframe',
				mainClass: 'mfp-fade',
				removalDelay: 160,
				preloader: false,
				fixedContentPos: false,
			});

			let countGallery = $(".popup-gallery").length;
			for (i=0; i<countGallery; i++) {
			  $(".gallery-"+i).magnificPopup({
					delegate: 'a',
					type: 'image',
					tLoading: 'Loading image #%curr%...',
					mainClass: 'mfp-img-mobile',
					gallery: {
						enabled: true,
						navigateByImgClick: true,
						preload: [0,1] // Will preload 0 - before current, and 1 after the current image
					},
					image: {
						tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
						titleSrc: function(item) {
							return item.el.attr('title') + '';
						}
					}
				});
			} 

		});
  </script>
</asp:Content>
