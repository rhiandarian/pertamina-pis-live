﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class unggah_sistem_manajemen : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }

                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "26"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;

                LoadData();
            }
        }
        public void LoadData()
        {
            if(Request.QueryString["ID"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                SistemManajemen sm = new SistemManajemen();
                DataTable dt = sm.Select("*", "ID = " + Id);
                nama.Text = dt.Rows[0]["Nama"].ToString();
                nama_en.Text = dt.Rows[0]["Nama_En"].ToString();
                pemberi.Text = dt.Rows[0]["Pemberi"].ToString();
                pemberi_en.Text = dt.Rows[0]["Pemberi_En"].ToString();
                pemberi_en.Text = dt.Rows[0]["Pemberi_En"].ToString();
                DateTime dt1 = Convert.ToDateTime(dt.Rows[0]["ValidasiFrom"].ToString());
                DateTime dt2 = Convert.ToDateTime(dt.Rows[0]["ValidasiTo"].ToString());
                from.Text = dt1.ToString("MM/dd/yyyy");
                until.Text = dt2.ToString("MM/dd/yyyy");
            }
            
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                SistemManajemen sm = new SistemManajemen();
                sm.Nama = nama.Text;
                sm.Nama_En = nama_en.Text;
                sm.ValidasiFrom = Convert.ToDateTime(from.Text);
                sm.ValidasiUntil = Convert.ToDateTime(until.Text);
                sm.Pemberi = pemberi.Text;
                sm.Pemberi_En = pemberi_en.Text;
                sm.Img_File = FileUpload1.FileName;

                if (Request.QueryString["ID"] == null)
                {
                    string insertStatus = sm.Insert(getip(),GetBrowserDetails());
                    if (insertStatus == "success")
                    {
                        UploadSO();
                        Response.Redirect(sm.listPage);
                    }
                    else
                    {
                        showValidationMessage(insertStatus);
                    }
                }
                else
                {
                    int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                    string cond = " ID = " + Id;
                    string updateStatus = sm.Update(Id,getip(),GetBrowserDetails(),cond);
                    if (updateStatus == "success")
                    {
                        if (FileUpload1.FileName != "")
                        {
                            UploadSO();
                        }
                        Response.Redirect(sm.listPage);
                    }
                    else
                    {
                        showValidationMessage(updateStatus);
                    }

                }
            }
            catch(Exception ex)
            {
                SistemManajemen si = new SistemManajemen();
                string action = Request.QueryString["ID"] == null ? "Insert" : "Update";
                si.ErrorLogHistory(getip(), GetBrowserDetails(), action, si.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                showValidationMessage(ex.Message.ToString());
            }
            
        }
        public void UploadSO()
        {
            SistemManajemen si = new SistemManajemen();

            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string SaveLocation = Server.MapPath(si.folder) + "\\" + fn;

            FileUpload1.PostedFile.SaveAs(SaveLocation);
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
    }
}