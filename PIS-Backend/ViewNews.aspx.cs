﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class ViewNews : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Request.QueryString["ID"] == null)
                {
                    Response.Redirect("News.aspx");
                }
                int Id = Convert.ToInt16(Request.QueryString["ID"]);
                News atc = new News();
                string select = "title, content, img_file, CONVERT(varchar, addDate, 106) as 'News Date' ";
                string conditions = "ID = " + Id;

                DataTable dt = atc.Select(select, conditions);

                title.InnerText = dt.Rows[0]["title"].ToString();

                LblDate.Text = dt.Rows[0]["News Date"].ToString();

                imgArticle.Attributes.Add("src", dt.Rows[0]["img_file"].ToString());

                content.InnerHtml = dt.Rows[0]["content"].ToString();
            }
        }
    }
}