﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


namespace PIS_Backend
{
    public partial class UploadDokumen : System.Web.UI.Page
    {
        SqlConnection con = Connection.conn();
        string folder = "documents";
        string listPage = "DokumentList.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "33"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
            }
            
        }
        public string validation()
        {
            if(keterangan.Text == "")
            {
                return "Keterangan harus diisi";
            }
            if(FileUpload1.FileName == "")
            {
                return "File harus di unggah";
            }
            return "Valid";
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string validate = validation();
                if(validate == "Valid")
                {
                    SaveData();
                    uploadFile();
                    Response.Redirect(listPage);
                    showValidationMessage(SaveQueryLogHistories());
                }
                else
                {
                    showValidationMessage(validate);
                }
            }
            catch(Exception ex)
            {
                ModelData db = new ModelData();
                db.ErrorLogHistory(getip(), GetBrowserDetails(), "Save", "Dokumen", Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                showValidationMessage(ex.Message.ToString());
            }
        }
        public string SaveQueryLogHistories()
        {
            ModelData db = new ModelData();
            string action = Session["Username"].ToString() + " Upload Dokumen with Id = '+CAST("+db.getLastIDQuery("Dokumen","ID")+"as varchar(MAX))+', file_url = " + folder + "/" + FileUpload1.FileName + ", keterangan = " + keterangan.Text;
            return db.logHistoryQuery(getip(), GetBrowserDetails(), action, Convert.ToInt32(Session["UserID"].ToString()), "Success");
        }
        public void SaveData()
        {
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "INSERT INTO [dbo].[Dokumen]([Keterangan] ,[file_url] ,[Date])  VALUES  ('"+keterangan.Text+"','/"+folder+"/"+FileUpload1.FileName+"', GETDATE()) "+SaveQueryLogHistories();
            cmd.ExecuteNonQuery();
            con.Close();

        }
        public void uploadFile()
        {
            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string SaveLocation = Server.MapPath(folder) + "\\" + fn;

            FileUpload1.PostedFile.SaveAs(SaveLocation);
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
    }
}