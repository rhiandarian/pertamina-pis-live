﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class VideoProfile : System.Web.UI.Page
    {
        General all = new General();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "3"))
                {
                    Response.Redirect("admin.aspx");
                }
                successdiv.Visible = false;
                LblErrorMessage.Visible = false;
                loadData();
            }
        }
        public DataTable getData()
        {
            Gallery g = new Gallery();
            string cond = " category_id = " + all.videoProfileID;
            DataTable dt = g.Select("top 1 *", cond);
            return dt;
            
        }
        public void loadData()
        {
            DataTable dt = getData();
            if(dt.Rows.Count > 0)
            {
                title.Text = dt.Rows[0]["title"].ToString();
                title_en.Text = dt.Rows[0]["title_en"].ToString();
                DateTime dates = Convert.ToDateTime(dt.Rows[0]["Dates"].ToString());
                date.Text = dates.ToString("MM/dd/yyyy");
                content.InnerHtml = dt.Rows[0]["content"].ToString();
                content_en.InnerHtml = dt.Rows[0]["content_en"].ToString();
                videoUrl.Text = dt.Rows[0]["video_url"].ToString();
                imageUrl.Text = dt.Rows[0]["img_url"].ToString();
            }
        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Gallery g = new Gallery();
                g.Title = title.Text;
                g.TitleEn = title_en.Text;
                g.Date = Convert.ToDateTime(date.Text);
                g.Content = content.InnerText.Replace("'","");
                g.ContentEn = content_en.InnerText.Replace("'","");
                g.Video_url = videoUrl.Text;
                g.Img_url = imageUrl.Text;

                string validate = g.ValidationVideoProfile();
                if(validate == "Valid")
                {
                    g.SaveVideoProfile(getip(),GetBrowserDetails());
                    successMessage("Video Profil berhasil tersimpan");
                    loadData();
                }
                else
                {
                    showValidationMessage(validate);
                }
            }
            catch(Exception ex)
            {
                Gallery g = new Gallery();
                g.ErrorLogHistory(getip(), GetBrowserDetails(), "Update", "Video Profile ("+g.tableName+")", Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                showValidationMessage(ex.Message.ToString());
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void showValidationMessage(string textMessage)
        {
            successdiv.Visible = false;
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void successMessage(string textMessage)
        {
            LblErrorMessage.Visible = false;
            successdiv.InnerHtml = textMessage;
            successdiv.Visible = true;

        }
    }
}