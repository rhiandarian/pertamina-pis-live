﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;


namespace PIS_Backend
{
    public partial class admin_add_news : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "4"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindCategory();
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
           
            }
            
        }
      
        public bool ImageFileType(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            switch (ext.ToLower())
            {
                case ".gif":
                    return true;
                case ".jpg":
                    return true;
                case ".jpeg":
                    return true;
                case ".png":
                    return true;
                default:
                    return false;
            }
        }
        public void BindCategory()
        {
            Category ctg = new Category();

            ctg.Category_Type_Id = 1;
            string selectCategory = "ID, Category_name";

            DataSet dt = ctg.SelectList(selectCategory, "and");

            ddlCategory.DataTextField = dt.Tables[0].Columns["Category_name"].ToString();
            ddlCategory.DataValueField = dt.Tables[0].Columns["ID"].ToString();
            ddlCategory.DataSource = dt.Tables[0];
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, "Pilih Kategori");
            ddlCategory.SelectedIndex = 0;



        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            InsertNews("Draft");
            
        }
        protected void BtnPublished_Click(object sender, EventArgs e)
        {
            InsertNews("Published");
        }
        public string randomTxt()
        {
            ImageResize ir = new ImageResize();
            return ir.textrandom();
        }
        public void InsertNews(string status)
        {
            bool valid = true;
            string validationMessage = "";
            string random = randomTxt();

            if (date.Text == "")
            {
                valid = false;
                validationMessage = "Tanggal harus di pilih !";
            }
            if(ddlCategory.SelectedValue == "Pilih Kategori")
            {
                valid = false;
                validationMessage = "Kategori harus di pilih";
            }
            if(key.Text == "")
            {
                valid = false;
                validationMessage = "Kata Kunci harus di isi";
            }
            if (key_en.Text == "")
            {
                valid = false;
                validationMessage = "Kata Kunci bahasa Inggris harus di isi";
            }
            if (content_en.InnerText == "")
            {
                valid = false;
                validationMessage = "Konten Bahasa Inggris harus di isi !";
            }
            if (content.InnerText == "")
            {
                valid = false;
                validationMessage = "Konten harus di isi !";
            }
            if (title_en.Text == "")
            {
                valid = false;
                validationMessage = "Judul dalam bahasa Inggris harus di isi !";
            }
            if (title.Text == "")
            {
                valid = false;
                validationMessage = "Judul dalam bahasa Indonesia harus di isi !";
            }
            try
            {
                if (valid)
                {
                    string SaveLocation = "";
                    if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
                    {
                        string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                        string SetFolderBeforeResize = Server.MapPath("Beritaa") + "\\" + "serize" + random + fn; //save folder  before image resize
                        string SetFolderAfterResize = Server.MapPath("Beritaa") + "\\"  + random + fn; //save image after resize
                        try
                        {
                            if (ImageFileType(FileUpload1.FileName))
                            {
                                FileUpload1.PostedFile.SaveAs(SetFolderBeforeResize);
                                ImageResize Iz = new ImageResize();
                                Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "Berita", "Cut"); //save image after resize and delete folder before
                            }
                            else
                            {
                                valid = false;
                                validationMessage = "Masukan file foto";
                            }





                        }
                        catch (Exception ex)
                        {
                            valid = false;
                            validationMessage = ex.Message;
                        }
                    }
                    else
                    {
                        valid = false;
                        validationMessage = "File Harus di Upload";
                    }
                    if (valid)
                    {
                        News atc = new News();
                        atc.Title = title.Text.Replace("'", "");
                        atc.TitleEn = title_en.Text.Replace("'", "");
                        atc.Content = content.InnerText.Replace("'", "");
                        atc.ContentEn = content_en.InnerText.Replace("'", "");
                        atc.Category_Id = Convert.ToInt32(ddlCategory.SelectedValue);
                        atc.Date = Convert.ToDateTime(date.Text);
                        atc.Img_File = "Beritaa/" + random + FileUpload1.FileName;
                        atc.Status = status;
                        atc.Key = key.Text;
                        atc.KeyEn = key_en.Text;

                        DataTable dt = atc.Insert(getip(),GetBrowserDetails());
                        if (dt.Rows[0]["Status"].ToString() == "Success")
                        {
                            LblErrorMessage.Visible = false;
                            Response.Redirect("ListNews.aspx");
                        }
                        else
                        {
                            LblErrorMessage.Visible = true;
                            LblErrorMessage.Text = dt.Rows[0]["Message"].ToString();
                        }


                    }
                    else
                    {
                        LblErrorMessage.Visible = true;
                        LblErrorMessage.Text = validationMessage;
                    }

                }
                else
                {
                    LblErrorMessage.Visible = true;
                    LblErrorMessage.Text = validationMessage;
                }
            }
            catch (Exception ex)
            {
                News n = new News();
                n.ErrorLogHistory(getip(), GetBrowserDetails(), "INSERT", n.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                LblErrorMessage.Visible = true;
                LblErrorMessage.Text = ex.Message.ToString();
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }

    }
}