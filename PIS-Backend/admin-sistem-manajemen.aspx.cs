﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class admin_sistem_manajemen : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if(Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "26"))
                {
                    Response.Redirect("admin.aspx");
                }
                loadSM();
            }
        }
        public void loadSM()
        {
            SistemManajemen sm = new SistemManajemen();
            DataTable dt = sm.Select("*");
            ListViewSO.DataSource = dt;
            ListViewSO.DataBind();
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            SistemManajemen sm = new SistemManajemen();
            Response.Redirect(sm.savePage+"?ID="+id);
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lb = (LinkButton)sender;
                string id = lb.CommandArgument;
                SistemManajemen sm = new SistemManajemen();
                sm.Delete(id,getip(),GetBrowserDetails(),"ID = " + id);
                loadSM();
            }
            catch(Exception ex)
            {
                General all = new General();
                SistemManajemen sm = new SistemManajemen();
                sm.ErrorLogHistory(getip(), GetBrowserDetails(), "Delete", sm.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                Response.Write(all.alert(ex.Message.ToString()));
            }
            
        }
    }
}