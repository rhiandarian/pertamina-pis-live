﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="EditVideos.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.EditVideos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid" style="background-color:ghostwhite">
        <div class="row pt-5">
             <div class="col text-center">
                 <h1>VIDEOS</h1>
             </div>
         </div><hr />
         <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
        <div class="row pt-5">
             <div class="col">
                 <form>
                      <div class="form-group">
                        <label for="title"><b>title</b></label>
                        <asp:TextBox ID="title" cssClass="form-control" placeholder="Input Title Video" runat="server"></asp:TextBox>
                      </div>
                     <div class="form-group">
                        <label for="content"><b>content</b></label>
                        <textarea id="content" class="form-control"  placeholder="Input Content Video"  runat="server" rows="10"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="videoUrl"><b>video Url</b></label>
                          <asp:TextBox ID="videoUrl" CssClass="form-control" placeholder="Input Link Url Video" runat="server"></asp:TextBox>
                     </div>
                     <div class="form-group">
                        <label for="imageUrl"><b>thumbnail Url</b></label>
                          <asp:TextBox ID="imageUrl" CssClass="form-control" placeholder="Input Thumbnail Image Url" runat="server"></asp:TextBox>
                     </div>
                     <div class="form-group">
                        <label for="imageUrl"><b>category</b></label>
                          <asp:TextBox ID="txtCategory" CssClass="form-control" placeholder="Input Category for this Video" runat="server"></asp:TextBox>
                     </div>
                     <div class="form-group">
                        <label for="embedUrl"><b>embed Url</b></label>
                          <asp:TextBox ID="embedUrl" CssClass="form-control" placeholder="Input Embed Url Video" runat="server"></asp:TextBox>
                     </div>
                    <div class="form-group">
                        <asp:Button ID="BtnSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="BtnSave_Click"     />
                    </div>
                      
                </form>
             </div>
             
         </div>
        <div class="text-right">
             <a href="AdminVideos.aspx" class="btn btn-info" ><i class="fa fa-home"></i></a>
         </div>
    </div>
</asp:Content>
