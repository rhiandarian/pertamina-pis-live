﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class SavePenghargaan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "27"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadData();
                
            }
        }
        public void LoadData()
        {
            if (Request.QueryString["ID"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                string cond = " ID = " + Id;
                Penghargaan p = new Penghargaan();
                DataTable dt = p.Select("*", cond);
                title.Text = dt.Rows[0]["title"].ToString();
                title_en.Text = dt.Rows[0]["title_en"].ToString();
                description.Text = dt.Rows[0]["description"].ToString();
            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string randomtext = textrandom();
                Penghargaan p = new Penghargaan();
                p.Title = title.Text;
                p.TitleEn = title_en.Text;
                p.Desc = description.Text;
                Response.Write("ukuran fotonya adalah " + FileUpload2.PostedFile.ContentLength.ToString());
                if (FileUpload2.FileName != "")
                {
                    p.Cover = randomtext + FileUpload2.FileName;

                }
                 

                if (Request.QueryString["ID"] == null)
                {

                    if (FileUpload2.FileName != "")
                    {
                        p.Cover = randomtext + FileUpload2.FileName;

                    }
                    else
                    {
                        p.Cover = "default-album.png";
                    }

                    string insertStatus = p.Insert(getip(),GetBrowserDetails());
                    if(insertStatus == "success")
                    {
                        if (p.Cover != "default-album.png")
                        {
                            UploadCover(p.folder, randomtext);
                        }
                        Response.Redirect(p.listPage);

                    }
                    else
                    {
                        showValidationMessage(insertStatus);
                    }

                }
                else
                {  
                    int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                    string cond = " ID = " + Id;
                    string updateStatus = p.Update(Id,getip(),GetBrowserDetails(),cond);
                    if(updateStatus == "success")
                    {
                         
                        if(FileUpload2.FileName != "")
                        {
                            UploadCover(p.folder, randomtext);

                        }
                        Response.Redirect(p.listPage);
                    }
                    else
                    {
                        showValidationMessage(updateStatus);
                    }
                }
            }
            catch(Exception ex)
            {
                Penghargaan p = new Penghargaan();
                string action = Request.QueryString["ID"] == null ? "Insert" : "Update";
                p.ErrorLogHistory(getip(), GetBrowserDetails(), action, p.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                showValidationMessage(ex.Message.ToString());
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void UploadCover(string folder,string randomtext)
        {
             

            string fn = System.IO.Path.GetFileName(FileUpload2.PostedFile.FileName);
            string SetFolderBeforeResize = Server.MapPath(folder) + "\\" + "serize" + fn;
            string SetFolderAfterResize = Server.MapPath(folder) + "\\" + randomtext + fn;
            FileUpload2.PostedFile.SaveAs(SetFolderBeforeResize);
            ImageResize Iz = new ImageResize();
            Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "Default-Thumbnail", "Cut");

        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }

        public string textrandom()
        {
            Random RNG = new Random();
            int length = 6;
            var rString = "";
            for (var i = 0; i < length; i++)
            {
                rString += ((char)(RNG.Next(1, 26) + 64)).ToString().ToLower();
            }
            return rString;
        }
    }
}