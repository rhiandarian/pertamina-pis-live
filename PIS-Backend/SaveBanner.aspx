﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="SaveBanner.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.SaveBanner" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Slider</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Beranda
                        </li> 
                        <li class="breadcrumb-item">
                            <a href="ListBanner.aspx">Slider</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="SaveBanner.aspx"><strong>Tambah Slider</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Unggah Slider</small></h5>
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                 <form>
                      <div class="form-group">
                          <div class="row">
                              <div class="col">
                                  <label for="title"><b>Judul (Bahasa Indonesia)</b></label>
                                  <asp:TextBox ID="title" cssClass="form-control" placeholder="Masukkan judul slider dalam bahasa Indonesia" runat="server"></asp:TextBox>
                              </div>
                              <div class="col">
                                  <label for="title_en"><b>Judul (Bahasa Inggris)</b></label>
                                  <asp:TextBox ID="title_en" cssClass="form-control" placeholder="Masukkan judul slider dalam bahasa Inggris" runat="server"></asp:TextBox>
                              </div>
                          </div>
                        
                      </div>
                     <div class="form-group">
                         <div class="row">
                             <div class="col">
                                 <label for="description"><b>Deskripsi</b></label>
                                 <asp:TextBox ID="description" runat="server" CssClass="form-control" placeholder="Masukkan deskripsi slider dalam bahasa Indonesia"></asp:TextBox>
                             </div>
                             <div class="col">
                                 <label for="description_en"><b>Deskripsi Bahasa Inggris</b></label>
                                 <asp:TextBox ID="description_en" runat="server" CssClass="form-control" placeholder="Masukkan deskripsi slider dalam bahasa Inggris"></asp:TextBox>
                             </div>
                         </div>
                        
                     </div>
                     <div class="form-group">
                        <label for="DdlUrl"><b>Tautan</b></label>
                         <asp:DropDownList ID="DdlUrl" runat="server" CssClass="form-control"  AutoPostBack="true" ></asp:DropDownList>
                     </div>
                     <div class="form-group">
                        <label for="FileUpload1"><b>Foto</b></label>
                        <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                     </div>
                     
                     <div class="form-group">
                        <div class="row">
                             <div class="col">
                                 <label for="btnText"><b>Kata Dalam Tautan (Bahasa Indonesia)</b></label>
                                 <asp:TextBox ID="btnText" runat="server" CssClass="form-control" placeholder="Masukkan kata yang ditampilkan di tautan dalam bahasa Indonesia"></asp:TextBox>
                             </div>
                            <div class="col">
                                 <label for="btnText"><b>Kata Dalam Tautan (Bahasa Inggris)</b></label>
                                 <asp:TextBox ID="btnTextEn" runat="server" CssClass="form-control" placeholder="Masukkan kata yang ditampilkan di tautan dalam bahasa Inggris"></asp:TextBox>
                             </div>
                         </div>
                     </div>
                    <div class="form-group">
                        <asp:Button ID="BtnSave" runat="server" CssClass="btn btn-primary" Text="Simpan" OnClick="BtnSave_Click"      />
                    </div>
                      
                </form>
            </div>
        </div>
    </div>
</asp:Content>
