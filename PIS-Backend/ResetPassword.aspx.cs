﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace PIS_Backend
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        SqlConnection con = Connection.conn();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                formUpdatePassword.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LblErrorMessage.Visible = false;

            }
        }

        protected void BtnCheckCode_Click(object sender, EventArgs e)
        {
            if(KeyCode.Text == Session["KeyCode"].ToString())
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "Update ResetPassword set active = 0 Where Code = " + KeyCode.Text + "";
                cmd.ExecuteNonQuery();
                con.Close();
                LblErrorMessage.Visible = false;
                formInputCode.Visible = false;
                formUpdatePassword.Visible = true;
            }
            else
            {
                LblErrorMessage.Visible = true;
                LblErrorMessage.Text = "Kode Aktivasi Salah";
            }
        }

        protected void BtnResetPassword_Click(object sender, EventArgs e)
        {
            if(NewPassword.Text == ConfirmPassword.Text)
            {
                Users u = new Users();
                con.Open();
                SqlCommand cmd = new SqlCommand("UpdatePasswordUser",con);
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@code", Session["KeyCode"].ToString());
                cmd.Parameters.AddWithValue("@password",u.EncryptPassword(NewPassword.Text));
                cmd.ExecuteNonQuery();
                con.Close();
                Response.Redirect("Login.aspx");
            }
            else
            {
                LblErrorMessage.Visible = true;
                LblErrorMessage.Text = "Password harus sama";
            }
        }
    }
}