﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="admin_dataallRUPS.aspx.cs" Inherits="PIS_Backend.admin_dataallRUPS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>RUPS</h2>
                    <ol class="breadcrumb">
                       <li class="breadcrumb-item">
                            Menu Lainnya 
                        </li>
                        <li class="breadcrumb-item">
                            Hubungan Investor
                        </li>
                         <li class="breadcrumb-item">
                            RUPS
                        </li>
                        <li class="breadcrumb-item">
                           <a href="admin_dataallRUPS.aspx"><strong>Data RUPS</strong></a> 
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row ml-2 pb-3">
            <a href="admin_addRUPS.aspx" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        </div>
        <div class="row">
            <asp:ListView ID="ListViewRUPS"  ItemPlaceholderID="itemPlaceHolder1" runat="server" OnPagePropertiesChanging="ListViewRUPS_PagePropertiesChanging" GroupPlaceholderID="groupPlaceHolder1">
                <GroupTemplate>
                                                    <tr>
                                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                                                    </tr>
                                                </GroupTemplate>
                <ItemTemplate>
                    <div class="col-xs-12 col-sm-3 box-content">
                                                        <div class="ibox">
                                                            <div class="ibox-content product-box">
                                                                <div class="product-imitation">
                                                                    <h3><%#  Eval("title") %></h3>
                                                                    
                                                                </div>
                                                                <div class="product-desc">
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            <p class="text-center">
                                                                                 <asp:LinkButton ID="LinkButton2" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="EditClick" runat="server">Edit <i class="fa fa-pencil"></i></asp:LinkButton>
                                                                                <asp:LinkButton ID="LinkButton3" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="DeleteClick" runat="server">Delete <i class="fa fa-trash"></i></asp:LinkButton>
                                                                            </p>
                                                                        </div>  
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                </ItemTemplate>
                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
                                                    <asp:DataPager ID="DataPager1" runat="server" PagedControlID="ListViewRUPS" PageSize="4">
                                                        <Fields>
                                                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
                                                                ShowNextPageButton="false" />
                                                            <asp:NumericPagerField ButtonType="Link" />
                                                            <asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton = "false" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </LayoutTemplate>
            </asp:ListView>

        </div>

        <a href="RUPS.aspx" target="_blank"><i class="fa fa-eye"></i> Lihat hasil website</a>
    </div>
</asp:Content>
