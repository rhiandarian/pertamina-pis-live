﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class Index_en : System.Web.UI.Page
    {
        General all = new General();
        string IndPage = "Index.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              
                loadfooter();
                if (Session["Lang"] == null || Session["Lang"].ToString() == "Ind")
                {

                    Response.Redirect(IndPage);
                }
                bindMenu();
                Kapal k = new Kapal();
                PagesData pd = k.getPages();
                totalkapal.InnerText = pd.others;
                kapalmilk.InnerText = pd.others2;
            }
        }
        public void bindMenu()
        {
            string currPage = "Index.aspx";
            string lang = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "En" : "Ind";

            List<Menu> mn = all.GenerateMenu(lang, currPage);
            ListViewMenu.DataSource = mn;
            ListViewMenu.DataBind();

            List<Menu> ot = all.MenuOthers(lang);
            ListViewOthers.DataSource = ot;
            ListViewOthers.DataBind();

            ListViewResponsive.DataSource = ot;
            ListViewResponsive.DataBind();

            string otmn = "Others";
            othermenu.Attributes["class"] = all.getOtherMenuClass(othermenu.Attributes["class"], currPage);
            othermenu.InnerHtml = "<a href=" + all.stripped("") + " onclick=" + all.stripped("openNavOverlay()") + " id=" + all.stripped("nav-right-option") + " >" + otmn + " <span class=" + all.stripped("") + "></span></a>";
            langMobile.InnerHtml = all.changeLangMobile("En");

            Carier pd = new Carier();
            DataTable dt = pd.Select("*", " ID = 1 ");
            AttributeCollection myAttributes = magang_section.Attributes;
            myAttributes.CssStyle.Add("background-image", dt.Rows[0]["others"].ToString());
        }
        protected void ChangeLang(object sender, EventArgs e)
        {
            if (Session["Lang"] == null)
            {
                Session.Add("Lang", "Ind");
            }
            else
            {
                Session["Lang"] = "Ind";
            }
            Response.Redirect(IndPage);
        }
        
        public string contentCard(string imgSrc, string newsDate, string Category, string title)
        {
            string image = "<img src=" + all.stripped(imgSrc) + " class=" + all.stripped("card - img") + " alt=" + all.stripped("") + " width=" + all.stripped("100%") + ">";
            string dateandtitle = "<em>" + newsDate + "</em> &nbsp;<span class=" + all.stripped("badge badge-danger") + ">" + Category.ToUpper() + "</span><br>" + title;
            string paragraph = all.paragraph(dateandtitle, "", "card-content p-4");

            string cardimgOverlay = all.div(paragraph, "", "card-img-overlay bg-black-transparent");



            return all.div(image + " " + cardimgOverlay, "", "card text-white overflow-hidden media-information-box card-block");

        }
        public string mediaInfoRight(string imgSrc, string newsDate, string Category, string title)
        {
            return all.div(contentCard(imgSrc, newsDate, Category, title), "", "col-12 pl-0 pr-0 mb-4 media-information-box-right");
        }
        
        public void DownloadArmada(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string fileName = lb.CommandArgument;
            string filePath = "Armada\\" + fileName;
            Response.ContentType = "image/jpg";
            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
            Response.TransmitFile(Server.MapPath(filePath));
            Response.End();
        }
        public void DownloadPengadaan(object sender, EventArgs e)
        {
            InfoPengadaan p = new InfoPengadaan();
            LinkButton lb = (LinkButton)sender;
            string filename = lb.CommandArgument;
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.TransmitFile(Server.MapPath("~/" + p.folder + "/" + filename));
            Response.End();
        }
        
        
       
        
       

        public void loadfooter()
        {
            ModelData Db = new ModelData();
            string query = "Select * from  Contact  where ID = 1";
            DataTable dt = Db.getData(query);
            address.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["address_en"].ToString() : dt.Rows[0]["address"].ToString(); ;
            phone.InnerHtml = dt.Rows[0]["phone"].ToString();
            email.InnerHtml = dt.Rows[0]["email"].ToString();
            link_fb.HRef = dt.Rows[0]["link_fb"].ToString();
            link_tw.HRef = dt.Rows[0]["link_tw"].ToString();
            link_ig.HRef = dt.Rows[0]["link_ig"].ToString();
            link_yt.HRef = dt.Rows[0]["link_yt"].ToString();
        }

        protected void BtnSignUp_Click(object sender, EventArgs e)
        {
            try
            {
                Client c = new Client();
                //c.Name = nameUser.Text;
                //c.Email = email.Text;

                //string insertstatus = c.Insert();
                //if (insertstatus == "success")
                //{
                //    Response.Write("<script>alert('Terima kasih telah mendaftar newsletter kami, nantikan update dari kami');</script>");
                //}
                //else
                //{
                //    Response.Write("<script>alert('" + insertstatus + "');</script>");
                //}
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message.ToString() + "');</script>");
            }

        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(all.search + "" + txtSearch.Value);
        }
    }
}