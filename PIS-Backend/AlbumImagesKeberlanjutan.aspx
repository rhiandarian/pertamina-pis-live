﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AlbumImagesKeberlanjutan.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.AlbumImagesKeberlanjutan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
                        <h2>Keberlanjutan</h2>
                        <ol class="breadcrumb">
                             <li class="breadcrumb-item">
                                <a>Menu Lainnya</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="admin_listkeberlanjutan.aspx">Keberlanjutan</a>
                            </li>
                            <li class="breadcrumb-item active">
                                 <strong>Unggah Album keberlanjutan</strong> 
                            </li>
                        </ol>
                    </div> 
            </div> 
        <div class="row ">
            <div class="col">
                <div class="album py-5 bg-light">
                    <div class="container">
                        <asp:LinkButton ID="LinkButtonUploadAlbum" CssClass="btn btn-info" runat="server" OnClick="LinkButtonUploadAlbum_Click"><i class="fa fa-plus"></i> Foto Baru</asp:LinkButton>
                       <div class="row pt-3">
                           <asp:ListView ID="ListViewAlbum" runat="server">
                                <ItemTemplate>
                                 <div class="col-md-4">
                                     <div class="card mb-4 box-shadow">
                                        <img class="card-img-top"  alt="Thumbnail [100%x225]" style="height: 225px; width: 100%; display: block;" src='<%# Eval("img_file") %>' data-holder-rendered="true">
                                        <div class="card-body">
                                          <p class="card-text"><%# Eval("title") %></p>
                                          <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                              <asp:LinkButton ID="LinkButtonDelete" CssClass="btn btn-danger" OnClick="Delete" CommandArgument='<%# Eval("ID") %>' runat="server"><i class="fa fa-trash"></i></asp:LinkButton>
                                            </div>
                                            <small class="text-muted"><i>On <%# Eval("image_date") %></i></small>
                                          </div>
                                        </div>
                                  </div>
                                
                          
                        </div>
                                    </ItemTemplate>
                            </asp:ListView>
                           
                        
                        </div>
                        <asp:LinkButton ID="LinkButtonBack" CssClass="btn btn-info" OnClick="ReturnHome"   runat="server">Kembali</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div> 
</asp:Content>
