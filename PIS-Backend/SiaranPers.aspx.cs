﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class SiaranPers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                BindGVPers();
                if (Session["Level"].ToString() != "1")
                {
                    btnAdd.Visible = false;
                }


            }
        }
        public void BindGVPers(string conditions = null)
        {
            Pers db = new Pers();
            string selected = "ID, title, SUBSTRING(content,1,100)as content";

            DataTable dt = conditions != null ? db.Select(selected, conditions) : db.Select(selected);

            GVPers.DataSource = dt;
            GVPers.DataBind();

        }

        protected void GVPers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (Session["UserID"] == null)
            {
                if (Session["Level"].ToString() != "1")
                {
                    e.Row.Cells[3].Visible = false;
                    e.Row.Cells[5].Visible = false;
                }
            }
        }
        protected void EditClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;

            Response.Redirect("EditPers.aspx?ID=" + id);
        }
        protected void ViewClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Response.Redirect("ViewPers.aspx?ID=" + id);
        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            string WhereId = "ID = " + id;
            Pers prs = new Pers();
            prs.Delete(WhereId);
            BindGVPers();
        }

        protected void GVPers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGVPers();
            GVPers.PageIndex = e.NewPageIndex;
            GVPers.DataBind();
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            string searchConditions = "title like '%"+TxtSearch.Text+"%' or  content like'%"+TxtSearch.Text+"%'";
            BindGVPers(searchConditions);
        }
    }
}