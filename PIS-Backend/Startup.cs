﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PIS_Backend.Startup))]
namespace PIS_Backend
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
