﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIS_Backend
{
    public class Menu
    {
        public string Name
        {
            get;
            set;
        }
        public string Link
        {
            get;
            set;
        }
        public string attr
        {
            get;
            set;
        }
        public string classMenu
        {
            get;
            set;
        }
        public List<Menu> SubMenu
        {
            get;
            set;
        }
    }
}