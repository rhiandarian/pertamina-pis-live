﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="Persentasi.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.Persentasi" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a id="htitle" href="Index.aspx">Beranda</a></li>
					    <li class="breadcrumb-item"><a href="" id="link1" runat="server">Media &amp; Informasi</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="link2" runat="server">Presentasi</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="htitle" runat="server">Presentasi</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <asp:Label ID="LblIndex" runat="server" Text="Label"></asp:Label>
    <section class="presentasi">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div class="col-12" id="content" runat="server">
	  		
	  			</div>
	  		</div>

	  		<div class="row mb-5 content">
	  			<div class="col-12">
	  				<div class="row">
                          <asp:ListView ID="ListViewPersentasi" runat="server">
                              <ItemTemplate>
                                  <div class="col-xs-12 col-sm-4 box-content">
			  				        <div class="col box-pis-sm">
			  					        <img src="<%# Eval("cover") %>" class="img-fluid mt-3 mb-2" alt="...">
			  					        <p class="text-left">
			  						        </p><center><strong><%# Eval("title") %></strong></center>
			  					        <p></p>
			  					        <p>
			  						       <asp:LinkButton ID="LinkButton1" CssClass="red_pis" CommandArgument='<%# Eval("url")  %>' OnClick="Download" runat="server">Download</asp:LinkButton>
			  					        </p>
			  				        </div>
			  			        </div>
                              </ItemTemplate>
                              <EmptyDataTemplate>
                                  No Data 
                              </EmptyDataTemplate>
                              <EmptyItemTemplate>
                                  No Data
                              </EmptyItemTemplate>
                          </asp:ListView>
	  					
			  			
	  				</div>
	  				<!---Pagination-->
				  	<div class="row mt-4">
          <div class="col-12 pagination-box">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <p id="detailPaging" runat="server"></p>
              </div>
            
              <div class="col-xs-12 col-sm-6 text-right">
                <nav aria-label="...">
                <asp:Repeater ID="RptPaging" runat="server">
                    <HeaderTemplate>
                    <ul class="pagination justify-content-end">
                        <li class="page-item">
                             <asp:LinkButton  runat="server" Text="<<" CommandArgument="first"
                             CssClass="page-link" OnClick="Page_Changed" ></asp:LinkButton>
                        </li>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li class='<%#Eval("active")%>'>
                            <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Index") %>' CommandArgument='<%# Eval("Index") %>'
                            CssClass="page-link" OnClick="Page_Changed" ></asp:LinkButton>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        <li class="page-item">
                            <asp:LinkButton  runat="server" Text=">>" CommandArgument="last"
                            CssClass="page-link" OnClick="Page_Changed" ></asp:LinkButton>
                        </li>
                      </ul>
                   </FooterTemplate>
               </asp:Repeater>
                  
                    
                </nav>
              </div>
            </div>
          </div>
        </div>
	  			</div>
	  			
	  			
	  		</div>

	  	</div>
  	</section>
    <script>
        var urlname = "", title = ""
        if ($("#LbEn").attr("class") == "active")
        {
            urlname = "presentation";
            title = "Home";
        }
        if ($("#LbInd").attr("class") == "active")
        {
            urlname = "presentasi";
            title = "Beranda";
        }
        $("#htitle").text(title);
        history.pushState({}, null, urlname);
    </script>
</asp:Content>
