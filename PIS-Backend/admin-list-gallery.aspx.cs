﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class admin_list_gallery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            General all = new General();
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
               
                BindGallery();
            }
        }
        public void BindGallery(string txtSearch = null)
        {
            string pageName = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
            Gallery g = new Gallery();
            int type = g.findCategoryPage(pageName);
            Level l = new Level();
            string pageID = g.findPageAdminID(pageName);
            if (!l.isValidAccessPage(Session["Level"].ToString(),pageID))
            {
                Response.Redirect("admin.aspx");
            }
            DataTable dt = g.AdminGallery(type);

            GridViewGallery.DataSource = dt;
            GridViewGallery.DataBind();
        }
        protected void EditClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string link = lb.Attributes["Link"];
            string id = lb.CommandArgument;
            Response.Redirect(link+"?ID=" + id);
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Gallery g = new Gallery();
            try
            {
                g.Delete(id,getip(),GetBrowserDetails()," ID = " + id);
                BindGallery();
            }
            catch(Exception ex)
            {
                General all = new General();
                g.ErrorLogHistory(getip(), GetBrowserDetails(), "Delete", g.tableName, Session["Username"].ToString(), Session["UserID"].ToString(),ex.Message.ToString());
                Response.Write(all.alert("Error " + ex.ToString()));
            }

        }
        public void GVGalleryPageindexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGallery();
            GridViewGallery.PageIndex = e.NewPageIndex;
            GridViewGallery.DataBind();
        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            string pageName = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
            
            Gallery g = new Gallery();
            General all = new General();
            string addPage = g.AddPageGallery(pageName);
            Response.Redirect(addPage);
        }
    }
}