﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="keberlanjutan-old.aspx.cs" Inherits="PIS_Backend.keberlanjutan_old" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     	<!-- Top Main -->
  	<section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="index.html" id="li_1" runat="server">Home</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="li_2" runat="server">Komitmen & Keberlanjutan</li>
					    <li class="breadcrumb-item active" aria-current="page" id="li_3" runat="server">Keberlanjutan</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="header_text" runat="server">Keberlanjutan</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>


  	<!-- Section -->
  	<section id="tata-kelola-perusahaan">
  		  <div id="content" runat="server" >

        </div>
  	</section> 

  <!-- Vendor JS Files -->
  <script src="vendor/jquery.min.js"></script>
  <script src="vendor/bootstrap-4.5.3-dist/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="vendor/jquery-sticky/jquery.sticky.js"></script>
<%--  <script src="vendor/venobox/venobox.min.js"></script>
  <script src="vendor/waypoints/lib/jquery.waypoints.min.js"></script>--%>
  <%--<script src="vendor/counterup/counterup.min.js"></script>--%>
  <script src="vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="vendor/aos/aos.js"></script>
  <script src="vendor/fontawesome/js/all.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Template Main JS File -->
  <script src="js/main.js?v=0.1"></script>

     <script type="text/javascript">
         $("#tanggung-jawab-sosial, #laporan-keberlanjutan").bind('click', function (e) {
             e.preventDefault();
             let id = $(this).prop('id');
             $(".list-content li").removeClass("active");
             $(this).parent().addClass("active");

             $(".detail-content .row").children().addClass("d-none");
             $(".c-" + id).removeClass("d-none");
             $(".c-" + id).children().children().removeClass("d-none");
         });
         $('.image-popup-vertical-fit').magnificPopup({
             type: 'image',
             closeOnContentClick: true,
             mainClass: 'mfp-img-mobile',
             image: {
                 verticalFit: true
             },
             zoom: {
                 enabled: true,
                 duration: 300
             }
         });
         $(".popup-gallery").magnificPopup({
             delegate: 'a',
             type: 'image',
             tLoading: 'Loading image #%curr%...',
             mainClass: 'mfp-img-mobile',
             gallery: {
                 enabled: true,
                 navigateByImgClick: true,
                 preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
             },
             image: {
                 tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                 titleSrc: function (item) {
                     return item.el.attr('title') + '';
                 }
             }
         }); 
     </script>
</asp:Content>