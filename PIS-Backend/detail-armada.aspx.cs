﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class detail_armada : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                LoadData();
                LoadPage();

            }
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
        public void LoadData()
        {
            if (Request.QueryString["search"] != null)
            {
                string uuid = Request.QueryString["search"].ToString();
                Kapal k = new Kapal();

                string cond = " name = REPLACE('" + uuid + "','-',' ') ";
                DataTable dt = k.Select("*", cond);
                ListViewDetailArmada.DataSource = dt;
                ListViewDetailArmada.DataBind();
            }
            else
            {
                Response.Redirect("Index.aspx");
            }
        }
        public void LoadPage()
        {
            bool langEn = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? true : false;
            linkTitle.InnerText = langEn ? "Home" : linkTitle.InnerText;
            link2.InnerText = langEn ? "Our Fleet" : link2.InnerText;
            htitle.InnerText = link2.InnerText;
            editClassLang();
            
        }
    }
}