﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class admin_visimisi : System.Web.UI.Page
    {

        public string PageUrl;
        string folderHeader = "Header";
        string folderDownload = "documents";
        General all = new General();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "11"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                successdiv.Visible = false;
                LoadData();

            }
        }
        public void LoadData()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 7 ");
            content.InnerHtml = dt.Rows[0]["content"].ToString();
            content_en.InnerHtml = dt.Rows[0]["content_en"].ToString();
            PageUrl = dt.Rows[0]["url"].ToString();
            FileUpload2.Visible = false;
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                PagesData pd = new PagesData();
                string validation = "";
                bool HeaderExsist = false;
                bool validheader = true;
                bool DownloadExsist = false;
                bool validDownload = true;
                string newRecord = "set ";
                if (FileUpload1.FileName != "")
                {
                    HeaderExsist = true;
                    newRecord += " Header = '" + folderHeader + "/VISIMISI-" + FileUpload1.FileName + "',";
                    if (!all.ImageFileType(FileUpload1.FileName))
                    {
                        validheader = false;
                    }

                }
                newRecord += "content = '" + content.InnerText + "', ";
                newRecord += "content_en = '" + content_en.InnerText + "' ";
                if(FileUpload2.FileName != "")
                {
                    DownloadExsist = true;
                    newRecord += ", others = '" + folderDownload + "/VISIMISI-" + FileUpload2.FileName + "'";
                    if (!all.PdfFileType(FileUpload2.FileName))
                    {
                        validDownload = false; 
                    }
                }
                if(validheader == false)
                {
                    validation = "Header harus Foto";
                }
                if(validDownload == false)
                {
                    validation = "File harus Pdf";
                }
                if(validation == "")
                {
                    if(HeaderExsist == true)
                    {
                        UploadFile(folderHeader, FileUpload1.PostedFile.FileName);
                    }
                    if(DownloadExsist == true)
                    {
                        UploadFilePdf(folderDownload, FileUpload2.PostedFile.FileName);
                    }
                    string cond = " ID = 7";
                    string Query = "Update Pages " + newRecord + " Where ID = 7";
                  //  string Query = "Insert Into current_log Values ('"+getip()+"','"+GetBrowserDetails()+"', '"+ DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','"+Session["Username"].ToString()+" Save Page Visi Misi "+newRecord.Replace("'","")+"',"+Convert.ToInt32(Session["UserID"].ToString()) +", 'Success')";
                    pd.setData(Query);
                    //pd.Save(getip(),GetBrowserDetails(),7,cond);
                    
                    successMessage(pd.successMessage);
                   // Response.Write(all.alert(Query));
                }
                else
                {
                    showValidationMessage(validation);
                }

            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void showValidationMessage(string textMessage)
        {
            successdiv.Visible = false;
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void successMessage(string textMessage)
        {
            LblErrorMessage.Visible = false;
            successdiv.InnerHtml = textMessage;
            successdiv.Visible = true;

        }
        public void UploadFile(string folderName, string fileUpload)
        {
            
            string fn = System.IO.Path.GetFileName(fileUpload);
            string SaveLocation = Server.MapPath(folderName) + "\\" +"VISIMISI-" + fn;

            FileUpload1.PostedFile.SaveAs(SaveLocation);
        }
        public void UploadFilePdf(string folderName, string fileUpload)
        {

            string fn = System.IO.Path.GetFileName(fileUpload);
            string SaveLocation = Server.MapPath(folderName) + "\\" + "VISIMISI-" + fn;

            FileUpload2.PostedFile.SaveAs(SaveLocation);
        }
    }
}