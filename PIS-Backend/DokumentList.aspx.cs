﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace PIS_Backend
{
    public partial class DokumentList : System.Web.UI.Page
    {
        SqlConnection con = Connection.conn();
        ModelData Db = new ModelData();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                    Level l = new Level();
                    if (!l.isValidAccessPage(Session["Level"].ToString(), "33"))
                    {
                        Response.Redirect("admin.aspx");
                    }

                    bindDokumen();
                    errordiv.Visible = false;
                
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        
        public void bindDokumen()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select * from Dokumen order by Date desc";

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            GridViewDokumen.DataSource = dt;
            GridViewDokumen.DataBind();
        }

        protected void GridViewDokumen_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            bindDokumen();
            GridViewDokumen.PageIndex = e.NewPageIndex;
            GridViewDokumen.DataBind();
        }

        
        protected void DeleteDocument(object sender, EventArgs e)
        {
            try
            {
                LinkButton lb = (LinkButton)sender;
                string id = lb.CommandArgument;
                DataTable dt = deleteDokumen(Convert.ToInt32(id));
                string fileName = dt.Rows[0]["File_to_Delete"].ToString();
                fileName = fileName.Replace("/documents/", "");
                string path = Server.MapPath("documents//" + fileName);
                FileInfo file = new FileInfo(path);
                //if (file.Exists)//check file exsit or not  
                //{
                //file.Delete();
                bindDokumen();
                //}
                //else
                //{
                //    showValidationMessage("File tidak ada, atau sudah terhapus manual");
                //}

            }

            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
            
        }
        public DataTable deleteDokumen(int id)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("DeleteDokumen", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID", id);
            cmd.Parameters.AddWithValue("@mainIP", getip());
            cmd.Parameters.AddWithValue("@browserDetail", GetBrowserDetails());
            cmd.Parameters.AddWithValue("@createdAt", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            cmd.Parameters.AddWithValue("@user_id",Session["UserID"].ToString());
            
            cmd.ExecuteNonQuery();
            con.Close();

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }
        public void showValidationMessage(string message)
        {
            errordiv.InnerHtml = message;
            errordiv.Visible = true;
        }
    }
}