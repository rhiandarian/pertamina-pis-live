﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="laporan-ikhtisar-keuangan.aspx.cs" Inherits="PIS_Backend.laporan_ikhtisar_keuangan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a id="ttl" href="Index.aspx"></a></li>
					    <li class="breadcrumb-item"><a href="" id="link1" runat="server">Hubungan Investor</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="link2" runat="server">Laporan &amp; Ikhtisar Keuangan</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="ttl" runat="server">Laporan &amp; Ikhtisar Keuangan</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader"  runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <section id="laporan-ikhtisar-keuangan">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div class="col-12" id="content" runat="server">
	  				
	  			</div>
	  		</div>

	  		<div class="row mb-5 content">
            <asp:ListView ID="ListViewKeuangan" runat="server">
                <ItemTemplate>
                    <div class="col-xs-12 col-sm-12 box-content">
	  				    <div class="row pt-3 pb-3">
	  					    <div class="col-xs-12 col-sm-4 content-list">
	  						    <span><%# Eval("title") %></span>
	  					    </div>
	  					    <div class="col-xs-12 col-sm-4 content-list">
	  						    <a href="LaporanKeuangan/<%# Eval("url") %>"><%# Eval("url") %></a>
	  					    </div>
	  				    </div>
	  			</div>
                </ItemTemplate>
            </asp:ListView>
	  			
	  			
	  		</div>

	  	</div>
  	</section>
    <script>
        var urlName = '', title = '';
        if ($("#LbInd").attr("class") == "active") {
            urlName = 'laporan-ikhtisar-keuangan';
            title = 'Beranda';
        }
        if($("#LbEn").attr("class") == "active")
        {
            urlName = 'financial-statement';
            title = 'Home';
        }
        history.pushState({}, null, urlName);
        $("#ttl").text(title);
    </script>
</asp:Content>
