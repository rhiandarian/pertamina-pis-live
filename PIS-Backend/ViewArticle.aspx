﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="ViewArticle.aspx.cs" Inherits="PIS_Backend.ViewArticle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid" style="background-color:ghostwhite">
        <div class="text-center pt-5">
            <h2>Article</h2>
        </div><hr />
        <div class="row">
            <div class="col">
                <h4 id="title" runat="server"></h4>
                <asp:Label ID="LblDate" runat="server" Text="Label" Font-Bold="true"></asp:Label>
            </div>
        </div>
        
        <div class="row pt-3">
            <div class="col">
                <asp:Image ID="imgArticle" runat="server"  Width="95%"/>
            </div>
        </div>

        <div class="row pt-3">
            <div class="col">
                <p id="content" runat="server">

                </p>
            </div>
        </div>
        <div class="text-right">
            <asp:LinkButton ID="LinkButtonBack" OnClick="LinkButtonBack_Click" CssClass="btn btn-info" runat="server"><i class="fa fa-home"></i></asp:LinkButton>
        </div>
    </div>
</asp:Content>
