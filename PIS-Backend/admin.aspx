﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="PIS_Backend.admin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper wrapper-content animated fadeIn">

               <h2>Dashboard</h2>
                Pertamina International Shipping - Halaman Admin

            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Sekilas Website</h5>
                            
                            <div class="ibox-tools">
                                
                            </div>
            </div>
            <div class="ibox-content">
                <ul class="list-group clear-list m-t" id="ticket-stats">
                    <asp:ListView ID="ListViewSummary" runat="server">
                        <ItemTemplate>
                            <li class="list-group-item">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href=' <%# Eval("Link") %>' target="_blank"><i class="<%# Eval("Icon") %>"></i> <%# Eval("Category_name") %></a>
                                </div>
                                <div class="col-md-6 text-right">
                                    <span class='<%# Eval("label") %>'>
                                        <%# Eval("CountCategory") %>
                                    </span>
                                </div>
                            
                            </div>
                            
                            
                         </li>
                        </ItemTemplate>
                        
                    </asp:ListView>
                    
                    

                </ul>
                
            </div>
        </div>
    </div>
</asp:Content>
