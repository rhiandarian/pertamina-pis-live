﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace PIS_Backend
{
    public class Image:ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName = "ImagesDetail";

        string viewName = "ViewImageFull";

        DateTime dateNow = DateTime.Now;

        public int imageMaxSize = 300000;

        public string imageMaxSizeString = "150 KB";

        General all = new General();

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        private string title;

        private string title_en;

        private string img_file;

        private int gallery_id;

        private int file_input_size;

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string TitleEn
        {
            get { return title_en; }
            set { title_en = value; }
        }
        public string Img_File
        {
            get { return img_file; }
            set { img_file = value; }
        }

        public int Gallery_Id
        {
            get { return gallery_id; }
            set { gallery_id = value; }
        }
        public int FileInputSize
        {
            get { return file_input_size; }
            set { file_input_size = value; }
        }
        public string Dates
        {
            get;
            set;
        }

        public DataTable Select(string field, string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            string query = Db.SelectQuery(field, viewName, conditions);

            DataTable dt = Db.getData(query);

            return dt;

        }
        public string SelectCategory(string field, string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            string query = Db.SelectQuery(field, "ViewCategoryImage", conditions);

            return query;
        }

        public DataSet LoadCategoryImage()
        {
            string Query = SelectCategory("ID, Category_name");
            DataSet dt = Db.getDataSet(Query);

            return dt;
        }
        public string validate()
        {
            if (title == null || title == "")
            {
                return "judul dalam bahasa Indonesia harus di isi";
            }
            if (title_en == null || title_en == "")
            {
                return "judul dalam bahasa Inggris harus di isi";
            }
            if(img_file == null || img_file == "")
            {
                return "Foto harus diunggah";
            }
            else
            {
                if(!all.ImageFileType(img_file))
                {
                    return "Masukan file foto";
                }
                if(file_input_size > imageMaxSize)
                {
                    return "Ukuran file terlalu besar, Ukuran maximum " + imageMaxSizeString;
                }
            }
            
            
            
            return "valid";
        }
        public string Insert(string mainIP,string browserDetail)
        {
            try
            {
                if (validate() != "valid")
                {
                    return validate();
                }
                string field = "title, ";
                field += "title_en, ";
                field += "img_file, ";
                field += "GalleryID, ";

                field += "addDate, addUser";

                string value = "'" + title + "', ";
                value += "'" + title_en + "', ";
                value += "'Gallerry/" + img_file + "', ";
                value += gallery_id + ", ";

                value += "'" + dateNow + "', " + user_id;

                string query = "INSERT INTO " + tableName + " (" + field + ") VALUES (" + value + ")";
                query += logHistoryQuery(mainIP, browserDetail, logActionInsert(user_id, tableName, field, value), Convert.ToInt32(user_id), "Success");

                Db.setData(query);

                return "success";
            }
            catch(Exception e)
            {
                ErrorLogHistory(mainIP,browserDetail,"Insert",tableName,getUserName(user_id),user_id,e.Message.ToString());
                return e.Message.ToString();
            }
            
        }
        
        public void Delete(string id,string mainIP,string browserDetail,string condition = null)
        {
            string conditions = condition != null ? condition : "";

            Db.Delete(tableName, dateNow, user_id, conditions,Convert.ToInt32(id),mainIP,browserDetail);
        }
        

        
    }
}