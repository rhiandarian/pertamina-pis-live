﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EditInfoPengadaan.aspx.cs" Inherits="PIS_Backend.EditInfoPengadaan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Informasi Pengadaan</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Menu Lainnya 
                        </li>
                        <li class="breadcrumb-item">
                            Pengadaan
                        </li> 
                        <li class="breadcrumb-item active">
                           <a href="SavePengadaan.aspx"><strong>Deskripsi</strong></a> 
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
        </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Halaman Pengadaan</small></h5>
                              <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                <div id="successdiv" class="alert alert-success" runat="server"></div>
                <div class="form-group">
                    <label for="FileUpload1"><b>Header Foto</b></label>
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </div>
                 <div class="form-group">
                    <label for="content"><b>content</b></label>
                   <textarea class="summernote" id="content" runat="server" placeholder="Input Content News" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <label for="content_en"><b>content english</b></label>
                   <textarea class="summernote" id="content_en" runat="server" placeholder="Input Content News" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <asp:Button ID="Save" CssClass="btn btn-primary" runat="server" Text="Save" OnClick="Save_Click"    />
                    <a href="informasi-pengadaan.aspx" target="_blank"><i class="fa fa-eye"></i> Lihat hasil website </a>
                </div>
                
            </div>
            
        </div>
    </div>
</asp:Content>
