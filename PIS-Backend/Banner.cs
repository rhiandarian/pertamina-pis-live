﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;


namespace PIS_Backend
{
    public class Banner : ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName = "Banner";

        public string viewName = "View_Banner";

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        public string title;

        public string title_en;

        public string description;

        public string description_en;

        public string url;

        public string img_file;

        public string buttonText;

        public string buttonTextEn;

        public string Title { get { return title; } set { title = value; } }

        public string TitleEn { get { return title_en; } set { title_en = value; } }

        public string Description { get { return description; } set { description = value; } }

        public string DescriptionEn { get { return description_en; } set { description_en = value; } }

        public string Url { get { return url; } set { url = value; } }

        public string Img_File { get { return img_file; } set { img_file = value; } }

        public string ButtonText { get { return buttonText; } set { buttonText = value; } }

        public string ButtonTextEn { get { return buttonTextEn; } set { buttonTextEn = value; } }

        public string Validation()
        {
            if (title == "")
            {
                return "Judul harus di isi";
            }
            if (buttonText == "")
            {
                return "Kata dalam Tautan harus di isi";
            }
            if (url == "Choose Page")
            {
                return "Tautan harus dipilih";
            }
            return "Valid";
        }
        public DataTable Select(string field, string cond = null)
        {
            cond = cond == null ? "" : cond;
            string Query = Db.SelectQuery(field, viewName, cond);

            DataTable dt = Db.getData(Query);
            return dt;
        }
        public string Insert(string mainIP,string browserDetail)
        {
            try
            {
                string valid = Validation();
                if (valid != "Valid")
                {
                    return valid;
                }
                string field = "title, title_en, description, description_en, url, img_file, buttonText, buttonTextEn, addDate, adduser";
                string value = "'" + title + "', '" + title_en + "', '" + description + "', '" + description_en + "', '" + url + "', 'Banner/" + img_file + "', '" + buttonText + "', '" + buttonTextEn + "', '" + dateNow + "', " + user_id;

                string Query = "INSERT INTO " + tableName + " (" + field + ") VALUES (" + value + ")";
                Query += logHistoryQuery(mainIP, browserDetail, logActionInsert(user_id, tableName, field, value),Convert.ToInt32(user_id),"Success");

                Db.setData(Query);

                return "success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }


        }
        public string Update(int id, string mainIP, string browserDetail, string conditions = null)
        {
            try
            {
                string valid = Validation();
                if (valid != "Valid")
                {
                    return valid;
                }

                string newRecord = "";
                newRecord += "title = '" + title + "', ";
                newRecord += "title_en = '" + title_en + "', ";
                newRecord += "description = '" + description + "', ";
                newRecord += "description_en = '" + description_en + "', ";
                newRecord += "url = '" + url + "', ";
                newRecord += "buttonText = '" + buttonText + "', ";
                newRecord += "buttonTextEn = '" + buttonTextEn + "', ";
                newRecord += img_file != null ? "img_file = 'Banner/" + img_file + "', " : "";

                conditions = conditions == null ? "" : conditions;

                Db.Update(tableName, newRecord, dateNow, user_id, conditions, mainIP, browserDetail, id);

                return "success";


            }
            catch (Exception ex)
            {
                return ex.ToString();
            }



        }
        public void Delete(string id, string mainIP, string browserDetail, string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            Db.Delete(tableName, dateNow, user_id, conditions, Convert.ToInt32(id), mainIP, browserDetail);

        }
    }
}