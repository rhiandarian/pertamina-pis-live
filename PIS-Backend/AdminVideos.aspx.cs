﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class AdminVideos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                BindGVVideos();
            }
        }
        public void BindGVVideos(string txtSearch = null)
        {
            Videos vd = new Videos();

            string conditions = txtSearch != null ? "title like '%" + txtSearch + "%' or content like '%" + txtSearch + "%' or category like '%" + txtSearch + "%' " : "";

            string select = "ID, title, SUBSTRING(content,1,100)as content, category";

            DataTable dt = txtSearch == null ? vd.Select(select) : vd.Select(select, conditions);

            GVVideos.DataSource = dt;
            GVVideos.DataBind();
        }
        protected void btnSearch(object sender, EventArgs e)
        {
            BindGVVideos(TxtSearch.Text);
        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            string WhereId = "ID = " + id;
            Videos vdo = new Videos();
            vdo.Delete(WhereId);
            BindGVVideos();
        }
        protected void EditClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Response.Redirect("EditVideos.aspx?ID=" + id);
            ;
            
        }
    }
}