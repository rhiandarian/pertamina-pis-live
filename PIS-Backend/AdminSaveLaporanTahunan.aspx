﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminSaveLaporanTahunan.aspx.cs" Inherits="PIS_Backend.AdminSaveLaporanTahunan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Laporan Tahunan</h2>
                    <ol class="breadcrumb">
                         <li class="breadcrumb-item">
                             Menu Lainnya 
                        </li> 
                        <li class="breadcrumb-item">
                             Hubungan Investor 
                        </li> 
                        <li class="breadcrumb-item">
                             Laporan Tahunan 
                        </li> 
                        <li class="breadcrumb-item active">
                           <a href="AdminLaporanTahunan.aspx"><strong>Buat Baru</strong></a> 
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Simpan Laporan Tahunan</small></h5>
                              <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                <div class="form-group">
                        <label for="category_name"><b>Year</b></label>
                        <asp:TextBox ID="year" cssClass="form-control" onkeypress="onlyNumberKey(event);" placeholder="Tahun " runat="server"></asp:TextBox>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                            ControlToValidate="year" runat="server"
                            ErrorMessage="Only Numbers allowed"
                            ValidationExpression="\d+">
                        </asp:RegularExpressionValidator>
                      </div>
                     <div class="form-group">
                            <label for="FileUpload1"><b>PDF</b></label>
                            <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                         </div>
                <div class="form-group">
                            <label for="FileUpload1"><b>Cover</b></label>
                            <asp:FileUpload ID="FileUpload2" cssClass="form-control-file" runat="server" />
                         </div>
                     <div class="form-group">
                         <asp:Button ID="BtnAdd" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="BtnAdd_Click"   />
                      </div>
            </div>
        </div>
    </div>
</asp:Content>
