﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace PIS_Backend
{
    public partial class AdminKapalMilikArmada : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "10"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindKapal();
                BindArmada();
                


            }
        }
        public void BindKapal()
        {
            Kapal kpl = new Kapal();
            kpl.OrderBy = "addDate desc";
            DataTable dt = kpl.Select("*");
            GridViewKapal.DataSource = dt;
            GridViewKapal.DataBind();
        }

        protected void EditClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Response.Redirect("adminSaveKapal.aspx?ID=" + id);
        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Kapal kpl = new Kapal();
            string cond = " ID = " + id;
            General all = new General();
            try
            {
                kpl.Delete(id,getip(),GetBrowserDetails(),cond);
                BindKapal();
            }
            catch(Exception ex)
            {
                kpl.ErrorLogHistory(getip(), GetBrowserDetails(), "Delete", kpl.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                Response.Write(all.alert(ex.Message.ToString()));
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void DeleteArmada(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Armada arm = new Armada();
            string cond = " ID = " + id;
            //arm.Delete(cond);
            BindArmada();
        }
        public void BindArmada()
        {
            Armada arm = new Armada();
            DataTable dt = arm.Select("*");
            ListViewArmada.DataSource = dt;
            ListViewArmada.DataBind();
        }



        protected void GridViewKapal_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindKapal();
            GridViewKapal.PageIndex = e.NewPageIndex;
            GridViewKapal.DataBind();
            TextBox datashow = (TextBox)Master.FindControl("datashow");
            datashow.Text = "";
            
        }

        protected void OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (ListViewArmada.FindControl("DataPager1") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            
            BindArmada();
            TextBox datashow = (TextBox)Master.FindControl("datashow");
            datashow.Text = "armada";
        }

    }
}