﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class AdminLaporanKeuangan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "22"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindLaporanKeuangan();
            }
        }
        public void BindLaporanKeuangan()
        {
            FinancialStatement fs = new FinancialStatement();
            DataTable dt = fs.Select("*");
            ListViewFS.DataSource = dt;
            ListViewFS.DataBind();
        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            FinancialStatement fs = new FinancialStatement();
            General all = new General();
            try
            {
                fs.Delete(id, getip(), GetBrowserDetails(), " ID = " + id);
                BindLaporanKeuangan();
            }
            catch(Exception ex)
            {
                fs.ErrorLogHistory(getip(), GetBrowserDetails(), "Delete", fs.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
            }
            
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
    }
}