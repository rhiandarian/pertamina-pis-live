﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class Register : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "35"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindLevel();
                LblErrorMessage.Visible = false;

            }
        }
        public void BindLevel()
        {
            Users u = new Users();
            DataSet dt = u.SelectLevels();

            ddlLevel.DataTextField = dt.Tables[0].Columns["name"].ToString();
            ddlLevel.DataValueField = dt.Tables[0].Columns["ID"].ToString();
            ddlLevel.DataSource = dt.Tables[0];
            ddlLevel.DataBind();
            ddlLevel.Items.Insert(0, "--Pilih Level Pengguna--");
            ddlLevel.SelectedIndex = 0;
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Users u = new Users();
                if (emp_id.Text != "")
                {
                    u.EmpId = Convert.ToInt32(emp_id.Text);
                }
                u.Username = username.Text;
                u.Password = password.Text;
                u.ConfirmPassword = ConfirmPassword.Text;
                u.Email = email.Text;
                if(ddlLevel.SelectedValue != "--Choose Level User--")
                {
                    u.Level = Convert.ToInt32(ddlLevel.SelectedValue);
                }
                string validation = u.validate();
                if(validation == "Valid")
                {
                    u.Level = Convert.ToInt32(ddlLevel.SelectedValue);
                    DataTable dt = u.SaveUser(getip(),GetBrowserDetails());
                    if(dt.Rows[0]["Status"].ToString() == "Success")
                    {
                        General all = new General();
                        Response.Redirect(u.listPage);
                    }
                    else
                    {
                        showValidationMessage(dt.Rows[0]["Message"].ToString());
                    }
                }
                else
                {
                    showValidationMessage(validation);
                }

            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
    }
}