﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;


namespace PIS_Backend
{
    public partial class AddArticle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                BindCategory();
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;

            }
        }
        public bool ImageFileType(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            switch (ext.ToLower())
            {
                case ".gif":
                    return true;
                case ".jpg":
                    return true;
                case ".jpeg":
                    return true;
                case ".png":
                    return true;
                default:
                    return false;
            }
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            bool valid = true;
            string validationMessage = "";




            

            if (ddlCategory.SelectedValue == "Pilih Category")
            {
                valid = false;
                validationMessage = "Category Must be Choiced";
            }
            if (content.InnerText == "")
            {
                valid = false;
                validationMessage = "Content Must be Filled !";
            }
            if (title.Text == "")
            {
                valid = false;
                validationMessage = "Title Must be Filled !";
            }
            if(city.Text == "")
            {
                valid = false;
                validationMessage = "City Must be Fiiled";
            }

            if (valid)
            {
                string SaveLocation = "";
                if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
                {
                    string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                    SaveLocation = Server.MapPath("Article") + "\\" + fn;
                    try
                    {
                        if(ImageFileType(FileUpload1.FileName))
                        {
                            FileUpload1.PostedFile.SaveAs(SaveLocation);
                        }
                        else
                        {
                            valid = false;
                            validationMessage = "File Must be Image";
                        }
                        
                       



                    }
                    catch (Exception ex)
                    {
                        valid = false;
                        validationMessage = ex.Message;
                    }
                }
                else
                {
                    valid = false;
                    validationMessage = "Please Select Upload File";
                }
                if(valid)
                {
                    Article atc = new Article();
                    atc.Title = title.Text;
                    atc.Content = content.InnerHtml;
                    atc.Category_Id = Convert.ToInt32(ddlCategory.SelectedValue);
                    atc.Img_File = "Article/" + FileUpload1.FileName;
                    atc.City = city.Text;
                    atc.Insert();

                    LblErrorMessage.Visible = false;
                    Response.Redirect("Articles.aspx");
                }
                else
                {
                    LblErrorMessage.Visible = true;
                    LblErrorMessage.Text = validationMessage;
                }
                
            }
            else
            {
                LblErrorMessage.Visible = true;
                LblErrorMessage.Text = validationMessage;
            }
            
            

            
        }
        public void BindCategory()
        {
            Category ctg = new Category();

            ctg.Category_Type_Id = 1;

            string selectCategory = "ID, Category_name";
            

            DataSet dt = ctg.SelectList(selectCategory,"and");

            ddlCategory.DataTextField = dt.Tables[0].Columns["Category_name"].ToString();
            ddlCategory.DataValueField = dt.Tables[0].Columns["ID"].ToString();
            ddlCategory.DataSource = dt.Tables[0];
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, "Pilih Category");
            ddlCategory.SelectedIndex = 0;



        }
    }
}