﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace PIS_Backend
{
    public class FinancialStatement:ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName = "LaporanKeuangan";

        string viewName = "View_LaporanKeuangan";

        public int PageSize = 8;

        public int PageID = 20;

        public string folderName = "LaporanKeuangan";

        public string listPage = "AdminLaporanKeuangan.aspx";

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        public string url;

        public int year;

        public string Url
        {
            get { return url; }
            set { url = value; }
        }
        public int Year
        {
            get { return year; }
            set { year = value; }
        }
        public DataTable Select(string field, string cond = null)
        {
            cond = cond == null ? "" : cond;
            string Query = Db.SelectQuery(field, viewName, cond);
            DataTable dt = Db.getData(Query+" Order by year desc");
            return dt;
        }
        public string validate()
        {
            if (year == 0)
            {
                return "tahun harus dipilih";
            }
            int yearNow = Convert.ToInt32(DateTime.Now.Year.ToString());
            if (year > yearNow)
            {
                return "tahun tidak valid";
            }
            if (url == null || url == "")
            {
                return "file harus di upload";
            }
            return "valid";
        }
        public PagesData getPage()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = " + PageID);

            PagesData p = new PagesData();
            p.Header = dt.Rows[0]["Header"].ToString();
            p.Content = dt.Rows[0]["content"].ToString();

            return p;

        }
        public string Insert(string mainIP,string browserDetail)
        {
            string valid = validate();
            if (valid != "valid")
            {
                return valid;
            }
            try
            {
                SqlCommand cmd = new SqlCommand("SaveLaporanKeuangan", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fileName", url);
                cmd.Parameters.AddWithValue("@year", year);
                cmd.Parameters.AddWithValue("@userID", user_id);
                cmd.Parameters.AddWithValue("@mainIP", mainIP);
                cmd.Parameters.AddWithValue("@browserDetail", browserDetail);
                cmd.Parameters.AddWithValue("@createdAt", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt.Rows[0]["Result"].ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }

        }
        public void Delete(string id,string mainIP,string browserDetail,string condition = null)
        {
            string conditions = condition != null ? condition : "";

            Db.Delete(tableName, dateNow, user_id, conditions,Convert.ToInt32(id),mainIP,browserDetail);
        }
    }
}