﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="laporan-tahunan.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.laporan_tahunan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
                <div class="row">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a id="linktitle" href="Index.aspx">Beranda</a></li>
                            <li class="breadcrumb-item"><a href="laporan-tahunan.aspx" id="link1" runat="server">Hubungan Investor</a></li>
                            <li class="breadcrumb-item active" aria-current="page" id="link2" runat="server">Laporan Tahunan</li>
                        </ol>
                    </nav>
                    <div class="col-12 page-title">
                        <h3 id="htitle" runat="server">Laporan Tahunan</h3>
                        <a id="adminsec" href="LaporanTahunanUpload.aspx" class="btn btn-primary" runat="server"><i class="fa fa-file"></i> Upload Laporan Tahunan</a>
                    </div>
                </div>
            </div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="header" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <asp:Label ID="LblIndex" runat="server" Text="Label"></asp:Label>
    <section id="laporan-tahunan">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div id="content" class="col-12" runat="server">
	  				
	  			</div>
	  		</div>

	  		<div class="row mb-5 content">
        <asp:ListView ID="ListViewLt" runat="server">
            <ItemTemplate>
                <div class="col-xs-12 col-sm-3 box-content">
	  				<div class="col box-pis-sm">
	  					<img src='<%# Eval("cover") %>' class="img-fluid mt-2 mb-2" alt="...">
	  					<p>
	  						<strong><%# Eval("title") %></strong>
	  						<br><asp:LinkButton ID="LbDownload" CssClass="red_pis" runat="server" CommandArgument='<%# Eval("url") %>' OnClick="Download">Download</asp:LinkButton>
	  					</p>
                        
                    
	  				</div>
	  			</div>
            </ItemTemplate>
        </asp:ListView>
	  			
	  			
	  		</div>

        <!---Pagination-->
        <div class="row mt-4">
          <div class="col-12 pagination-box">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <p id="detailPaging" runat="server"></p>
              </div>
            
              <div class="col-xs-12 col-sm-6 text-right">
                <nav aria-label="...">
                <asp:Repeater ID="RptPaging" runat="server">
                    <HeaderTemplate>
                    <ul class="pagination justify-content-end">
                        <li class="page-item">
                             <asp:LinkButton  runat="server" Text="<<" CommandArgument="first"
                             CssClass="page-link" OnClick="Page_Changed"></asp:LinkButton>
                        </li>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li class='<%#Eval("active")%>'>
                            <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Index") %>' CommandArgument='<%# Eval("Index") %>'
                            CssClass="page-link" OnClick="Page_Changed"></asp:LinkButton>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        <li class="page-item">
                            <asp:LinkButton  runat="server" Text=">>" CommandArgument="last"
                            CssClass="page-link" OnClick="Page_Changed"></asp:LinkButton>
                        </li>
                      </ul>
                   </FooterTemplate>
               </asp:Repeater>
                  
                    
                </nav>
              </div>
            </div>
          </div>
        </div>

	  	</div>
  	</section>
    <script>
        var urlName = '', title = "";
        if ($("#LbEn").attr("class") == "active") {
            urlName = 'annual-report';
            title = "Home";
        }
        if ($("#LbInd").attr("class") == "active") {
            urlName = 'laporan-tahunan';
            title = "Beranda";
        }
        $("#linktitle").text(title);
        history.pushState({}, null, urlName);
    </script>
</asp:Content>
