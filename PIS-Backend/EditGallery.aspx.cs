﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class EditGallery : System.Web.UI.Page
    {
        General all = new General();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                LoadCategory();
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadData();
            }
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void LoadCategory()
        {
            Gallery g = new Gallery();

            DataSet dt = g.LoadCategory(2);

            DdlCategory.DataTextField = dt.Tables[0].Columns["Category_name"].ToString();
            DdlCategory.DataValueField = dt.Tables[0].Columns["ID"].ToString();
            DdlCategory.DataSource = dt.Tables[0];
            DdlCategory.DataBind();
            DdlCategory.Items.Insert(0, "Pilih Category");
            DdlCategory.SelectedIndex = 0;
        }
        public void LoadData()
        {
            if (Request.QueryString["ID"] == null)
            {
                Response.Redirect("AdminGallery.aspx");
            }
            int Id = Convert.ToInt32(Request.QueryString["ID"]);
            Gallery g = new Gallery();
            string field = "title, content, video_url, img_url, embed_url, category_id";
            string conditions = " ID = " + Id;

            DataTable dt = g.Select(field, conditions);
            title.Text = dt.Rows[0]["title"].ToString();
            DdlCategory.SelectedValue = dt.Rows[0]["category_id"].ToString();
            if(Convert.ToInt32(dt.Rows[0]["category_id"].ToString()) != all.FotoCategoryID)
            {
                LinkButtonUploadAlbum.Visible = false;
            }

            DdlCategory.Enabled = false;
            if (dt.Rows[0]["category_id"].ToString() == all.VideoCategoryID.ToString())
            {
                PanelVideo.Visible = true;
                PanelImage.Visible = false;
                content.InnerHtml = dt.Rows[0]["content"].ToString();
                videoUrl.Text = dt.Rows[0]["video_url"].ToString();
                imageUrl.Text = dt.Rows[0]["img_url"].ToString();
                
            }
            else
            {
                PanelImage.Visible = true;
                PanelVideo.Visible = false;
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            Gallery g = new Gallery();
            g.Title = title.Text;
            if(DdlCategory.SelectedValue != "Pilih Category")
            {

                g.Category = Convert.ToInt32(DdlCategory.SelectedValue);
            }
            
            
            bool fileExsist = false;

            if(g.Category == all.VideoCategoryID)
            {
                g.Content = content.InnerHtml;
                g.Video_url = videoUrl.Text;
                g.Img_url = imageUrl.Text;
                
            }
            else
            {
                if(DdlCategory.SelectedValue != "Pilih Category")
                {
                    if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
                    {
                        g.Img_File = FileUpload1.FileName;
                        fileExsist = true;
                    }
                }
            }
            int Id = Convert.ToInt32(Request.QueryString["ID"]);
            string conditions = " ID = " + Id;
            string UpdateStatus = g.Update(DdlCategory.SelectedValue,Id,getip(),GetBrowserDetails(),conditions);
            if(UpdateStatus == "success")
            {
                if(fileExsist)
                {
                    string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                    string SaveLocation = Server.MapPath("Gallery") + "\\" + fn;

                    FileUpload1.PostedFile.SaveAs(SaveLocation);
                }
                Response.Redirect("AdminGallery.aspx");
            }
            else
            {
                showValidationMessage(UpdateStatus);
                LoadData();
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = Convert.ToInt32(Request.QueryString["ID"]);
            Response.Redirect("AlbumImages.aspx?GalleryID=" + Id);
        }
    }
}