﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="PIS_Backend.ResetPassword" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/image/pertamina-7oziq.jpg" type="image/png">

    <title> Pertamina International Shipping | Reset Password</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown" style="background-color: white;width: 400px;padding: 40px;margin-top: 100px;">
        <div>
            <div class="form-group">

                 <img src="image/logo-PIS.jpg" width="90%" />
            </div>
            <p>Reset Password.</p>
            <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
            <form id="formInputCode" class="m-t" role="form" runat="server">
                
                <div class="form-group">
                    <asp:TextBox ID="KeyCode" CssClass="form-control" placeholder="Please Input Code on your email" runat="server"></asp:TextBox>
                </div>
                <asp:Button ID="BtnCheckCode" CssClass="btn btn-danger block full-width m-b" runat="server" Text="Send" OnClick="BtnCheckCode_Click" />
             </form>
            <form id="formUpdatePassword" class="m-t" role="form" runat="server">
                <div class="form-group">
                    <asp:TextBox ID="NewPassword" CssClass="form-control" placeholder="New Password" TextMode="Password" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="ConfirmPassword" CssClass="form-control" placeholder="Confirm Password" TextMode="Password" runat="server"></asp:TextBox>
                </div>
                <asp:Button ID="BtnResetPassword" CssClass="btn btn-danger block full-width m-b" runat="server" Text="Reset Password" OnClick="BtnResetPassword_Click" />
               
                 
               
            </form>
            
        </div>
    </div>

    <!-- Mainly scripts -->
    


</body>

</html>
