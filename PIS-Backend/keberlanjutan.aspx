﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="keberlanjutan.aspx.cs" Inherits="PIS_Backend.keberlanjutan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     	<!-- Top Main -->
  	<section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx" id="li_1" runat="server">Home</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="li_2" runat="server">Komitmen & Keberlanjutan</li>
					    <li class="breadcrumb-item active" aria-current="page" id="li_3" runat="server">Keberlanjutan</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="header_text" runat="server">Keberlanjutan</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>


  	<!-- Section -->
<section id="tata-kelola-perusahaan">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div class="col-12">
	  				<h3 id="header_text1" runat="server">Keberlanjutan</h3>
	  			</div>
	  		</div>

	  		<div class="row mb-5 content">
	  			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 list-content">
	  				<ul>
	  					<li class="active">
	  						<a href="component/tata-kelola.html" id="tanggung-jawab-sosial"   class="listTataKelola"><span id="tjs" runat="server">Tanggung Jawab Sosial</span></a>
	  					</li>
	  					<li>
	  						<a href="component/tujuan-penerapan.html" id="laporan-keberlanjutan"   class="listTataKelola d-none"><span id="lk" runat="server">Laporan Keberlanjutan</span></a>
	  					</li>
	  				</ul>
	  			</div>
	  			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 detail-content">
	  				<div class="row">
	  					
	  					<div class="col-12 c-tanggung-jawab-sosial">
                <div class="row">
                  <div class="col-12" id="content" runat="server">
    	  						 
                  </div>

                  <div class="col-12">
                    <div class="row">
                        <asp:ListView ID="ListViewPhoto" runat="server">
                            <ItemTemplate>
                              <div class="col-md-6 col-xs-12 p-3 popup-gallery album-<%# Eval("No") %>">
                                <div class="box-pis p-3 text-left">
                                  <a href="<%# Eval("Img_File") %>" class="image-popup-vertical-fits" title='<%# Eval("Title") %>'>
                                    <img src="<%# Eval("Img_File") %>" class="img-fluid mt-2 mb-2" alt="...">
                                  </a> 
                                  <p>
                                    <em><%# Eval("Dates") %></em> <em class="float-right"><%# Eval("detindex") %></em>
                                    <br><strong><%# Eval("Title") %></strong>  
                                  </p>

                                    <asp:ListView ID="ListViewDetailImage" runat="server" DataSource='<%# Eval("ImageDetail") %>'>
                                        <ItemTemplate>
                                            <a class="image-popup-vertical-fit" href='<%# Eval("Img_File") %>' title='<%# Eval("Title") %>'></a>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </div>
                              </div>
                          </ItemTemplate>
                       </asp:ListView>
                    </div>
                    
                  </div>
                  
                </div>


	  					</div>

	  					<div class="col-12 c-laporan-keberlanjutan d-none">
	  						<p class="title">Laporan Keberlanjutan </p>
	  						<div class="row">
                              <div class="col-md-6 col-xs-12 p-3">
                                <div class="box-pis p-3 text-left">
                                  <a href="images/default-photo.png" class="image-popup-vertical-fit">
                                    <img src="images/default-photo.png" class="img-fluid mt-2 mb-2" alt="...">
                                  </a>
                                  <p>
                                    <small>05 Mei 2021</small>
                                    <br><strong>Doa Bersama dan Santunan Anak Yatim Safari Ramadhan</strong>
                                  </p>
                                </div>
                              </div>
                              <div class="col-md-6 col-xs-12 p-3">
                                <div class="box-pis p-3 text-left">
                                  <a href="images/default-photo.png" class="image-popup-vertical-fit">
                                    <img src="images/default-photo.png" class="img-fluid mt-2 mb-2" alt="...">
                                  </a>
                                  <p>
                                    <small>05 Mei 2021</small>
                                    <br><strong>Doa Bersama dan Santunan Anak Yatim Safari Ramadhan</strong>
                                  </p>
                                </div>
                              </div>
                            </div>
	  					</div>

	  				</div>
	  			</div>
	  		</div>

	  	</div>
  	</section> 

  <!-- Vendor JS Files -->
  <script src="vendor/jquery.min.js"></script>
  <script src="vendor/bootstrap-4.5.3-dist/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="vendor/venobox/venobox.min.js"></script>
  <script src="vendor/waypoints/lib/jquery.waypoints.min.js"></script>
  <%--<script src="vendor/counterup/counterup.min.js"></script>--%>
  <script src="vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="vendor/aos/aos.js"></script>
  <script src="vendor/fontawesome/js/all.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Template Main JS File -->
  <script src="js/main.js?v=0.1"></script>

     <script type="text/javascript">
         var urlName = '';
         if ($("#LbInd").attr("class") == "active") {
             urlName = 'keberlanjutan';
         }
         if ($("#LbEn").attr("class") == "active") {
             urlName = 'sustainability';
         }
         history.pushState({}, null, urlName);
         $("#tanggung-jawab-sosial, #laporan-keberlanjutan").bind('click', function (e) {
             e.preventDefault();
             let id = $(this).prop('id');
             $(".list-content li").removeClass("active");
             $(this).parent().addClass("active");

             $(".detail-content .row").children().addClass("d-none");
             $(".c-" + id).removeClass("d-none");
             $(".c-" + id).children().children().removeClass("d-none");
         });
         $('.image-popup-vertical-fit').magnificPopup({
             type: 'image',
             closeOnContentClick: true,
             mainClass: 'mfp-img-mobile',
             image: {
                 verticalFit: true
             },
             zoom: {
                 enabled: true,
                 duration: 300
             }
         });

         let countGallery = $(".popup-gallery").length;
         for (i = 1; i <= countGallery; i++) {
             $(".album-" + i).magnificPopup({
                 delegate: 'a',
                 type: 'image',
                 tLoading: 'Loading image #%curr%...',
                 mainClass: 'mfp-img-mobile',
                 gallery: {
                     enabled: true,
                     navigateByImgClick: true,
                     preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                 },
                 image: {
                     tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                     titleSrc: function (item) {
                         return item.el.attr('title') + '';
                     }
                 }
             });
         }



     </script>
</asp:Content>