﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace PIS_Backend
{
    public partial class SaveLayanan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "9"))
                {
                    Response.Redirect("admin.aspx");
                }

                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;

                LoadData();
            }
            
        }
        public void LoadData()
        {
            if (Request.QueryString["ID"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                Layanan ly = new Layanan();
                string cond = " ID = " + Id;
                DataTable dt = ly.Select("*", cond);
                title.Text = dt.Rows[0]["title"].ToString();
                title_en.Text = dt.Rows[0]["title_en"].ToString() != null ? dt.Rows[0]["title_en"].ToString() : "";
                description.InnerHtml = dt.Rows[0]["description"].ToString();
                description_en.InnerHtml = dt.Rows[0]["description_en"].ToString() != null ? dt.Rows[0]["description_en"].ToString() : "";

            }
        }
        public string txtRandom()
        {
            ImageResize ir = new ImageResize();
            return ir.textrandom();
        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            //try
            //{
                string random = txtRandom();
                Layanan ly = new Layanan();
                ly.Title = title.Text;
                ly.TitleEn = title_en.Text;
                ly.Description = description.InnerText;
                ly.DescriptionEn = description_en.InnerText;
                
               

                if (Request.QueryString["ID"] == null)
                {
                    
                    if (FileUpload1.FileName == "")
                    {
                        showValidationMessage("Foto harus Upload");
                    }
                    else
                    {
                        ly.Img_File = random + FileUpload1.FileName;
                        string insertStatus = ly.Insert(getip(),GetBrowserDetails());
                        if (insertStatus == "success")
                        {
                            UploadLayanan(random);
                        General all = new General();
                        Response.Redirect(ly.IndexPage);
                        }
                        else
                        {
                            showValidationMessage(insertStatus);
                        }
                    }

                }
                else
                {
                    if(FileUpload1.FileName != "")
                    {
                        ly.Img_File = random + FileUpload1.FileName;
                    }
                    int Id = Convert.ToInt32(Request.QueryString["ID"]);
                    string conditions = " ID = " + Id;
                    string updateStatus = ly.Update(Id,getip(),GetBrowserDetails(),conditions);
                    
                    if (updateStatus == "success")
                    {
                        if (FileUpload1.FileName != "")
                        {
                            UploadLayanan(random);
                        }
                        Response.Redirect(ly.IndexPage);
                    }
                    else
                    {
                        showValidationMessage(updateStatus);
                    }
                }
            //}
            //catch (Exception ex)
            //{
            //    Layanan ly = new Layanan();
            //    string actionStatus = Request.QueryString["ID"] == null ? "Insert" : "Update";
            //    ly.ErrorLogHistory(getip(), GetBrowserDetails(), actionStatus, ly.tableName,  Session["Username"].ToString(), Session["UserID"].ToString(), " Error with " + ex.ToString());
            //    showValidationMessage(ex.Message.ToString());
            //}


        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void UploadLayanan(string random)
        {
            ImageResize Iz = new ImageResize();
            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string SetFolderBeforeResize = Server.MapPath("Layanan") + "\\" + "serize" + random + fn; //gambar yang akan diresize sesudah direze gambar akan dihapus
            string SetFolderAfterResize = Server.MapPath("Layanan") + "\\" + random + fn;
            FileUpload1.PostedFile.SaveAs(SetFolderBeforeResize);
            Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "Layanan Kami", "Cut");
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
    }
}