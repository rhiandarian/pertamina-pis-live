﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class AlbumImages : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                 LoadAlbum();
            }
        }
        public void LoadAlbum()
        {
            if (Request.QueryString["GalleryID"] != null)
            {
                int GalleryID = Convert.ToInt32(Request.QueryString["GalleryID"].ToString());
                Image img = new Image();
                string conditions = " GalleryID =" + GalleryID;
                DataTable dt = img.Select("ID,title, img_file, CONVERT(varchar,addDate,106) as 'image_date'", conditions);
                ListViewAlbum.DataSource = dt;
                ListViewAlbum.DataBind();
            }
            else
            {
                Response.Redirect("AdminGallery.aspx");
            }
        }
        public void ReturnHome(object sender, EventArgs e)
        {
            int GalleryID = Convert.ToInt32(Request.QueryString["GalleryID"].ToString());
            Response.Redirect("admin-edit-gallery.aspx?ID=" + GalleryID);
        }

        protected void LinkButtonUploadAlbum_Click(object sender, EventArgs e)
        {
            int GalleryID = Convert.ToInt32(Request.QueryString["GalleryID"].ToString());
            Response.Redirect("UploadAlbum.aspx?GalleryID=" + GalleryID);
        }
        public void Delete(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            int imageId = Convert.ToInt32(lb.CommandArgument);

            Image img = new Image();
            string conditions = " ID =" + imageId;
            img.Delete(imageId.ToString(),getip(),GetBrowserDetails(),conditions);

            LoadAlbum();
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        

    }
}