﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="Board of Commissioners.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.Board_of_Commissioners" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Top Main -->
  	<section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx">Home</a></li>
					    <li class="breadcrumb-item"><a href="">Management</a></li>
					    <li class="breadcrumb-item active" aria-current="page">Board of Commissioners</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3>COMMISIONERS</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>


  	<!--  -->
  	<section id="dewan-komisaris">
  		<div class="container">
	  		<div class="row">
	  			<div class="col-12">
  					<h5><strong>Board of Commissioners</strong></h5>
                      <div id="content" runat="server" ></div>
	  			</div>
                  
  					
	  			
	  			<div class="col-lg-12 mt-5">
	  				<div class="row">
                          <asp:ListView ID="ListViewKomut" runat="server">
                              <ItemTemplate>
                                  <div class="col-md-10 offset-md-1 col-sm-12 mb-3">
                                      <div class="row">
                                          <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-6 col-12">
                                            <!--<a class="modal-profile" href="components/modal-profile.html">-->
                                            <a class="popup-modal" href="#modal-profile-<%# Eval("ID") %>">
                                              <div class="col box-gradient">
			  						                <center>
			  							                <img src='<%# Eval("Img_File") %>' class="img-fluid" alt="...">
			  							                <p><strong><%# Eval("Name") %></strong>
			  								                <br><%# ((string)Eval("Division")).Replace("`s","'s")  %>
			  							                </p>
			  						                </center>
		  						                </div>
                                            </a>
                                          </div>
                                        </div>
                                      </div>
	  						  </ItemTemplate>
                          </asp:ListView>
                          <asp:ListView ID="ListViewKomisaris" runat="server">
                              <ItemTemplate>
                                  <div class="col-lg-3 col-md-6 col-sm-6 col-12">
	  						        <a class="popup-modal" href="#modal-profile-<%# Eval("ID") %>">
		  						        <div class="col box-gradient">
			  						        <center>
			  							        <img src='<%# Eval("Img_File") %>' class="img-fluid" alt="...">
			  							        <p><strong><%# Eval("Name") %></strong>
			  								        <br><%# ((string)Eval("Division")).Replace("`s","'s") %>
			  							        </p>
			  						        </center>
		  						        </div>
	  						        </a>
	  					         </div>
                              </ItemTemplate>
                          </asp:ListView>
	  					
	  					

	  				</div>
	  			</div>
	  		</div>

	  	</div>
  	</section>
     <asp:ListView ID="ListViewModalKomisaris" runat="server">
        <ItemTemplate>
            <div id="modal-profile-<%# Eval("ID") %>" class="zoom-anim-dialog mfp-hide modal-profile container pb-4 pt-4">
  	            <a class="btn-close-modal"><span class="bi bi-x"></span></a>
  	            <div class="row">
  		            <div class="col-lg-4 col-md-4 col-sm-12 image-profile">
  			            <img src="<%# Eval("img_file") %>" class="img-fluid" alt="...">
  		            </div>
  		            <div class="col-lg-8 col-md-8 col-sm-12">
  			            <div class="row">
  				            <div class="col-12 title">
		  			            <h5 class="mb-0"><strong><%# Eval("name") %></strong></h5>
		  			            <p class="division"><%# Eval("division_en") %></p>
		  			            <!--
		  			            <p class="d-none d-md-block">Dr. A. Junaedy Ganie, FCBArb, MCIArb ANZIIF (Fellow), AIIK (HC), CIP, ChFC, CLU</p>
		  			            <p class="d-sm-block d-md-none">Komisaris</p>
		  			            -->
  				            </div>
  				            <div class="col-12 desc">
  					            <%# Eval("description_en") %></p>

  				            </div>
  			            </div>
  		            </div>
  	              </div>
	          </div>
        </ItemTemplate>
    </asp:ListView>
      <script type="text/javascript">
          history.pushState({}, null, "commissioners");
          function removeStrip(element)
          {
              $("."+element).each(function (i, val) {
                  let desc = $(this).html();
                  var rep = desc.replace("`s", "'s");
                  $(this).html(rep);
              })
          }
          $(document).ready(function () {
              removeStrip("desc");
              removeStrip("division");
              $('.popup-modal').magnificPopup({
                  type: 'inline',
                  preloader: true,
                  modal: true,
                  mainClass: 'my-mfp-zoom-in',
                  closeOnBgClick: true,
                  closeOnContentClick: true,
              });

              $(".btn-close-modal").bind('click', function (e) {
                  e.preventDefault();
                  $.magnificPopup.close();
              });

          });

          $(document).click(function (event) {
              var className = $(event.target).prop('class');
              if (className == 'mfp-content') {
                  $.magnificPopup.close();
              }
          });
      </script>
</asp:Content>
