﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PIS_Backend
{
    public partial class career : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();

            }
        }
        public void LoadData()
        {
            Carier pd = new Carier();
            DataTable dt = pd.Select("*", " ID = 1 ");
            imgHeader.Src = dt.Rows[0]["Header"].ToString(); 
            //string decodecontent = Encoding.UTF8.GetString(Convert.FromBase64String(Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content"].ToString()));
            //content.InnerHtml = decodecontent;
            content.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content"].ToString();
            content_disclaimer.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["disclaimer_en"].ToString() : dt.Rows[0]["disclaimer"].ToString();
            content_magang.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["magang_en"].ToString() : dt.Rows[0]["magang"].ToString();
            //conte_nt.InnerHtml = decodecontent;
            AttributeCollection myAttributes = magang_section.Attributes;
            myAttributes.CssStyle.Add("background-image", dt.Rows[0]["others"].ToString());
            mail_lamaran.HRef = "mailto:" + dt.Rows[0]["mail_lamaran_kerja"].ToString() ?? "";
            mail_magang.HRef = "mailto:" + dt.Rows[0]["mail_permohonan_magang"].ToString() ?? "";
            mail_lamaran.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Submit Your Application" : "Kirim Lamaran Kerja";
            mail_magang.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Internship Application" : "Permohonan Magang";

            li_1.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Home" : "Beranda";
            li_2.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Career" : "Karier"; 
            header_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "CAREER" : "KARIER";
            editClassLang();
        }

        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
    }
}