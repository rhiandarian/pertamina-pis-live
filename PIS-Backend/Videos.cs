﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace PIS_Backend
{
    public class Videos
    {
        SqlConnection con = Connection.conn();

        string tableName = "Video";

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"].ToString();

        ModelData Db = new ModelData();

        private string title;

        private string content;

        private string video_url;

        private string img_url;

        private string embed_url;

        private string category;

        public string Title {
            get { return title; }
            set { title = value; }
        }
        public string Content {
            get { return content; }
            set { content = value; }
        }
        public string Video_url {
            get { return video_url; }
            set { video_url = value; }
        }
        public string Img_url {
            get { return img_url; }
            set { img_url = value; }
        }
        public string Embed_url
        {
            get { return embed_url; }
            set { embed_url = value; }
        }
        public string Category {
            get { return category; }
            set { category = value; }
        }

        public string validate()
        {
            if (title == null || title == "") {
                return "Title must be filled";
            }
            if (content == null || content == "") {
                return "Content must be filled";
            }
            if (video_url == null || video_url == "") {
                return "Video Url must be Filled";
            }
            if (!Uri.IsWellFormedUriString(video_url, UriKind.RelativeOrAbsolute))
            {
                return "Video Url must be Url Format";
            }
            if (img_url == null || img_url == "") {
                return "Thumbnail Image Url must be Filled";
            }
            if(!Uri.IsWellFormedUriString(img_url,UriKind.RelativeOrAbsolute))
            {
                return "Thumbnail Image Url must be Url Format";
            }
            if(embed_url == null || embed_url == "")
            {
                return "Embed Url must be Filled";
            }
            if (category == null || category == "") {
                return "Category must be Filled";
            }
            return "success";

        }
        public string Insert()
        {
            if(validate() != "success")
            {
                return validate();
            }
            try
            {
                string values = "'" + title + "', '" + content + "', '" + video_url + "', '" + img_url + "', '" + category + "', '"+embed_url+"', ";
                values += "'" + dateNow + "', " + user_id;

                string query = "INSERT INTO " + tableName + " (title, content, video_url, img_url, category, embed_url, addDate, addUser) VALUES (" + values + ")";

                Db.setData(query);

                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            
           
        }
        public string Update(string conditions = null)
        {
            if (validate() != "success")
            {
                return validate();
            }
            try
            {
                string newRecord = "set";

                newRecord += " title = '" + title + "',  ";
                newRecord += " content = '" + content + "',  ";
                newRecord += " video_url = '" + video_url + "',  ";
                newRecord += " img_url = '" + img_url + "',  ";
                newRecord += " category = '" + category + "',  ";
                newRecord += " embed_url = '"+embed_url+"', ";

                string flagUpdate = " editDate = '" + dateNow + "', ";
                flagUpdate += "editUser = " + user_id;

                string Query = "Update " + tableName + " " + newRecord + "" + flagUpdate;
                Query += conditions != null ? "Where " + conditions : "";

                Db.setData(Query);

                return "success";

            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }

        }
        public DataTable Select(string field, string conditions = null)
        {

            string and = conditions != null ? " and" : "";
            string flagConditions = and + " (fl_deleted != 1 or fl_deleted IS NULL)";

            string query = "Select " + field + " from " + tableName + " Where " + conditions + "" + flagConditions;

            DataTable dt = Db.getData(query);

            return dt;
        }
        public void Delete(string conditions = null)
        {
            string query = "Update " + tableName + " set fl_deleted = 1, deleteDate = '" + dateNow + "', deleteUser = " + user_id;
            query += conditions != null ? "Where " + conditions : "";
            Db.setData(query);

        }

    }
}