﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PIS_Backend
{
    public class ModelData
    {
        SqlConnection con = Connection.conn();

        
        private string orderBy;

        public string OrderBy
        {
            get { return orderBy; }
            set { orderBy = value; }
        }

        
        


        public void setData(string query)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public DataTable getData(string Query)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = Query;

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public DataTable Sp_Result(string spName, string param)
        {
            return getData("Exec " + spName + " " + param);
        }
        public void Sp_Execute(string spName, string param)
        {
            setData("Exec " + spName + " " + param);
        }
        public DataSet getDataSet(string query)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;

            DataSet dt = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
		public string SelectQuery(string field, string tableOrView, string conditions = null, string others = null)
		{
			string query = "Select " + field + " from "+tableOrView+" ";
            query += conditions != "" ? "Where " + conditions : "";
            query += others != null ? others : "";
            if(orderBy != null)
            {
                query += " Order by " + orderBy;
            }
			
			return query;

		}
        public string loghistoryActionUpdate(string editUser,string tableName, int Id, string newRecord)
        {
            return getUserName(editUser) + " Update " + tableName + " Id " + Id + " with " + newRecord.Replace("'", "");
        }
		public void Update(string tableName,string newRecord,DateTime editDate, string editUser, string conditions,string mainIP,string browserDetail,int Id)
		{
			string flagUpdate = "editDate = '" + editDate + "', editUser = " +editUser;
			string newRecords = "set "+newRecord+" "+flagUpdate;
			string Query = "Update " + tableName + " " + newRecords;
			Query += conditions != "" ? "Where " + conditions : "";

            Query += logHistoryQuery(mainIP, browserDetail, loghistoryActionUpdate(editUser, tableName, Id, newRecord),Convert.ToInt32(editUser),"Success");
            setData(Query);
            
		}
        public void Updates(string tableName, string newRecord,string conditions, DateTime dateNow, string user_id,string mainIP,string browserDetail,int Id)
        {
            Update(tableName, newRecord, dateNow, user_id,conditions,mainIP,browserDetail,Id);
        }
        
		public void Delete(string tableName,DateTime deleteDate, string deleteUser,string conditions,int Id,string mainIP,string browserDetail)
		{
			string Query = "Update " + tableName + " set fl_deleted = 1, deleteDate = '" + deleteDate + "', deleteUser = " + deleteUser;
            Query += conditions != "" ? " Where " + conditions : "";
            string action = getUserName(deleteUser) + " Delete " + tableName + " with Id " + Id;
            Query += logHistoryQuery(mainIP, browserDetail, action, Convert.ToInt32(deleteUser), "Success");
            setData(Query);
		}
        public string logActionInsert(string user_id, string tableName, string field, string value)
        {
            return getUserName(user_id) + " Insert " + tableName + " with Id = '+CAST("+getLastIDQuery(tableName,"ID")+" AS VARCHAR(MAX))+' Field " + field + " Value " + value.Replace("'", "");
        }
        public string InsertQuery(string tableName, string field, string value,DateTime dateNow,string user_id,string mainIP,string browserDetail)
        {
            string flagField = "addDate, addUser";
            string flagValue = "'" + dateNow + "', " + user_id;

            field += flagField;
            value += flagValue;
            string Query = "INSERT INTO "+tableName+" ("+field+") VALUES ("+value+") ";
            //string loghistoryaction = "(Select User_name from Users Where Id = " + user_id + ")+ Insert " + tableName + " with field: " + field + " value: " + value;
            Query += logHistoryQuery(mainIP, browserDetail, logActionInsert(user_id, tableName, field, value), Convert.ToInt32(user_id), "Success");
            

            return Query;
        }
        public void Insert(string tableName, string field, string value, DateTime dateNow, string user_id,string mainIP,string browserDetail)
        {
            string Query = InsertQuery(tableName, field, value,dateNow,user_id,mainIP,browserDetail);
            setData(Query);
        }
        public string getLastIDQuery(string tableName, string IDField)
        {
            
            return "(Select MAX(" + IDField + ") from " + tableName + ")";
        }
        public string getUserName(string user_id)
        {
            Users u = new Users();
            return "'+(Select User_name from " + u.viewName + " Where Id =" + user_id + ")+'";
        }
        public string logHistoryQuery(string mainIp, string browserDetail, string action, int user_id, string status)
        {
            return "INSERT INTO current_log VALUES ('" + mainIp + "','" + browserDetail + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + action + "'," + user_id + ",'" + status + "')";
        }
		public void SaveLogHistory(string logHistoryQuery)
        {

            setData(logHistoryQuery); 

        }
        public void ErrorLogHistory(string mainIp, string browserDetail,string action, string tablename, string username,string user_id,string errorMessage)
        {
            string actionLog = username + " Error When "+action+" " + tablename + " with " + errorMessage;
            SaveLogHistory(logHistoryQuery(mainIp,browserDetail,actionLog,Convert.ToInt32(user_id),"Failed"));
        }
        public void ErrorLogHistoryFull(string mainIp, string browserDetail,string tablename, string username, string user_id, string errorMessage,string id = null)
        {
            string action = id == null ? "Insert" : "Update";
            ErrorLogHistory(mainIp,browserDetail,action,tablename,username,user_id,errorMessage);
        }

        
        




    }
}