﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="VideoProfile.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.VideoProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Video Profil</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Beranda
                        </li>
                        <li class="breadcrumb-item">
                            <a><strong>Video Profil</strong></a>
                        </li>
                        
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Unggah Video Profil</h5>
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                <div id="successdiv" class="alert alert-success" runat="server"></div>
                 <form>
                      <div class="form-group">
                          <div class="row">
                              <div class="col">
                                  <label for="title"><b>judul</b></label>
                                 <asp:TextBox ID="title" cssClass="form-control" placeholder="Masukan Judul Video Profil" runat="server"></asp:TextBox>
                              </div>
                              <div class="col">
                                  <label for="title_en"><b>judul bahasa inggris</b></label>
                                 <asp:TextBox ID="title_en" cssClass="form-control" placeholder="Masukan Judul Video Profil dalam Bahasa Inggris" runat="server"></asp:TextBox>
                              </div>
                          </div>
                        
                      </div>
                     
                     <div class="form-group">
                        <label for="date"><b>tanggal</b></label>
                        <asp:TextBox ID="date" cssClass="form-control" placeholder="Masukan Tanggal" runat="server"></asp:TextBox>
                      </div>
                     <asp:Panel ID="PanelVideo" runat="server">
                         <div class="form-group">
                        <label for="content"><b>kontent</b></label>
                        <textarea id="content" class="summernote"   runat="server" rows="10"></textarea>
                      </div>
                         <div class="form-group">
                        <label for="content_en"><b>kontent bahasa inggris</b></label>
                        <textarea id="content_en" class="summernote"   runat="server" rows="10"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="videoUrl"><b>Link video</b></label>
                          <asp:TextBox ID="videoUrl" CssClass="form-control" placeholder="Masukan Link Url Video" runat="server"></asp:TextBox>
                     </div>
                     <div class="form-group">
                        <label for="imageUrl"><b>Video Thumbnail</b></label>
                          <asp:TextBox ID="imageUrl" CssClass="form-control" placeholder="Masukan Thumbnail Url Youtube" runat="server"></asp:TextBox>
                     </div>
                     
                     </asp:Panel>
                     
                    <div class="form-group">
                        <asp:Button ID="BtnSave" runat="server" CssClass="btn btn-primary" Text="Simpan" OnClick="BtnSave_Click"    />
                    </div>
                      
                </form>
            </div>
        </div>
    </div>
</asp:Content>
