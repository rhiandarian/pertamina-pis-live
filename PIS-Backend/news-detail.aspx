﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="news-detail.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.news_detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <main id="main">

  	<!-- Top Main -->
  	<section id="breadcrumbs" class="pt-3">
			<div class="container">
				<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a id="htitle" href="Index.aspx">Beranda</a></li>
					    <li class="breadcrumb-item"><a href="" id="link1" runat="server">Media &amp; Informasi</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="ttl" runat="server"></li>
					  </ol>
					</nav>
					<div class="col-lg-12 page-title">
						<h3 id="htitle" runat="server">Berita</h3>
					</div>
				</div>
					
				</div>
  	</section>


  	<!-- ======= Berita Section ======= -->
    <section class="berita-detail mb-5">
      <div class="container">
				<div class="row no-gutters">
          <div class="col-lg-9 col-12 box-left">
          	<div class="row">
        <asp:ListView ID="ListViewNews" runat="server">
            <ItemTemplate>
                <div class="col-lg-12 title">
          			<h5><strong><%# Eval("title") %></strong></h5>
          			<p><em><%# Eval("news_date") %></em> &nbsp;<span class="badge badge-danger"><%# Eval("Category_name") %></span></p>
          		</div>
                <div class="col-12 desc">
          			<img src='<%# Eval("img_file") %>' class="" alt="..." width="100%">
          			<p><strong><%# Eval("city") %></strong> <%# Eval("content") %></p>
          		</div>
            </ItemTemplate>
        </asp:ListView>
                  <div class="col-12">
                      <asp:LinkButton ID="LinkButton1" OnClick="Button1_Click" CssClass="btn btn-radius-pis bg-red pl-4 pr-4 mt-3 mb-5" runat="server"><strong>Download Versi PDF <i class="bi bi-chevron-right"></i></strong></asp:LinkButton>
                  <asp:LinkButton ID="LinkButton2" OnClick="Button1_Click" CssClass="btn btn-radius-pis bg-red pl-4 pr-4 mt-3 mb-5" runat="server"><strong>Download PDF Version <i class="bi bi-chevron-right"></i></strong></asp:LinkButton>
                  </div>
                  
          		
          	</div>
          </div>
          <div class="col-lg-3 box-right">
          	<div class="row mb-5">
          		<div class="col-lg-12 related-news">
          			<div class="col title">
          				<h5 id="ctgr" runat="server">BERITA LAINNYA</h5>
          			</div>
            <asp:ListView ID="ListViewLatestNews" runat="server">
                <ItemTemplate>
                    <div class="col desc">
          				<a href="">
          					<p><em><%# Eval("news_date") %></em> &nbsp;<span class="badge badge-danger"><%# Eval("Category_name") %></span></p>
          					<a href='<%# Eval("link") %>'><%# Eval("title") %></a>
                        
          				</a>
          			</div>
                </ItemTemplate>
            </asp:ListView>
                      
                      
		          
          			
          			
          		</div>
          	</div>
          </div>
        </div>
      </div>
        <asp:Label ID="LblkeyWord" runat="server" Text="Label"></asp:Label>
    </section><!-- Media Information Content Section -->
        <script>
            $("#ContentPlaceHolder1_LblkeyWord").hide();
            var urlName = '', title = '';
            if ($("#LbInd").attr("class") == "active") {
                urlName = 'berita-detail?title=' + $("#ContentPlaceHolder1_LblkeyWord").text();
                title = $("#htitle").text();
            }
            if ($("#LbEn").attr("class") == "active") {
                urlName = 'news-detail?title=' + $("#ContentPlaceHolder1_LblkeyWord").text();
                title = "Home";
            }
            history.pushState({}, null, urlName);
            $("#htitle").text(title);
        </script>
  </main>
</asp:Content>
