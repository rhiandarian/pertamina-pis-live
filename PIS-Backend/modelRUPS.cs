﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace PIS_Backend
{
    public class modelRUPS:ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName = "RUPS";

        string viewName = "ViewRUPS";

        public int PageSize = 8;

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        public string url;

        public int year;
        public string title;
        public string title_en;
        public string cover;

        public string Url
        {
            get { return url; }
            set { url = value; }
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string Title_En
        {
            get { return title_en; }
            set { title_en = value; }
        }
        public int  Year
        {
            get { return year; }
            set { year = value; }
        }
        public string Cover
        {
            get { return cover; }
            set { cover = value; }
        }
        public DataTable Select(string field, string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;

            string query = Db.SelectQuery(field, viewName, conditions);


            DataTable dt = Db.getData(query);

            return dt;

        }
        public string validate()
        {
            if (year == 0)
            {
                return "tahun harus di isi";
            }
            //int yearNow = Convert.ToInt32(DateTime.Now.Year.ToString());

            //if(year > yearNow)
            //{
            //    return "Year not Valid";
            //}

            if (url == null || url == "")
            {
                return "file harus di unggah";
            }

            if (title == null || title == "")
            {
                return "judul dalam bahasa Indonesia harus di isi";
            }

            if (title_en == null || title_en == "")
            {
                return "judul dalam bahasa Indonesia harus di isi";
            }

            return "valid";
        }
        public string Insert(string mainIP,string browserDetail)
        {
            string valid = validate();
            if(valid != "valid")
            {
                return valid;
            }
            try
            {
                //SqlCommand cmd = new SqlCommand("SaveRUPS", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@fileName", url);
                //cmd.Parameters.AddWithValue("@year", year);
                //cmd.Parameters.AddWithValue("@userID", user_id);

                //DataTable dt = new DataTable();
                //SqlDataAdapter da = new SqlDataAdapter(cmd);
                //da.Fill(dt);
                string field = "title, title_en, url, cover, year,";
                string value = "'" + title + "', '" + title_en + "', '" + url + "', '" + cover + "', "+year+",";
                Db.Insert(tableName, field, value, dateNow, user_id,mainIP,browserDetail);

                return "Success";
            }
            catch(Exception e)
            {
                return e.ToString();
            }
            
        }
        public string Update(int Id,string mainIP,string browserDetail,string cond = null)
        {
            string validation = validate();
            if (validation != "valid")
            {
                return validation;
            }
            string newRecord = " title = '" + title + "', ";
            newRecord += " title_en = '" + title_en + "', "; 
            newRecord += " year = '" + year + "', ";
            if (cover != "#")
            {
                newRecord += " cover = '" + cover + "', ";
            }
            if (url != "#")
            {
                newRecord += " url = '" + url + "', ";
            } 

            cond = cond == null ? "" : cond;
            Db.Update(tableName, newRecord, dateNow, user_id, cond,mainIP,browserDetail,Id);

            return "Success";
        }
        public DataTable LoadlAll(int PageIndex,int PageSizes = 0)
        {
            PageSizes = PageSizes == 0 ? PageSize : PageSizes;








            SqlCommand cmd = new SqlCommand("GetAllRUPS", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PageIndex", PageIndex);
            cmd.Parameters.AddWithValue("@PageSize", PageSizes);
            

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public void Delete(string id,string mainIP,string browserDetail,string condition = null)
        {
            string conditions = condition != null ? condition : "";

            Db.Delete(tableName, dateNow, user_id, conditions,Convert.ToInt32(id),mainIP,browserDetail);
        }
    }
}