﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace PIS_Backend
{
    public partial class LaporanTahunanUpload : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                year.Text = "0";
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                LaporanTahunan lt = new LaporanTahunan();
                lt.Year = Convert.ToInt32(year.Text);
                lt.Url = FileUpload1.FileName;

                if(!all.PdfFileType(FileUpload1.PostedFile.FileName))
                {
                    showValidationMessage("File must be Pdf");
                }
                else
                {
                    string insertStatus = lt.Insert(getip(),GetBrowserDetails());

                    if (insertStatus == "Success")
                    {
                        string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                        string SaveLocation = Server.MapPath("Laporan Tahunan") + "\\" + fn;

                        FileUpload1.PostedFile.SaveAs(SaveLocation);

                        Response.Redirect("laporan-tahunan.aspx");
                    }
                    else
                    {
                        showValidationMessage(insertStatus);
                    }

                }

                
            }
            catch(Exception ex)
            {
                LaporanTahunan lt = new LaporanTahunan();
                lt.ErrorLogHistory(getip(), GetBrowserDetails(), "Save", lt.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                showValidationMessage(ex.ToString());
            }
            
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        
    }
}