﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


namespace PIS_Backend
{
    public class Level:ModelData
    {
        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";
        public string id;
        public string levelname;
        public int pageid;
        public string ID  { get { return id; } set { id = value; } }
        public string LevelName { get { return levelname; } set { levelname = value; } }
        public int PageID { get { return pageid; } set { pageid = value; } }
        string spSaveName = "SetLevel";
        public DataTable SelectLevelPages(string field,string cond = null)
        {
            string where = cond == null ? "" : "Where " + cond;
            DataTable dt = getData("Select " + field + " from ViewLevelPages " + where);
            return dt;
        }
        public bool isValidAccessPage(string levelId,string pagesId)
        {
            string cond = "Level_Id = " + levelId + " AND Pages_Id = " + pagesId;
            DataTable dt = SelectLevelPages("top 1 *", cond);
            if(dt.Rows.Count == 0)
            {
                return false;
            }
            return true;
        }
        public string validation()
        {
            if(levelname == "")
            {
                return "Level harus diisi";
            }
            if(pageid == 0)
            {
                return "Halaman harus di pilih";
            }
            return "Success";
        }
        public DataTable SaveLevel(string mainIP, string browserDetail)
        {
            string param = "'" + levelname + "', " + pageid + ", '" + mainIP + "', '" + browserDetail + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', " + Convert.ToInt32(user_id);
            if(id != null)
            {
                param += ", " + Convert.ToInt32(id);
            }
            DataTable dt = getData("Exec " + spSaveName + " " + param);
            return dt;
        }
    }
}