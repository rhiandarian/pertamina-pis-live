﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="SaveDireksi.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.SaveDireksi" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Data Direksi</h2>
                    <ol class="breadcrumb">
                         <li class="breadcrumb-item">
                            Tentang PIS
                        </li>
                        <li class="breadcrumb-item">
                            <a>Manajemen</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Direksi</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="SaveDireksi.aspx"><strong> Direksi</strong></a>
                        </li>
                        
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Buat Baru</small></h5>
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                 <form>
                      <div class="form-group">
                        <label for="nama"><b>Nama</b></label>
                        <asp:TextBox ID="nama" cssClass="form-control" placeholder="Masukkan nama direksi" runat="server"></asp:TextBox>
                      </div>
                     <div class="form-group">
                         <div class="row">
                             <div class="col">
                                 <label for="division"><b>division</b></label>
                                 <asp:TextBox ID="division" cssClass="form-control" placeholder="Masukkan divisi direksi dalam bahasa Indonesia" runat="server"></asp:TextBox>
                             </div>
                             <div class="col">
                                 <label for="division_en"><b>division english</b></label>
                                 <asp:TextBox ID="division_en" cssClass="form-control" placeholder="Masukkan divisi direksi dalam bahasa Inggris" runat="server"></asp:TextBox>
                             </div>
                         </div>
                       </div>
                     <div class="form-group">
                        <label for="description"><b>Description</b></label>
                         <textarea ID="description" runat="server" class="summernote"  rows="10"/>
                     </div>
                     <div class="form-group">
                        <label for="description_en"><b>Description in English</b></label>
                         <textarea ID="description_en" runat="server" class="summernote"  rows="10"/>
                     </div>
                      <div class="form-group">
                        <label for="FileUpload1"><b>Image</b></label>
                        <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                     </div>
                     <div class="form-group">
                        <asp:Button ID="Save" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="BtnSave_Click"    />
                    </div>
                      
                </form>
            </div>
        </div>
    </div>
</asp:Content>
