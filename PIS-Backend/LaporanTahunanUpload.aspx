﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="LaporanTahunanUpload.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.LaporanTahunanUpload" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <script>
    function onlyNumberKey(evt) {
         
        // Only ASCII character in that range allowed
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
    }
</script>
    <div class="container-fluid" style="background-color:ghostwhite">
        <div class="row pt-5">
             <div class="col text-center">
                 <h1>Upload Laporan Tahunan</h1>
             </div>
         </div><hr />
        <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
        <div class="row pt-5 pb-5">
             <div class="col">
                 <form>
                     <div class="form-group">
                        <label for="category_name"><b>Year</b></label>
                        <asp:TextBox ID="year" cssClass="form-control" onkeypress="onlyNumberKey(event);" placeholder="Tahun " runat="server"></asp:TextBox>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                            ControlToValidate="year" runat="server"
                            ErrorMessage="Only Numbers allowed"
                            ValidationExpression="\d+">
                        </asp:RegularExpressionValidator>
                      </div>
                     <div class="form-group">
                            <label for="FileUpload1"><b>Image</b></label>
                            <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                         </div>
                     <div class="form-group">
                         <asp:Button ID="BtnAdd" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="BtnAdd_Click"  />
                      </div>
                 </form>
             </div>
        </div>
        <div class="text-right">
           <a href="laporan-tahunan.aspx" class="btn btn-info"><i class="fa fa-home"></i></a>
        </div>
     </div>
</asp:Content>
