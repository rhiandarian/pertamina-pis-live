﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace PIS_Backend
{ 
    public class Penghargaan:ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName = "Penghargaan";

        string viewName = "View_Penghargaan";

        public int PageSize = 8;

        public int pageID = 24;

        General all = new General();

        public string folder = "ImagePenghargaan";

        public string listPage = "admin_listpenghargaan.aspx";

        public string savePage = "SavePenghargaan.aspx";

        public string title; 
        public string title_en;

        public string desc;

        public string cover;

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string TitleEn
        {
            get { return title_en; }
            set { title_en = value; }
        }


        public string Desc
        {
            get { return desc; }
            set { desc = value; }
        }


        public string Cover
        {
            get { return cover; }
            set { cover = value; }
        }

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        public DataTable Select(string field, string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            string Query = Db.SelectQuery(field, viewName, conditions);

            DataTable dt = Db.getData(Query);

            return dt;
        }

        public PagesData getPage()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = " + pageID);

            PagesData p = new PagesData();
            p.Header = dt.Rows[0]["Header"].ToString();
            p.Content = dt.Rows[0]["content"].ToString();

            return p;

        }
        public DataTable LoadData(int PageIndex)
        {
            SqlCommand cmd = new SqlCommand("GetAllPenghargaan", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PageIndex", PageIndex);
            cmd.Parameters.AddWithValue("@PageSize", PageSize);


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public string validate(int covers)

        {
            if (title == "")
            {
                return "Judul dalam bahasa Indonesia harus di isi";
            }
            if (title_en == "")
            {
                return "Judul dalam bahasa Inggris harus di isi";
            }
            if (desc == "")
            {
                return "Tahun harus di isi";
            }
            if (cover != "")
            {
                if (covers == 1)
                {
                    if (!all.ImageFileType(cover))
                    {
                        return "Masukan file foto";
                    }
                }
            }
            return "Valid";
        }
        public string Insert(string mainIP,string browserDetail)
        {
            
            string validation = validate(1);
            if (validation != "Valid")
            {
                return validation;
            } 
            if (cover == "")
            {
                return "File harus di Upload";
            }
            string field = "title,title_en, description, cover, ";
            string value = "'" + title + "',  '" + title_en + "','" + desc + "', '"+folder+"/" + cover + "', ";
            Db.Insert(tableName, field, value, dateNow, user_id,mainIP,browserDetail);

            return "success";
        }
        public string Update(int Id,string mainIP, string browserDetail,string cond = null)
        {
            string validation = validate(0);
            if (validation != "Valid")
            {
                return validation;
            }
            string newRecord = " title = '" + title + "', ";
            newRecord += " title_en = '" + title_en + "', ";
            newRecord += desc != "" ? " description = '" + desc + "', " : "";
            if (cover != null)
            {
                newRecord += cover != "" ? " cover = '" + folder + "/" + cover + "', " : "";
            }
            cond = cond == null ? "" : cond;
            Db.Update(tableName, newRecord, dateNow, user_id, cond,mainIP,browserDetail,Id);

            return "success";
        }
        public void Delete(string Id,string mainIP, string browserDetail,string condition = null)
        {
            string conditions = condition != null ? condition : "";

            Db.Delete(tableName, dateNow, user_id, conditions,Convert.ToInt32(Id),mainIP,browserDetail);
        }

    }
}