﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="R-U-P-S.aspx.cs" Inherits="PIS_Backend.R_U_P_S" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx" id="li_1" runat="server">Home</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="li_2" runat="server">Informasi Saham</li>
					    <li class="breadcrumb-item active" aria-current="page" id="li_3" runat="server">RUPS</li>
					  </ol>
					</nav>
					<div class="col-12 page-title"> 
						<h3 id="header_text" runat="server">RUPS</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<%--<img src="images/Header/kapal_header_15.png" class="img-fluid" alt="...">--%>
				<img id="header" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <section id="rups">
  		<div class="container">
	  		<div class="row mb-5">
	  			<%--<div class="col-12">
	  				<h3>Laporan &amp; Ikhtisar Keuangan</h3>
	  			</div>--%>
				  <div id="content" class="col-12" runat="server">
	  				
	  			</div>
	  		</div>

	  		<div class="row mb-5 content">
				   <asp:ListView ID="ListViewLt" runat="server">
						<ItemTemplate>
	  						<div class="col-xs-12 col-sm-12 box-content">
	  							<div class="row pt-3 pb-3">
									  <div class="col-xs-12 col-sm-4 content-list">
	  						    <span><%# Eval("title") %></span>
	  					    </div>
	  					    <div class="col-xs-12 col-sm-4 content-list">
	  						    <a href="<%# Eval("url") %>"><%# Eval("cover") %></a>
	  					    </div>
	  								<%--<div class="col-xs-12 col-sm-4 content-list">
	  									<span> <a href="<%#Eval("url") %>" target="_blank"> <%#Eval("title") %> </a></span>
	  								</div>--%>
	  							</div>
	  						</div>
							</ItemTemplate>
					</asp:ListView>
	  			<%--<div class="col-xs-12 col-sm-12 box-content">
	  				<div class="row pt-3 pb-3">
	  					<div class="col-xs-12 col-sm-4 content-list">
	  						<span>RUPS TAHUNAN DAN RUPS LUAR BIASA 2019</span>
	  					</div>
	  				</div>
	  			</div>
	  			<div class="col-xs-12 col-sm-12 box-content">
	  				<div class="row pt-3 pb-3">
	  					<div class="col-xs-12 col-sm-4 content-list">
	  						<span>RUPS TAHUNAN DAN RUPS LUAR BIASA 2018</span>
	  					</div>
	  				</div>
	  			</div>
	  			<div class="col-xs-12 col-sm-12 box-content">
	  				<div class="row pt-3 pb-3">
	  					<div class="col-xs-12 col-sm-4 content-list">
	  						<span>RUPS TAHUNAN DAN RUPS LUAR BIASA 2017</span>
	  					</div>
	  				</div>
	  			</div>
	  			<div class="col-xs-12 col-sm-12 box-content">
	  				<div class="row pt-3 pb-3">
	  					<div class="col-xs-12 col-sm-4 content-list">
	  						<span>RUPS TAHUNAN DAN RUPS LUAR BIASA 2016</span>
	  					</div>
	  				</div>
	  			</div>
	  			<div class="col-xs-12 col-sm-12 box-content">
	  				<div class="row pt-3 pb-3">
	  					<div class="col-xs-12 col-sm-4 content-list">
	  						<span>RUPS TAHUNAN DAN RUPS LUAR BIASA 2015</span>
	  					</div>
	  				</div>
	  			</div>
	  			<div class="col-xs-12 col-sm-12 box-content">
	  				<div class="row pt-3 pb-3">
	  					<div class="col-xs-12 col-sm-4 content-list">
	  						<span>RUPS TAHUNAN DAN RUPS LUAR BIASA 2014</span>
	  					</div>
	  				</div>
	  			</div>--%>
	  		</div>

	  	</div>
  	</section>
    <script>
        var urlName = '';
        if ($("#LbInd").attr("class") == "active") {
            urlName = 'rups';
        }
        if($("#LbEn").attr("class") == "active")
        {
            urlName = 'agms';
        }
        history.pushState({}, null,urlName);
    </script>
</asp:Content>
