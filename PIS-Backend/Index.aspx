﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="PIS_Backend.Index" EnableEventValidation="false"%>



<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Pertamina PIS</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="images/favicon.png" rel="icon">
  <link href="images/favicon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="vendor/bootstrap-4.5.3-dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/animate/animate.css" rel="stylesheet" />
  <link href="vendor/venobox/venobox.css" rel="stylesheet">
  <link href="vendor/aos/aos.css" rel="stylesheet">
  <!--
    <link href="vendor/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
  -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="vendor/owl-carousel/dist/assets/owl.carousel.min.css">
  <link rel="stylesheet" type="text/css" href="vendor/owl-carousel/dist/assets/owl.theme.default.css">

  <!-- Template Main CSS File -->
    <link href="css/v01/style.css" rel="stylesheet" />
</head>

<body>
  <form runat="server">
  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        
          <asp:LinkButton ID="LbInd" CssClass="active" runat="server">ID</asp:LinkButton> | <asp:LinkButton ID="LbEnd" OnClick="ChangeLang" runat="server">EN</asp:LinkButton>
      </div>
      <div class="contact-info float-right">
      	<a href="contacts.aspx"><i class="bi bi-envelope"></i></a>
      	<a href="#search"><i class="bi bi-search"></i></a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
        <a href="Index.aspx"><img src="images/Logo_Pertamina_PIS.svg" alt="" class="img-fluid"></a>
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
      	<a href="" id="nav-right-option" class="nav-right-option float-right mobile-nav-toggle d-lg-none"><i class="bi bi-x"></i></a>
        <ul>
          
            <asp:ListView ID="ListViewMenu" runat="server">
                <ItemTemplate>
                    <li class='<%# Eval("classMenu") %>'>
                        <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                        <ul>
                            <asp:ListView ID="ListViewSubMenu" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                <ItemTemplate>
                                    <li class='<%# Eval("classMenu") %>'>
                                        <a href='<%# Eval("Link") %>'><%# System.Web.HttpUtility.HtmlEncode((string)Eval("Name")) %></a>
                                        <ul>
                                        <asp:ListView ID="ListViewSubMenu2" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                            </ul>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                        </ul>
                    </li>
                </ItemTemplate>
            </asp:ListView>
            <asp:ListView ID="ListViewResponsive" runat="server">
                            <ItemTemplate>
                                <li class='<%# Eval("classMenu") %> d-lg-none'>
                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                    <ul>
                                        <asp:ListView ID="ListViewSubOthers" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li class='<%# Eval("classMenu") %>'>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                    <ul>
                                                        <asp:ListView ID="ListViewSub2Othes" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                                            <ItemTemplate>
                                                                <li>
                                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </ul>
                                                    
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </ul>
                                </li>
                            </ItemTemplate>
                        </asp:ListView>
		      

          <li class="nav-right-option d-none d-lg-block" id="othermenu" runat="server"></li>
        </ul>
        <div id="langMobile" runat="server" class="text-center lang-mobile d-lg-none">
		      
           
		    </div>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- Nav Overlay -->
  <div id="navOverlay" class="nav-overlay d-none d-lg-block">
  	<div class="bg-black-transparent">
	  	<div class="container">
		    <div class="nav-overlay-content">
		    	
                <ul>
                        <asp:ListView ID="ListViewOthers" runat="server">
                            <ItemTemplate>
                                <li class='<%# Eval("classMenu") %>'>
                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                    <ul>
                                        <asp:ListView ID="ListViewSubOthers" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li class='<%# Eval("classMenu") %>'>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                    <ul>
                                                        <asp:ListView ID="ListViewSub2Othes" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                                            <ItemTemplate>
                                                                <li>
                                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </ul>
                                                    
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </ul>
                                </li>
                            </ItemTemplate>
                        </asp:ListView>
                        
		    		
		    	</ul>
		    </div>
	  	</div>
  	</div>
  </div><!-- End Nav Overlay -->

  <!-- Search -->
  <div id="search">
  	<div class="bg-overlay">
	    <button type="button" class="close"><i class="bi bi-x close"></i></button>
	    <form action="pencarian.html">
	    <input type="search" id="txtSearch" runat="server" value="" autocomplete="off" placeholder="Ketik Kata Kunci disini" />
            <asp:Button ID="BtnSearch" runat="server" OnClick="BtnSearch_Click" CssClass="btn bg-red" Text="Cari" />
	    </form>
  	</div>
	</div><!-- End Search -->



  <main id="main">
      
	  <!-- ======= Hero Section ======= -->
	  <section id="hero">
	    <div class="hero-container">
	      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

	        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

	        <div id="banner" class="carousel-inner" role="listbox">
               
	          

	          

	        </div>

	        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
	          <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"><i class="bi bi-chevron-left"></i></span>
	          <span class="sr-only">Previous</span>
	        </a>
	        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
	          <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"><i class="bi bi-chevron-right"></i></span>
	          <span class="sr-only">Next</span>
	        </a>

	      </div>
	    </div>
	  </section><!-- End Hero -->

    <!-- ======= Service Section ======= -->
    <section id="service" class="service">
      <div class="container">

        <div id="lyn" class="row">
          <div class="col-lg-12 text-center mb-5">
          	<h2>Layanan Kami</h2>
          </div>
            <%--<asp:ListView ID="ListViewLayanan" runat="server">
                <ItemTemplate>
                    
                </ItemTemplate>
            </asp:ListView>--%>
        </div>

      </div>
    </section>
      <!-- End Service Section -->

    <!-- ======= Media Information Section ======= -->
    <section class="media-information section-bg">
      <div class="container">
				<div class="row no-gutters">
          <div class="col-lg-12 mb-3 media-information-top">
          	<div class="row">
          		<div class="col-lg-7 col-12">
          			<h2>Media & Informasi</h2>
          		</div>
          		<div class="col-lg-5 col-12 text-right media-information-source">
          			<div class="row">
          				<!--
          				<div class="col-lg-4 col-4 d-none d-lg-block"></div>
          				<div class="col-lg-4 col-6 media-information-option-left">
          					<a href="" class="active">Berita</a>
          				</div>
          				<div class="col-lg-4 col-6 media-information-option-right">
          					<a href="">Galeri</a>
          				</div>
          				-->
          				<div class="col-lg-6 col-4 d-none d-lg-block"></div>
          				<div class="col-lg-3 col-6 media-information-option-center">
          					<a href="" class="active" id="news">Berita</a>
          				</div>
          				<div class="col-lg-3 col-6 media-information-option-center">
          					<a href="" id="gallery">Galeri</a>
          				</div>
          			</div>
          		</div>
          	</div>
          </div>
          <div class="col-lg-12 col-12" id="homenews">
          	
          </div>
          <div class="col-lg-12 col-12 d-none" id="homegallery">
          	<div class="row mt-1">
					    <div id="glr" runat="server" class="col-12 mt-3 gallery-carousel owl-carousel owl-theme">
                           
					    </div>
					  </div>
          </div>

        </div>
      </div>
    </section>
      <!-- Media Information Section -->

    <!-- ======= Video PIS Section ======= -->
    <section class="video-profile">
      <div class="container">

        <div id="ytb" class="row">
	    	 
        </div>

      </div>
    </section>
      <!-- End Video PIS Section -->

    <!-- ======= Armada Section ======= -->
    <section class="armada section-bg">
      <div class="container">
				<div class="row no-gutters">
					<div class="col-12 mb-3">
						<div class="row">
							<div class="armada-top-box col-12 bg-blue p-4">
								<div class="row">
									<div class="col-xs-3 col-sm-6 text-center">
										<h2 id="totalKapal" runat="server" class="mb-0"></h2>
										<p class="mb-0">Total Kapal</p>
									</div>
									<div class="col-xs-3 col-sm-6 text-center">
										<h2 id="kapalmilik" runat="server" class="mb-0"></h2>
										<p class="mb-0">Kapal Milik</p>
									</div>
								</div>
							</div>
							<div class="armada-bottom-box col-12 p-4 bg-white">
								<p class="text-center" id="kapaldesc"></p>
								<h5 class="text-center mt-4">Armada Kami</h5>

								<ul class="nav nav-tabs row-eq-height mt-5 d-none" id="myTab" role="tablist">
									<li class="nav-item col-xs-5 col-sm-6 col-md-3 pl-0 pr-0">
								    <a class="nav-link active btn btn-radius-pis m-0" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
									    Kapal Milik	
									  </a>
								  </li>
								  <li class="nav-item col-xs-5 col-sm-6 col-md-3 pl-0 pr-0">
								    <a class="nav-link btn btn-radius-pis m-0" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
								    	<center><div class="rounded-circle armada-circle"></div></center>
							    		Profil Armada
								    </a>
								  </li>
								</ul>
								<div class="tab-content bg-white pt-0 pb-5 pl-4 pr-4" id="myTabContent">
								  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
								  	<div class="row">
								  		<div id="kpl" class="col-12 mt-3 armada-carousel owl-carousel owl-theme">
									  		
									  		

									  		

							  			</div>

							  			<div class="col-12 all-armada text-right">
												<a href="Armada-Kami.aspx" class="link_pis">Lihat Semua</a>
											</div>

								  	</div>
								  </div>
								  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
								  	<div class="row">
								  		<div class="col-12 mt-3 profil-carousel owl-carousel owl-theme">

									  		<div class="box-content">
								  				<div class="col box-pis-sm pt-3">
								  					<a class="image-popup-vertical-fit" href="images/default-photo.png">
								  						<img src="images/default-photo.png" class="img-fluid mt-2 mb-2" alt="...">
								  					</a>
								  					<p class="text-center">
								  						<strong>Armada</strong>
								  					</p>
								  					<p class="text-center">
								  						<a href="" class="red_pis">Download</a>
								  					</p>
								  				</div>
								  			</div>
								  			<div class="box-content">
								  				<div class="col box-pis-sm pt-3">
								  					<a class="image-popup-vertical-fit" href="images/default-photo.png">
								  						<img src="images/default-photo.png" class="img-fluid mt-2 mb-2" alt="...">
								  					</a>
								  					<p class="text-center">
								  						<strong>Armada</strong>
								  					</p>
								  					<p class="text-center">
								  						<a href="" class="red_pis">Download</a>
								  					</p>
								  				</div>
								  			</div>
								  			<div class="box-content">
								  				<div class="col box-pis-sm pt-3">
								  					<a class="image-popup-vertical-fit" href="images/default-photo.png">
								  						<img src="images/default-photo.png" class="img-fluid mt-2 mb-2" alt="...">
								  					</a>
								  					<p class="text-center">
								  						<strong>Armada</strong>
								  					</p>
								  					<p class="text-center">
								  						<a href="" class="red_pis">Download</a>
								  					</p>
								  				</div>
								  			</div>
								  			<div class="box-content">
								  				<div class="col box-pis-sm pt-3">
								  					<a class="image-popup-vertical-fit" href="images/default-photo.png">
								  						<img src="images/default-photo.png" class="img-fluid mt-2 mb-2" alt="...">
								  					</a>
								  					<p class="text-center">
								  						<strong>Armada</strong>
								  					</p>
								  					<p class="text-center">
								  						<a href="" class="red_pis">Download</a>
								  					</p>
								  				</div>
								  			</div>

								  		</div>

								  		<div class="col-12 all-armada text-right">
												<a href="Armada-Kami.aspx" class="link_pis">Lihat Semua</a>
											</div>

								  	</div>
								  </div>
								</div>
							</div>
						</div>

						
					</div>
        </div>
      </div>
    </section>
    <!-- Armada Section -->

    <!-- ======= Informasi Pengadaan Section ======= -->
    <section class="informasi-pengadaan">
      <div class="container">
				<div class="row no-gutters">
					<div class="col-12">
						<h3 class="text-center">Informasi Pengadaan</h3>
						<p class="text-pengadaan" id="pengadaandesc" ></p>
					</div>
					<div class="col-12 pl-2 pr-2">
						<div class="row">
							<div id="ip" class="col-12 mt-3 pengadaan-carousel owl-carousel owl-theme">
                                
							    
				  			
							</div>

							<div class="col-12 all-armada text-right">
								<a href="informasi-pengadaan.aspx" class="link_pis">Lihat Semua</a>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- ======= Career Section ======= -->
    <section class="internship career-section pb-5 pt-5"  id="magang_section" runat="server">
		<div class="bg-overlay">
      <div class="container">
				<div class="row no-gutters">
					<div class="col-12 pb-5 pt-5">
						<div class="row row-eq-height">
							<div class="col-sm-6">
								<h5>Bekerja di PT Pertamina International Shipping (PIS)</h5>
								<h5>Shipping the Energy Worldwide, <br>Energizing the Nation with Pride.</h5>

								<p class="mt-3">Jadilah bagian dari kebanggaan Indonesia bersama PIS!</p>
							</div>
							<div class="col-sm-6 align-self-center">
								<a href="career.aspx" class="btn btn-radius-pis bg-red pl-4 pr-4 mt-3">
		              <strong>Lihat Lowongan <i class="bi bi-chevron-right"></i></strong>
		            </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</section>

    
     
  </main>

  <!-- ======= Footer ======= -->
	<div id="footer-top-border">
		<div class="col-lg-12 col-12" style="top:0">
			<div class="row">
				<div class="col-lg-6 col-6 footer-border-1" style="top:0"></div>
				<div class="col-lg-3 col-3 footer-border-2" style="top:0"></div>
				<div class="col-lg-3 col-3 footer-border-3" style="top:0"></div>
			</div>
		</div>
	</div>
	<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-4 col-12 footer-info">
            <h6><strong>KANTOR PUSAT</strong></h6>
            <p id="address" runat="server" class="mt-3">
              Patra Jasa Office Tower Lantai 3 & 14 
              <br>JJl. Jend. Gatot Subroto Kav 32–34
              <br>Setiabudi, Jakarta 12950 
              <br>Indonesia 
            </p>
            <div class="d-flex mt-2">
              <div class="align-self-center rounded-icon">
	  					  <span class="bi bi-telephone-fill phone-icon"></span>
              </div>
              <div class="align-self-center">
               &nbsp; <span id="phone" runat="server"></span>
              </div>
	  				</div>
	  				<div class="d-flex mt-2">
              <div class="align-self-center rounded-icon">
	  					  <span class="bi bi-envelope-fill phone-icon"></span>
              </div>
              <div class="align-self-center">
                &nbsp; <span id="email" runat="server"></span>
              </div>
	  				</div>
	  				<div class="d-flex mt-2">
              <div class="align-self-center">
	  					  <img src="images/logo-135.jpg" width="25px" alt="" class="img-fluid">
              </div>
              <div class="align-self-center">
                &nbsp; Call Center <strong>135</strong>
              </div>
	  				</div>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>


          <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong id="perusahaan" runat="server">TAUTAN </strong></h6>
            <ul class="mt-3">
              <li><i class="bx bx-chevron-right"></i> <a href="https://pertamina.com" id="pt1" runat="server">PT Pertamina (Persero)</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://bumn.go.id" id="pt2" runat="server">Kementerian BUMN</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://www.esdm.go.id" id="pt3" runat="server">Kementerian ESDM RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="http://www.dephub.go.id" id="pt4" runat="server">Kementerian Perhubungan RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://www.bkpm.go.id" id="pt5" runat="server">BKPM</a></li>
            </ul>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>

          <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong>TEMUKAN KAMI</strong></h6>
            <ul class="mt-3 social-links">
              <li >
              	<a id="link_fb" runat="server" class="d-flex">
	              	<div class="align-self-center">
	              		<span class="bi bi-facebook facebook"></span>
	              	</div>
	              	<div class="align-self-center">Facebook<br><em>Pertamina International Shipping</em></div>
	              </a>
	            </li> 
              <li>
              	<a id="link_ig" runat="server" class="d-flex">
	              	<div class="align-self-center">
	              		<span class="bi bi-instagram instagram"></span>
	              		</div>
	              	<div class="align-self-center">Instagram<br><em>@pertamina_pis</em></div> 
	              </a>
	            </li>
              <li>
                <a id="link_tw" runat="server" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-twitter twitter"></span>
                    </div>
                  <div class="align-self-center">Twitter<br><em>@Pertamina_PIS</em></div> 
                </a>
              </li>
              <li>
                <a id="link_yt" runat="server" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-youtube twitter"></span>
                    </div>
                  <div class="align-self-center">Youtube<br><em>PT Pertamina International Shipping</em></div> 
                </a>
              </li>
            </ul>
          </div>
          <div class="col-12 d-lg-none separate"><hr></div>

        </div>
      </div>
    </div>

    <div class="container">
    	<div class="row d-flex">
	      <div class="col-lg-8 col-12 align-self-center part-of">
	        <span>PART OF</span>
	        <img src="images/logo-bumn.svg" height="30px" class="ml-3" alt="...">
	        <img src="images/logo-pertamina.svg" height="30px" class="ml-3" alt="...">
	      </div>
    		<div class="col-lg-4 col-12">
    			<a href="" class="btn btn-whistle">
            	<div class="row">
            		<div class="col pr-0 d-flex mr-2">
          				<img src="images/icons/wbs.svg">
            		</div>
            		<div class="col text-left pl-0">
		          		Whistle Blowing System
		          		<br><em>https://pertaminaclean.tipoffs.info/</em>
            		</div>
            	</div>
          	</a>
    		</div>
    		<div class="col-12 mt-3">
    			&copy; Copyright 2021 <strong><span>Pertamina International Shipping</span></strong>. All Rights Reserved
    		</div>
    	</div>
    </div>
  </footer><!-- End Footer -->

  <!-- Vendor JS Files -->
  <script src="vendor/jquery.min.js"></script>
  <script src="vendor/bootstrap-4.5.3-dist/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="vendor/venobox/venobox.min.js"></script>
  <script src="vendor/waypoints/lib/jquery.waypoints.min.js"></script>
  <script src="vendor/counterup/counterup.min.js"></script>
  <script src="vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="vendor/aos/aos.js"></script>
  <script src="vendor/fontawesome/js/all.min.js"></script>
  <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
  <script src="vendor/owl-carousel/dist/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="js/main.js?v=0.1"></script>

  <script type="text/javascript">
      history.pushState({}, null, "/");
      function changeLang()
      {
           var button = document.getElementById("<%= LbEnd.ClientID %>");
           button.click();
      }
 	$(document).ready(function(){
 	    $.ajax({
 	        type: "GET",
 	        url: "BannerJson",
 	        dataType: "json",
 	        success: function (res) {
 	            var content = "";
 	            var herocarousel = '';
 	            $.each(res, function (i, val) {
 	                herocarousel += '<li data-target="#heroCarousel" data-slide-to="'+i+'" class="'+val.active+'"></li>';
 	                content += '<div class="carousel-item ' + val.active + '" >';
 	                content += '<img src="' + val.img_file + '" alt="" width="100%">';
 	                content += '<div class="carousel-container">';
 	                content += '<div class="carousel-content container">';
 	                content += '<h2 class="animated fadeInDown">' + val.title + '</h2>';
 	                content += '<p class="animated fadeInUp">' + val.description + '</p>';
 	                content += '<a href=' + val.url + ' class="btn-get-started animated fadeInUp scrollto">' + val.buttonText + ' <i class="bi bi-chevron-right"></i></a></div></div></div>';
 	            });
 	            $("#hero-carousel-indicators").html(herocarousel);
 	            $("#banner").html(content);
 	            
 	        }
 	    });
 	    $.ajax({
 	        type: "GET",
 	        url: "LayananJson",
 	        dataType: "json",
 	        success: function (res) {
 	            var content = '';
 	            $.each(res, function (i, val) {
 	                content += '<div class="col-lg-4 col-12 mb-4">';
 	                content += '<div class="col pl-0 pr-0">';
 	                content += '<div class="card bg-dark text-white overflow-hidden service-box card-block">';
 	                content += '<a href="layanan-kami.aspx">';
 	                content += '<img src="' + val.img_file + '" class="card-img" alt="...">';
 	                content += '<div class="card-img-overlay align-self-center text-center bg-gradient-' + (i + 1) + '">';
 	                content += '<p class="card-title my-auto">' + val.title + '</p>';
 	                content += '</div></a></div></div></div>';
 	            });
 	            $("#lyn").append(content);
 	        }
 	    });
 	    $.ajax({
 	        type: "GET",
 	        url: "NewsJson",
 	        dataType: "json",
 	        success: function (res) {

 	            var content = '<div class="row"><div class="col-lg-7 col-12 mb-4 media-information-box-left"><div class="col pl-0 pr-0"><div class="card text-white overflow-hidden media-information-box card-block"><a href="news-detail.aspx?title=' + res[0]["key"] + '"><div class="box-image"><img src="' + res[0]["img_file"] + '" class="card-img" alt="" height="100%" width="100%"></div><div class="card-img-overlay bg-black-transparent"><p class="card-content p-4 "><em>' + res[0]["news_date"] + '</em> &nbsp;<span class="badge badge-danger">' + res[0]["Category_name"] + '</span><br>' + res[0]["title"] + '</p></div></a></div></div></div><div class="col-lg-5 col-12 mb-4"><div class="row"><div class="col-12 pl-0 pr-0 mb-3 media-information-box-right"><div class="card text-white overflow-hidden media-information-box card-block"><a href="news-detail.aspx?title=' + res[1]["key"] + '"><img src="' + res[1]["img_file"] + '" class="card-img" alt="..." height="100%" width="100%"><div class="card-img-overlay bg-black-transparent"><p class="card-content pl-4 pb-1"><em>' + res[1]["news_date"] + '</em> &nbsp;<span class="badge badge-danger">' + res[1]["Category_name"] + '</span><br>' + res[1]["title"] + '</p></div></a></div></div><div class="col-12 pl-0 pr-0 mb-4 media-information-box-right"><div class="card text-white overflow-hidden media-information-box card-block"><a href="news-detail.aspx?title=' + res[2]["key"] + '"><img src="' + res[2]["img_file"] + '" class="card-img" alt="..." height="100%" width="100%"><div class="card-img-overlay bg-black-transparent"><p class="card-content pl-4 pb-1"><em>' + res[2]["news_date"] + '</em> &nbsp;<span class="badge badge-danger">' + res[2]["Category_name"] + '</span><br>' + res[2]["title"] + '</p></div></a></div></div></div></div>';
 	            $("#homenews").append(content);

 	        }
 	    });
 	   $.ajax({
 	        type: "GET",
 	        url: "GalleryJson",
 	        dataType: "json",
 	        success: function (res) {
 	            
 	            var content = ""; var x = 0;
 	            $.each(res, function (i, val) {
 	                var id = val.ID;
 	                var Category_name = val.Category_name;
 	                var card = "card", aclass = "", title = "", albums = "";
 	                if(Category_name == "Foto")
 	                {
 	                    x++;
 	                    card += " popup-gallery gallery-" + x;
 	                    aclass = "image-popup-vertical-fit";
 	                    title = 'title="' + val.title + '"';
 	                    
 	                    var album = val.Album;
 	                    if(album.length > 0)
 	                    {
 	                        $.each(album, function (ix, vl) {
 	                            var titles = '"' + vl.title + '"';
 	                            albums += '<a class="image-popup-vertical-fit"  href="'+vl.img_file+'" title="'+titles+'"></a>';
 	                        });
 	                    }
 	                }
 	                if(Category_name == "Video")
 	                {
 	                    aclass = "popup-youtube";
 	                }
 	                if(Category_name == "Infografis")
 	                {
 	                    aclass = "image-popup-vertical-fits";
 	                    title = 'title="' + val.title+'"';
 	                }
 	                content += '<div class="' + card+ '">';
 	                content += '<a  href="' + val.Url + '" class="'+aclass+'" '+title+' >';
 	                content += '<img class="card-img-top" src="' + val.Thumbnail + '" alt="Card image cap">';
 	                content += '<div class="card-body">';
 	                content += '<p class="mb-0"><em>' + val.Gallery_Date + '</em> &nbsp;<span class="badge badge-danger">' + val.Category_name + '</span></p>';
 	                content += "<p><strong>" + val.title + "</strong></p>";
 	                content += "</div></a>";
 	                content += albums != "" ? albums : "";
 	                content += "</div>";
 	            });
 	            $("#glr").html(content);
 	            let owl = $('.gallery-carousel');
 	            owl.owlCarousel({
 	                margin: 15,
 	                nav: true,
 	                navText: ["<div class='nav-btn prev-slide justify-content-center'><i class='bi bi-chevron-left'></i></div>", "<div class='nav-btn next-slide'><i class='bi bi-chevron-right'></div>"],
 	                loop: false,
 	                autoplayTimeout: 3000,
 	                autoplayHoverPause: false,
 	                responsive: {
 	                    0: {
 	                        items: carouselItem
 	                    }
 	                },
 	                onDragged: carouselCallback,
 	            });
 	            $(".popup-gallery").magnificPopup({
 	                delegate: 'a',
 	                type: 'image',
 	                tLoading: 'Loading image #%curr%...',
 	                mainClass: 'mfp-img-mobile',
 	                gallery: {
 	                    enabled: true,
 	                    navigateByImgClick: true,
 	                    preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
 	                },
 	                image: {
 	                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
 	                    titleSrc: function (item) {
 	                        return item.el.attr('title') + '';
 	                    }
 	                }
 	            });

 	            $('.image-popup-vertical-fits').magnificPopup({
 	                type: 'image',
 	                closeOnContentClick: true,
 	                mainClass: 'mfp-img-mobile',
 	                image: {
 	                    verticalFit: true
 	                },
 	                zoom: {
 	                    enabled: true,
 	                    duration: 300
 	                }
 	            });

 	            $('.popup-youtube').magnificPopup({
 	                disableOn: 700,
 	                type: 'iframe',
 	                mainClass: 'mfp-fade',
 	                removalDelay: 160,
 	                preloader: false,
 	                fixedContentPos: false,
 	            });
 	           

 	        }
 	   });
 	   $.ajax({
 	       type: "GET",
 	       url: "VideoJson",
 	       dataType: "json",
 	       success: function (res) {
 	           var content = '';
 	           $.each(res, function (i, val) {
 	               content += '<div class="col-lg-6 col-12 mb-4 d-flex">';
 	               content += '<div class="col pl-0 pr-0 video-profile-info">';
 	               content += '<h3 class="mb-4">' + val.title + '</h3>';
 	               content += '<p>' + val.content + '</p></div></div>';
 	               content += '<div class="col-lg-6 col-12 mb-4 image-video align-self-center">';
 	               content += '<a href=' + val.video_url + ' class="popup-youtube">';
 	               content += '<img src=' + val.img_url + ' class="rounded" alt="Responsive image">';
 	               content += '<center><img src="images/youtube-play-button.png" class="playButton" alt="Responsive image"></center>';
 	               content += '</a></div>';

 	           });
 	           $("#ytb").html(content);
 	       }
 	   });
 	    $.ajax({
 	        type: "GET",
 	        url: "KapalPageDesc",
 	        success: function (res) {
 	            $("#kapaldesc").html(res);
 	        }
 	    });
       
 	    $.ajax({
 	        type: "GET",
 	        url: "KapalJson",
 	        dataType: "json",
 	        success: function (res) {
 	            var content = '';
 	            $.each(res, function (i, val) {
 	                content +=  '<div class="box-content"><div class=""><a href="detail-armada.aspx?search='+val.Search+'">';
 	                content +=	'<div class="box-image"><img src="'+val.img_file+'" class="" alt="..."></div>';
					content +=  '<p class="text-left"><strong>'+val.name+'</strong></p>';
					content += 	'<table class="table table-borderless pl-0">';
					content += 	'<tr><td>TYPE</td><td><strong>'+val.type+'</strong></td></tr>';
					content += 	'<tr><td>BUILT</td><td><strong>'+val.built+'</strong></td></tr>';
					content += 	'<tr><td>SIZE (DWT)</td><td><strong>'+val.size+'</strong></td></tr>';
					content += 	'<tr><td>CAPACITY (BBLS)</td><td><strong>'+val.capacity+'</strong></td></tr>';
					content += '</table></a></div></div>';
								              
 	            });
 	            $("#kpl").html(content);
 	            let armada = $('.armada-carousel');
 	            armada.owlCarousel({
 	                margin: 15,
 	                nav: true,
 	                navText: ["<div class='nav-btn prev-slide justify-content-center bg-green'><i class='bi bi-chevron-left'></i></div>", "<div class='nav-btn next-slide bg-green'><i class='bi bi-chevron-right'></div>"],
 	                loop: false,
 	                autoplayTimeout: 3000,
 	                autoplayHoverPause: false,
 	                responsive: {
 	                    0: {
 	                        items: carouselItem
 	                    }
 	                },
 	                onDragged: carouselCallback,
 	            });
 	        }
 	    });
 	    
 	    $.ajax({
 	        type: "GET",
 	        url: "PengadaanDesc",
 	        success: function (res) {
 	            $("#pengadaandesc").html(res);
 	        }
 	    });
 	    
 	    $.ajax({
 	        type: "GET",
 	        url: "PengadaanJson",
 	        dataType: "json",
 	        success: function (res) {
 	            
 	            var content = "";
 	            $.each(res,function(i,val){
 	                content += '<div class="box-content">';
 	                content += '<div class="col box-pis-sm" style="top:0;left:0">';
 	                content += '<img src="'+val.cover+'" class="img-fluid mt-3 mb-2" alt="...">';
 	                content += '<p class="text-center"><em>' + val.date+'</em><br><strong>'+val.title+'</strong></p>';
 	                content += '<p class="text-center"><a href="/I_P/'+val.url+'" class="red_pis" >Download </a></p>';
 	                content += "</div></div>";
 	            }); 
 	            $("#ip").html(content);
 	            let pengadaan = $('.pengadaan-carousel');
 	            pengadaan.owlCarousel({
 	                margin: 15,
 	                nav: true,
 	                navText: ["<div class='nav-btn prev-slide justify-content-center bg-green'><i class='bi bi-chevron-left'></i></div>", "<div class='nav-btn next-slide bg-green'><i class='bi bi-chevron-right'></div>"],
 	                loop: false,
 	                autoplayTimeout: 3000,
 	                autoplayHoverPause: false,
 	                responsive: {
 	                    0: {
 	                        items: carouselItem4
 	                    }
 	                },
 	                onDragged: carouselCallback,
 	            });
 	        }
 	    });
 	    
 	    
				  				            
				  					           
        
 		$("#news, #gallery").bind('click', function(e){
 			e.preventDefault();
 			$(this).addClass("active");
 			let id = $(this).attr("id");
 			$("#home"+id).removeClass("d-none");
 			if(id==="news"){
 				$("#homegallery").addClass("d-none");
 				$("#gallery").removeClass("active");
 			} else {
 				$("#homenews").addClass("d-none");
 				$("#news").removeClass("active");
 			}
 		});

 		

		//$('.popup-modal').magnificPopup({
		//	type: 'inline',
		//	preloader: false,
		//	focus: '#username',
		//	modal: true,
		//	mainClass: 'my-mfp-zoom-in',
		//}).magnificPopup('open');

		$(".btn-close-modal").bind('click', function(e){
			e.preventDefault();
			$.magnificPopup.close();
		});

		

		

    

    let profil = $('.profil-carousel');
    profil.owlCarousel({
      margin: 15,
      nav: true,
      navText:["<div class='nav-btn prev-slide justify-content-center bg-green'><i class='bi bi-chevron-left'></i></div>","<div class='nav-btn next-slide bg-green'><i class='bi bi-chevron-right'></div>"],
      loop: false,
      autoplayTimeout:3000,
      autoplayHoverPause:false,
      responsive: {
        0: {
          items: carouselItem
        }
      },
      onDragged: carouselCallback,
    });

    

 	});

 	function carouselCallback(){
	  let val = $(".gallery-carousel").find(".active").find(".discount").val();
	  $(".gallery-carousel").html(val);
	}

	$(document).click(function(event) {
    var className = $(event.target).prop('class');
    if(className=='mfp-content'){
      $.magnificPopup.close();
    }
  });

  //let imgHighlighLeft = $(".media-information-box-left").find('img');
  //$.each(imgHighlighLeft, function(key, value){
  //  console.log(value);
  //  let widthImage = value.width;
  //  let heightImage = value.height;
  //  if(heightImage<425){
  //    $(this).css({'height': '100%', 'width': 'auto'});
  //  } else {
  //    $(this).css({'width': '100%', 'height': 'auto'});
  //  }
  //});

  //let imgHighlighRight = $(".media-information-box-right").find('img');
  //$.each(imgHighlighRight, function(key, value){
  //  let widthImage = value.width;
  //  let heightImage = value.height;
  //  console.log(heightImage);
  //  if(heightImage<201){
  //    $(this).css({'height': '100%', 'width': 'auto'});
  //  } else {
  //    $(this).css({'width': '100%', 'height': 'auto'});
  //  }
  //});
  let shipImg = $(".armada-carousel").find('img');
  $.each(shipImg, function (key, value) {
      let widthImage = value.width;
      let heightImage = value.height;
      if (heightImage < 201) {
          $(this).css({ 'width': '100%', 'height': 'auto' });
      } else {
          $(this).css({ 'height': '100%', 'width': 'auto' });
      }
  });
  </script>
</form>
</body>

</html>

