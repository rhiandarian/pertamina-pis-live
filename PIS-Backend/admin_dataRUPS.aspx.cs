﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace PIS_Backend
{
    public partial class admin_dataRUPS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                BindLaporan();
            }
        }
        public void BindLaporan()
        {
            RUPSModel lt = new RUPSModel();
            DataTable dt = lt.Select("*");
            ListViewLaporanTahunan.DataSource = dt;
            ListViewLaporanTahunan.DataBind();
        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            LaporanTahunan lt = new LaporanTahunan();
            string cond = " ID = " + id;
            lt.Delete(cond);
            BindLaporan();
        }

        protected void ListViewLaporanTahunan_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (ListViewLaporanTahunan.FindControl("DataPager1") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindLaporan();
        }
    }
}