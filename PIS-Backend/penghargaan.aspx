﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="penghargaan.aspx.cs" Inherits="PIS_Backend.penghargaan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx" id="li_1" runat="server">Home</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="li_2" runat="server">Komitmen &amp; Keberlanjutan</li>
					    <li class="breadcrumb-item active" aria-current="page" id="li_3" runat="server">Penghargaan</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="header_text" runat="server">Penghargaan</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  				<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <section id="penghargaan">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div class="col-12">
	  				<h3 id="content_text" runat="server">Penghargaan</h3>
	  			</div>
	  		</div>

	  		<div class="row mb-5 content">
	  			<%--<div id="content" runat="server" class="col-md-12">
                
              </div> --%>
                  <div class="col-12">
                    <div class="row">
                         <asp:ListView ID="ListViewLt" runat="server">
						    <ItemTemplate>
                              <div class="col-xs-12 col-sm-4 box-content">
                                <div class="col pt-3 pl-0 pr-0">
                                  
                                    <img src="<%#Eval("Cover") %>" class="img-fluid mt-2 mb-2" alt="...">
                                   
                                  <p class="text-center"><strong><%#Eval("Desc") %></strong><br> <%#Eval("Title") %></p>
                                </div>
                              </div> 
                          </ItemTemplate>
					    </asp:ListView>
                    </div>
                  </div>
	  		</div>

	  	</div>
  	</section>
	<script type="text/javascript">
        
        var urlName = '';
        if ($("#LbInd").attr("class") == "active") {
            urlName = 'penghargaan';
        }
        if($("#LbEn").attr("class") == "active")
        {
            urlName = 'awards';
        }
        history.pushState({}, null,urlName);
  
    $(document).ready(function(){
      $('.image-popup-vertical-fit').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-img-mobile',
        image: {
          verticalFit: true
        },
        zoom: {
          enabled: true,
          duration: 300 
        }
      });
    });
    </script>
</asp:Content>
