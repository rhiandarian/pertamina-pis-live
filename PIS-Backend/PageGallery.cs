﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIS_Backend
{
    public class PageGallery
    {
       
        public int ID
        {
            get;
            set;
        }
        public string ListPage
        {
            get;
            set;
        }
        public string AddPage
        {
            get;
            set;
        }
        public string EditPage
        {
            get;
            set;
        }
        public string PageAdminID
        {
            get;
            set;
        }
    }
}