﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PIS_Backend
{
    public partial class contacts : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindMenu();
                loadcontact(); 
                editClassLang();
                home.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Home" : home.InnerText;
                kontak.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Contact us" : kontak.InnerText;
                title_kontak.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "CONTACT US" : title_kontak.InnerText;
                kantor.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Head Office" : kantor.InnerText;
                temukan.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Find Us" : temukan.InnerText;
                title_pesan.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Send us your message" : title_pesan.InnerText;
                desc_pesan.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Share your details and the message you want to inform us." : desc_pesan.InnerText;
                name_pesan.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Name" : name_pesan.InnerText;
                email_pesan.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "E-mail" : email_pesan.InnerText;
                telp_pesan.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Phone number" : telp_pesan.InnerText;
                institusi_pesan.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Institution" : institusi_pesan.InnerText;
                judul_pesan.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Title" : judul_pesan.InnerText;
                isi_pesan.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Message" : isi_pesan.InnerText;
                BtnMessage.Text = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Send Message" : BtnMessage.Text;

                kantor_footer.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "HEAD OFFICE" : kantor_footer.InnerHtml; 
                findus.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "FIND US" : findus.InnerHtml;

                perusahaan.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "LINKS" : perusahaan.InnerHtml;
                pt2.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Ministry of State Owned Enterprises" : pt2.InnerHtml;
                pt3.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Ministry of Energy and Mineral Resources" : pt3.InnerHtml;
                pt4.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Ministry of Transportation " : pt4.InnerHtml;
                pt5.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "BKPM" : pt5.InnerHtml;

                Institusi_form.Items.Clear();
                if (Session["Lang"] != null && Session["Lang"].ToString() == "En")
                {
                    string[] arr = { "Media", "Investor", "General" };
                    var listItems = arr.Select((r, Index) => new ListItem { Text = r, Value = r });
                    Institusi_form.Items.AddRange(listItems.ToArray());
                }
                else
                {
                    string[] arr = { "Media", "Investor", "Umum" };
                    var listItems = arr.Select((r, Index) => new ListItem { Text = r, Value = r });
                    Institusi_form.Items.AddRange(listItems.ToArray());
                }
              
            }
        }

        public void loadcontact()
        {
            ModelData Db = new ModelData();
            string query = "Select * from  Contact  where ID = 1";
            DataTable dt = Db.getData(query); 
            address.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["address_en"].ToString() : dt.Rows[0]["address"].ToString();
            phone.InnerHtml = dt.Rows[0]["phone"].ToString();
            link_fb.HRef = dt.Rows[0]["link_fb"].ToString();
            link_tw.HRef = dt.Rows[0]["link_tw"].ToString();
            link_ig.HRef = dt.Rows[0]["link_ig"].ToString();
            link_yt.HRef = dt.Rows[0]["link_yt"].ToString();
            email.InnerText= dt.Rows[0]["email"].ToString();

            address_footer.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["address_en"].ToString() : dt.Rows[0]["address"].ToString(); 
            phone_footer.InnerHtml = dt.Rows[0]["phone"].ToString();
            link_fb_footer.HRef = dt.Rows[0]["link_fb"].ToString();
            link_tw_footer.HRef = dt.Rows[0]["link_tw"].ToString();
            link_ig_footer.HRef = dt.Rows[0]["link_ig"].ToString();
            link_yt_footer.HRef = dt.Rows[0]["link_yt"].ToString();
            email.InnerText = dt.Rows[0]["email"].ToString();
            email_footer.InnerText = dt.Rows[0]["email"].ToString();
        }
        public void bindMenu()
        {
            string fullPath = Request.Url.AbsolutePath;
            string pageName = System.IO.Path.GetFileName(fullPath);
            string currPage = pageName + ".aspx";
            
            string lang = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "En" : "Ind";

            List<Menu> mn = all.GenerateMenu(lang, currPage);
            ListViewMenu.DataSource = mn;
            ListViewMenu.DataBind();

            List<Menu> ot = all.MenuOthers(lang,currPage);
            ListViewOthers.DataSource = ot;
            ListViewOthers.DataBind();

            ListViewResponsive.DataSource = ot;
            ListViewResponsive.DataBind();

            string otmn = lang == "En" ? "Others" : "Menu Lainnya";
            othermenu.Attributes["class"] = all.getOtherMenuClass(othermenu.Attributes["class"], currPage);
            othermenu.InnerHtml = "<a href=" + all.stripped("") + " onclick=" + all.stripped("openNavOverlay()") + " id=" + all.stripped("nav-right-option") + " >" + otmn + " <span class=" + all.stripped("") + "></span></a>";
            langMobile.InnerHtml = all.changeLangMobile(lang, true);
        }

        public bool ValidChar(string input)
        {
            string[] notvalid = { "'", "~", "{", "}", "Select", "SELECT", "INSERT", "Insert", "UPDATE", "Update", "Delete", "DELETE", "!", "=","Where"," OR " };
            for(int i=0;i<notvalid.Length; i++)
            {
                if(input.Contains(notvalid[i]))
                {
                    return false;
                }
            }
            return true;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            
            if (judul_form.Text != "" && nama_form.Text != "" && email_form.Text != "" && notelp_form.Text != "" && pesan_form.InnerText != "")
            {
                if (ValidChar(judul_form.Text) && ValidChar(nama_form.Text) && ValidChar(pesan_form.InnerText))
                {
                    ModelData Db = new ModelData();
                    string email = ConfigurationManager.AppSettings["emailsend"].ToString();
                    string pw = ConfigurationManager.AppSettings["pwsend"].ToString();
                    string emailto = ConfigurationManager.AppSettings["emailtosend"].ToString();
                    MailMessage msg = new MailMessage();
                    string Query = $@"insert into Inbox (judul,nama,email,no_telp,institusi,isi_pesan) values ('{judul_form.Text}','{nama_form.Text}','{email_form.Text}','{notelp_form.Text}','{Institusi_form.Value}','{ pesan_form.InnerText}')";
                    Db.setData(Query);
                    msg.From = new MailAddress(email);
                    msg.To.Add(emailto);
                    msg.Subject = judul_form.Text + " - Website PIS";
                    msg.Body = TemplateEmail(judul_form.Text, nama_form.Text, email_form.Text, notelp_form.Text, Institusi_form.Value, pesan_form.InnerText);
                    msg.IsBodyHtml = true;
                    //msg.Priority = MailPriority.High;


                    using (SmtpClient client = new SmtpClient())
                    {
                        client.EnableSsl = true;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential(email, pw);
                        client.Host = "smtp.gmail.com";
                        client.Port = 587;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;

                        //try
                        //{
                        //    client.Send(msg);
                        //    notif.ForeColor = System.Drawing.Color.Green;
                        //    notif.Text = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Message Sent" : "Pesan Terkirim ";
                        //}
                        //catch (Exception ex)
                        //{
                        //    notif.ForeColor = System.Drawing.Color.Red;
                        //    notif.Text = "Error toccured while sending your message." + ex.Message;
                        //}
                    }
                    clear();
                    notif.ForeColor = System.Drawing.Color.Green;
                    notif.Text = "Pesan berhasil terkirim";
                }
                else
                {
                    notif.ForeColor = System.Drawing.Color.Red;
                    notif.Text = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "please not use symbol in all data except email" : "kata tidak boleh mengandung simbol ";
                }
                

            }
            else
            {
                notif.ForeColor = System.Drawing.Color.Red;
                notif.Text = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "please fill in all data" : "harap isi semua data";
            }
            //Response.Redirect("contacts.aspx");
            //Console.WriteLine(12);
        }

       


        private string TemplateEmail(string judul, string name, string email, string no_telp,string institusi ,string description)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Email.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Judul}", judul);
            body = body.Replace("{Name}", name);
            body = body.Replace("{Email}", email);
            body = body.Replace("{No_telp}", no_telp);
            body = body.Replace("{Institusi}", institusi);
            body = body.Replace("{isi_pesan}", description);
            return body;
        }

        //protected void BtnSearch_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect(all.search + "" + txtSearch.Value);
        //}

        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                { 

                    LbEn.CssClass = "active";
                    LbInd.CssClass = LbInd.CssClass.Replace("active", "");

                }
                else
                {

                    LbInd.CssClass = "active";
                    LbEn.CssClass = LbEn.CssClass.Replace("active", ""); 
                }

            }
        }

        public void setLang(string lang)
        {
            if (Session["Lang"] == null)
            {
                Session.Add("Lang", lang);
            }
            else
            {
                Session["Lang"] = lang;
            }

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            Response.Redirect(url);

        }
        protected void LangInd_Click(object sender, EventArgs e)
        {
            setLang("Ind");
        }
        protected void LangEn_Click(object sender, EventArgs e)
        {
            setLang("En");
        }

        public void clear()
        {
            judul_form.Text = "";
            nama_form.Text = "";
            email_form.Text = "";
            notelp_form.Text = "";
            Institusi_form.Value = ""; 
            pesan_form.InnerText = "";
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("Search.aspx?search=" + txtSearch.Value);
        }
        protected string file_get_contents()
        {
            WebClient webClient = new WebClient();
            string publicIp = webClient.DownloadString("https://api.ipify.org");
            return publicIp;
        }
    }
}