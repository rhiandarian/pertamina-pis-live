﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace PIS_Backend
{
    public partial class Search : System.Web.UI.Page
    {
        SqlConnection con = Connection.conn();
        int pageSize = 8;
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadData(1);
                LoadPages();
            }
            
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"].ToString() == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
        public void LoadPages()
        {
            editClassLang();
            bool langEn = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? true : false;
            linkTitle.InnerText = langEn ? "Home" : "Beranda";
            link1.InnerText = langEn ? "Search" : link1.InnerText;
            title.InnerText = link1.InnerText;
        }
        public bool ValidChar(string input)
        {
            string[] notvalid = { "'", "~", "{", "}", "Select", "SELECT", "INSERT", "Insert", "UPDATE", "Update", "Delete", "DELETE", "!", "=", "Where", " OR " };
            for (int i = 0; i < notvalid.Length; i++)
            {
                if (input.Contains(notvalid[i]))
                {
                    return false;
                }
            }
            return true;
        }
        public void LoadData(int PageIndex)
        {
            bool langEn = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? true : false;

            string search = Request.QueryString["search"].ToString();
            if(ValidChar(search))
            {
                DataTable dt = GetData(PageIndex);
                if (dt.Rows.Count > 0)
                {
                    int totalRecord = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());
                    searchResult.InnerHtml = langEn ? "<strong>" + totalRecord + "</strong> data founds for : " + search : "<strong>" + totalRecord + "</strong> data ditemukan untuk : " + search;
                    ListViewSearch.DataSource = dt;
                    ListViewSearch.DataBind();
                    populatePaging(totalRecord, PageIndex);
                }
            }
            

        }
        public DataTable GetData(int PageIndex)
        {
            string lang = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "En" : "Ind";
            string search = Request.QueryString["search"].ToString();
            SearchValue.Text = search;
            SqlCommand cmd = new SqlCommand("SearchAll", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PageIndex", PageIndex);
            cmd.Parameters.AddWithValue("@txtSearch", search);
            cmd.Parameters.AddWithValue("@PageSize", pageSize);
            cmd.Parameters.AddWithValue("@Lang", lang);


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            if(dt.Rows.Count > 0)
            {
                return dt;
            }
            else
            {
                DataTable dta = new DataTable();
                return dta;
            }
            
        }
        private void populatePaging(int TotalCount, int Index)
        {
            List<Paging> pages = all.PopulatePaging(TotalCount, Index, pageSize);
            RptSrch.DataSource = pages;
            RptSrch.DataBind();
            detailSearch.InnerHtml = "Pages: " + Index + " of " + pages.Count;
        }
        protected void Page_Changed(object sender, EventArgs e)
        {
            string commendArgument = (sender as LinkButton).CommandArgument.ToString();
            if (commendArgument == "first")
            {
                LoadData(1);
            }
            else if (commendArgument == "last")
            {


                DataTable dt = GetData(1);


                int totalCount = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());

                List<Paging> pages = all.PopulatePaging(totalCount, 1, pageSize);

                int totalPaging = pages.Count;

                LoadData(totalPaging);

            }
            else
            {

                int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
                LoadData(pageIndex);
            }

        }
    }
}