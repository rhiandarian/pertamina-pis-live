﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class ListNews : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "4"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindGVArticle();
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void BindGVArticle(string txtSearch = null)
        {
            News atc = new News();


            DataTable dt = atc.LoadData(txtSearch);

            GVArticles.DataSource = dt;
            GVArticles.DataBind();

        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            string WhereId = "ID = " + id;

            News Db = new News();
            try
            {
                Db.Delete(id,getip(),GetBrowserDetails(),WhereId);
                BindGVArticle();
            }
            catch(Exception ex)
            {
                General all = new General();
                Db.ErrorLogHistoryFull(getip(), GetBrowserDetails(), Db.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                Response.Write("Error " + ex.Message.ToString());
            }
            
        }
        protected void EditClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;

            Response.Redirect("ubah-berita?ID=" + id);
        }
        protected void GVArticles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGVArticle();
            GVArticles.PageIndex = e.NewPageIndex;
            GVArticles.DataBind();
        }

        
    }
}