﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class LayananJson : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Response.Write(data());
            }
        }
        public string data()
        {
            General all = new General();
            Layanan ly = new Layanan();
            DataTable dt = ly.Select(" TOP 3 *");
            string print = "[";
            for(int i=0; i< dt.Rows.Count; i++)
            {
                string coma = i < dt.Rows.Count - 1 ? ", " : "";
                print += "{" + all.addJsonFieldValue("img_file", dt.Rows[i]["img_file"].ToString());
                print += all.addJsonFieldValue("title_en", dt.Rows[i]["title"].ToString());
                print += all.addJsonFieldValue("title", dt.Rows[i]["title"].ToString(), true) + "}" + coma;
            }
            print += "]";
            return print;
        }
    }
}