﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewTemplate.aspx.cs" Inherits="PIS_Backend.NewTemplate" %>





<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Pertamina PIS</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="images/favicon.png" rel="icon">
  <link href="images/favicon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="vendor/bootstrap-4.5.3-dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/animate/animate.css" rel="stylesheet" />
  <link href="vendor/venobox/venobox.css" rel="stylesheet">
  <link href="vendor/aos/aos.css" rel="stylesheet">
  <!--
    <link href="vendor/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
  -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="vendor/owl-carousel/dist/assets/owl.carousel.min.css">
  <link rel="stylesheet" type="text/css" href="vendor/owl-carousel/dist/assets/owl.theme.default.css">

  <!-- Template Main CSS File -->
    <link href="css/v01/style.css" rel="stylesheet" />
</head>

<body>
  <form method="post" action="./" id="ctl00">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="zIX3xA8gWHCV8RPlyHsv9Yak10A9L5EU5SAAaQDeAP2o2LbMjsQ3FzLK4LMo6is5wVj5r2kD4nXAbiqygazAfGIhn0SSFn9vCP4e8MkVa6h7XnGIyq3QIlJEb5pbIDE4CJv7BwSII/zJYdAfl6I+gJtc9JniMWGcMjS5tlMFsVLJ25M6H8olEVI/U0gu3Rv1CxxcntyMqDZLdrOYQ2qH/0eTABf3y7gBEStgpfPhejbpdbIkGkrroI95aRLVGSdVzLh7+QyFO3/MRIdWa/gXJuw+5a0FuvfngUl5tNtclhDkoXdc72EcU1FezbBStqR9G597yZvVqG0GcFp3AeHYAXWvKk1u19XcM17zS/nkQb6CvaW1X53Z1husETeEArmlDt+BjGbyvNMGw1+B1zclBacrH+5bNnxX+kYwzQzjh+oSlmb1eidJJDNwe86TlxJt4LqCZa/OrFXQpSasMH0ZDMVQL3gJl0pSJZtjwe9ynNqI0kRypOFWxjXBtknVXzbnVwoiGmmdBT0K6EcQbN5gu/Rjy74UQ3c3VQkJgFvgYiW4EEDGdEZVSNUfy8kn/tikKOxewHWtM0YUspBoiU9nOe76s+QQLJtBcCHd3BTbUwPTCsfzXFy0aJSTKsBRgNfLIBXTCZcrED3NAqlvOxoQWe5ftuzFfmz3XghT3+/u/8SX7F5gBZLXMjeuZhb3ODjXYDms4l7C2aDxk3loFB48iZKUIKX2TPLlgLU/nv//3R8R8tuvAhcxzTNd9R8TbUZA2W83wa60O55/fw881dwiEJ9wOsE+yfOnGzqz4D+7mqc3VChiOdhmBmuR1L0iAAeF0zN+n/edgAD1HexwpDA4egmHXJuffKwNrghxZJX7Rdv0a6dm+ZWky8mFPXH+WL6M5M+fo3FxiGCQzeS2AInL3735aEYNCGKxXkE6KNvLpwy3Yladw1X+hOzloPEvfAWD7BuXjhI4UImZNpHKgTA7PBckHUNJoBJOJpSrLkkKJgN5L9xv8pviOTwS0hPkuSwNpeWODbXX8Hypc9cQKXb2SNnW+4ahGYdX39wf9PqPV5qMZJ+Q4Gj1OGQ/OFnWZZDFl34c87QjCsRseU6r/BnuvGcwMiOeOIFXsdyvKw0fzWXdq2VJofxECRmPxoiE1O5ugJU+ohIwl7PQ7F2kT1GIum75xADWsM3bWqes4AUxOTPVAHbhDJx07MCdrd4QTrF8bo2StW9G2JpH8plGNqDn0M6oN+DiTNs6fVFPgi6Jko4imHpTqRRbuN5KE08JgboI+bdi5rpwy+gg2Ku155e+QuuAY3PST9agOd8/8L7tO481HKWEqDtqGcf/VO+YQWN0cDDrq3hHdvQktktlDagTFI1lmiGc5r4lbLTMTKh964j8Enk5MiooxM3tSb8fGoa12Wtp1Q1qj0qa5qj+xAMacrMp0xwjWrpZYEsBJRC6s+F/DDmVIh0qVJiN/NvaPUZTd86c+98nWXEOnki8C/QTIeP+YBtPj9v0fLk/FqEeD5TbpfSDgZ4SgZHZi/GVx6xkdHbOcjVr57EfozqcSsQUHbb0e0I4wtD3sAUzSkuFzyrhXPQiu6HaR/mFUzt00BgwLj6C9GAdOtNHywwAQ4E1jwHmMDiZapEw1Y+kDJFipKBW/ZKuxqibAKP2D+RNuZzHwUaHcaHDW3tpaomENCPA41opR/0M40fRYNhGI4aHb2SV54o4hXEElFK6CZkChDJ9r5pIVfYcDcWMiZBow3T4PITQB8bJ1s4oYVmnz4jvNvidAlnOx1i/0DUSGMWvhzqnLb2Hd6xqre1Rsom2jiIobgAdZJGZp2wHjbM30vaI10Avr7js6X6JV61BkZrZzytnFS3JcCZ45fdS+YRIN0W6Dv8ejJcA7HVAB/vRgLq0PRQWq3UvJsYXn5khrgCgbMP3haPhyIr6WfBDEZKUlKWMXKETOr+E6KRxgEAsLbSHAsOGKkYkhvC9HcC+ciBRbRvdkQTV+fp2iie28YvkYD4url+8IcKzg3LCPIs7sRGM/bp9svZxCeD2IWdl2vvBHHEfBEX/44BYUPaAggHCAlyN0J01fXKF1a1VH31dgzZwCj0mcDTxAtNCgDkrLDU4wAVv/o0HVcnPJMmX3miYRSaHa5hwhANWEHlc2vzUvEdbhHRgGlngftYi2MA1S10oMNNIPKQYHb5ll3zUjhMDcA9upGNppfAE59WG3TSS/YZiotY11BP9RLeWv04i0KyerhQj1LPG/R9nf/dla1flowGVXk9/KYfqu2p7V+snCq0geG0rvhVFnzPauaeFFd7vgj1YQhpewTgIDO5ownZljA6RuQvPXQEpsNW+wWfaeuHqvefFRErr70nRBDHwQ0jeb3puHzYLheu5m4BIls8jK3awquFXab19sKa7Gs74dXzztAMm/sSJtt17ej+iJ/CFkD6byklP5UbrlVQQqXg+1STs6UeZ25Vqs8E9crbKeP7g2YzIcYwjqvST+ztOdrGkTc6YTHpsdpaVfhOt5egK0ITt43s+u0BIrZpi4YfycxTxmvSQj86Y3SgICvpGLQvYDzTD4WyeowScTUcq7Xc+pt9IYmNk3EM4ei7g8joSTr9qfRNUfSjjVyC1R6jb4r+DQkbuC56xjlDDBiBkEm0LjbLRY7PJ9tpPf3LNa6J2TS/MOE6wCGUtbv6R2zddugaD6evF9b+OF8GF6jd2tNjcIeM54beXQY2RrN7HvyTINILvuMb1yUIQyS8ztXNjcFyQ4FBj7oXh4WKmDwQSAl5lU538y8fCOpc3CQrO03rRy3WrFURx8rwwKZNk66idMbKyrQYlcRH5yx6RNT0mgyfzI/QbIv36M373hVnaNjbCOTZQSJEAIwNxPJ4QL7mhrwwcJQZ+ldiIv7Xr3akFNLb+Ev+iXZHYCSQEkjk9eP85FsDxlEPr/Po+Vto8v2owNhTm/MldE1B/MxafLoLQlBzbu9X+mXy+2tSa0PP72ZKoiyz5jCC1L24QCWGNKaTb285Pd8XAYoQvZVo+97B05/ReVRzxtloWbYhKPfcrqcjmm6RjJOTWEwWF5JOJf/ICnJv4TM2Tj195JIJ11KNGSCDmAafHxcoZCDYtE0qfGsQkRFQ2fFCL/JVWDu0dhaDxJdkI/B/K43hClwUdTPb+tudAf25dB7C+1rH2JUgFgKRzMinBGGJ7E5VxZ7OWZcfP5kU4FWie39cZ7ModPhB46NK+8IBsi3wUDB25SOBTUzJbGTB+Ywm41aIua/SMwBL540Xwk3khQlRKy+OD1b4w2Z9jesSldavB82POP6L2YxWOYcUkeMFD/yNEML2luqvSy+V5QcEhq6EPQABN6WwHxrUwrRuHTmkykL3fFCRjK3C0e0Xr6ftnr1B3B6TCN7oBneuBzFt2httDCQf9zE3u7PDrVM7SRAWa/JMBFrefUIU9ItF4wGmEPEO2IsgzSERM/fppSxrniOlkmKoYwzin4Hlr8O402HSvaO1tV0mB1eYTYx0/Wq+tur4uvluelOkJcnLe2PobBVB5LfmkkHxnUvFA2GMVixSnyw7a9yf6ipc41ED0yT7I2Oc7v/qy6hq0dyosAb1f77z9iuh6wZ8/kH4RT2lDpJ5m7jP5DDR/5ROEXfAjedMFfdOsJ3yL9BBEIOEVCs5KuIlgF6yIB7Q9HqP74UxGp0surnGiKVzA8K+dENwCodgwRZcrFzvKYhxZK6qhR9m8q/I9FH9w+LKiRagcdsZeiBPc5gFUNvuMBNSpu+zPOMLsnn/1+oQL6YXHKCe41IBccZksvlb1g5OI3ZPris3kQhPLLbLIJqns8sqhtOOq23Vop1KcqJrwh9ObBWnaQR0UVgcv4QhJbp+BR6Dv72CnWXhQpdi68W2i/SDRE5KTOnHre2d0mVMYn0Gf5wvhd/C8UyKxXsOvlYUwkMITVEEAUV6YGAnY0umRJ7s5b3nDjREAiQsb3Egdid5ItDEKgzQdNb6kgkoeB2I63/X8P0V1FzAYB2UJ8KB1PV2to3FTTgMFS++t/uBEbnuyPilU8ztq2R8GD045RNYPw+dLQ9NVlCUCd+zxNT9R9la7PuY7bTc+1fewWFM5NP3/5WEDWwtjkPdqLCt64reILl/uqYOPr/YRdW0ChAD0TH/Oshqjr+NG/moiYjMouCsCYFh2JQ27oW9+Nrnw3A/b8hLn2hNygj6X/bQZVJjre08aLU7VrDP8hgoRvTN/7KRJBbZHkd7jGvQ8uATdvVxjBc3/CKOaWIV3pBHhsZRLFdBNnrlpz9AIzR0Y5vLnc3Iss4RFhBVxjXRibEgY4HHvn7E4JMzwsqBG5aNE7VpPN3Y2kIP9eyVMqqqyGfK1stgLcnW2chXYA6BIPtegMWDFy2hos4c2ia+alSL/Lo9/tnwdlom0CGrRLAMKpVsJGx8ELnf89q8Q2wZplo5gUvzb1sQcxRMhinKgieKUw3Iv3UxQzwXt82dRGVPNxL9nYZeLxfFmaEz9+qbgXU5+5HNriUTVUJ/3OhJ1c1Wx9RhfZHRuxJwk5V2QHw+iJd1cxAHwonh8hvtoz9DhJemK66csl67h4ckEjUQ35SPGjvWbAUB2VQaOeSqo1hnOwkApXjNSyKWXysQQiTL8jnwxNRRSa/aY9qB/n+H0cWdIX2UjT57gTb1fqEY/pD5cy285vVHxliD8hlrx3IZBfSabhDXXqNmbsOS5EbX2ou4uzQLaDIM+KMLcLrQl2HOBjHa0VW30Ga7daKvq/xcRj347icnXnT5+1/oWgxD6dvs8kFDAYG2DS4YESYNxPuMlhLBDh50OjNUZdjaJhkIDJGZBV6E3Qkfoa9QZpzCrNaNe99yjRzwjJykwk3wJDZpcP1vrAJ+Evwmogc/38mH1vDWiBBsv5+cTMcEj5p8dMBnUqfiqxOZctz0Ruj66OSFvit8tTqYpEBN0qcgDQbW8AUCzfESEIK8ykHxdwlBsXgcRU2L1wEVyrkHtV3aX6d5ct3/U7IrsoO7A7TM3qE8TFtQjZcutfeaegPNTY//QqzTh/CXy6efUOpdq3+ZMVZ78eb62/RWMu1FFKMV6ed89sYbjs91DqxF9ikprGwhDc5m/KUrsLm1yd+TBYqZ4jWE4xyuWzEnnqTqUpWyHWkp06F7nwilL4q5xfr/3GMxX3Cvk+y2fCcVMZvXLAavxleLEwtmfxTphWyX0eHTv+dL/rCwhUHApoNB35YmVV4TRFGCGK0k7JL0E58vrOwOPveBY80u2RuNQFlE3ADEUw1vBoF08ZRfJjrPH9vTyvVxVmyM5Nlu5oQxrr4tfmBDdxi8p/7YE8wdFp7P/3KJ6XfMHqqhs84Um6Ufv+x+g7I95m5wwFTiZNL7Zb3l2jJz59PJl7v59tgmxxEaj3IOCZ4t5iv0v0bLnFOuOTdMeqVhq7PicztfcB5LLC8CDLZieK7LpLDzcjCKO2pwuayGMxgiPYXJJdfBwjJP2m6kLncSAvuTgGg5fucIPs0iQ2TpDPkX5lTemRNpv9Je1ma9MAtCXrb++/jHLRk7aNW42SLCm68wp0sGOLaPwuL62OSO9srdf0Q5LzLepEANytOK1bU91r+DgntVf4iX2jLrfk8GAU//ra2gf9Dd5GbFLxjHYEqVTQtMnnld49lSTRHn0U5H/WYmPFxp9QtSZRzCRCBmEaBGPrS6RoEAZBdJhI4An6Doq0d7zFzFsJ2VBkinPd3TtOeaDDXbMy4pmmHDuRNQiCFcr/HN1FBuXjavxZHKej0ha/sZRnSinD5EIwns3E1DWK2xmGutMBlp1rdm+jkbuOwku2/kcNkoRnXPhnXx8q4LWfMmbrOsEGgHmM+0SkiweYXuqHKEi2TQgPZn+lCqZY0zUlz08m6W+FxLDj4XoQfrcMulswPCTPoPfFhNtWVDA4m7BGQVzHewJktJJoeAN/FPyxWyuxIo9RNKbLcxSeXssqnltu5Ya/pyanDkpshIFfE2gG7t5BeYHX7srdtai0zI6tSEw6caBR8NifDofEpK4LcPmUseH6Runp5O1YwmP4HywPfXIPQMKekm64QfkaMk1JjTNdR6aog78i2spZFq1jFhwcgoYI6MQgcA700fWhFcDYK2SJ83sdDWyqXafbb7jKdbhBP05jFQoWCPtSNmo5OkS7OQNAAtrD8Q1zEiwxcqW2bsFz+BwNO9dLeTqJ2T+Rv6H4dvL0ZmdI6Wk65xAsP0FLLvg1oBWo3M+ThPWps8TrmIy57c0TwWKblrDPDB7Hcqn3TdG4LSnsbOwH4bOLRLmsUmqytrpMRbevJsOrS10TbBoXrih2j+gViqDFAbqyq4PCAQ3e0A5X35owISTd1ZusiQOzQsDE1qGczppK9cs33Qo6FOl1ulBrUSX1FahaT7XG6OwC9CwicyMdhNr8FlMJirwP06dGwfM3W58fa+84RGEPCPFxcBPj9zKE3VBxV2nfEdsUD8cEyKVTZKQFrSOiHdZ/fzANKkaCTOZWwoBGCXddAwBIzx8FDc8hP5SwImupo2A7oBApAhUbgiBv71Ub8nlKWJ3n/2i640JZbejUJwnaiiFdSr2rBHt/M5kkf5vSbWXZvauG+mqUfs3SNT7cp8uN17nWwZzG2SxjGt6TorYWiaJCwqxTXOFaYSC8h9uqcnDIkC/d0TK7Oe0KdjHhxSZrK7NflHDYZBmjJaZx7gSGXhK1+O2F1PiqJ/rEjoz0ylEuzIR5zpy5L/DLicanGaycZlPvnpsChzPXKTV2vsbWWmIlXo1Qsy3E99hpLUTnGKNm8/WkhkQ0/dj1v+GaLRojR0mb8BhRYYBmDwAKtQGw/NGLWea1m00cW6ihFgVcw9PGGwHnr7Ia9HewDI09YSGm6euTyWctlSamgUlA7x4jFNu5/3ktPuwYsN/YkFknBuuPd6yk4ckYHnwz4Sa2DDmt9iKEd52B+vwGSO1DrUARcLkZwMK7x251SK3RunzRbOSYYD9twrRX4UpSrJZgHPUiGz6WdZNvLNbb3Wf6c6ah5N3+znxRHD/MXkqg0TIPYMBVnF+BTSkD+4PCDgQSkDTRu65f8iwQ4CeROPvqoirfbLaw/PgbjYncFm5uHCwknq5rVCH6uEOap3rxFVS9w9//UbX4g96c5nLsaM+6EmvVRUr8J0yhszLNiqOse/5K/0ZjaB0HSSbtv7krzmSngsRT2iQPfcCagYM01qF+6dV6wrf/XcEk9RuFeQ7z51HOLSHpuDcD9SKgtS7HPFIDF0Rrph0JzQrVFOzR6R35FtEsknsEj70IFtgVJamMP0gPzLMQstcawh2XuDHW8mZ+hRpCXTpczkwpDy4nLxmfs30CMuKO5pDZ4IKdjLVFXctSfegYem35TWci0WUWL2e1OFQMaatXHmMG75Ox/U7peJpPHaWuMQ9no6YOlImw7F2rKcKE1G/siMYbd/sRKt2DA4E/O/fpHGlIxdsbilP1cMvEJgU3HWL7ZOPEjjGOTVgRR4DRys5JDukE+Q3RvddNhY2gTXAeUjt4L0Lv3scQtyg/nXa03C6v7X1zA1H8F4+lrv5bu92PIVcBlPUa0sG9G+DNaIBSs7h61zruf24oYUljNBML9ZHHl7Ndds4gfVVj2Tov20qjfRFXxD7spodUBLWXPCebvbFwAWwIruu7UouA7spdTVH5vyQnQ7JFrZUTm4ag+rqceWuoJzi1zOI+9FsAEgVHIwu+SP+TMvyst9//yJKxU+CQXajywi3cu0xLHFtAjIk8z5xYf+EeH7eyg/S80bnbhuCw6fvIPbfgqmK1QO5q8b4JC5YaZfNGk4xXB6y5bTKAzd6sc1U0TULnYTbYrFfPbUamYpSoHiqE/0qoH5ipVn8YH35sVp2h+4LhCNbGaoVTPRCz2nAb/qoF4h+y540q612mdg2u6cCztBulHVC5R1Aoa7imH8T5u5BTrlZETx5SJixAO5fzMP1AOYn0A5TjUyVZ/Ux2a3tqFwAUBH6lFpldTZnAK8OHFQnLL3WtuGF55znKtYlKGDfKczKWtV7mWgz06PiGZnGDBkG7wL0gXanMBrDNZkd6XGI052gan7sypmITHQXFT6Z7qxYVlA1QFlObayqXUyilrYTp+tyAImVYNwrDAGgNU6MwIWn7mPHmO43Rx/3h6sBTeSnIp/nAcGmApUbM5Fek9D22PPMt4y/wLCcbSpLmB97ZublrC4tveBiFiDZyAX0kI6LIXFBIuqxPza3qLRh2pLnNzv39Pv2XAzdt34fWnHeADwBe0sEqSxOKBEhyplG7set0fWJX60nbL6v8XEsPdDac9SkX7jGUN+wpExUidfBAQoSEuF8pueUx9AapvlLQ3LxAe/gl0OUuYkKV1i3TgwcmhxgTnNHtJIzZIostCVwknngCXPjDRYSgeEO9ThwlqLLgBZkoPaNnptdKFL/7UBwlQsixFhNGkUjmyIw5lxqwDmqmb97iKUrtjwXhG0cyA3TcZpPWDeSgYmsJzcF3NBHmLATD4tD4X5JQD/EcBh14wXRxQyznsPv+T7Oq801pzvgPoWSda4Ns4Q9prvWzUUnxqfh5J3ybv2d+1cJxFZxNQ6b0C3ng17CZeNPQhJJAoWWL5mbojQ/J5Z8SM0YgIhB3XnVa6FJXIMievS8B5hxJ8zXUxFhGec9HKv88jNwpJcP9FcEA8V+I6bfZHW3gFgMLtZ0OXYP1i0xdkhmxjvE+y40A2KypqvfrhrJqOLsK5sAWLKfGINPqqtBpXx5ScK1KujHdCJ4JFjZzBkhnYth2ZTyisBVFr8QEaF5m71SA6Dx1mBYK5e7Bd+3BJHgvHR2iDTb4Geqzfx8QC/b7p0JQTpxy/VFd+1m8m/f1as/EeexyRh+XDhXl54V2x027Av9nejgIIZ7brakOUQOsvt/+nSI36d3G0+r2OG0T6mkiqWQAbg5Reb663lYBw1IF2MMofaAzV0Tam1Tw88w2CE7jR5e330dS/gTm+77lgaiVB5WORa5rZPh/C2bjihJ/ZV5vbot59gLfdKkbynl3gl6s4lmnMkRmJLZBGt40MEDnqrtgGaaJyQATiGsN+D7phFNJBj9xl4yPLwZ+2aKsSeOyl9AemQevCfDWoLZFHcMFZAQly7vp12f00ps7QnlbMsGsKds6XRejoPLSI0Q54Wmz3wEquRSaZPE+eME7+/zumeDUAQf51p1SFxfGk9ofRg/JajWP76af/u3C1wS1qQXrqeSHrk8tUoCO/w/gqx/gf1Yu/likLAeg7p4GmNpwcOdelYBpGekdfv0XmggGC2OPfRCRkxp1KtsFoVRNfKlCh2CMPSYaCIMM/TLxYO7adiN/tQQzftfaevZprVrKx0wJ7phMoIxuJPndRLL3MSIgH+vS6AkUFUQM+PtUQvc+F3sQ4+yKIbGkoc+RNWaORA16C15UCxRpTJ5Bl3zWHyJ+uykIZBBWzmVRmMBle1w7Vxyqy+WBaSkpLYUx9VnGkGD9ExxSeTXvINbVvB51j8gyoV8R+RADc7QB3XAlGGta6fwCofRcNmVfLGqxFxdS+QS7yrsCW2meMAJ0wamTT/QuqJz9w7TZDOaLH9QuwzLk4oyuV4umu8ZyLhZxl9vy5fzVqaU4sLky4CWjXDCyPjKfsnbv59+z0b0IASXxrUL5M7TYWUhkqrPQFE8Z82sutiA3zC3AwijbVRi156n/y3IKJReJXUyOo3K97S5V6cjJ+F/Ruv2xOT9c5/KaO8o1v1G/iAsUNVbu5IO6z3potsktZ9Y25bwdAVKUHohTJqQSOJK7FIvJh1io3adW7hUIvmxkh39DqWXoKPmdtnAkeZzGnigF6dIUl744tJXE8l/A12HVQK5q1qh7mts8K8FD6yQBIzl27CZQAfMQItGnnYKVG+Zs/2KZ57oEFPUhUUDkYwBjcJSUA/iRtYevDjohNwk5WhtelxllffdOj+GgXbaoGcWdTXWmoGPVCM0y2m+Ad/kSNKh+YGMYNkBcuMhLejLKPPqVIRO6BeuQegOp5B39R+1ip+0lXZbYm/IOsugfoWUTFr2z/pjsV+T6Gr1S1NqtUtyv2FqgKOpC3OlQ6lNPtT0Uk5z6V95nNVZVywWuXNGwOXyWNExEHhg4EgRh4LQUhBqrtj0CY4gEgViC1+vNzH5g08au54wS8sGOUi7wDsv1RObUTSm812AdQvA6dkUrI6U/zEX1BjZHfvSt7DffUsQyFbjOkCIJ93BMkhC4/89A0CAgIyWhzIXTUULeS7Exgsm2P9p3YA/c0q/QaIKEWUk90BsEIFTtR4QSIugHmXWFbxVYx5myaxXOIYkp86yWaz2SpFr+6a4Xy07gUQWzuRNlkoOV8SGI4iV4cWcFlvXSJioF7mysHX9wxpLEJCLNYcJOgac7UkUiUETU9qLCvERXsuCOCrfOpD1NOkJE9PX9Zl91d3eyt6DXO2SYiz9+DVqaYZ80Tz9cP93AGWMBIsLNNV2LJGh4S01tiHzz/JgJdqZ6XNYb/WdAsMS2TkvNlLT2j8YPN3egAWMOPIxZASBrMKDleGQ2yWpqw803gakItCBcqqX8yl4SNnmwX6IYFpRgOZzkSX4pTvhlxG+KiiSm2wo5ZTGVnQB7J9qLH4hmS3FWVc+KJVFUj7x/RoikDLbdz17dvHQ4db9h69i0OJyohIyl4B2UsNpDtkVYai73nIkSgt6XP3dEzaGjkKeZdxDrD0oj9YSTtxub6h+sHjZ10QnKmb/Xh5MIp/9v6Fmua2RoQkTSEiNt/RP5p45KfBHKiMODw9ApbQnH0nzNKXzniG7slpoTe6rWK+wQnf52H76nXR6QG+uBFS1A0LREy7xGMrGdrsk0+CGsglNeie36V1t3GCPVJC+vpRxNcAOtnx56hXsa//pTyoSH2vPJxbdWysevRYxYy7CTXamRU/hgf8cvh/XhxqNFXwoHYu2tf9DkuPAi9Zmsxb9rPgdNsSX7a503tK3a/3SVS3l4iMv6z0+MoBZwXzHPtXdLVxAEzZf0BzUJsZnAYOMxzUjcPVMWtUJ5+J19T8aBLkJ0URDkWJAnZjNyG2l6X+tQUs+ry5crB33X1kUbYdCFrhR8Ie+80pNSKxMuJRm9jxkqzcB7HWTSdZZRba7bq+a+o0MwjN89lPDEJ/RBcf5Dzio2bE3J8mMiEzBe7tZ1BPQApeIPNiW7pqrA2rDfIMzktm0j3m+U478QslSgt/vdBnj8N7hGvSoaVl8SfYpjX+IJyQVDHuHM1aWluXKn5iR90xCTViRg1dCKvectqdadLwZo9emvFME4Yy658NTVxt/pr2FwS29EvCYFzWkoEPuZWzY/zwzBcwUIHvUV7/Pv6WzLJYkaI0NXhTqxWjbRDqUCIsMh1f6TW21+7aEoz+oGFaAXddh4KEl1Ak/KZrevOdGcWiSrxulioCaSmxSG9mjOlLfTKz+A5qhSwWoHWIgQI0wKD9zoFnO8z9RxbaoXiajwytNZcV4n9JgwkeWOgyVgzlaGs+qrWx1ULH8/UdWaEAHsbCy8g7rPApVlD2wECBNV3mm2K/4fqfk2OT+czHOzvH6UW/Qx5zNcdeczeedx5cr/cYChjPLUGJKFd/NU80oTfheWpQnQiLKbB/4so668ZEq4qVEZeVhDvbumasAvQ2hvnAHWdlDngcQNP/gpL4MmjD/8/YPl2rRF3ahUFgsSgWYGTipMuAzqKWmx/UAfVOdPhlaQ0/MU3+bXFgw7TB6NQvnzKPFr4qdv3EkIr1ZQHCACsyd+oUJYV3OJg2eHkoKfcidzdcePIjMUFulw5HPrQD9QuF4OeUk2azN5hXMrVWpY3Oh5jnfknEAA/WGnxzA1FpxHZ9SBnkCSjSVW1prir137chdBF6a0T4XLdwO9S/VlsZg0SiO6UeEwy1euo2PbNCgzyfKXZg0zKLdkQq3/GfKq+GRL6CY2wvsfnicMnwayFpTSmRYBR20fmz5RTf7d1dtggdOor/k1gdJMZDd9Pj7nqTxxR7LXFEGPOYwcPmhqUDvz1XmGUJkCuYpAluFamO2aY/H4MU8GhKmOjEN2XjEWn53jY2o2BaZiQPFRq/G84p3KP0s48DChCdwpZPCzAsbpY4DrYQgTIBqBR1kITT9eIFsf+V7xHVQV1y2O11aL8eNSfu8qWsnbAKwe58NZjQZdnpmEVyrNp/NEsh2Hrgoqj6Rdv7IzuHoyY/fbMuF2YOWLPIzU82ZssaPFnjAarF86M0TLeEko/WvcLc5RUhnYNIVBmbIZeXmyC9nmosIWXtPrSQLj5iPpYVbUcNQepJZGcWgRFPr7SNPfzIlxUTCCjCu8QCGOhCQBufB8W32dZT4HHXp04EJgm5M57zo/WVyK8A3s7pMQ9SkQUCfAf+zvy0Yc6GibeIz3r6nEkJy9DOy5qyEc9qzFKWQLc+YeNuapFMdNlJ8vlpjtsFz1TCf6SPpkzxKG9x/ujQARD6tMyChcZiq/UU2YrMdmPxFMzppTYNyleR+L7JznVaJG9JEGQ0rfm4E3ZGH27MuM9+iNS/DYHa9MgdtVGWJIT02sUWLWGy9awfV1knU89GVUj1QlPIRv4m0QKHrxs3s2ybf4pig20cLdc+MrmdP+aQ9f8gqtpcKeNHx9B+NUsnX61zXk4uWrTpDby8LksYbXeYXwqGhaW+YgHB1aBKF+KDVpF4ZOtlXhvNR6HHMomZI5WHgNAqmJ1nnhYiSfN431vYWbwHZ/T+IUa6rsvRlDpxoZe3IR+BOQOe66Ur1EQt9ExrlkkxrSVHL6fTVXy/NHOoxo1QWtC6USrF72zF8nT6BaWK9lOERoJKOzzSNmIKiwQ2VlxMZJbH6j2WJ07oKil5sKjhxNDB1huHDFH8ubpwK1UcCAbasS2FW9qZNRMdrAn6LCAcD56QPyjvFoSQymKlrnihcDTWmInWNrQWFOU8FnEwxfSx6KcXsbk0wdyhqgyBK01f3F7JF7sTHUn1CvCqrmnFV8MXLNwdgADUUtg/PIQNqjso1TKw+lktu8EdG0pNW8QhzplpZZ4PSYQO5oR1vrfCgYh4S6wNEkWmSJZYkn0IwuVAWBYiGlYir6WOCnSzXieHI7ir5BjRIYnF7HV6e4twDNJc74bAY/Wm9fSATglUUHJ2Y1kPZLIWg2Z5Lv9L20iRKR2Atl2jhRDmSAJ3P0UIDETLrnhmUSxkYmJJDW4uM6V4Le8J18qPOFqVur5+y5IY6uSwNFs+brraUsc8OhaGLSDwo39dhmP6rI8dn4UAbVtkQBlfyONI6lHSfwpCigv3Alk9mqwciw4VFLWgbxa/pmW1PX6xkRzpe4dS9pr9qSudtM0vXkG4SLVwvNLiFKcF3aotdi7kt954RbpbSvm4/7u9fERS3p22taSq7gMd1i4MUYarQkF0X8vg9bmEz8UT2IsOelvuh+XeFvADWiUril5aP+5KKLbbx1orWXPinEdHWrftT6EhstprkaD2d98tSoZy+ZfFFdozVif2MhW0lvs4vCfMSuGOS0nn7qX35gu7Hk9hu0edv7xbHyHKpudGBMR3uwSbp/5CgE87V/Srbqm/k8//5b7DBLy8/tnwsPSOAxL1iGV9sdNKwzvIv/sDFImMq15uE8RwyogilOWpeb9l5nOHJQ16lWqP+Nw1r10z2mH3ceIhsP2VDppdX0PbczrEcyD0PON7CPbBYDRuxdkeMXDY9TbKPJ26ipkaRddpTpA+lZMbeC8Dg+ftetKPr/6izfvqjxxNAeIT2xNORHIZa8Q4Cz76kW8jveFnEXHP2E8zJ0HS3RPuSkMhe1Kx3THoBjS4GZ6YVVBNYirtR1Var48TEQ1WRZMArlWf6U94y4B86OJ+G5K4sWiMhVzeVvQvAHbGJaMb62iUMe9ZNCqjishk1X5xU23we1ew8WDjFPZe5YOg1jlIusqbcaJLNOxcUIwte8TK6Woi05EDigTcHUkHDIrtdtSvvnfYefoTlCFYpiGFK5ZdQ2lEa5eHoKNlb/QzzJQDQASjj36zBD2g3jidf6z8bnpWC7wEJy7eUupVU56FgiQlmrQHaCnf5S6f1hIF1QrX9AggViwTYvJj5eLBoyiNE9ZCuRgOUKXSgUQqmqBrSS2pwoBevzWwLG7TlissMohaBnv+srhXrVjL548Zgo7PpoFTm3U1R7TKiIYCiPQaudFHVnEZNYP2U4/FGuivLjGKizEb4cT+Bi2I9ZPm1vusiKSndr6404YZlhlP/vmDtTGd6TCOj16BXinsJpYt8o7wXXgqjsUrpO8arcwecTpQGcKOrk5uOuMUfhzHiqlBGG+JURdE9QcqnsIg2klOkDPv6GRVzha2WNG/qDuLypkGrpQGVPG2BymVoUuuOA9/XMhZDHOQwb7a8k5paTqwiiwLt4PuDwPpIDqg+rpqnwsP7n+1e1+WQwXBR9jqZxVT2OcRGc0l2Bov9miJazi6bqEJK4ggMbT1LJtnb4upl4ioaIsRwdvueWQCRPvZwJY3CCwzqZiYXib+9wOlaIx/2eTKRU7r6oC86d1VqMCH0sSa8+QfMfh8qpfeZQDQYC3z7VybbMmVRf5OI2RGaSoqSbyLUs4n1+UR5mwCDsV1MZuFSnZ3u9a99Fo+YJPFdArENB5kAcFlw5GOryBRUfA5okNtnh8JcCTNscGh1hPTjSNS150r/SPJlv7hJqD8vr5jBKjxNX8bLhDAgYiJhpJ5LbuCj1Vp50e695kXEA3v30AvrGD73PBtPAmiOc8wnG9pgXbDTZBhCdl/g/1W23YY23SrrRBXENQbz8UGBhCiDfA12BOOOqV+X56GzybCDFs4asRr7bo6mrJ5M9xJSrtGPTiFoZotQj9JzIrv38d8jLuN8KVXfEg+CAQ4iVegI1AE8AoxLogmvYlEvky5CDgTfzXoqjZhQv9LaZni3fXr8Ra+CmKRuXdaKVUDpGSedy3EGkDwGJaH6rxlZB7tBxw0xzv06zGAa/o6J0bWzif5ZFHdAzcE6WRt33P6LWvnqDC1+I+kNE76JHngQBiyPPR0oKRnuIlLaDlvFXR56qiqJfb1xH/W5W2KG3r2TZWRnkD8/lvmqV1/c/Vn9KwdvYljyZzyC0Z3HKo/Q+e33seHBnmpTNYN3eyqQA6wHKpc6yWWkZiq5jzsHgtuGLn0kEomp3i0K0OssrrgPuCcTrtdUFtt6pU8EkeLHSQWTndMZahLR1K5qnAjPn86Dm83jbU/OmP5Ie0+AfOXJGKaM6f1iffBlQv7+l6+NdEhgHmxWC1EsqfzHKgHU5CBpjGsv+EkXYT84+o471tdXM+CxeTQQGePcXxjmLMo4E/4XCycHwTkfFLuCqr9SOMx4qF9uqrnfuyKIVr12iaSMsIReJ+aHlNax++wdnxfSia7FXe4/7wjLsNvnSv/4VOuycAPHdp+JCHci0fou3rysgvuaPHUmSKkFXmlCIzNFACoLrN0BJkqe5Di/FZBZ3OJUBD949/tvIq0rQ51yx5i08CxjQW1Rg15cTBv/vVYkl79vTvhFZCvBtf/XnRatXb+vJE9UenWbinhbSOnZXvLV+xkUvXnYEvQPSvpuoFElXI4nJKAE/LUJ+g46Fc4QgMt7dHIt6GVoWNVIveyWPusU29GBo8zc3AFVdV5vcVB3T8IYfNR7vxdzQm271Kcn2C4OqrK/lTlCZfOWhhRJG9scEeDTETf8K2kVz3Lmf60hUYnvl1DEFd9LYYFSota6dIgtSMuygInnrEvEDK2XAqzVVIKsfJbbBdwj+RpOCcv0O1Xg2tc4TnbxaWJluEcswCZv4Wi+b1OdBMwCG5+uTc6jpyOBaCpqXjOgvcLjVMyS/tpR7h7HbpJ4XjErNOG6dbMroayeC2Vrs8h8E5vIeWavxpI5uwocshnkEF/kQV+XmWZFgRwcLAsekj0gTXb5nf79MAtNSDlutMwfRqm7dXk4FhgmlXhEEGE6wWhOdZY93If/AECt3qUImjvFCyh1eTNMoS8V2oxwG0j2ma+9yyEjx3klmjFnQt58Ym0j4c7J7WTkjwQpbPrHAtfMCXLaFROqEBFXK2rZj5R9gUJb19AlBwwhp5TfJzWv4M1aDKV2FdIIGWc+h1Zab7mQjQSixd79gmD1q9XecYNzYWFTH6cmLCA0Zty57NVXflelOUBj1K3A4SKsSpyuOZDWmEdZdYl4FDQox6W4aLwceCEmbCISnuZ8beG5TgWU0YC96LMBaqyMxr4j4hTbHiYrwU+1OrQTG31jOAjgAJRbvYKZUa0dk1OMQkb1xVWPU0CDjZeXTS0eDQjVAe3KI53//7hibOtv6J0/SqJqwuZpSK6ESVmwUTJxKeTDBP3ufdUTSwP8vauX+n6qJ0kf29O7wc+EhPEj+qDyg2bd2gu/WeSYUEL2egGYG2FEXA9MIQDgYkLDRPCAab+O+3dO0eCagUF+AvAFwqQG6PRsilPqmMRyzXUNStyzc0OMGLEO0bkTxZlu+aoZ9HoUBlgkuUzwcaMp9yJL4hJdeDPZ/kE3mDe73SuNgJo4aL5+YdCdqpckEHvcVUn4b7gHASx1Q02boUV2KIRRlqx4z/1B4L3YZBqqJ/jjOlLg+XAJDkm7peJn9QSuS+8xFI+zD3hSK8v+6G3zoCN/xQjy/41CGLs2lekYXVA6YuQ9abVZSLDgBa0o/uyq3L2nG0ydaIlCce/QZ/pJfOqhjaVeJoX/gh6fG0qZgXHKdy5JUFvO/CF/XvH0wPYU2VfF7WAYCN5O2rp/FXJZnLkWFxIlI7rOXlmVp3DjYu8ql7GN5rHpSxTSg1TWq3b6Kg5gxSisAb6/gS9aYbER08kvcawBOYVRQrw1RI939hqylbCkHNVeK/9p6oDWrgLOJJk7lZWpEVoRkW5ED94uMacG+g55yyTIkFUOIDJFKCublDdBpayUxKRh21QRnROKLYrlWXWLX9FRa5lXTxKLpdeH2ix4FGPt5ZMTS+versLjgM33y1tqWvYWhFK9/utbyG9/C+DUds7KOT7uKIjIsjyZKe53zo4Hm+pha1dWFc2kLcKlONlfsgq/y2TnmiqQVEbaABjFiAK7NyfhgARTnprKtCmKTZLYoXc8t+7t2CkuxKSvLwF6/U7wky1xnTGLcWaNwMkGzKEAMUPTSTMSFYSS656q7dIxdIp0BxzKzbUKRp4Zi8RR0dgV/5vTLdmXWIKzIo2of2BDt+nlAHJpXC88cHkOYN4O3CeObXlDHmxqFXGSVJ8KJajS4Re5O7iUOySbtsbZ5Re8LzcsNqdDLF4DbK9BJg1PhnH1N60dfcvtlBkFwgqF0uNxNkM6xAWwaWEvWhGaIJAMV5lU5N2pYXyGJfKaBTjwMzLv5zUUhAjiaoEUQsKlZz2O0HBBc+UHdxl89BJMa9kSXqWJ83UrYPWQudaaAfe1zdOrZ1ORlXiA5LjHIgEuw9pVdomYEnvw9jvXOX30YJFPJi0QNfpFBqtbxXqV3AXXmxa/gKPuvWXi9E/aKOrDnjndgco+LgHle14IDmjeWC4hKWkEeER7CIC8kh6spACRAfJv/dnFj6k6+vVRNAAAKutrrwW9szxfVVA/YN71T3nkvq6oHGRchiqYpwdJPv75bJ2s7tdLVNwnrTDvNm+wu18vGWpLjzLNzmrSUvxhCOlVLT70gcTp5FxeaAVeiLFYt62m579YXTnQ4wqzhjibjF2fCW7SfSt8UNQmBp3/6uks6Xd/zFyXl/dVjqhXEcSqnxQGCgl1RsaPQbHRCU+TS+zkNNz5RPA9b3oc9thfKqJ4IIoGvE+p/p6o4MlDp/EoaecQsLRbaN5bn+/Ue0Rj5ifnkfY8LGTzBoaKe5fKZvZVEc6VDh+rbDkRonNJH7YaZ+1o1K0mjAvSX/k1uY97D4Qk1iD8byhO97vSXgsVCdJSFYetfrIsPHG8hSG4ygpL1Aywz8KkFERJb0ZTrN0FzF6AQGdbyrzTlT/v7G5kdGUPUP+oVLhsmfTS9HtF/x+DFvcWU4suVjqc3483vLgx9/zpuIEZasLYkp+WO5ZmE0uA58Vr2Obr4r1BJBF2n+NKgQJa31cOC/OuV85cYCxhiaar/TgRQ44yiuXpLvga8HvJYu4auNNOLhYlHt2AbIfgfdeWGsJgoy8nRZ/pdCp8xiYvZBABMsU6NLu33y4ZDDGBNEeLj1NYA1C6EUWHjvpjeXuzq8o1lE/BXkRs/K4YXBcrZUpqF9JwpRsYRKmnTDP85imWZO3nx8M8+au4jMmwx8lKRqCfNPK4IsNYYzrMuq+2RSu3MyBmtOImyyd7ywLVZkbvpOrF4WzD1U2osgy425YfufIevJk7T/5gCY27lhMiS2xsRZ41gAfhxxV+JQ0XXQhTsoFEU34BdeJ5B9YsbBaEIGksN+JTgipbZGvjVXj+hEQeCsA/Fm+liEGjYJOJ4HnWJdylNr205n2SV9EodAQDQ/sBMJ7SF/b3ev38dcZUJ+6j3xyZsoUrSCB04GGSeCC52I0E4vF3ZMGCAKziwGFYjrM0AFcyl+1hRko9LkIf7mZblRamQGYtFMceehAH2cMDpisLlI3X1PVSOuMIhvCGe5FM+anXBjaqKN81Ei/GN0Nvh9xrMv9xMSWUwnQsm1rOJi5Q9VQ5NRAaSpyncMKbsPD/VPFPx6QEs9MFORy2AO0Xg4WR47ANXTEIUcb1P9NV7VPFrV9jeNSbD1yNXpEE4mwz1Wo8pHVg9RtbAhYgbaP+D74+1xOlZ203xdrG7qqybMJ9ac3rZDiepdJ38Lsr9AjybbHAVa/m/DW2Jr92ki9rnA8mXnr5qgIr+k+d7vt0hMQWQG2AC7pdnvn+N3HZKK+UT1zfGjmB1g8Jyyb+MQRD6mePhtXrz3g8wE3e9znN5raZYfNtzRN0OP5n9s44lYcL+3QPklPaTqDCWgZfZLo50IwC6STH+YtpUiqqGuYIBy4fftUawjFYSQLlFRM/y1NP1uUX9K0aRotZ2Oj33AvdAdSViGshCwuJCc4Oztdia21WlYmfB511ZHpB6JFtAGJgZCIkTrgIlRHDDVS/n2HBuBS8DetQU7pn1xjYdcCVIHwQ1u4byz6LFB3KBcA8hnIAJqgXKmFZA05lhHmPsfI5EgILbNIk0lNE6Xy2QDQ+QsmSaWvu6JQqMKbaaj/rg9wVcLAHfrp4PjlMIGAmZaSGWCb9guEWtVL9rE8e9MghAzdq7VfbVLUp9Mqxo4UV0q83EqIZiQlduE0ABusjYr6+WW94j897HuzQK/NXQSALZKacKmQ8dGpnD4xEd8RjyZvZsPsxrsVhcjXHCjCpnDLxSerz4e82Z7uzzArbYQkPV82Ttzr8HAatEFEXIsSFaD1EaasiXpo51TrvWGwkTaxPt6YB4ygrPXKEjjnYdcELA0MdqWnjienDIuDcukyF7lyIZyTzj2jAwywoo/KWNbZV/TNeleDvd+0a0m893r095jaM/rXkc13JtHVtQpRecwYS3B1BjhhI7AHd7iNPrWoYj+MoK/xnDiLZfExRVoGq9B0EHa8EBbxF0PRadS5wWv2UIwKtmGsRKqfD2zuanlxwpNNzq/q2A9WgToqea8iEOUDlt4IA0ozt3TpA7UIIP7NNttC3JEo/OXJ/fntbhfeMzU2xEfNSULXpv3TQZwg9g5oatCuSTe5y2omUQb7p0w+JAlOwSa5P1RgLmLmB2IwlR0A7Y5gc/TlFNyR0X11Q5i+RaMPOXH/rcrmBeDCv+fmzq2rABBZp76tuuEaxT1FOi6GFj+6NOdsR4dVE6Fp/Ty+1rhwaCF6eNTUvrQget3wubwDYviZyTZ1I34/qdXihY5QhGUE1iK23V99xXPeKl+EYFJ/5a1kdPtIoF2fy10k8/apNg313i/oVXTl+rlS1vUZH48zG3XZSLQ7GzaX49/e5lHJjHDaS9ysrfOyFx5ft3cOPhKLY/Qva9Mh4i2H/xb6BuoyUW+aybhORlXvoPGYskNrB958as0YXGlsLvVKhRw2wc7dDCR9ayQtYGHutSaOCXlfGql5q2i/MZ+rxP4CePGU3r23KTKMCi4iahdbKkqEcrqWOFBDlveVP9EQDN/kOjCL1Wr61OO8zA0emzfNZE9ACPTLDzxY/8hrwVMZEDUO/EtubqjuXwoyuczni9zCXGUrm+qvUPO6FpZBzhzscRBKYJG/uAO4ljPEojV1AWYumzJOhHnGnDbLEGWWH28A9SQRm9di326NBChTDQ6CgwhQOBwRVfT3bgtgTlAsrPuLRDm7Jk988Kqfq8wdjWvUMUuLVWJ/6ukknjrdgkXCUoX5zL/BNP0sNHd6UeAVCf6QuOkLJUCI4NTSUlH2FKseWUybmiclHiu8ikriqIkzDj05IPe5hZpCnVA2nW/S238k2yWY0N3I4jdXUlwkNxLmryqqg+WevCrCJjCobfENaFD3c1EUBxvV63iKzwk7p0ZZMLOKKsA2moU2CsoPdKLJG52zBJ0lVhLaEXFKN7tPL1CjyJxRG7Io8yk8WsUKVocqzl24bDJrOzneMbQNuI2fY5DATubrWsNe6fe5NUL+Ufxn+yWYTkyOGqXapr1gUPRDTb9L8YYCM94loKIzXhPUrzmck9uH6YqYO3oYKbpwpaDm2v46F/Ucj2dVZgSzOJ3NSkCPt6SREL1C7W0o3BhhkzvjpNUqiAWCH+9xcEsnCYXE2PbftvoZ9LF5rM7OGcI3r1U2mNb2YVGUk2QWsHW2ZB8UBDUZNKp+tWAl3H9puA3m2732fGedkn/DwSYKLnDT6PZ30ExUJ4FvtEi3lvnwTgWWNt7VfMmDlzeCp6LxDwpZQnJ6Zs73lMD6rJVzVs2b1b723j9/cS8pyeEoDlTwMlu98MPxtzYonTp9fY9wLUNjvR8FpGtncEMFQCLEyaSDURYCMjfl3047TpBX+lxl09m82fKo/UbkMQyKiBRciJjeTtFMdKwJvB9sY+qFTAFzS2DTqS12Tuu+nP6OthQRN7Jk1PlavpURESVn8SWN1lR+1MHr52oEJJxhMDRRDtYnXYqvGkJKuLPBeG4dgP3+lGy/9QF7n+q8HhSuViqkl4Rta7EZ2q9tf2rVqdvrP8W6HWnPsjNOD0C1sYgNHrBtYZ9ZspdGwPZqi5bAOpMx5crR53S9R8cFt1o9tZrQNnHrg1Q/AdDywC2HBY8cMxMZ1kvGlNcgKgRKnCVmG9NaVKCm/bCSWqfG68MorlmKeNopIlYQ75ufK4vzqhADn/4tKrGvPXqn8uMKt2CfadC+TLjHs/c8RdpdRS11gPrs1GPC9KDRV9/5V10wQXRKsbw/YJ9gEmHbZ+SacGjgI5/cMjeIhGBk=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['ctl00'];
if (!theForm) {
    theForm = document.ctl00;
}
console.log('asdf');
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="90059987" />
</div>
  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        
          <a id="LbInd" class="active" href="javascript:__doPostBack(&#39;LbInd&#39;,&#39;&#39;)">ID</a> | <a id="LbEnd" href="javascript:__doPostBack(&#39;LbEnd&#39;,&#39;&#39;)">EN</a>
      </div>
      <div class="contact-info float-right">
      	<a href="contacts.aspx"><i class="bi bi-envelope"></i></a>
      	<a href="#search"><i class="bi bi-search"></i></a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
        <a href="Index.aspx"><img src="images/Logo_Pertamina_PIS.svg" alt="" class="img-fluid"></a>
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
      	<a href="" id="nav-right-option" class="nav-right-option float-right mobile-nav-toggle d-lg-none"><i class="bi bi-x"></i></a>
        <ul>
          
            
                    <li class='active'>
                        <a href='/'>Beranda</a>
                        <ul>
                            
                        </ul>
                    </li>
                
                    <li class='drop-down'>
                        <a href=''>Tentang PIS</a>
                        <ul>
                            
                                    <li class=''>
                                        <a href='VisidanMisi.aspx'>Visi dan Misi</a>
                                        <ul>
                                        
                                            </ul>
                                    </li>
                                
                                    <li class=''>
                                        <a href='Tata_Nilai_dan_Budaya.aspx'>Tata Nilai</a>
                                        <ul>
                                        
                                            </ul>
                                    </li>
                                
                                    <li class='drop-down'>
                                        <a href=''>Profil Perusahaan</a>
                                        <ul>
                                        
                                                <li>
                                                    <a href='Tentang_PIS.aspx'>Sekilas Tentang PIS</a>
                                                </li>
                                            
                                                <li>
                                                    <a href='makna-logo-pis.aspx'>Makna Logo PIS</a>
                                                </li>
                                            
                                                <li>
                                                    <a href='TonggakSejarah.aspx'>Tonggak Sejarah</a>
                                                </li>
                                            
                                            </ul>
                                    </li>
                                
                                    <li class='drop-down'>
                                        <a href=''>Manajemen</a>
                                        <ul>
                                        
                                                <li>
                                                    <a href='Dewan_Komisaris.aspx'>Dewan Komisaris</a>
                                                </li>
                                            
                                                <li>
                                                    <a href='Direksi.aspx'>Dewan Direksi</a>
                                                </li>
                                            
                                                <li>
                                                    <a href='sambutan-direktur-utama.aspx'>Sambutan Direksi</a>
                                                </li>
                                            
                                                <li>
                                                    <a href='Struktur Organisasi.aspx'>Struktur Organisasi & <br> Kelompok Perusahaan</a>
                                                </li>
                                            
                                            </ul>
                                    </li>
                                
                                    <li class=''>
                                        <a href='Standard%20International.aspx'>Standar Internasional</a>
                                        <ul>
                                        
                                            </ul>
                                    </li>
                                
                        </ul>
                    </li>
                
                    <li class='drop-down'>
                        <a href=''>Aktivitas Bisnis</a>
                        <ul>
                            
                                    <li class=''>
                                        <a href='layanan-kami.aspx'>Layanan Kami</a>
                                        <ul>
                                        
                                            </ul>
                                    </li>
                                
                                    <li class=''>
                                        <a href='Armada-Kami.aspx'>Armada Kami</a>
                                        <ul>
                                        
                                            </ul>
                                    </li>
                                
                        </ul>
                    </li>
                
                    <li class='drop-down'>
                        <a href=''>Media & Informasi</a>
                        <ul>
                            
                                    <li class=''>
                                        <a href='AllNews.aspx'>Berita</a>
                                        <ul>
                                        
                                            </ul>
                                    </li>
                                
                                    <li class=''>
                                        <a href='AllGallery.aspx'>Galeri</a>
                                        <ul>
                                        
                                            </ul>
                                    </li>
                                
                                    <li class=''>
                                        <a href='Persentasi.aspx'>Presentasi</a>
                                        <ul>
                                        
                                            </ul>
                                    </li>
                                
                        </ul>
                    </li>
                
            
                                <li class='drop-down d-lg-none'>
                                    <a href=''>Hubungan Investor</a>
                                    <ul>
                                        
                                                <li class=''>
                                                    <a href='laporan-tahunan.aspx'>Laporan Tahunan</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                                <li class=''>
                                                    <a href='laporan-ikhtisar-keuangan.aspx'>Laporan & Ikhtisar Keuangan</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                                <li class='drop-down'>
                                                    <a href=''>Informasi Saham</a>
                                                    <ul>
                                                        
                                                                <li>
                                                                    <a href='komposisi-pemegang-saham.aspx'>Komposisi Pemegang Saham</a>
                                                                </li>
                                                            
                                                                <li>
                                                                    <a href='informasi-dividen.aspx'>Informasi Dividen</a>
                                                                </li>
                                                            
                                                    </ul>
                                                    
                                                </li>
                                            
                                                <li class=''>
                                                    <a href='RUPS.aspx'>RUPS</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                    </ul>
                                </li>
                            
                                <li class='drop-down d-lg-none'>
                                    <a href=''>Komitmen & Keberlanjutan</a>
                                    <ul>
                                        
                                                <li class=''>
                                                    <a href='tata-kelola-perusahaan.aspx'>Tata Kelola Perusahaan</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                                <li class=''>
                                                    <a href='penghargaan.aspx'>Penghargaan</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                                <li class=''>
                                                    <a href='keberlanjutan.aspx'>Keberlanjutan</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                    </ul>
                                </li>
                            
                                <li class='drop-down d-lg-none'>
                                    <a href=''>Pengadaan</a>
                                    <ul>
                                        
                                                <li class=''>
                                                    <a href='informasi-pengadaan.aspx'>Informasi Pengadaan</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                                <li class=''>
                                                    <a href='https://smart.gep.com/publicrfx/pertamina?oloc=215#/'>Pengadaan Umum & e-Procurement</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                    </ul>
                                </li>
                            
                                <li class=' d-lg-none'>
                                    <a href='career.aspx'>Karier</a>
                                    <ul>
                                        
                                    </ul>
                                </li>
                            
                                <li class=' d-lg-none'>
                                    <a href='contacts.aspx'>Kontak</a>
                                    <ul>
                                        
                                    </ul>
                                </li>
                            
		      

          <li id="othermenu" class="nav-right-option d-none d-lg-block"><a href=""  onclick="openNavOverlay()"  id="nav-right-option"  >Menu Lainnya <span class="" ></span></a></li>
        </ul>
        <div id="langMobile" class="text-center lang-mobile d-lg-none"><span href=""  class="active"  >ID</span> | <span href=""  onclick="changeLang()"  >EN</span></div>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- Nav Overlay -->
  <div id="navOverlay" class="nav-overlay d-none d-lg-block">
  	<div class="bg-black-transparent">
	  	<div class="container">
		    <div class="nav-overlay-content">
		    	
                <ul>
                        
                                <li class='drop-down'>
                                    <a href=''>Hubungan Investor</a>
                                    <ul>
                                        
                                                <li class=''>
                                                    <a href='laporan-tahunan.aspx'>Laporan Tahunan</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                                <li class=''>
                                                    <a href='laporan-ikhtisar-keuangan.aspx'>Laporan & Ikhtisar Keuangan</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                                <li class='drop-down'>
                                                    <a href=''>Informasi Saham</a>
                                                    <ul>
                                                        
                                                                <li>
                                                                    <a href='komposisi-pemegang-saham.aspx'>Komposisi Pemegang Saham</a>
                                                                </li>
                                                            
                                                                <li>
                                                                    <a href='informasi-dividen.aspx'>Informasi Dividen</a>
                                                                </li>
                                                            
                                                    </ul>
                                                    
                                                </li>
                                            
                                                <li class=''>
                                                    <a href='RUPS.aspx'>RUPS</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                    </ul>
                                </li>
                            
                                <li class='drop-down'>
                                    <a href=''>Komitmen & Keberlanjutan</a>
                                    <ul>
                                        
                                                <li class=''>
                                                    <a href='tata-kelola-perusahaan.aspx'>Tata Kelola Perusahaan</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                                <li class=''>
                                                    <a href='penghargaan.aspx'>Penghargaan</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                                <li class=''>
                                                    <a href='keberlanjutan.aspx'>Keberlanjutan</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                    </ul>
                                </li>
                            
                                <li class='drop-down'>
                                    <a href=''>Pengadaan</a>
                                    <ul>
                                        
                                                <li class=''>
                                                    <a href='informasi-pengadaan.aspx'>Informasi Pengadaan</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                                <li class=''>
                                                    <a href='https://smart.gep.com/publicrfx/pertamina?oloc=215#/'>Pengadaan Umum & e-Procurement</a>
                                                    <ul>
                                                        
                                                    </ul>
                                                    
                                                </li>
                                            
                                    </ul>
                                </li>
                            
                                <li class=''>
                                    <a href='career.aspx'>Karier</a>
                                    <ul>
                                        
                                    </ul>
                                </li>
                            
                                <li class=''>
                                    <a href='contacts.aspx'>Kontak</a>
                                    <ul>
                                        
                                    </ul>
                                </li>
                            
                        
		    		
		    	</ul>
		    </div>
	  	</div>
  	</div>
  </div><!-- End Nav Overlay -->

  <!-- Search -->
  <div id="search">
  	<div class="bg-overlay">
	    <button type="button" class="close"><i class="bi bi-x close"></i></button>
	    <form action="pencarian.html">
	    <input name="txtSearch" type="search" id="txtSearch" autocomplete="off" placeholder="Ketik Kata Kunci disini" />
            <input type="submit" name="BtnSearch" value="Cari" id="BtnSearch" class="btn bg-red" />
	    </form>
  	</div>
	</div><!-- End Search -->



  <main id="main">
      
	  <!-- ======= Hero Section ======= -->
	  <section id="hero">
	    <div class="hero-container">
	      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

	        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

	        <div class="carousel-inner" role="listbox">
                
                        <!-- Slide 1 -->
	                  <div class="carousel-item active" >
                        <img src='Banner/obtotbPertamina-PIS-headline-1.jpg' alt="" width="100%">
	                    <div class="carousel-container">
	                      <div class="carousel-content container">
	                        <h2 class="animated fadeInDown">Pertamina PRIDE & PRIME</h2>
	                        <p class="animated fadeInUp">Kebanggaan Anak Bangsa Persembahan Pertamina International Shipping</p>
	                        <a href='layanan-kami.aspx' class="btn-get-started animated fadeInUp scrollto">Layanan Kami <i class="bi bi-chevron-right"></i></a>
	                      </div>
	                    </div>
	                  </div>
                    
                        <!-- Slide 1 -->
	                  <div class="carousel-item " >
                        <img src='Banner/nqnudgPertamina-PIS-headline-3.jpg' alt="" width="100%">
	                    <div class="carousel-container">
	                      <div class="carousel-content container">
	                        <h2 class="animated fadeInDown">Integrated Marine Logistics Company</h2>
	                        <p class="animated fadeInUp">Terkemuka di Asia</p>
	                        <a href='contacts.aspx' class="btn-get-started animated fadeInUp scrollto">Kontak Kami <i class="bi bi-chevron-right"></i></a>
	                      </div>
	                    </div>
	                  </div>
                    
                        <!-- Slide 1 -->
	                  <div class="carousel-item " >
                        <img src='Banner/grvpnrPertamina-PIS-headline-2.jpg' alt="" width="100%">
	                    <div class="carousel-container">
	                      <div class="carousel-content container">
	                        <h2 class="animated fadeInDown">Hantarkan Energi ke Penjuru Dunia, Bangga Berikan Energi pada Bangsa</h2>
	                        <p class="animated fadeInUp">#DeliveringPromises</p>
	                        <a href='Tentang_PIS.aspx' class="btn-get-started animated fadeInUp scrollto">Selengkapnya Tentang PIS <i class="bi bi-chevron-right"></i></a>
	                      </div>
	                    </div>
	                  </div>
                    
	          

	          

	        </div>

	        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
	          <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"><i class="bi bi-chevron-left"></i></span>
	          <span class="sr-only">Previous</span>
	        </a>
	        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
	          <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"><i class="bi bi-chevron-right"></i></span>
	          <span class="sr-only">Next</span>
	        </a>

	      </div>
	    </div>
	  </section><!-- End Hero -->

    <!-- ======= Service Section ======= -->
    <section id="service" class="service">
      <div class="container">

        <div class="row">
          <div class="col-lg-12 text-center mb-5">
          	<h2>Layanan Kami</h2>
          </div>
            
                    <div class="col-lg-4 col-12 mb-4">
    	  		        <div class="col pl-0 pr-0">
		  	    	        <div class="card bg-dark text-white overflow-hidden service-box card-block">
		  	    		        <a href="layanan-kami.aspx">
									        <img src='Layanan/Layanan-Kami1-Pertamina-PIS.jpeg' class="card-img" alt="...">
									        <div class="card-img-overlay align-self-center text-center bg-gradient-1">
										        <p class="card-title my-auto">Jasa Pengangkutan Laut Antar pelabuhan</p>
									        </div>
		  	    		        </a>
				            </div>
    	  		        </div>
	    	          </div>
                
                    <div class="col-lg-4 col-12 mb-4">
    	  		        <div class="col pl-0 pr-0">
		  	    	        <div class="card bg-dark text-white overflow-hidden service-box card-block">
		  	    		        <a href="layanan-kami.aspx">
									        <img src='Layanan/Layanan-Kami2-Pertamina-PIS.jpeg' class="card-img" alt="...">
									        <div class="card-img-overlay align-self-center text-center bg-gradient-2">
										        <p class="card-title my-auto">Jasa Pengangkutan Kargo Ekspor/Impor</p>
									        </div>
		  	    		        </a>
				            </div>
    	  		        </div>
	    	          </div>
                
                    <div class="col-lg-4 col-12 mb-4">
    	  		        <div class="col pl-0 pr-0">
		  	    	        <div class="card bg-dark text-white overflow-hidden service-box card-block">
		  	    		        <a href="layanan-kami.aspx">
									        <img src='Layanan/Layanan-Kami3-Pertamina-PIS.jpeg' class="card-img" alt="...">
									        <div class="card-img-overlay align-self-center text-center bg-gradient-3">
										        <p class="card-title my-auto">Jasa Penyewaan Alat-Alat Pelayaran</p>
									        </div>
		  	    		        </a>
				            </div>
    	  		        </div>
	    	          </div>
                
        </div>

      </div>
    </section><!-- End Service Section -->

    <!-- ======= Media Information Section ======= -->
    <section class="media-information section-bg">
      <div class="container">
				<div class="row no-gutters">
          <div class="col-lg-12 mb-3 media-information-top">
          	<div class="row">
          		<div class="col-lg-7 col-12">
          			<h2>Media & Informasi</h2>
          		</div>
          		<div class="col-lg-5 col-12 text-right media-information-source">
          			<div class="row">
          				<!--
          				<div class="col-lg-4 col-4 d-none d-lg-block"></div>
          				<div class="col-lg-4 col-6 media-information-option-left">
          					<a href="" class="active">Berita</a>
          				</div>
          				<div class="col-lg-4 col-6 media-information-option-right">
          					<a href="">Galeri</a>
          				</div>
          				-->
          				<div class="col-lg-6 col-4 d-none d-lg-block"></div>
          				<div class="col-lg-3 col-6 media-information-option-center">
          					<a href="" class="active" id="news">Berita</a>
          				</div>
          				<div class="col-lg-3 col-6 media-information-option-center">
          					<a href="" id="gallery">Galeri</a>
          				</div>
          			</div>
          		</div>
          	</div>
          </div>
          <div id="homenews" class="col-lg-12 col-12">
          	<div class="row">
                  
                          <div class="col-lg-7 col-12 mb-4 media-information-box-left">
			    	  	    <div class="col pl-0 pr-0">
				  	    	    <div class="card text-white overflow-hidden media-information-box card-block">
          					    <a href="news-detail.aspx?title=go-digital-go-productive-dan-efficient-pertamina-international-shipping-resmi-luncurkan-website-perkuat-brand-awareness-dan-public-trust">
          						    <div class="box-image">
												    <img src='Beritaa/awwfteUM-050PIS01102021S9---Go-Global,-Go-Produktif-&-Efisien,-Pertamina-International-Shipping-Resmi-Luncurkan-Website-Perkuat-Brand-Awareness-dan-Public_4.jpg' class="card-img" alt="" height="100%" width="100%">
          						    </div>
											    <div class="card-img-overlay bg-black-transparent">
												    <p class="card-content p-4 ">
													    <em>19 Oct 2021</em> &nbsp;<span class="badge badge-danger">SIARAN PERS</span>
													    <br>Go Digital, Go Productive dan Efficient, Pertamina International Shipping Resmi Luncurkan Website Perkuat Brand Awareness dan Public Trust
												    </p>
											    </div>
			    	  			    </a>
									    </div>
								    </div>
			    	      </div>
                      
        			
			    	  <div class="col-lg-5 col-12 mb-4">
			    	  	<div class="row">
                              
                                      <div class="col-12 pl-0 pr-0 mb-3 media-information-box-right">
					  	    	        <div class="card text-white overflow-hidden media-information-box card-block">
					  	    		        <a href="news-detail.aspx?title=town-hall-meeting-thm-pertamina-international-shipping-pola-bisnis-harus-berubah-menjadi-profit-center">
												        <img src='Beritaa/mssfoeUM 049PIS01102021S9 - Town Hall Meeting (THM) Pertamina International Shipping, Pola Bisnis Harus Berubah Menjadi Profit Center.jpg' class="card-img" alt="..." height="100%" width="100%">
												        <div class="card-img-overlay bg-black-transparent">
													        <p class="card-content pl-4 pb-1">
														        <em>11 Oct 2021</em> &nbsp;<span class="badge badge-danger">SIARAN PERS</span>
														        <br>Town Hall Meeting (THM) Pertamina International Shipping, Pola Bisnis Harus Berubah Menjadi Profit Center
													        </p>
												        </div>
											        </a>
										        </div>
									        </div>
                                  
				    	  	
                              
                                      <div class="col-12 pl-0 pr-0 mb-4 media-information-box-right">
					  	    	        <div class="card text-white overflow-hidden media-information-box card-block">
					  	    		        <a href="news-detail.aspx?title=pertamina-international-shipping-pis-ambil-alih-saham-peteka-karya-tirta-pkt-restrukturisasi-pis-menjadi-subholding-integrated-marine-logistics">
												        <img src='Beritaa/bgpwxePertamina Ambil Alih Saham Peteka Karya Tirta, Dukung Restrukturisasi PIS Menjadi Subholding Integrated Marine & Logistics.jpeg' class="card-img" alt="..." height="100%" width="100%">
												        <div class="card-img-overlay bg-black-transparent">
													        <p class="card-content pl-4 pb-1">
														        <em>07 Oct 2021</em> &nbsp;<span class="badge badge-danger">SIARAN PERS</span>
														        <br>PT Pertamina International Shipping (PIS) Ambil Alih Saham PT Peteka Karya Tirta (PKT), Dukung Restrukturisasi PIS Menjadi Subholding Integrated Marine & Logistics
													        </p>
												        </div>
											        </a>
										        </div>
									        </div>
                                  
									
			    	  	</div>
			    	  </div>
          	</div>
          </div>
          <div class="col-lg-12 col-12 d-none" id="homegallery">
          	<div class="row mt-1">
					    <div id="glr" class="col-12 mt-3 gallery-carousel owl-carousel owl-theme"><div class="card" ><a  href="https://www.youtube.com/watch?v=gA1Y0ZBShuA"  class="popup-youtube"   ><img class="card-img-top"  src="Gallerry/video web pis pertamina.jpg"  alt="Card image cap" ><div class="card-body" ><p class="mb-0" ><em>07 Oct 2021</em> &nbsp;<span class="badge badge-danger" >VIDEO</span></p><p><strong>Peluncuran Website Pertamina International Shipping (PIS)</strong></p></div></a></div><div class="card" ><a  href="https://www.youtube.com/watch?v=_8nXVpx0Bqw"  class="popup-youtube"   ><img class="card-img-top"  src="Gallerry/Video-PIS-Global.jpg"  alt="Card image cap" ><div class="card-body" ><p class="mb-0" ><em>26 Aug 2021</em> &nbsp;<span class="badge badge-danger" >VIDEO</span></p><p><strong>Pertamina International Shipping Going Global</strong></p></div></a></div><div class="card" ><a  href="https://www.youtube.com/watch?v=5O0Ob9GGamA"  class="popup-youtube"   ><img class="card-img-top"  src="Gallerry/Video-Pertamina-Global.jpg"  alt="Card image cap" ><div class="card-body" ><p class="mb-0" ><em>25 Aug 2021</em> &nbsp;<span class="badge badge-danger" >VIDEO</span></p><p><strong>Pertamina Go Global</strong></p></div></a></div><div class="card" ><a  href="Gallerry/pertamina-go-global.jpg"  class="image-popup-vertical-fits"  title="Pertamina Go Global PIS, Taklukkan Perairan Internasional"  ><img class="card-img-top"  src="Gallerry/pertamina-go-global.jpg"  alt="Card image cap" ><div class="card-body" ><p class="mb-0" ><em>25 Aug 2021</em> &nbsp;<span class="badge badge-danger" >INFOGRAFIS</span></p><p><strong>Pertamina Go Global PIS, Taklukkan Perairan Internasional</strong></p></div></a></div><div class="card" ><a  href="https://www.youtube.com/watch?v=-8NmoMQQTVc"  class="popup-youtube"   ><img class="card-img-top"  src="Gallerry/video-pis-minas.jpg"  alt="Card image cap" ><div class="card-body" ><p class="mb-0" ><em>19 Aug 2021</em> &nbsp;<span class="badge badge-danger" >VIDEO</span></p><p><strong>MT Minas Sigap Mencegah COVID-19</strong></p></div></a></div><div class="card" ><a  href="https://www.youtube.com/watch?v=aQyu_K4oIiA"  class="popup-youtube"   ><img class="card-img-top"  src="Gallerry/video-kedaulatan-merdeka-bumn.jpg"  alt="Card image cap" ><div class="card-body" ><p class="mb-0" ><em>17 Aug 2021</em> &nbsp;<span class="badge badge-danger" >VIDEO</span></p><p><strong>Kedaulatan Energi BUMN</strong></p></div></a></div></div>
					  </div>
          </div>

        </div>
      </div>
    </section>
      <!-- Media Information Section -->

    <!-- ======= Video PIS Section ======= -->
    <section class="video-profile">
      <div class="container">

        <div class="row">
	    	 
                    <div class="col-lg-6 col-12 mb-4 d-flex">
    	  		        <div class="col pl-0 pr-0 video-profile-info">
		  	    	        <h3 class="mb-4">Company Profile Video - PT Pertamina International Shipping (PIS)</h3>
		  	    	        <p><p>Asia’s leading shipping company, championing the economic development of Indonesia.</p><p>#IntegratedMarineLogistics #IntegratedMarineLogisticsCompany</p></p>
    	  		        </div>
	    	      </div>
                    <div class="col-lg-6 col-12 mb-4 image-video align-self-center">
	    	  	        <a href='https://www.youtube.com/watch?v=ms_v9qpcnqw' class="popup-youtube">
                          <img src='https://pertamina-pis.com/documents/pertamina-pis-profile.jpeg' class="rounded" alt="Responsive image">
                          <center>
                              <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
                          </center>
	                    </a>
	    	      </div>
                
        </div>

      </div>
    </section><!-- End Video PIS Section -->

    <!-- ======= Armada Section ======= -->
    <section class="armada section-bg">
      <div class="container">
				<div class="row no-gutters">
					<div class="col-12 mb-3">
						<div class="row">
							<div class="armada-top-box col-12 bg-blue p-4">
								<div class="row">
									<div class="col-xs-3 col-sm-6 text-center">
										<h2 class="mb-0">750</h2>
										<p class="mb-0">Total Kapal</p>
									</div>
									<div class="col-xs-3 col-sm-6 text-center">
										<h2 class="mb-0">11</h2>
										<p class="mb-0">Kapal Milik</p>
									</div>
								</div>
							</div>
							<div class="armada-bottom-box col-12 p-4 bg-white">
								<p id="kapaldesc" class="text-center"> 						<p><strong>Armada Kami</strong></p> 						<p>Hingga saat ini, <b>PT Pertamina International Shipping (PIS) </b>sebagai <b>Subholding Integrated Marine Logistics</b>&nbsp;telah memiliki total 750 kapal. Selain kapal milik, PIS juga mengelola <i>time charter</i> dan <i>spot charter </i>yang dapat disewa melalui <i>e-chartering.</i></p><p><b>Terdapat 11 kapal utama yang menjadi kebanggaan PIS, yaitu:</b></p>				</p>
								<h5 class="text-center mt-4">Armada Kami</h5>

								<ul class="nav nav-tabs row-eq-height mt-5 d-none" id="myTab" role="tablist">
									<li class="nav-item col-xs-5 col-sm-6 col-md-3 pl-0 pr-0">
								    <a class="nav-link active btn btn-radius-pis m-0" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
									    Kapal Milik	
									  </a>
								  </li>
								  <li class="nav-item col-xs-5 col-sm-6 col-md-3 pl-0 pr-0">
								    <a class="nav-link btn btn-radius-pis m-0" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
								    	<center><div class="rounded-circle armada-circle"></div></center>
							    		Profil Armada
								    </a>
								  </li>
								</ul>
								<div class="tab-content bg-white pt-0 pb-5 pl-4 pr-4" id="myTabContent">
								  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
								  	<div class="row">
								  		<div class="col-12 mt-3 armada-carousel owl-carousel owl-theme">
									  		
                                                      <div class="box-content">
								  				<div class="">
								  					<a href="detail-armada.aspx?search=PIS-Paragon">
									  					<div class="box-image">
									  						<img src='Kapal/uninedPertamina-PIS-Shipping-Paragon-Kapal.jpg' class="" alt="...">
								  						</div>
									  					<p class="text-left">
									  						<strong>PIS Paragon</strong>
									  					</p>
									  					<table class="table table-borderless pl-0">
									  						<tr>
								                              <td>TYPE</td>
								                              <td><strong>MEDIUM RANGE (ABT 45 K DWT)</strong></td>
								                            </tr>
								                            <tr>
								                              <td>BUILT</td>
								                              <td><strong>2010</strong></td>
								                            </tr>
								                            <tr>
								                              <td>SIZE (DWT)</td>
								                              <td><strong>45902</strong></td>
								                            </tr>
								                            <tr>
								                              <td>CAPACITY (BBLS)</td>
								                              <td><strong>320000</strong></td>
								                            </tr>
								              </table>
							              </a>
								  				</div>
								  			</div>
                                                  
                                                      <div class="box-content">
								  				<div class="">
								  					<a href="detail-armada.aspx?search=PIS-Polaris">
									  					<div class="box-image">
									  						<img src='Kapal/bfwkbvPertamina-PIS-Shipping-Polaris-Kapal.jpg' class="" alt="...">
								  						</div>
									  					<p class="text-left">
									  						<strong>PIS Polaris</strong>
									  					</p>
									  					<table class="table table-borderless pl-0">
									  						<tr>
								                              <td>TYPE</td>
								                              <td><strong>MEDIUM RANGE (ABT 45 K DWT)</strong></td>
								                            </tr>
								                            <tr>
								                              <td>BUILT</td>
								                              <td><strong>2009</strong></td>
								                            </tr>
								                            <tr>
								                              <td>SIZE (DWT)</td>
								                              <td><strong>45902</strong></td>
								                            </tr>
								                            <tr>
								                              <td>CAPACITY (BBLS)</td>
								                              <td><strong>320000</strong></td>
								                            </tr>
								              </table>
							              </a>
								  				</div>
								  			</div>
                                                  
                                                      <div class="box-content">
								  				<div class="">
								  					<a href="detail-armada.aspx?search=MT-Sungai-Gerong">
									  					<div class="box-image">
									  						<img src='Kapal/isdqsbPertamina-PIS-Shipping-Sungai Gerong-Kapal.jpg' class="" alt="...">
								  						</div>
									  					<p class="text-left">
									  						<strong>MT Sungai Gerong</strong>
									  					</p>
									  					<table class="table table-borderless pl-0">
									  						<tr>
								                              <td>TYPE</td>
								                              <td><strong>MEDIUM RANGE (ABT 30 K DWT)</strong></td>
								                            </tr>
								                            <tr>
								                              <td>BUILT</td>
								                              <td><strong>2011</strong></td>
								                            </tr>
								                            <tr>
								                              <td>SIZE (DWT)</td>
								                              <td><strong>29756</strong></td>
								                            </tr>
								                            <tr>
								                              <td>CAPACITY (BBLS)</td>
								                              <td><strong>250000</strong></td>
								                            </tr>
								              </table>
							              </a>
								  				</div>
								  			</div>
                                                  
                                                      <div class="box-content">
								  				<div class="">
								  					<a href="detail-armada.aspx?search=MT-Fastron">
									  					<div class="box-image">
									  						<img src='Kapal/pxmodoPertamina-PIS-Shipping-Fastron-Kapal.jpg' class="" alt="...">
								  						</div>
									  					<p class="text-left">
									  						<strong>MT Fastron</strong>
									  					</p>
									  					<table class="table table-borderless pl-0">
									  						<tr>
								                              <td>TYPE</td>
								                              <td><strong>MEDIUM RANGE (ABT 30 K DWT)</strong></td>
								                            </tr>
								                            <tr>
								                              <td>BUILT</td>
								                              <td><strong>2005</strong></td>
								                            </tr>
								                            <tr>
								                              <td>SIZE (DWT)</td>
								                              <td><strong>30770</strong></td>
								                            </tr>
								                            <tr>
								                              <td>CAPACITY (BBLS)</td>
								                              <td><strong>250000</strong></td>
								                            </tr>
								              </table>
							              </a>
								  				</div>
								  			</div>
                                                  
                                                      <div class="box-content">
								  				<div class="">
								  					<a href="detail-armada.aspx?search=MT-Sei-Pakning">
									  					<div class="box-image">
									  						<img src='Kapal/gthjvvPertamina-PIS-Shipping-Sei Pakning-Kapal.jpg' class="" alt="...">
								  						</div>
									  					<p class="text-left">
									  						<strong>MT Sei Pakning</strong>
									  					</p>
									  					<table class="table table-borderless pl-0">
									  						<tr>
								                              <td>TYPE</td>
								                              <td><strong>MEDIUM RANGE (ABT 30 K DWT)</strong></td>
								                            </tr>
								                            <tr>
								                              <td>BUILT</td>
								                              <td><strong>2010</strong></td>
								                            </tr>
								                            <tr>
								                              <td>SIZE (DWT)</td>
								                              <td><strong>29756</strong></td>
								                            </tr>
								                            <tr>
								                              <td>CAPACITY (BBLS)</td>
								                              <td><strong>250000</strong></td>
								                            </tr>
								              </table>
							              </a>
								  				</div>
								  			</div>
                                                  
                                                      <div class="box-content">
								  				<div class="">
								  					<a href="detail-armada.aspx?search=MT-Sambu">
									  					<div class="box-image">
									  						<img src='Kapal/fjvwcnPertamina-PIS-Shipping-Sambu-Kapal.jpg' class="" alt="...">
								  						</div>
									  					<p class="text-left">
									  						<strong>MT Sambu</strong>
									  					</p>
									  					<table class="table table-borderless pl-0">
									  						<tr>
								                              <td>TYPE</td>
								                              <td><strong>MEDIUM RANGE (ABT 30 K DWT)</strong></td>
								                            </tr>
								                            <tr>
								                              <td>BUILT</td>
								                              <td><strong>2011</strong></td>
								                            </tr>
								                            <tr>
								                              <td>SIZE (DWT)</td>
								                              <td><strong>29756</strong></td>
								                            </tr>
								                            <tr>
								                              <td>CAPACITY (BBLS)</td>
								                              <td><strong>250000</strong></td>
								                            </tr>
								              </table>
							              </a>
								  				</div>
								  			</div>
                                                  
                                                      <div class="box-content">
								  				<div class="">
								  					<a href="detail-armada.aspx?search=PIS-Patriot">
									  					<div class="box-image">
									  						<img src='Kapal/dgqfjkPertamina-PIS-Shipping-Patriot-Kapal.jpg' class="" alt="...">
								  						</div>
									  					<p class="text-left">
									  						<strong>PIS Patriot</strong>
									  					</p>
									  					<table class="table table-borderless pl-0">
									  						<tr>
								                              <td>TYPE</td>
								                              <td><strong>GENERAL PURPOSE (ABT 17 K DWT)</strong></td>
								                            </tr>
								                            <tr>
								                              <td>BUILT</td>
								                              <td><strong>2008</strong></td>
								                            </tr>
								                            <tr>
								                              <td>SIZE (DWT)</td>
								                              <td><strong>100000</strong></td>
								                            </tr>
								                            <tr>
								                              <td>CAPACITY (BBLS)</td>
								                              <td><strong>150097</strong></td>
								                            </tr>
								              </table>
							              </a>
								  				</div>
								  			</div>
                                                  
                                                      <div class="box-content">
								  				<div class="">
								  					<a href="detail-armada.aspx?search=FSO-Abherka">
									  					<div class="box-image">
									  						<img src='Kapal/becxagPertamina-PIS-Shipping-Abherka-Kapal (1).jpg' class="" alt="...">
								  						</div>
									  					<p class="text-left">
									  						<strong>FSO Abherka</strong>
									  					</p>
									  					<table class="table table-borderless pl-0">
									  						<tr>
								                              <td>TYPE</td>
								                              <td><strong>FLOATING STORAGE & OFFLOADING</strong></td>
								                            </tr>
								                            <tr>
								                              <td>BUILT</td>
								                              <td><strong>1985 (CONVERTED TO FSO 2012)</strong></td>
								                            </tr>
								                            <tr>
								                              <td>SIZE (DWT)</td>
								                              <td><strong>87202</strong></td>
								                            </tr>
								                            <tr>
								                              <td>CAPACITY (BBLS)</td>
								                              <td><strong>600000</strong></td>
								                            </tr>
								              </table>
							              </a>
								  				</div>
								  			</div>
                                                  
                                                      <div class="box-content">
								  				<div class="">
								  					<a href="detail-armada.aspx?search=MT-PIS-Pioneer">
									  					<div class="box-image">
									  						<img src='Kapal/fcjaqjPertamina-PIS-Shipping-Pioneer-Kapal.jpg' class="" alt="...">
								  						</div>
									  					<p class="text-left">
									  						<strong>MT PIS Pioneer</strong>
									  					</p>
									  					<table class="table table-borderless pl-0">
									  						<tr>
								                              <td>TYPE</td>
								                              <td><strong>VLCC</strong></td>
								                            </tr>
								                            <tr>
								                              <td>BUILT</td>
								                              <td><strong>2004</strong></td>
								                            </tr>
								                            <tr>
								                              <td>SIZE (DWT)</td>
								                              <td><strong>303240</strong></td>
								                            </tr>
								                            <tr>
								                              <td>CAPACITY (BBLS)</td>
								                              <td><strong>2000000</strong></td>
								                            </tr>
								              </table>
							              </a>
								  				</div>
								  			</div>
                                                  
                                                      <div class="box-content">
								  				<div class="">
								  					<a href="detail-armada.aspx?search=Pertamina-PRIME">
									  					<div class="box-image">
									  						<img src='Kapal/urreaqPertamina-PIS-Shipping-Prime-Kapal.jpg' class="" alt="...">
								  						</div>
									  					<p class="text-left">
									  						<strong>Pertamina PRIME</strong>
									  					</p>
									  					<table class="table table-borderless pl-0">
									  						<tr>
								                              <td>TYPE</td>
								                              <td><strong>VLCC</strong></td>
								                            </tr>
								                            <tr>
								                              <td>BUILT</td>
								                              <td><strong>2021</strong></td>
								                            </tr>
								                            <tr>
								                              <td>SIZE (DWT)</td>
								                              <td><strong>0</strong></td>
								                            </tr>
								                            <tr>
								                              <td>CAPACITY (BBLS)</td>
								                              <td><strong>2000000</strong></td>
								                            </tr>
								              </table>
							              </a>
								  				</div>
								  			</div>
                                                  
                                                      <div class="box-content">
								  				<div class="">
								  					<a href="detail-armada.aspx?search=Pertamina-PRIDE">
									  					<div class="box-image">
									  						<img src='Kapal/eoccdrkapal-pertamina-pride-pis.jpg' class="" alt="...">
								  						</div>
									  					<p class="text-left">
									  						<strong>Pertamina PRIDE</strong>
									  					</p>
									  					<table class="table table-borderless pl-0">
									  						<tr>
								                              <td>TYPE</td>
								                              <td><strong>VLCC</strong></td>
								                            </tr>
								                            <tr>
								                              <td>BUILT</td>
								                              <td><strong>2021</strong></td>
								                            </tr>
								                            <tr>
								                              <td>SIZE (DWT)</td>
								                              <td><strong>0</strong></td>
								                            </tr>
								                            <tr>
								                              <td>CAPACITY (BBLS)</td>
								                              <td><strong>2000000</strong></td>
								                            </tr>
								              </table>
							              </a>
								  				</div>
								  			</div>
                                                  
									  		

									  		

							  			</div>

							  			<div class="col-12 all-armada text-right">
												<a href="Armada-Kami.aspx" class="link_pis">Lihat Semua</a>
											</div>

								  	</div>
								  </div>
								  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
								  	<div class="row">
								  		<div class="col-12 mt-3 profil-carousel owl-carousel owl-theme">

									  		<div class="box-content">
								  				<div class="col box-pis-sm pt-3">
								  					<a class="image-popup-vertical-fit" href="images/default-photo.png">
								  						<img src="images/default-photo.png" class="img-fluid mt-2 mb-2" alt="...">
								  					</a>
								  					<p class="text-center">
								  						<strong>Armada</strong>
								  					</p>
								  					<p class="text-center">
								  						<a href="" class="red_pis">Download</a>
								  					</p>
								  				</div>
								  			</div>
								  			<div class="box-content">
								  				<div class="col box-pis-sm pt-3">
								  					<a class="image-popup-vertical-fit" href="images/default-photo.png">
								  						<img src="images/default-photo.png" class="img-fluid mt-2 mb-2" alt="...">
								  					</a>
								  					<p class="text-center">
								  						<strong>Armada</strong>
								  					</p>
								  					<p class="text-center">
								  						<a href="" class="red_pis">Download</a>
								  					</p>
								  				</div>
								  			</div>
								  			<div class="box-content">
								  				<div class="col box-pis-sm pt-3">
								  					<a class="image-popup-vertical-fit" href="images/default-photo.png">
								  						<img src="images/default-photo.png" class="img-fluid mt-2 mb-2" alt="...">
								  					</a>
								  					<p class="text-center">
								  						<strong>Armada</strong>
								  					</p>
								  					<p class="text-center">
								  						<a href="" class="red_pis">Download</a>
								  					</p>
								  				</div>
								  			</div>
								  			<div class="box-content">
								  				<div class="col box-pis-sm pt-3">
								  					<a class="image-popup-vertical-fit" href="images/default-photo.png">
								  						<img src="images/default-photo.png" class="img-fluid mt-2 mb-2" alt="...">
								  					</a>
								  					<p class="text-center">
								  						<strong>Armada</strong>
								  					</p>
								  					<p class="text-center">
								  						<a href="" class="red_pis">Download</a>
								  					</p>
								  				</div>
								  			</div>

								  		</div>

								  		<div class="col-12 all-armada text-right">
												<a href="Armada-Kami.aspx" class="link_pis">Lihat Semua</a>
											</div>

								  	</div>
								  </div>
								</div>
							</div>
						</div>

						
					</div>
        </div>
      </div>
    </section>
    <!-- Armada Section -->

    <!-- ======= Informasi Pengadaan Section ======= -->
    <section class="informasi-pengadaan">
      <div class="container">
				<div class="row no-gutters">
					<div class="col-12">
						<h3 class="text-center">Informasi Pengadaan</h3>
						<p id="pengadaandesc" class="text-pengadaan"><p style="text-align: justify; ">Pengadaan barang atau jasa di <b>PT Pertamina International Shipping (PIS) </b>selalu berupaya memperhatikan prinsip-prinsip efisiensi dan efektivitas, serta mengedepankan sifat kompetitif, transparan, adil dan wajar, akuntabel, nilai budaya, serta tata kelola perusahaan yang baik.</p><p style="text-align: justify; ">Tata cara pengadaan barang atau jasa dilakukan sesuai praktik umum melalui pelelangan, pemilihan langsung, penunjukan langsung atau pembelian langsung. PIS selanjutnya melakukan evaluasi untuk mendapatkan penawaran yang responsif, terbaik, serta dapat dipertanggungjawabkan.</p><p style="text-align: justify; "><b>Pengumuman Pelelangan:</b></p></p>
					</div>
					<div class="col-12 pl-2 pr-2">
						<div class="row">
							<div class="col-12 mt-3 pengadaan-carousel owl-carousel owl-theme">
                                
                                        <div class="box-content">
				  				            <div class="col box-pis-sm" style="top:0;left:0">
				  					            <img src='I_P/Invitation To Bid-02.jpg' class="img-fluid mt-3 mb-2" alt="...">
				  					            <p class="text-center">
				  						            <em>25 May 2021</em><br>
				  						            <strong>2 (Dua) Unit abt. 22,000 </strong>
				  					            </p>
				  					            <p class="text-center">
                                                      <a id="ListViewPengadaan_LinkButton1_0" class="red_pis" href="javascript:__doPostBack(&#39;ListViewPengadaan$ctrl0$LinkButton1&#39;,&#39;&#39;)">Download</a>
				  						            
				  					            </p>
				  				            </div>
				  			            </div>
                                    
                                        <div class="box-content">
				  				            <div class="col box-pis-sm" style="top:0;left:0">
				  					            <img src='I_P/Invitation To Bid-01.jpg' class="img-fluid mt-3 mb-2" alt="...">
				  					            <p class="text-center">
				  						            <em>25 May 2021</em><br>
				  						            <strong>2 (Dua) Unit abt. 22,000 </strong>
				  					            </p>
				  					            <p class="text-center">
                                                      <a id="ListViewPengadaan_LinkButton1_1" class="red_pis" href="javascript:__doPostBack(&#39;ListViewPengadaan$ctrl1$LinkButton1&#39;,&#39;&#39;)">Download</a>
				  						            
				  					            </p>
				  				            </div>
				  			            </div>
                                    
                                        <div class="box-content">
				  				            <div class="col box-pis-sm" style="top:0;left:0">
				  					            <img src='I_P/Invitation To Bid-03.jpg' class="img-fluid mt-3 mb-2" alt="...">
				  					            <p class="text-center">
				  						            <em>28 Oct 2020</em><br>
				  						            <strong>Sewa 1 (Satu) Unit VLCC- </strong>
				  					            </p>
				  					            <p class="text-center">
                                                      <a id="ListViewPengadaan_LinkButton1_2" class="red_pis" href="javascript:__doPostBack(&#39;ListViewPengadaan$ctrl2$LinkButton1&#39;,&#39;&#39;)">Download</a>
				  						            
				  					            </p>
				  				            </div>
				  			            </div>
                                    
                                        <div class="box-content">
				  				            <div class="col box-pis-sm" style="top:0;left:0">
				  					            <img src='I_P/Invitation To Bid-04.jpg' class="img-fluid mt-3 mb-2" alt="...">
				  					            <p class="text-center">
				  						            <em>14 Sep 2020</em><br>
				  						            <strong>Sewa 1 (Satu) Unit GP CP </strong>
				  					            </p>
				  					            <p class="text-center">
                                                      <a id="ListViewPengadaan_LinkButton1_3" class="red_pis" href="javascript:__doPostBack(&#39;ListViewPengadaan$ctrl3$LinkButton1&#39;,&#39;&#39;)">Download</a>
				  						            
				  					            </p>
				  				            </div>
				  			            </div>
                                    
							    
				  			
							</div>

							<div class="col-12 all-armada text-right">
								<a href="informasi-pengadaan.aspx" class="link_pis">Lihat Semua</a>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- ======= Career Section ======= -->
    <section class="internship career-section pb-5 pt-5" style="background-image: url(../images/Karier_Dummy_Desktop.jpg)">
		<div class="bg-overlay">
      <div class="container">
				<div class="row no-gutters">
					<div class="col-12 pb-5 pt-5">
						<div class="row row-eq-height">
							<div class="col-sm-6">
								<h5>Bekerja di PT Pertamina International Shipping (PIS)</h5>
								<h5>Shipping the Energy Worldwide, <br>Energizing the Nation with Pride.</h5>

								<p class="mt-3">Jadilah bagian dari kebanggaan Indonesia bersama PIS!</p>
							</div>
							<div class="col-sm-6 align-self-center">
								<a href="career.aspx" class="btn btn-radius-pis bg-red pl-4 pr-4 mt-3">
		              <strong>Lihat Lowongan <i class="bi bi-chevron-right"></i></strong>
		            </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</section>

    <a class="popup-modal d-none" href="#modal-sub"></a>
	  <div id="modal-sub" class="zoom-anim-dialog mfp-hide modal-sub container pb-4 pt-4">
			<a class="btn-close-modal"><span class="bi bi-x"></a>
	  	<div class="row">
	  		<div class="col-12 modal-image-top">
	  			<div class="row">
	  				<img src="images/kapal3.png" class="img-fluid" alt="...">
	  			</div>
	  		</div>
	  		<div class="col-12 p-4">
	  			<div class="row">
	  				<div class="col-12 title">
			  			<h5 class="mb-0"><strong>Daftar Buletin PIS</strong></h5>
			  		</div>
	  				<div class="col-12">
	  					<p>Berlangganan buletin kami dan jadilah orang pertama yang mengetahui tentang perkembangan terbaru kami.</p>

	  					<form>
	  						<div class="row">
								  <div class="form-group col-12">
								    <input type="text" class="form-control" placeholder="Masukkan Nama Anda">
								  </div>
								  <div class="form-group col-12">
								    <input type="email" class="form-control" placeholder="Masukkan Email Anda">
								  </div>
		  					</div>
		  					<div class="row">
								  <div class="col-12  mt-2">
								  	<button class="btn btn-radius-pis bg-red pl-4 pr-4">
								  		<strong>Daftar <i class="bi bi-chevron-right"></i></strong>
								  	</button>
								  </div>
		  					</div>
	  					</form>

	  				</div>
	  			</div>
	  		</div>
	  	</div>
		</div>
     
  </main>

  <!-- ======= Footer ======= -->
	<div id="footer-top-border">
		<div class="col-lg-12 col-12" style="top:0">
			<div class="row">
				<div class="col-lg-6 col-6 footer-border-1" style="top:0"></div>
				<div class="col-lg-3 col-3 footer-border-2" style="top:0"></div>
				<div class="col-lg-3 col-3 footer-border-3" style="top:0"></div>
			</div>
		</div>
	</div>
	<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-4 col-12 footer-info">
            <h6><strong>KANTOR PUSAT</strong></h6>
            <p id="address" class="mt-3">Graha Pertamina – Tower Pertamax Lantai 19<br>Jalan Medan Merdeka Timur No. 6<br>Gambir, Jakarta Pusat 10110, Indonesia <br></p>
            <div class="d-flex mt-2">
              <div class="align-self-center rounded-icon">
	  					  <span class="bi bi-telephone-fill phone-icon"></span>
              </div>
              <div class="align-self-center">
               &nbsp; <span id="phone">(021) 5290 0271 / 5290 0272 </br> &nbsp;  Fax: (021) 52906273</span>
              </div>
	  				</div>
	  				<div class="d-flex mt-2">
              <div class="align-self-center rounded-icon">
	  					  <span class="bi bi-envelope-fill phone-icon"></span>
              </div>
              <div class="align-self-center">
                &nbsp; <span id="email">corsec.pis@pertamina.com</span>
              </div>
	  				</div>
	  				<div class="d-flex mt-2">
              <div class="align-self-center">
	  					  <img src="images/logo-135.jpg" width="25px" alt="" class="img-fluid">
              </div>
              <div class="align-self-center">
                &nbsp; Call Center <strong>135</strong>
              </div>
	  				</div>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>


          <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong id="perusahaan">TAUTAN </strong></h6>
            <ul class="mt-3">
              <li><i class="bx bx-chevron-right"></i> <a href="https://pertamina.com" id="pt1">PT Pertamina (Persero)</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://bumn.go.id" id="pt2">Kementerian BUMN</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://www.esdm.go.id" id="pt3">Kementerian ESDM RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="http://www.dephub.go.id" id="pt4">Kementerian Perhubungan RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://www.bkpm.go.id" id="pt5">BKPM</a></li>
            </ul>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>

          <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong>TEMUKAN KAMI</strong></h6>
            <ul class="mt-3 social-links">
              <li >
              	<a href="https://www.facebook.com/Pertamina-International-Shipping-106722181759077/" id="link_fb" class="d-flex">
	              	<div class="align-self-center">
	              		<span class="bi bi-facebook facebook"></span>
	              	</div>
	              	<div class="align-self-center">Facebook<br><em>Pertamina International Shipping</em></div>
	              </a>
	            </li> 
              <li>
              	<a href="https://www.instagram.com/pertamina_pis/ " id="link_ig" class="d-flex">
	              	<div class="align-self-center">
	              		<span class="bi bi-instagram instagram"></span>
	              		</div>
	              	<div class="align-self-center">Instagram<br><em>@pertamina_pis</em></div> 
	              </a>
	            </li>
              <li>
                <a href="https://twitter.com/pertamina_pis " id="link_tw" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-twitter twitter"></span>
                    </div>
                  <div class="align-self-center">Twitter<br><em>@Pertamina_PIS</em></div> 
                </a>
              </li>
              <li>
                <a href="https://www.youtube.com/channel/UC3KgM_Zg3u7tCqr-1hXxnkw" id="link_yt" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-youtube twitter"></span>
                    </div>
                  <div class="align-self-center">Youtube<br><em>PT Pertamina International Shipping</em></div> 
                </a>
              </li>
            </ul>
          </div>
          <div class="col-12 d-lg-none separate"><hr></div>

        </div>
      </div>
    </div>

    <div class="container">
    	<div class="row d-flex">
	      <div class="col-lg-8 col-12 align-self-center part-of">
	        <span>PART OF</span>
	        <img src="images/logo-bumn.svg" height="30px" class="ml-3" alt="...">
	        <img src="images/logo-pertamina.svg" height="30px" class="ml-3" alt="...">
	      </div>
    		<div class="col-lg-4 col-12">
    			<a href="" class="btn btn-whistle">
            	<div class="row">
            		<div class="col pr-0 d-flex mr-2">
          				<img src="images/icons/wbs.svg">
            		</div>
            		<div class="col text-left pl-0">
		          		Whistle Blowing System
		          		<br><em>https://pertaminaclean.tipoffs.info/</em>
            		</div>
            	</div>
          	</a>
    		</div>
    		<div class="col-12 mt-3">
    			&copy; Copyright 2021 <strong><span>Pertamina International Shipping</span></strong>. All Rights Reserved
    		</div>
    	</div>
    </div>
  </footer><!-- End Footer -->

  <!-- Vendor JS Files -->
  <script src="vendor/jquery.min.js"></script>
  <script src="vendor/bootstrap-4.5.3-dist/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="vendor/venobox/venobox.min.js"></script>
  <script src="vendor/waypoints/lib/jquery.waypoints.min.js"></script>
  <script src="vendor/counterup/counterup.min.js"></script>
  <script src="vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="vendor/aos/aos.js"></script>
  <script src="vendor/fontawesome/js/all.min.js"></script>
  <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
  <script src="vendor/owl-carousel/dist/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="js/main.js?v=0.1"></script>

  <script type="text/javascript">
      history.pushState({}, null, "/");
      function changeLang()
      {
           var button = document.getElementById("LbEnd");
           button.click();
      }
 	$(document).ready(function(){
 	   
 		$("#news, #gallery").bind('click', function(e){
 			e.preventDefault();
 			$(this).addClass("active");
 			let id = $(this).attr("id");
 			$("#home"+id).removeClass("d-none");
 			if(id==="news"){
 				$("#homegallery").addClass("d-none");
 				$("#gallery").removeClass("active");
 			} else {
 				$("#homenews").addClass("d-none");
 				$("#news").removeClass("active");
 			}
 		});

 		$('.popup-youtube').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false,
		});

		//$('.popup-modal').magnificPopup({
		//	type: 'inline',
		//	preloader: false,
		//	focus: '#username',
		//	modal: true,
		//	mainClass: 'my-mfp-zoom-in',
		//}).magnificPopup('open');

		$(".btn-close-modal").bind('click', function(e){
			e.preventDefault();
			$.magnificPopup.close();
		});

		$(".popup-gallery").magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title') + '';
				}
			}
		});

		$('.image-popup-vertical-fits').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			mainClass: 'mfp-img-mobile',
			image: {
				verticalFit: true
			},
			zoom: {
				enabled: true,
				duration: 300 
			}
		});

		let owl = $('.gallery-carousel');
    owl.owlCarousel({
      margin: 15,
      nav: true,
      navText:["<div class='nav-btn prev-slide justify-content-center'><i class='bi bi-chevron-left'></i></div>","<div class='nav-btn next-slide'><i class='bi bi-chevron-right'></div>"],
      loop: false,
      autoplayTimeout:3000,
      autoplayHoverPause:false,
      responsive: {
        0: {
          items: carouselItem
        }
      },
      onDragged: carouselCallback,
    });

    let armada = $('.armada-carousel');
    armada.owlCarousel({
      margin: 15,
      nav: true,
      navText:["<div class='nav-btn prev-slide justify-content-center bg-green'><i class='bi bi-chevron-left'></i></div>","<div class='nav-btn next-slide bg-green'><i class='bi bi-chevron-right'></div>"],
      loop: false,
      autoplayTimeout:3000,
      autoplayHoverPause:false,
      responsive: {
        0: {
          items: carouselItem
        }
      },
      onDragged: carouselCallback,
    });

    let profil = $('.profil-carousel');
    profil.owlCarousel({
      margin: 15,
      nav: true,
      navText:["<div class='nav-btn prev-slide justify-content-center bg-green'><i class='bi bi-chevron-left'></i></div>","<div class='nav-btn next-slide bg-green'><i class='bi bi-chevron-right'></div>"],
      loop: false,
      autoplayTimeout:3000,
      autoplayHoverPause:false,
      responsive: {
        0: {
          items: carouselItem
        }
      },
      onDragged: carouselCallback,
    });

    let pengadaan = $('.pengadaan-carousel');
    pengadaan.owlCarousel({
      margin: 15,
      nav: true,
      navText:["<div class='nav-btn prev-slide justify-content-center bg-green'><i class='bi bi-chevron-left'></i></div>","<div class='nav-btn next-slide bg-green'><i class='bi bi-chevron-right'></div>"],
      loop: false,
      autoplayTimeout:3000,
      autoplayHoverPause:false,
      responsive: {
        0: {
          items: carouselItem4
        }
      },
      onDragged: carouselCallback,
    });

 	});

 	function carouselCallback(){
	  let val = $(".gallery-carousel").find(".active").find(".discount").val();
	  $(".gallery-carousel").html(val);
	}

	$(document).click(function(event) {
    var className = $(event.target).prop('class');
    if(className=='mfp-content'){
      $.magnificPopup.close();
    }
  });

  //let imgHighlighLeft = $(".media-information-box-left").find('img');
  //$.each(imgHighlighLeft, function(key, value){
  //  console.log(value);
  //  let widthImage = value.width;
  //  let heightImage = value.height;
  //  if(heightImage<425){
  //    $(this).css({'height': '100%', 'width': 'auto'});
  //  } else {
  //    $(this).css({'width': '100%', 'height': 'auto'});
  //  }
  //});

  //let imgHighlighRight = $(".media-information-box-right").find('img');
  //$.each(imgHighlighRight, function(key, value){
  //  let widthImage = value.width;
  //  let heightImage = value.height;
  //  console.log(heightImage);
  //  if(heightImage<201){
  //    $(this).css({'height': '100%', 'width': 'auto'});
  //  } else {
  //    $(this).css({'width': '100%', 'height': 'auto'});
  //  }
  //});
  let shipImg = $(".armada-carousel").find('img');
  $.each(shipImg, function (key, value) {
      let widthImage = value.width;
      let heightImage = value.height;
      if (heightImage < 201) {
          $(this).css({ 'width': '100%', 'height': 'auto' });
      } else {
          $(this).css({ 'height': '100%', 'width': 'auto' });
      }
  });
  </script>
</form>
</body>

</html>



