﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;

namespace PIS_Backend
{
    public partial class AddPers : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if(Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;

            }
        }
        
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            bool valid = true;
            string validationMessage = "";

            if (content.InnerText == "")
            {
                valid = false;
                validationMessage = "Content Must be Filled !";
            }
            if (title.Text == "")
            {
                valid = false;
                validationMessage = "Title Must be Filled !";
            }
            if(city.Text == "")
            {
                valid = false;
                validationMessage = "City Must be Filled";
            }
            if (valid)
            {
                if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
                {
                    string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                    string SaveLocation = Server.MapPath("Pers") + "\\" + fn;
                    try
                    {
                        if (all.ImageFileType(FileUpload1.FileName))
                        {
                            FileUpload1.PostedFile.SaveAs(SaveLocation);
                        }
                        else
                        {
                            valid = false;
                            validationMessage = "File Must be Image";
                        }


                    }
                    catch (Exception ex)
                    {
                        valid = false;
                        validationMessage = ex.Message;
                    }
                }
                else
                {
                    valid = false;
                    validationMessage = "Please Select Upload File";
                }
                if(valid)
                {
                    Pers prs = new Pers();
                    prs.Title = title.Text;
                    prs.Content = content.InnerHtml;
                    prs.Img_File = "Pers/" + FileUpload1.FileName;
                    prs.City = city.Text;

                    prs.Insert();
                    LblErrorMessage.Visible = false;

                    Response.Redirect("SiaranPers.aspx");
                }
                else
                {
                    LblErrorMessage.Visible = true;
                    LblErrorMessage.Text = validationMessage;
                }
            }
            else
            {
                LblErrorMessage.Visible = true;
                LblErrorMessage.Text = validationMessage;
            }
        }
            

            
        }
    }
