﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="ViewVideos.aspx.cs" Inherits="PIS_Backend.ViewVideos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="gallery">
      <div class="container">
				<div class="row no-gutters">
					<div class="col-lg-12 mb-3">
						<ul class="nav nav-tabs row-eq-height" id="myTab" role="tablist">
							<li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5 active" id="video-tab" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="true">
							   Video
							  </a>
						  </li>
						  <li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5" id="photo-tab" data-toggle="tab" href="#photo" role="tab" aria-controls="photo" aria-selected="false">
						    	Foto
						    </a>
						  </li>
						  <li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5" id="infografis-tab" data-toggle="tab" href="#infografis" role="tab" aria-controls="infografis" aria-selected="false">
						    	Infografis
						    </a>
						  </li>
						</ul>
						<div class="tab-content bg-white pt-5 pb-5" id="myTabContent">
						  <div class="tab-pane fade show active" id="video" role="tabpanel" aria-labelledby="video-tab">
						  	<div class="row">
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<!--
						  			<div class="embed-responsive embed-responsive-16by9">
										  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/URHBX4oK3Wc" allowfullscreen></iframe>
										</div>
										-->
										<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-lg-4 col-md-6 mb-5 col-12 box-video">
						  			<div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">VIDEO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  	</div>

						  	<!---Pagination-->
						  	<div class="row mt-4">
				        	<div class="col-12">
				        		<div class="row">
				        			<div class="col-6">
				        				<p>Pages: 1 of 52</p>
				        			</div>
				        			<div class="col-6 text-right">
				        				<nav aria-label="...">
												  <ul class="pagination  justify-content-end">
												    <li class="page-item disabled">
												      <span class="page-link">Prev</span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">1</a></li>
												    <li class="page-item active">
												      <span class="page-link">
												        2 <span class="sr-only">(current)</span>
												      </span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">3</a></li>
												    <li class="page-item">
												      <a class="page-link" href="#">Next</a>
												    </li>
												  </ul>
												</nav>
				        			</div>
				        		</div>
				        	</div>
				        </div>

						  </div>

						  <div class="tab-pane fade" id="photo" role="tabpanel" aria-labelledby="photo-tab">
						  	<div class="row">
						  		<a class="col-4 mb-5 image-popup-vertical-fit" href="images/image-holder1.jpg">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</a>
						  		<a class="col-4 mb-5 image-popup-vertical-fit" href="images/image-holder1.jpg">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</a>
						  		<a class="col-4 mb-5 image-popup-vertical-fit" href="images/image-holder1.jpg">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</a>
						  		<a class="col-4 mb-5 image-popup-vertical-fit" href="images/image-holder1.jpg">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</a>
						  		<a class="col-4 mb-5 image-popup-vertical-fit" href="images/image-holder1.jpg">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</a>
						  		<a class="col-4 mb-5 image-popup-vertical-fit" href="images/image-holder1.jpg">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</a>
						  		<a class="col-4 mb-5 image-popup-vertical-fit" href="images/image-holder1.jpg">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>25 April 2021</em> &nbsp;<span class="badge badge-danger">PHOTO</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</a>
						  	</div>

						  	<!---Pagination-->
						  	<div class="row mt-4">
				        	<div class="col-12">
				        		<div class="row">
				        			<div class="col-6">
				        				<p>Pages: 1 of 52</p>
				        			</div>
				        			<div class="col-6 text-right">
				        				<nav aria-label="...">
												  <ul class="pagination  justify-content-end">
												    <li class="page-item disabled">
												      <span class="page-link">Prev</span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">1</a></li>
												    <li class="page-item active">
												      <span class="page-link">
												        2 <span class="sr-only">(current)</span>
												      </span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">3</a></li>
												    <li class="page-item">
												      <a class="page-link" href="#">Next</a>
												    </li>
												  </ul>
												</nav>
				        			</div>
				        		</div>
				        	</div>
				        </div>
						  </div>
						  <div class="tab-pane fade" id="infografis" role="tabpanel" aria-labelledby="infografis-tab">
						  	<div class="row">
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  		<div class="col-4 mb-5">
						  			<div class="box-image">
					  					<img src="images/image-holder1.jpg" class="image-link" alt="...">
					  				</div>
										<div class="mt-3">
											<p class="mb-0"><em>24 April 2021</em> &nbsp;<span class="badge badge-danger">INFOGRAFIS</span></p>
          						<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong></p>
										</div>
						  		</div>
						  	</div>

						  	<!---Pagination-->
						  	<div class="row mt-4">
				        	<div class="col-12">
				        		<div class="row">
				        			<div class="col-6">
				        				<p>Pages: 1 of 52</p>
				        			</div>
				        			<div class="col-6 text-right">
				        				<nav aria-label="...">
												  <ul class="pagination  justify-content-end">
												    <li class="page-item disabled">
												      <span class="page-link">Prev</span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">1</a></li>
												    <li class="page-item active">
												      <span class="page-link">
												        2 <span class="sr-only">(current)</span>
												      </span>
												    </li>
												    <li class="page-item"><a class="page-link" href="#">3</a></li>
												    <li class="page-item">
												      <a class="page-link" href="#">Next</a>
												    </li>
												  </ul>
												</nav>
				        			</div>
				        		</div>
				        	</div>
				        </div>

						  </div>
						</div>

					</div>
        </div>
        
      </div>
    </section>
    <%--<div class="container-fluid" style="background-color:ghostwhite">
        <div class="text-center pt-5">
            <h2>Videos</h2>
        </div><hr />
        <div class="row">
            <div class="col">
                <h4 id="title" runat="server"></h4>
                <asp:Label ID="LblDate" runat="server" Text="Label" Font-Bold="true"></asp:Label>
            </div>
        </div>
        
        <div class="row pt-3">
            <div class="col">
               <div>
                     <div class="image-video">
											<a href="https://www.youtube.com/watch?v=URHBX4oK3Wc" class="popup-youtube">
					                <img src="https://img.youtube.com/vi/URHBX4oK3Wc/0.jpg" class="rounded" alt="Responsive image">
					                <center>
					                    <img src="images/youtube-play-button.png" class="playButton" alt="Responsive image">
					                </center>
					            </a>
										</div>
                </div
            </div>
        </div>

        <div class="row pt-3">
            <div class="col">
                <p id="content" runat="server">

                </p>
            </div>
        </div>
        <div class="text-right">
            <a href="AdminVideos.aspx" class="btn btn-info" ><i class="fa fa-home"></i></a>
        </div>
    </div>--%>
</asp:Content>
