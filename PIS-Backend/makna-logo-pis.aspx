﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="makna-logo-pis.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.makna_logo_pis" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx" id="li_1" runat="server">Home</a></li>
					    <li class="breadcrumb-item" id="li_2" runat="server"> Profil Perusahaan </li>
					    <li class="breadcrumb-item active" aria-current="page" id="li_3" runat="server">Makna Logo PIS</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="header_text" runat="server">MAKNA LOGO PIS</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  				<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <section id="makna-logo">
  		<div class="container">
	  		<div class="row mb-5">
	  		<%--	<div class="col-lg-12 content">
	  				<h5>Makna dari logo Pertamina International Shipping</h5>

	  				<img src="images/logo-color.png">

	  				<p><strong>Makna dari logo Pertamina International Shipping adalah:</strong></p>

	  				<ol>
	  					<li>Warna <span class="blue_pis"><strong>biru</strong></span> memiliki arti andal, dapat dipercaya dan bertanggung jawab.</li>
	  					<li>Warna <span class="green_pis"><strong>hijau</strong></span> memiliki arti sumber daya energi yang berwawasan lingkungan.</li>
	  					<li>Warna <span class="red_pis"><strong>merah</strong></span> memiliki arti keuletan dan ketegasan serta keberanian dalam menghadapi berbagai macam kesulitan. Warna ini juga terlihat di nama “INTERNATIONAL SHIPPING” sebagai nama perusahaan yang berdiri sendiri sebagai subholding Pertamina yang ulet dalam mengembangkan bisnisnya.</li>
	  				</ol>

	  				<br>
	  				<p><strong>Simbol grafis memiliki arti: </strong></p>
	  				<ol>
	  					<li>Bentuk anak panah menggambarkan aspirasi organisasi Pertamina untuk senantiasa bergerak ke depan, maju dan progresif. Simbol ini juga mengisyaratkan huruf “P” yakni huruf pertama dari Pertamina.</li>
	  					<li>Tiga elemen berwarna melambangkan pulau-pulau dengan berbagai skala yang merupakan bentuk negara Indonesia.</li>
	  				</ol>
                      <a href="images/Logo-Pertamina-PIS/logo-pertamina.svg" target="_blank" class="btn btn-radius-pis bg-red pl-4 pr-4 mt-3">
              <strong>Download Logo PIS <i class="bi bi-chevron-right"></i></strong>
            </a>
	  			</div>--%>
				   
              <div id="content" runat="server" class="col-lg-12 content">
                
              </div> 
				<a id="fileDownload" runat="server" target="_blank" class="btn btn-radius-pis bg-red pl-4 pr-4 mt-3">
					<strong><span id="download_text" runat="server">Download Logo PIS </span><i class="bi bi-chevron-right"></i></strong>
				</a>
	  		</div>
	  	</div>
  	</section>
    <script>
        var urlName = '';
        if($("#LbEn").attr("class") == "active")
        {
            urlName = 'our-logo';
        }
        else
        {
            urlName = 'makna-logo-pis';
        }
        history.pushState({}, null, urlName);
        $("#ContentPlaceHolder1_fileDownload").hide();
        $("#btnDownload").attr("href", $("#ContentPlaceHolder1_fileDownload").attr("href"));
    </script>
</asp:Content>
