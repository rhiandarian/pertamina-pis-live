﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="PIS_Backend.Login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/image/pertamina-7oziq.jpg" type="image/png">
    
    <title> Pertamina International Shipping | Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown" style="background-color: white;width: 400px;padding: 40px;margin-top: 100px;">
        <div>
            
            <div class="form-group">

                 <img src="image/logo-PIS.jpg" width="100%" />
            </div>
            
            <asp:Label ID="LblErrorEmail" runat="server" Text="Email Invalid !"></asp:Label>
            <asp:Label ID="LblErrorPassword" runat="server" Text="Invalid Password!"></asp:Label>
            <form id="form1" class="m-t" role="form" runat="server">
                <div class="form-group">
                    <asp:TextBox ID="Email" CssClass="form-control" TextMode="Email"  placeholder="Email" autocomplete="off" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="Password" CssClass="form-control" placeholder="Password" TextMode="Password" runat="server"></asp:TextBox>
                </div>
                
                 <asp:Button ID="BtnLogin" CssClass="btn btn-danger block full-width m-b" runat="server" Text="Login" OnClick="BtnLogin_Click" />

                <a href="ForgotPassword.aspx"><small>Forgot password?</small></a> 
             
               
            </form>

            
            
        </div>
    </div>

    <!-- Mainly scripts -->
    


</body>

</html>
