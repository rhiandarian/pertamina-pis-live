﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class SaveBanner : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "2"))
                {
                    Response.Redirect("admin.aspx");
                }

                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                Load_Pages();
                LoadData();
            }
        }
        public void LoadData()
        {
            if (Request.QueryString["ID"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                Banner bnr = new Banner();
                string cond = " ID = " + Id;
                DataTable dt = bnr.Select("*", cond);
                title.Text = dt.Rows[0]["title"].ToString();
                title_en.Text = dt.Rows[0]["title_en"] != null ? dt.Rows[0]["title_en"].ToString() : "";
                description.Text = dt.Rows[0]["description"].ToString();
                description_en.Text = dt.Rows[0]["description_en"] != null ? dt.Rows[0]["description_en"].ToString() : "";
                btnTextEn.Text = dt.Rows[0]["buttonTextEn"] != null ? dt.Rows[0]["buttonTextEn"].ToString() : "";
                DdlUrl.SelectedValue = dt.Rows[0]["url_id"].ToString();
                btnText.Text = dt.Rows[0]["buttonText"].ToString();
            }
        }
        public void Load_Pages()
        {
            PagesData pd = new PagesData();
            DataSet dt = pd.GetData("ID, name");

            DdlUrl.DataTextField = dt.Tables[0].Columns["name"].ToString();
            DdlUrl.DataValueField = dt.Tables[0].Columns["ID"].ToString();
            DdlUrl.DataSource = dt.Tables[0];
            DdlUrl.DataBind();
            DdlUrl.Items.Insert(0, "Pilih halaman yang diarahkan ketika klik tautan");
            DdlUrl.SelectedIndex = 0;
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ImageResize Iz = new ImageResize();
                string random = Iz.textrandom();
                
                Banner bnr = new Banner();
                bnr.Title = title.Text;
                bnr.TitleEn = title_en.Text;
                bnr.Description = description.Text;
                bnr.DescriptionEn = description_en.Text;
                bnr.ButtonTextEn = btnTextEn.Text;


                bnr.Url = DdlUrl.SelectedValue;
                
                bnr.ButtonText = btnText.Text;
               

                if (Request.QueryString["ID"] == null)
                {
                    
                    string validationMessage = "";
                    if ((FileUpload1.PostedFile == null) && (FileUpload1.PostedFile.ContentLength == 0))
                    {
                        validationMessage = "File Wajib di Upload";
                    }
                    if (!all.ImageFileType(FileUpload1.FileName))
                    {
                        validationMessage = "File harus Foto";
                    }
                    if(validationMessage == "")
                    {
                        bnr.Img_File = random + FileUpload1.FileName;
                        string insertStatus = bnr.Insert(getip(),GetBrowserDetails());
                        if(insertStatus == "success")
                        { 
                            UploadBanner(random);
                            Response.Redirect("ListBanner.aspx");
                        }
                        else
                        {
                            showValidationMessage(insertStatus);
                        }

                    }
                    else
                    {
                        showValidationMessage(validationMessage);
                    }
                }
                else
                {
                    int Id = Convert.ToInt32(Request.QueryString["ID"]);
                    string conditions = " ID = " + Id;
                    if (FileUpload1.FileName != "")
                    {
                        bnr.Img_File = random + FileUpload1.FileName;
                    }
                    string UpdateStatus = bnr.Update(Id,getip(),GetBrowserDetails(),conditions);
                    
                    if (UpdateStatus == "success")
                    {
                        string withFile = "";
                        if (FileUpload1.FileName != "")
                        {
                            UploadBanner(random);
                            withFile += " file " + FileUpload1.FileName + " file length " + FileUpload1.PostedFile.ContentLength;
                        }
                        
                        Response.Redirect("ListBanner.aspx");
                    }
                    else
                    {
                        showValidationMessage(UpdateStatus);
                        LoadData();
                    }
                }
            }
            catch(Exception ex)
            {
                Banner bnr = new Banner();
                string action = Request.QueryString["ID"] == null ? "Insert" : "Update";
                bnr.ErrorLogHistory(getip(), GetBrowserDetails(), action, bnr.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                showValidationMessage(ex.ToString());
            }

        }
        public void UploadBanner(string random)
        {
            ImageResize Iz = new ImageResize();
            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string SetFolderBeforeResize = Server.MapPath("Banner") + "\\" + "serize" + random + fn; //gambar yang akan diresize sesudah direze gambar akan dihapus
            string SetFolderAfterResize = Server.MapPath("Banner") + "\\" + random + fn;
            FileUpload1.PostedFile.SaveAs(SetFolderBeforeResize);
            Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "Banner", "Cut");
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
    }
}