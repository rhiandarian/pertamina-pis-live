﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class VideoJsonEn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Response.Write(data());
            }
        }
        public string data()
        {
            General all = new General();
            Gallery g = new Gallery();
            DataTable dt = g.ShowVideoProfile("En");
            string print = "[";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string coma = i < dt.Rows.Count - 1 ? ", " : "";
                print += "{" + all.addJsonFieldValue("title", dt.Rows[i]["title"].ToString());
                print += all.addJsonFieldValue("title_en", dt.Rows[i]["title_en"].ToString());
                print += all.addJsonFieldValue("content", dt.Rows[i]["content"].ToString());
                print += all.addJsonFieldValue("video_url", dt.Rows[i]["video_url"].ToString());
                print += all.addJsonFieldValue("img_url", dt.Rows[i]["img_url"].ToString(), true) + "}" + coma;
            }
            print += "]";
            return print;
        }
    }
}