﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PIS_Backend
{
    public partial class keberlanjutan : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadPage();
                getPhoto(1);
            }
        }
        public void loadPage()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 19 ");
            imgHeader.Src = dt.Rows[0]["Header"].ToString();
            string decodecontent = Encoding.UTF8.GetString(Convert.FromBase64String(Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content"].ToString()));
            content.InnerHtml = decodecontent;

            li_1.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Home" : "Beranda";
            li_2.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Commitment & Sustainibility" : "Komitmen & Keberlanjutan";
            li_3.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Sustainability" : "Keberlanjutan";
            header_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "SUSTAINABILITY" : "KEBERLANJUTAN";
            header_text1.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Sustainability" : "Keberlanjutan";
            tjs.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Social Responsibilities" : "Tanggung Jawab Sosial";
            lk.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Sustainability Report" : "Laporan Keberlanjutan";
            editClassLang();
        }


        public void getPhoto(int PageIndex)
        {
            Gallery g = new Gallery();
            DataTable pt = g.GetAll(1, all.CategoryKeberlanjutanID);
            int totalCount = Convert.ToInt32(pt.Rows[0]["Total Count"]);
            PageIndex = PageIndex <= totalCount ? PageIndex : 1;
            List<Gallery> dt = GetImages(PageIndex, all.CategoryKeberlanjutanID);
            ListViewPhoto.DataSource = dt;
            ListViewPhoto.DataBind();

        }

        public List<Gallery> GetImages(int PageIndex, int CategoryID)
        {
            Gallery g = new Gallery();
            DataTable dt = g.GetAll(PageIndex, CategoryID);
            List<Gallery> glr = new List<Gallery>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int GalleryID = Convert.ToInt32(dt.Rows[i]["ID"].ToString());
                List<Image> img = new List<Image>();
                DataTable imageDetails = g.GetDetailImage(GalleryID);
                for (int a = 0; a < imageDetails.Rows.Count; a++)
                {
                    img.Add(new Image()
                    {
                        Title = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? imageDetails.Rows[a]["title_en"].ToString() : imageDetails.Rows[a]["title"].ToString(),
                        Img_File = imageDetails.Rows[a]["img_file"].ToString(),
                        Dates = imageDetails.Rows[a]["image_date"].ToString(),
                    });

                }
                int countImageInAlbum = img.Count + 1;
                glr.Add(new Gallery()
                {
                    No = i + 1,
                    Title = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[i]["title_en"].ToString()  : dt.Rows[i]["title"].ToString(),
                    Img_File = dt.Rows[i]["Thumbnail"].ToString(),
                    Dates = dt.Rows[i]["news_dates"].ToString(),
                    CategoryName = dt.Rows[i]["Category_name"].ToString(),
                    ImageDetail = img,
                    detindex = "1 of " + countImageInAlbum 
                });
            }
            return glr;
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
    }
}