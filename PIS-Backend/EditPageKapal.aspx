﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EditPageKapal.aspx.cs" Inherits="PIS_Backend.EditPageKapal1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Deskripsi Armada Kami</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="">Aktivitas Bisnis</a>
                        </li>
                        
                        <li class="breadcrumb-item"> 
                            Armada Kami
                        </li>
                        <li class="breadcrumb-item">
                           <a href="EditPageKapal.aspx"><strong>Deskripsi</strong></a> 
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Deskripsi Kapal Milik & Armada</small></h5>
                              <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                <div id="successdiv" class="alert alert-success" runat="server"></div>
                <div class="form-group">
                    <label for="totalkapal"><b>Total Kapal</b></label>
                    <asp:TextBox ID="totalKapal" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="kapalMilik"><b>Kapal Milik</b></label>
                    <asp:TextBox ID="kapalMilik" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                 <div class="form-group">
                    <label for="content"><b>konten</b></label>
                   <textarea class="summernote" id="content" runat="server" placeholder="Input Content News" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <label for="content_en"><b>konten bahasa inggris</b></label>
                   <textarea class="summernote" id="content_en" runat="server" placeholder="Input Content News" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <asp:Button ID="Save" CssClass="btn btn-primary" runat="server" Text="Simpan" OnClick="Save_Click"     />
                    <a href="Armada-Kami.aspx" target="_blank"><i class="fa fa-eye"></i> Lihat hasil website </a>
                </div>
                
            </div>
            
        </div>
    </div>
</asp:Content>
