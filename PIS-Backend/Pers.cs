﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;


namespace PIS_Backend
{
    public class Pers
    {
        SqlConnection con = Connection.conn();

        string tableName = "Pers";

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"].ToString();

        ModelData Db = new ModelData();

        private int id;

        private string title;

        private string content;

        private string img_file;

        private string city;
        

        public string Title
        {
            get { return title; }
            set { title = value; }
        }


        public string Content
        {
            get { return content; }
            set { content = value; }
        }
        public string Img_File
        {
            get { return img_file; }
            set { img_file = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        




        public void Insert()
        {
             string field = "";
             string value = "";

            if (title != null)
            {
                field += "title, ";
                value += "'"+title+"', ";
            }
            if (content != null)
            {
                field += "content, ";
                value += "'"+content+"', ";
            }
            if (img_file != null)
            {
                field += "img_file, ";
                value += "'"+img_file+"', ";
            }
            if(city != null)
            {
                field += "city, ";
                value += "'" + city + "', ";
            }
            field += "addDate, addUser";

            
            value += "'" + dateNow + "', " + user_id;

            string query = "INSERT INTO "+tableName+" ("+field+") VALUES ("+value+")";
            
            Db.setData(query);
        }
        public void Update(string conditions)
        {
            string newRecord = "set";

            newRecord += title != null ? " title = '" + title + "',  " : "";
            newRecord += content != null ? " content = '" + content + "',  " : "";
            newRecord += img_file != null ? " img_file = '" + img_file + "',  " : "";
            newRecord += city != null ? " city = '" + city + "', " : "";


            string flagUpdate = " editDate = '" + dateNow + "', ";
            flagUpdate += "editUser = " + user_id;



            string Query = "Update " + tableName + " " + newRecord + "" + flagUpdate;
            Query += conditions != "" ? "Where " + conditions : "";
            
            Db.setData(Query);
        }
        public void Delete(string conditions)
        {
            string query = "Update " + tableName + " set fl_deleted = 1, deleteDate = '" + dateNow + "', deleteUser = " + user_id;
            query += conditions != null ? "Where " + conditions : "";
            Db.setData(query);
        }
        public DataTable Select(string field,string conditions = null)
        {
            string and = conditions != null ? " and " : "";
            string flagConditions = and+" (fl_deleted != 1 or fl_deleted IS NULL)";

            string query = "Select "+field+" from "+tableName+" Where "+conditions+""+flagConditions;
            
            DataTable dt = Db.getData(query);

            return dt;
        }

    }
}