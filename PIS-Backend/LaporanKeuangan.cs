﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace PIS_Backend
{
    public class LaporanKeuangan
    {
        SqlConnection con = Connection.conn();

        string tableName = "LaporanKeuangan";

        string viewName = "View_LaporanKeuangan";

        public int PageSize = 8;

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        public string url;

        public int year;

        public string Url
        {
            get { return url; }
            set { url = value; }
        }
        public int Year
        {
            get { return year; }
            set { year = value; }
        }
        public DataTable Select(string field,string cond = null)
        {
            cond = cond == null ? "" : cond;
            string Query = Db.SelectQuery(field, viewName, cond);
            DataTable dt = Db.getData(Query);
            return dt;
        }
    }
}