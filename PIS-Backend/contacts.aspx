﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contacts.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.contacts" %>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Pertamina PIS</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="images/favicon.png" rel="icon">
  <link href="images/favicon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="vendor/bootstrap-4.5.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/animate/animate.css" rel="stylesheet" />
  <link href="vendor/venobox/venobox.css" rel="stylesheet">
  <link href="vendor/aos/aos.css" rel="stylesheet">
  <!--
    <link href="vendor/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
  -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  <link href="css/v01/style.css" rel="stylesheet" />
  <!-- Template Main CSS File -->
  
</head>

<body>
    <form runat="server" >
  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
         <asp:LinkButton ID="LbInd" CssClass="active"  OnClick="LangInd_Click" runat="server">ID</asp:LinkButton> | <asp:LinkButton ID="LbEn" OnClick="LangEn_Click" runat="server">EN</asp:LinkButton> 
      </div>
      <div class="contact-info float-right">
      	<a href="contacts.aspx"><i class="bi bi-envelope"></i></a>
      	<a href="#search"><i class="bi bi-search"></i></a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
       <a href="Index.aspx"><img src="images/Logo_Pertamina_PIS.svg" alt="" class="img-fluid"></a>
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
        <a href="" id="nav-right-option" class="nav-right-option float-right mobile-nav-toggle d-lg-none"><i class="bi bi-x"></i></a>
       <ul>
          
            <asp:ListView ID="ListViewMenu" runat="server">
                <ItemTemplate>
                    <li class='<%# Eval("classMenu") %>'>
                        <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                        <ul>
                            <asp:ListView ID="ListViewSubMenu" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                <ItemTemplate>
                                    <li class='<%# Eval("classMenu") %>'>
                                        <a href='<%# Eval("Link") %>'><%# System.Web.HttpUtility.HtmlEncode((string)Eval("Name")) %></a>
                                        <ul>
                                        <asp:ListView ID="ListViewSubMenu2" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                            </ul>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                        </ul>
                    </li>
                </ItemTemplate>
            </asp:ListView>
           <asp:ListView ID="ListViewResponsive" runat="server">
                            <ItemTemplate>
                                <li class='<%# Eval("classMenu") %> d-lg-none'>
                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                    <ul>
                                        <asp:ListView ID="ListViewSubOthers" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li class='<%# Eval("classMenu") %>'>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                    <ul>
                                                        <asp:ListView ID="ListViewSub2Othes" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                                            <ItemTemplate>
                                                                <li>
                                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </ul>
                                                    
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </ul>
                                </li>
                            </ItemTemplate>
                        </asp:ListView>
		      

          <li class="nav-right-option d-none d-lg-block" id="othermenu" runat="server"></li>
        </ul>
        <div id="langMobile" runat="server" class="text-center lang-mobile d-lg-none">
          
        </div>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- Nav Overlay -->
  <div id="navOverlay" class="nav-overlay d-none d-lg-block">
  	<div class="bg-black-transparent">
	  	<div class="container">
		    <div class="nav-overlay-content">
		    	 <ul>
                        <asp:ListView ID="ListViewOthers" runat="server">
                            <ItemTemplate>
                                <li class='<%# Eval("classMenu") %>'>
                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                    <ul>
                                        <asp:ListView ID="ListViewSubOthers" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li class='<%# Eval("classMenu") %>'>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                    <ul>
                                                        <asp:ListView ID="ListViewSub2Othes" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                                            <ItemTemplate>
                                                                <li>
                                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </ul>
                                                    
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </ul>
                                </li>
                            </ItemTemplate>
                        </asp:ListView>
                        
		    		
		    	</ul>
		    </div>
	  	</div>
  	</div>
  </div><!-- End Nav Overlay -->

  <!-- Search -->
  <div id="search">
    <div class="bg-overlay">
      <button type="button" class="close"><i class="bi bi-x close"></i></button> 
      <input type="search" id="txtSearch" runat="server" value="" autocomplete="off" placeholder="Type keyword(s) here" /> 
          <asp:LinkButton ID="btnSearch" OnClick="btnSearch_Click" Text="Cari" CssClass="btn bg-red" runat="server" /> 
    </div>
  </div><!-- End Search -->

  <main id="main">

  	<!-- Top Main -->
  	<section id="breadcrumbs" class="pt-3">
			<div class="container">
				<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx" id="home" runat="server">Beranda</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="kontak" runat="server">Kontak Kami</li>
					  </ol>
					</nav>
					<div class="col-lg-12 page-title">
						<h3 id="title_kontak" runat="server">HUBUNGI KAMI</h3>
					</div>
				</div> 
				</div>
  	</section>

  	<!-- Map -->
  	<section id="contact-map" class="pb-0">
			<div class="col-lg-12 p-0">
  			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7825.69167056266!2d106.83224985058668!3d-6.178388384766061!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xed586f520a43f3c2!2sGraha%20Pertamina!5e0!3m2!1sen!2sid!4v1631540833734!5m2!1sen!2sid" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
			</div>
  	</section>
      <style>
          .white-color{
              color:rgb(248 248 248 / 0.00);
          }
      </style>
  	<!-- Form -->
  	<section id="contact-form" style="padding: 60px 0;">
  		<div class="container">
	  		<div class="row">
	  			<div class="col-lg-8">   
                      <%--<asp:TextBox CssClass="form-control white-color" id="ipaddress" runat="server" ></asp:TextBox>--%>
	  				<h3 class="title mb-4" id="title_pesan" runat="server">Sampaikan pesan Anda</h3> 
	  				<p class="mb-5" id="desc_pesan" runat="server">Masukkan data diri dan pesan yang ingin Anda sampaikan kepada kami.</p>
                     <asp:Label ID="notif" runat="server" Text=""></asp:Label> 
	  					<div class="row">
							  <div class="form-group col-lg-6">
							    <label id="name_pesan" runat="server">Nama</label>
							    <asp:TextBox   CssClass="form-control" id="nama_form" runat="server" ></asp:TextBox>
							  </div>
							  <div class="form-group col-lg-6">
							    <label id="email_pesan" runat="server">Email</label>
							    <asp:TextBox   CssClass="form-control" id="email_form"   runat="server"></asp:TextBox>
							  </div>
	  					</div>
	  					<div class="row">
							  <div class="form-group col-lg-6">
							    <label id="telp_pesan" runat="server">Nomor Telepon</label>
                                  <asp:TextBox   CssClass="form-control" id="notelp_form"   runat="server"></asp:TextBox> 
							  </div>
	  						<div class="form-group col-lg-6">
							    <label id="institusi_pesan" runat="server">Institusi</label>
							    <select class="form-control" id="Institusi_form" runat="server">
							      <option value="-">--Institusi--</option>
							      <option value="Media">Media</option>
							      <option value="Investor">Investor</option>
							      <option value="Umum">Umum</option>
							    </select>
							  </div>
	  					</div>
						  <div class="row">
							  <div class="form-group col-lg-12">
							    <label id="judul_pesan" runat="server">Judul</label>
                                   <asp:TextBox   CssClass="form-control" id="judul_form" runat="server"></asp:TextBox> 
							  </div>
							  <div class="form-group col-lg-12">
							    <label id="isi_pesan" runat="server">Isi Pesan</label>
                                  
							    <textarea class="form-control" id="pesan_form" runat="server" rows="3"></textarea>
							  </div>
							  <%--<div class="col-lg-12">
							  	<div class="g-recaptcha" id="rcaptcha"  data-sitekey="6Ldq8cAaAAAAAJ6V578rD62f2WAE81wNI8JM2n_t"></div>
									<span id="captcha" style="color:red" /></span> <!-- this will show captcha errors -->
							  </div>--%>
							  <div class="col-lg-12 mt-4">
							  	<asp:Button ID="BtnMessage" OnClick="Button1_Click" CssClass="btn btn-radius-pis bg-red pl-4 pr-4" runat="server" Text="Kirim Pesan" />
							  </div>
	  					</div> 
	  			</div>
	  			<div class="d-none d-lg-block col-lg-4 contact-info">
	  				<div class="row">
	  					<div class="col-12 mb-5">
			  				 <p class="info-title bg-green" id="kantor" runat="server">Kantor Pusat</p>
			  				<p class="info-content pl-2" id="address" runat="server">
			  					 
			  				</p>
                              <div class="info-phone pl-1 d-flex mb-1">
                                  <div class="align-self-center rounded-icon">
                                    <span class="bi bi-envelope-fill phone-icon"></span>
                                  </div>
                                  <div class="align-self-center">
                                    &nbsp; <span id="email" runat="server">corsec.pis@pertamina.com</span>
                                  </div>
                                </div>
			  				<div class="info-phone pl-1 d-flex">
                                <div class="align-self-center rounded-icon">
			  					  <span class="bi bi-telephone-fill phone-icon"></span>
                  </div>
                  <div class="align-self-center">
                    &nbsp; <span id="phone" runat="server"> </span>
                  </div>
			  				</div>
                <!--
                <div>
                  <a href="mailto://corsec.pis@pertamina.com" class="info-email pl-2 mt-2 d-flex">
                    <div class="align-self-center rounded-icon">
                      <i class="bi bi-envelope-fill"></i>
                    </div>
                    <div class="align-self-center">
                      &nbsp; corsec.pis@pertamina.com
                    </div>
                  </a>
                </div>
                -->
                <div class="mt-4 col-xs-12 col-sm-10 offset-sm-1 info-135">
                  <center><a href="tel:135"><img src="images/logo-135.jpg" alt="" class="img-fluid"></a></center>
                </div>
	  					</div>
	  					<div class="col-12">
			  				<p class="info-title bg-blue mb-4" id="temukan" runat="server">Temukan Kami</p>
			  				<ul class="mt-3 pl-2 social-links">
		              <li >
		              	<a id="link_fb" runat="server" class="d-flex">
			              	<div class="align-self-center">
			              		<span class="bi bi-facebook facebook"></span>
			              	</div>
			              	<div class="align-self-center">Facebook<br><em>Pertamina International Shipping</em></div>
			              </a>
			            </li>
		              <li>
		              	<a id="link_ig" runat="server" class="d-flex">
			              	<div class="align-self-center">
			              		<span class="bi bi-instagram instagram"></span>
			              		</div>
			              	<div class="align-self-center">Instagram<br><em>@pertamina_pis</em></div> 
			              </a>
			            </li>
		              <li>
		              	<a id="link_tw" runat="server" class="d-flex">
			              	<div class="align-self-center">
			              		<span class="bi bi-twitter twitter"></span>
			              		</div>
			              	<div class="align-self-center">Twitter<br><em>@Pertamina_PIS</em></div> 
			              </a>
			            </li>
                        <li>
                            <a id="link_yt" runat="server" class="d-flex">
                              <div class="align-self-center">
                                <span class="bi bi-youtube twitter"></span>
                                </div>
                              <div class="align-self-center">Youtube<br><em>PT Pertamina International Shipping</em></div> 
                            </a>
                        </li>
		            </ul>
	  					</div>
	  				</div>
	  			</div>
	  		</div>
	  	</div>
  	</section>

  </main>

  <!-- ======= Footer ======= -->
  <div id="footer-top-border">
    <div class="col-lg-12 col-12">
      <div class="row">
        <div class="col-lg-6 col-6 footer-border-1"></div>
        <div class="col-lg-3 col-3 footer-border-2"></div>
        <div class="col-lg-3 col-3 footer-border-3"></div>
      </div>
    </div>
  </div>
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-4 col-12 footer-info">
            <h6><strong id="kantor_footer" runat="server">KANTOR PUSAT</strong></h6>
            <p class="mt-3" id="address_footer" runat="server">
               
            </p>
            <div class="d-flex mt-2">
              <div class="align-self-center rounded-icon">
                <span class="bi bi-telephone-fill phone-icon"></span>
              </div>
              <div class="align-self-center">
                &nbsp; <span id="phone_footer" runat="server"> </span>
              </div>
            </div>

              <div class="d-flex mt-2">
                  <div class="align-self-center rounded-icon">
	  					      <span class="bi bi-envelope-fill phone-icon"></span>
                  </div>
                  <div class="align-self-center">
                    &nbsp; <span id="email_footer" runat="server">corsec.pis@pertamina.com</span>
                  </div>
	  		 </div>
              
            <div class="d-flex mt-2">
              <div class="align-self-center">
                <img src="images/logo-135.jpg" alt="" class="img-fluid" width="25px">
              </div>
              <div class="align-self-center">
                &nbsp; Call Center <strong>135</strong>
              </div>
            </div>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>


       <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong id="perusahaan" runat="server">TAUTAN </strong></h6>
            <ul class="mt-3">
              <li><i class="bx bx-chevron-right"></i> <a href="https://pertamina.com" id="pt1" runat="server">PT Pertamina (Persero)</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://bumn.go.id" id="pt2" runat="server">Kementerian BUMN</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://www.esdm.go.id" id="pt3" runat="server">Kementerian ESDM RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="http://www.dephub.go.id" id="pt4" runat="server">Kementerian Perhubungan RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://www.bkpm.go.id" id="pt5" runat="server">BKPM</a></li>
            </ul>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>
                
          <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong id="findus" runat="server">TEMUKAN KAMI</strong></h6>
            <ul class="mt-3 social-links">
              <li  >
                <a id="link_fb_footer" runat="server" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-facebook facebook"></span>
                  </div>
                  <div class="align-self-center">Facebook<br><em>Pertamina International Shipping</em></div>
                </a>
              </li>

              <li>
                <a id="link_ig_footer" runat="server" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-instagram instagram"></span>
                    </div>
                  <div class="align-self-center">Instagram<br><em>@pertamina_PIS</em></div> 
                </a>
              </li>
              <li>
                <a id="link_tw_footer" runat="server" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-twitter twitter"></span>
                    </div>
                  <div class="align-self-center">Twitter<br><em>@Pertamina_PIS</em></div> 
                </a>
              </li>
              <li>
                <a id="link_yt_footer" runat="server" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-youtube twitter"></span>
                    </div>
                  <div class="align-self-center">Youtube<br><em>PT Pertamina International Shipping</em></div> 
                </a>
              </li>
            </ul>
          </div>
          <div class="col-12 d-lg-none separate"><hr></div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="row d-flex">
        <div class="col-lg-8 col-12 align-self-center part-of">
          <span>PART OF</span>
          <img src="images/logo-bumn.svg" class="ml-3" alt="..." height="30px">
          <img src="images/logo-pertamina.svg" class="ml-3" alt="..." height="30px">
        </div>
        <div class="col-lg-4 col-12">
          <a href="" class="btn btn-whistle">
              <div class="row">
                <div class="col pr-0 d-flex mr-2">
                  <img src="images/icons/wbs.svg">
                </div>
                <div class="col text-left pl-0">
                  Whistle Blowing System
                  <br><em>https://pertaminaclean.tipoffs.info/</em>
                </div>
              </div>
            </a>
        </div>
        <div class="col-12 mt-3">
          © Copyright 2021 <strong><span>Pertamina International Shipping</span></strong>. All Rights Reserved
        </div>
      </div>
    </div>
  </footer><<!-- End Footer -->
    

   

  <!-- Vendor JS Files -->
  <script src="vendor/jquery.min.js"></script>
  <script src="vendor/bootstrap-4.5.3-dist/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="vendor/venobox/venobox.min.js"></script>
  <script src="vendor/waypoints/lib/jquery.waypoints.min.js"></script>
  <script src="vendor/counterup/counterup.min.js"></script>
  <script src="vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="vendor/aos/aos.js"></script>
  <script src="vendor/fontawesome/js/all.min.js"></script>

  <!-- Template Main JS File -->
  <script src="js/main.js?v=0.1"></script>

  <script src='https://www.google.com/recaptcha/api.js'></script>
       <%-- <script type="text/javascript">
            window.onload = function () {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.src = "https://api.ipify.org?format=jsonp&callback=DisplayIP";
                document.getElementsByTagName("head")[0].appendChild(script);
            };
            function DisplayIP(response) {
                document.getElementById("<%=ipaddress.ClientID%>").value = response.ip;
            }
        </script>--%>
	<script>
       function changeLang(lang)
      {
           var button = lang == "En" ? document.getElementById("<%= LbInd.ClientID %>") : document.getElementById("<%= LbEn.ClientID %>");
           button.click();
      }
	    var urlName = '', txtSearchText = '', btnSearchText = '';
        if($("#LbEn").attr("class") == "active")
        {
            urlName = 'contacts';
            txtSearchText = "Type keyword(s) here";
            btnSearchText = "Search";
        }
        else
        {
            urlName = 'kontak';
            txtSearchText = "Ketik kata kunci disini";
            btnSearchText = "Cari";
        }ipt>
        $("#txtSearch").attr('placeholder', txtSearchText);
        $("#btnSearch").text(btnSearchText);
        history.pushState({}, null,urlName);
   
	function get_action(form) {
    let v = grecaptcha.getResponse();
    if(v.length == 0){
        document.getElementById('captcha').innerHTML="You can't leave Captcha Code empty";
      return false;
    } else{
        document.getElementById('captcha').innerHTML="Captcha completed";
      return true; 
    }
	}
    </script>
        </form>
</body>

</html>

