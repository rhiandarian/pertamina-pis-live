﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="edittonggaksejarah.aspx.cs" Inherits="PIS_Backend.edittonggaksejarah" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Tonggak Sejarah Page</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Page
                        </li>
                        <li class="breadcrumb-item">
                            <a>Layanan</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="editpagelayanan.aspx"><strong>Tonggak Sejarah Page Edit</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Tonggak Sejarah Save Page.</small></h5>
                              <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                <div id="successdiv" class="alert alert-success" runat="server"></div>
                <div class="form-group">
                    <label for="FileUpload1"><b>Header Image</b></label>
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </div>
                 <div class="form-group">
                    <label for="content"><b>content</b></label>
                   <textarea class="summernote" id="content" runat="server"  rows="10"></textarea>
                </div>
                <div class="form-group">
                    <asp:Button ID="Save" CssClass="btn btn-primary" runat="server" Text="Save" OnClick="Save_Click"    />
                </div>
                <a href="TonggakSejarah.aspx" target="_blank"><i class="fa fa-eye"></i> See Result on Website </a>
            </div>
            
        </div>
    </div>
</asp:Content>
