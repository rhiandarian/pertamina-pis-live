﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="AddArticle.aspx.cs" Inherits="PIS_Backend.AddArticle" validateRequest="false" enableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="container-fluid" style="background-color:ghostwhite">
        <div class="row pt-5">
             <div class="col text-center">
                 <h1>ARTICLE</h1>
             </div>
         </div><hr />
        <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
        <div class="row pt-5">
             <div class="col">
                 <form>
                      <div class="form-group">
                        <label for="title"><b>title</b></label>
                        <asp:TextBox ID="title" cssClass="form-control" placeholder="Input Title Article" runat="server"></asp:TextBox>
                      </div>
                     <div class="form-group">
                        <label for="city"><b>city</b></label>
                        <asp:TextBox ID="city" cssClass="form-control" placeholder="Input City Article" runat="server"></asp:TextBox>
                      </div>
                     <div class="form-group">
                        <label for="content"><b>content</b></label>
                        <textarea class="form-control" id="content" runat="server" placeholder="Input Content Article" rows="10"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="ddlCategory"><b>category</b></label>
                          <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control"></asp:DropDownList>
                      </div>
                     <div class="form-group">
                        <label for="FileUpload1"><b>Article Image</b></label>
                        <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                     </div>
                    <div class="form-group">
                        <asp:Button ID="BtnSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="BtnSave_Click"   />
                    </div>
                      
                </form>
             </div>
         </div>
        <div class="text-right">
           <a href="Articles.aspx" class="btn btn-info"><i class="fa fa-home"></i></a>
        </div>
    </div>
</asp:Content>
