﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="UserEdit.aspx.cs" Inherits="PIS_Backend.UserEdit" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
                        <h2>Edit Pengguna</h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="UserList.aspx"> Semua Pengguna</a>
                            </li>
                            <li class="breadcrumb-item active">
                                 <strong>Ubah Data Pengguna</strong> 
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">

                    </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Ubah Data Pengguna.</small></h5>
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                 <form>
                     <div class="form-group">
                        <label for="Username"><b>Nama Pengguna</b></label>
                        <asp:TextBox ID="Username" cssClass="form-control" placeholder="Masukan Nama Pengguna" runat="server"></asp:TextBox>
                      </div>  
                      <div class="form-group">
                        <label for="Email"><b>Email</b></label>
                        <asp:TextBox ID="Email" cssClass="form-control" placeholder="Input " runat="server"></asp:TextBox>
                      </div> 
                     <div class="form-group">
                        <label for="password"><b>Kata Sandi </b> (<small style="color:red">jika tidak mengubah Harap Diabaikan</small>)</label>
                         <input type="password"  class="form-control" id="Password" runat="server"  value="" /> 
                      </div> 
                     <div class="form-group">
					    <label>Level Pengguna</label>
					    <select class="form-control" id="level" runat="server"> 
							   
						</select>
					 </div>
                    <div class="form-group">
                        <asp:Button ID="Save" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="BtnSave_Click"      />
                    </div> 
                </form>
            </div>
        </div>
    </div>
</asp:Content>
