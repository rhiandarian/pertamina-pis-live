﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PIS_Backend
{
    public partial class AddVideos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;

            }
        }
        public void showErrorMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Videos vd = new Videos();
                vd.Title = title.Text;
                vd.Content = content.InnerHtml;
                vd.Video_url = videoUrl.Text;
                vd.Img_url = imageUrl.Text;
                vd.Category = txtCategory.Text;
                vd.Embed_url = embedUrl.Text;

                string insertStatus = vd.Insert();
                if (insertStatus == "success")
                {
                    Response.Redirect("AdminVideos.aspx");
                }
                else
                {
                    showErrorMessage(insertStatus);
                }

            }
            catch (Exception ex)
            {
                showErrorMessage(ex.Message.ToString());
            }
        }
    }
}