﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PIS_Backend
{
    public partial class PengadaanDescEn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InfoPengadaan ip = new InfoPengadaan();
                PagesData pd = ip.getPage();
                Response.Write(pd.content_en);
            }
        }
    }
}