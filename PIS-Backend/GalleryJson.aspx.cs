﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;




namespace PIS_Backend
{
    public partial class GalleryJson : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Response.Write(data());
            }
        }

        public string data()
        {
            General all = new General();

            string print = "[";
            Gallery g = new Gallery();
            DataTable dt = g.GetLatestGallery();
            for(int i=0; i < dt.Rows.Count; i++)
            {
                string coma = i < dt.Rows.Count - 1 ? ", " : "";
                print += "{" + all.addJsonFieldValueInt("ID", Convert.ToInt32(dt.Rows[i]["ID"].ToString()));
                print += all.addJsonFieldValue("title", dt.Rows[i]["title"].ToString());
                print += all.addJsonFieldValue("title_en", dt.Rows[i]["title_en"].ToString());
                print += all.addJsonFieldValue("Thumbnail", dt.Rows[i]["Thumbnail"].ToString());
                print += all.addJsonFieldValue("Url", dt.Rows[i]["Url"].ToString());
                print += all.addJsonFieldValue("Category_name", dt.Rows[i]["Category_name"].ToString());
                print += all.addJsonFieldValue("Category_En", dt.Rows[i]["Category_En"].ToString());
                print += all.addJsonFieldValue("Gallery_Date", dt.Rows[i]["Gallery_Date"].ToString());
                if (dt.Rows[i]["Category_name"].ToString() == "Foto")
                {
                    print += all.addJsonFieldValueObject("Album", loadAlbum(g, Convert.ToInt32(dt.Rows[i]["ID"].ToString())),true);
                }
                else
                {
                    print += all.addJsonFieldValueObject("Album", "[]", true);
                }
                print += "}" + coma;
                
            }
            print += "]";
            return print;
        }
        public string loadAlbum(Gallery g, int ID)
        {
            General all = new General();
            DataTable dt = g.GetDetailImage(ID);
            string print = "[";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string coma = i < dt.Rows.Count - 1 ? ", " : "";
                print += "{" + all.addJsonFieldValue("title", dt.Rows[i]["title"].ToString());
                print += all.addJsonFieldValue("title_en", dt.Rows[i]["title_en"].ToString());
                print += all.addJsonFieldValue("img_file", dt.Rows[i]["img_file"].ToString());
                print += all.addJsonFieldValue("image_date", dt.Rows[i]["image_date"].ToString(),true)+"}"+coma;
            }
            print += "]";
            return print;
        }
    }
}