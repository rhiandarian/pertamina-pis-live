﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class Galleries : System.Web.UI.Page
    {
        General all = new General();
        string IndPage = "Galleri.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getPhoto(1);
                getVideo(1);
                getInfografis(1);
                bindMenu();
                loadfooter();
                if(Session["Lang"] == null || Session["Lang"].ToString() == "Ind")
                {
                    Response.Redirect(IndPage);
                }
            }
            LblIndexFoto.Visible = false;
            LblIndexVideo.Visible = false;
            LblIndexInfografis.Visible = false;
        }
        public void bindMenu()
        {
            string fullPath = Request.Url.AbsolutePath;
            string pageName = System.IO.Path.GetFileName(fullPath);
            string currPage = pageName + ".aspx";
            //Response.Write(all.alert(currPage));
            string lang = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "En" : "Ind";

            List<Menu> mn = all.GenerateMenu(lang, currPage);
            ListViewMenu.DataSource = mn;
            ListViewMenu.DataBind();

            List<Menu> ot = all.MenuOthers(lang);
            ListViewOthers.DataSource = ot;
            ListViewOthers.DataBind();

            ListViewResponsive.DataSource = ot;
            ListViewResponsive.DataBind();

            string otmn = "Others";
            othermenu.Attributes["class"] = all.getOtherMenuClass(othermenu.Attributes["class"], currPage);
            othermenu.InnerHtml = "<a href=" + all.stripped("") + " onclick=" + all.stripped("openNavOverlay()") + " id=" + all.stripped("nav-right-option") + " >" + otmn + " <span class=" + all.stripped("") + "></span></a>";
            langMobile.InnerHtml = all.changeLangMobile("En");
        }
        public void loadfooter()
        {
            ModelData Db = new ModelData();
            string query = "Select * from  Contact  where ID = 1";
            DataTable dt = Db.getData(query);
            address.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["address_en"].ToString() : dt.Rows[0]["address"].ToString(); ;
            phone.InnerHtml = dt.Rows[0]["phone"].ToString();
            email.InnerHtml = dt.Rows[0]["email"].ToString();
            link_fb.HRef = dt.Rows[0]["link_fb"].ToString();
            link_tw.HRef = dt.Rows[0]["link_tw"].ToString();
            link_ig.HRef = dt.Rows[0]["link_ig"].ToString();
            link_yt.HRef = dt.Rows[0]["link_yt"].ToString();
        }
        private List<Paging> PopulatePaging(int TotalCount, int Index, int PageSize)
        {
            List<Paging> pages = all.PopulatePaging(TotalCount, Index, PageSize);
            return pages;
            // detailPagings.InnerHtml = "Pages: " + Index + " of " + TotalCount;


        }
        public List<Gallery> GetImages(int PageIndex, int CategoryID)
        {
            Gallery g = new Gallery();
            DataTable dt = g.GetAll(PageIndex, CategoryID);
            List<Gallery> glr = new List<Gallery>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int GalleryID = Convert.ToInt32(dt.Rows[i]["ID"].ToString());
                List<Image> img = new List<Image>();
                DataTable imageDetails = g.GetDetailImage(GalleryID);
                for (int a = 0; a < imageDetails.Rows.Count; a++)
                {
                    img.Add(new Image()
                    {
                        Title = imageDetails.Rows[a]["title"].ToString(),
                        Img_File = imageDetails.Rows[a]["img_file"].ToString(),
                        Dates = imageDetails.Rows[a]["image_date"].ToString(),
                    });

                }
                int countImageInAlbum = img.Count + 1;
                glr.Add(new Gallery()
                {
                    No = i + 1,
                    Title = dt.Rows[i]["title_en"].ToString(),
                    Img_File = dt.Rows[i]["Thumbnail"].ToString(),
                    Dates = dt.Rows[i]["news_date"].ToString(),
                    CategoryName = dt.Rows[i]["Category_name"].ToString(),
                    ImageDetail = img,
                    detindex = "1 of "+countImageInAlbum
                });
            }
            return glr;
        }
        public void getPhoto(int PageIndex)
        {
            Gallery g = new Gallery();
            LblIndexFoto.Text = PageIndex.ToString();
            DataTable pt = g.GetAll(1, all.FotoCategoryID);
            int totalCount = Convert.ToInt32(pt.Rows[0]["Total Count"]);
            PageIndex = PageIndex <= totalCount ? PageIndex : 1;
            List<Gallery> dt = GetImages(PageIndex, all.FotoCategoryID);
            ListViewPhoto.DataSource = dt;
            ListViewPhoto.DataBind();

            List<Paging> pg = PopulatePaging(totalCount, PageIndex, g.PageSize);
            RptPaging.DataSource = pg;
            RptPaging.DataBind();
            int totalPages = all.getTotalPaging(totalCount, PageIndex, g.PageSize);
            detailPagingPhoto.InnerHtml = all.getDetailPaging("En", PageIndex, totalPages);

        }
        protected void ChangeLang(object sender, EventArgs e)
        {
            if (Session["Lang"] == null)
            {
                Session.Add("Lang", "Ind");
            }
            else
            {
                Session["Lang"] = "Ind";
            }
            Response.Redirect(IndPage);
        }
        public void getInfografis(int PageIndex)
        {
            Gallery g = new Gallery();
            LblIndexInfografis.Text = PageIndex.ToString();
            DataTable pt = g.GetAll(1, all.InfografisCategoryID);
            int totalCount = Convert.ToInt32(pt.Rows[0]["Total Count"]);
            PageIndex = PageIndex <= totalCount ? PageIndex : 1;
            List<Gallery> dt = GetImages(PageIndex, all.InfografisCategoryID);
            ListViewInfografis.DataSource = dt;
            ListViewInfografis.DataBind();

            List<Paging> pg = PopulatePaging(totalCount, PageIndex, g.PageSize);
            RptInfg.DataSource = pg;
            RptInfg.DataBind();
            int totalPages = all.getTotalPaging(totalCount, PageIndex, g.PageSize);
            detailInfografisPaging.InnerHtml = all.getDetailPaging("Ind", PageIndex, totalPages);
        }
        public void getVideo(int PageIndex)
        {
            Gallery g = new Gallery();
            LblIndexVideo.Text = PageIndex.ToString();
            DataTable pt = g.GetAll(PageIndex, all.VideoCategoryID);
            int totalCount = Convert.ToInt32(pt.Rows[0]["Total Count"]);
            PageIndex = PageIndex <= totalCount ? PageIndex : 1;
            ListViewVideo.DataSource = pt;
            ListViewVideo.DataBind();
            List<Paging> pg = PopulatePaging(totalCount, PageIndex, g.PageSize);
            RptVideo.DataSource = pg;
            RptVideo.DataBind();
            int totalPages = all.getTotalPaging(totalCount, PageIndex, g.PageSize);
            detailpagingVideo.InnerHtml = all.getDetailPaging("Ind", PageIndex, totalPages);
        }
        protected void Page_Changed(object sender, EventArgs e)
        {
            string commendArgument = (sender as LinkButton).CommandArgument.ToString();
            string category = (sender as LinkButton).Attributes["category"].ToString();

            int pageIndexFoto = int.Parse(LblIndexFoto.Text);
            int pageIndexVideo = int.Parse(LblIndexVideo.Text);
            int pageIndexInfografis = int.Parse(LblIndexInfografis.Text);

            if (category == "photo")
            {
                if (commendArgument == "first")
                {
                    if (pageIndexFoto > 1)
                    {
                        pageIndexFoto--;
                        getPhoto(pageIndexFoto);
                    }

                }
                else if (commendArgument == "last")
                {
                    Gallery g = new Gallery();
                    DataTable pt = g.GetAll(1, all.FotoCategoryID);
                    int totalCount = Convert.ToInt32(pt.Rows[0]["Total Count"]);

                    List<Paging> pages = all.PopulatePaging(totalCount, 1, g.PageSize);

                    int totalPaging = pages.Count;
                    int limit = all.getTotalPaging(totalCount, 1, g.PageSize);
                    if (pageIndexFoto < limit)
                    {
                        pageIndexFoto++;
                        getPhoto(pageIndexFoto);
                    }



                }
                else
                {

                    int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
                    getPhoto(pageIndex);



                }
                DataShow.Text = "photo";
            }
            if (category == "infografis")
            {
                if (commendArgument == "first")
                {
                    if (pageIndexInfografis > 1)
                    {
                        pageIndexInfografis--;
                        getInfografis(pageIndexInfografis);
                    }

                }
                else if (commendArgument == "last")
                {
                    Gallery g = new Gallery();
                    DataTable pt = g.GetAll(1, all.InfografisCategoryID);
                    int totalCount = Convert.ToInt32(pt.Rows[0]["Total Count"]);

                    List<Paging> pages = all.PopulatePaging(totalCount, 1, g.PageSize);
                    int totalPaging = pages.Count;

                    int limit = all.getTotalPaging(totalCount, 1, g.PageSize);
                    if (pageIndexInfografis < limit)
                    {
                        pageIndexInfografis++;
                        getPhoto(pageIndexInfografis);
                    }

                    getInfografis(totalPaging);
                }
                else
                {
                    int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
                    getInfografis(pageIndex);

                }
                DataShow.Text = "infografis";
            }
            if (category == "video")
            {
                if (commendArgument == "first")
                {
                    if (pageIndexVideo > 1)
                    {
                        pageIndexVideo--;
                        getVideo(pageIndexVideo);
                    }

                }
                else if (commendArgument == "last")
                {
                    Gallery g = new Gallery();
                    DataTable pt = g.GetAll(1, all.VideoCategoryID);
                    int totalCount = Convert.ToInt32(pt.Rows[0]["Total Count"]);

                    List<Paging> pages = all.PopulatePaging(totalCount, 1, g.PageSize);
                    int totalPaging = pages.Count;
                    int limit = all.getTotalPaging(totalCount, 1, g.PageSize);
                    if (pageIndexVideo < limit)
                    {
                        pageIndexVideo++;
                        getVideo(pageIndexVideo);
                    }

                    getVideo(totalPaging);

                }
                else
                {
                    int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
                    getVideo(pageIndex);

                }
                DataShow.Text = "videos";
            }


        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(all.search + "" + txtSearch.Value);
        }
    }
}