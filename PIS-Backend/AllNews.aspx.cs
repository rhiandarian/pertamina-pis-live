﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class AllNews : System.Web.UI.Page
    {
        General all = new General();
     
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                News n = new News();
                string urlName = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? n.EnPage : n.IndPage;
               
                BindAllNews(1);
                LoadNews();
                TotalAllCategory();
                loadsosmed();
                LoadLang();
            }
            LblIndex.Visible = false;
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
        public void LoadLang()
        {
            editClassLang();
            link1.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Media & Information" : link1.InnerText;
            link2.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "News" : link2.InnerText;

            hTitle.InnerText = link2.InnerText.ToUpper();
            ctgr.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Categories" : ctgr.InnerText;
            //arsip.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Archieve" : arsip.InnerText;
            allshow.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Show All" : allshow.InnerText;
        }
        public void BindAllNews(int Index)
        {
            string lang = Session["Lang"] == null ? "Ind" : Session["Lang"].ToString();
            LblIndex.Text = Index.ToString();
            News n = new News();
            int Category = Request.QueryString["Category"] == null ? 0 : Convert.ToInt32(Request.QueryString["Category"].ToString());
            DataTable dt = n.GetAllNews(Index,lang,Category);
            ListViewNews.DataSource = dt;
            ListViewNews.DataBind();

            int totalCount = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());
            PopulatePaging(totalCount, Index, n.PageSize);
            
        }
        public void TotalAllCategory()
        {
            string lang = Session["Lang"] == null ? "Ind" : Session["Lang"].ToString();
            News nws = new News();
            DataTable dt = nws.CountAllCategory(lang);
            ListViewCategory.DataSource = dt;
            ListViewCategory.DataBind();
        }
        private void PopulatePaging(int TotalCount,int Index,int PageSize)
        {
            string lang = Session["Lang"] == null ? "Ind" : Session["Lang"].ToString();

            List<Paging> pages = all.PopulatePaging(TotalCount,Index,PageSize);
            RptPaging.DataSource = pages;
            RptPaging.DataBind();
            int totalPages = all.getTotalPaging(TotalCount, Index, PageSize);
            detailPagings.InnerHtml = all.getDetailPaging(lang, Index, totalPages);


        }
        protected void Page_Changed(object sender, EventArgs e)
        {
            string commendArgument = (sender as LinkButton).CommandArgument.ToString();
            int pagesIndex = int.Parse(LblIndex.Text);
            if (commendArgument == "first")
            {
                if (pagesIndex > 1)
                {
                    pagesIndex--;
                    BindAllNews(pagesIndex);
                }
            }
            else if (commendArgument == "last")
            {

                News n = new News();
                DataTable dt = n.GetAllNews(1);


                int totalCount = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());

                List<Paging> pages = all.PopulatePaging(totalCount, 1, n.PageSize);

                int totalPaging = pages.Count;
                int limit = all.getTotalPaging(totalCount, 1, n.PageSize);
                if (pagesIndex < limit)
                {
                    pagesIndex++;
                    BindAllNews(pagesIndex);
                }

            }
            else
            {

                int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
                BindAllNews(pageIndex);
            }

        }
        public List<News> getNews(DataTable dt, int i)
        {
            List<News> n1 = new List<News>();
            n1.Add(new News()
            {
                UUID = dt.Rows[i]["uuid"].ToString(),
                Img_File = dt.Rows[i]["img_file"].ToString(),
                Dates = dt.Rows[i]["news_date"].ToString(),
                Categories = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[i]["Category_en"].ToString() : dt.Rows[i]["Category_name"].ToString(),
                Title = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[i]["title_en"].ToString() : dt.Rows[i]["title"].ToString(),
                Key = dt.Rows[i]["key"].ToString()
            });

            return n1;
        }
        public void LoadNews()
        {
            News n = new News();
            DataTable dt = n.LoadNews();

            List<News> n1 = getNews(dt, 0);
            
            ListViewNews1.DataSource = n1;
            ListViewNews1.DataBind();
            string ID = dt.Rows[0]["uuid"].ToString();
            string imgSrc = dt.Rows[0]["img_file"].ToString();
            string newsDate = dt.Rows[0]["news_date"].ToString();
            string category = dt.Rows[0]["Category_name"].ToString();
            string title = dt.Rows[0]["title"].ToString();


            List<News> n2 = getNews(dt, 1);
            ListViewNews2.DataSource = n2;
            ListViewNews2.DataBind();
            ID = dt.Rows[1]["uuid"].ToString();
            imgSrc = dt.Rows[1]["img_file"].ToString();
            newsDate = dt.Rows[1]["news_date"].ToString();
            category = dt.Rows[1]["Category_name"].ToString();
            title = dt.Rows[1]["title"].ToString();



            List<News> n3 = getNews(dt, 2);
            ListViewNews3.DataSource = n3;
            ListViewNews3.DataBind();
            ID = dt.Rows[2]["uuid"].ToString();
            imgSrc = dt.Rows[2]["img_file"].ToString();
            newsDate = dt.Rows[2]["news_date"].ToString();
            category = dt.Rows[2]["Category_name"].ToString();
            title = dt.Rows[2]["title"].ToString();

            




        }
        public string contentCard(string ID, string imgSrc, string newsDate, string Category, string title)
        {
            string image = "<img src=" + all.stripped(imgSrc) + " class=" + all.stripped("card - img") + " alt=" + all.stripped("") + " width=" + all.stripped("100%") + ">";
            string dateandtitle = "<em>" + newsDate + "</em> &nbsp;<span class=" + all.stripped("badge badge-danger") + ">" + Category.ToUpper() + "</span><br>" + title;
            string paragraph = all.paragraph(dateandtitle, "", "card-content p-4");

            string cardimgOverlay = all.div(paragraph, "", "card-img-overlay bg-black-transparent");



            return all.div(image + " " + cardimgOverlay, "", "card text-white overflow-hidden media-information-box card-block");

        }
        public string mediaInfoRight(string id,string imgSrc, string newsDate, string Category, string title)
        {
            return all.div(contentCard(id,imgSrc, newsDate, Category, title), "", "col-12 pl-0 pr-0 mb-4 media-information-box-right");
        }

        public void loadsosmed()
        {
            ModelData Db = new ModelData();
            string query = "Select * from  Contact  where ID = 1";
            DataTable dt = Db.getData(query); 
            link_ig.HRef = dt.Rows[0]["link_ig"].ToString();
            //link_fb.HRef = dt.Rows[0]["link_fb"].ToString(); 
             
        }
    }
}