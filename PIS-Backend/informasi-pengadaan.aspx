﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="informasi-pengadaan.aspx.cs" Inherits="PIS_Backend.informasi_pengadaan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a id="htitle" href="Index.aspx"></a></li>
					    <li class="breadcrumb-item"><a href="informasi-pengadaan.aspx" id="link1" runat="server">Pengadaan</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="link2" runat="server">Informasi Pengadaan</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="headerTitle" runat="server">Informasi Pengadaan</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <asp:Label ID="LblIndex" runat="server" Text="Label"></asp:Label>
    <section id="laporan-tahunan">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div class="col-12">
	  				<h3 id="title" runat="server">Informasi Pengadaan</h3>
	  			</div>
	  		</div>

	  		<div class="row mb-5 content">
	  			<div id="content" runat="server" class="col-12 mb-4">
	  			</div>
                  <asp:ListView ID="ListViewPengadaan" runat="server">
                      <ItemTemplate>
                          <div class="col-xs-12 col-sm-3 box-content">
	  				        <div class="col box-pis-sm">
	  					        <img src='<%# Eval("cover") %>' class="img-fluid mt-2 mb-2" alt="...">
	  					        <p class="text-center">
	  						        <em><%# Eval("pengadaan_date") %></em><br>
	  						        <strong><%# Eval("title") %></strong>
	  					        </p>
	  					        <p>
                                      <asp:LinkButton ID="LinkButton1" CssClass="red_pis" CommandArgument='<%# Eval("url")  %>' OnClick="Download" runat="server">Download</asp:LinkButton>
	  						      
	  					        </p>
	  				        </div>
	  			</div>
                      </ItemTemplate>
                  </asp:ListView>
	  			
	  			
	  		</div>

        <!---Pagination-->
        <div class="row mt-4">
          <div class="col-12 pagination-box">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <p id="detailPaging" runat="server"></p>
              </div>
            
              <div class="col-xs-12 col-sm-6 text-right">
                <nav aria-label="...">
                <asp:Repeater ID="RptPaging" runat="server">
                    <HeaderTemplate>
                    <ul class="pagination justify-content-end">
                        <li class="page-item">
                             <asp:LinkButton  runat="server" Text="<<" CommandArgument="first"
                             CssClass="page-link" OnClick="Page_Changed" ></asp:LinkButton>
                        </li>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li class='<%#Eval("active")%>'>
                            <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Index") %>' CommandArgument='<%# Eval("Index") %>'
                            CssClass="page-link" OnClick="Page_Changed"  ></asp:LinkButton>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        <li class="page-item">
                            <asp:LinkButton  runat="server" Text=">>" CommandArgument="last"
                            CssClass="page-link" OnClick="Page_Changed"  ></asp:LinkButton>
                        </li>
                      </ul>
                   </FooterTemplate>
               </asp:Repeater>
                  
                    
                </nav>
              </div>
            </div>
          </div>
        </div>

	  	</div>
  	</section>
    <script>
        var urlName = '', title = '';
        if ($("#LbInd").attr("class") == "active") {
            title = "Beranda";
            urlName = 'informasi-pengadaan';
        }
        if($("#LbEn").attr("class") == "active")
        {
            title = "Home";
            urlName = 'procurement-information';
        }
        $("#htitle").text(title);
        history.pushState({}, null,urlName);
    </script>
</asp:Content>
