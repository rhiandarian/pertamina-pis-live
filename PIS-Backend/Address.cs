﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIS_Backend
{
    public class Address
    {
        public string Country
        {
            get;
            set;
        }
        public string State
        {
            get;
            set;
        }
        public string City
        {
            get;
            set;
        }
    }
}