﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class layanan_kami : System.Web.UI.Page
    {
        General all = new General();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {

                loadContent();
                loadpage();
               
            }
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"].ToString() == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
        public void loadpage()
        {
            editClassLang();
            bool langEn = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? true : false;
            linkTitle.InnerText = langEn ? "Home" : linkTitle.InnerText;
            link1.InnerText = langEn ? "Business Activity" : link1.InnerText;
            link2.InnerText = langEn ? "Our Services" : link2.InnerText;

            htitle.InnerText = link2.InnerText.ToUpper();

            Layanan ly = new Layanan();
            DataTable dt = ly.GetPageData();
            imgHeader.Src = dt.Rows[0]["Header"].ToString();
            contentPage.InnerHtml = langEn ? dt.Rows[0]["content_en"].ToString().Replace("<p><br></p>","") : dt.Rows[0]["content"].ToString().Replace("<p><br></p>","");
        }
        public void loadContent()
        {
            bool langEn = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? true : false;
            Layanan ly = new Layanan();
            DataTable dt = ly.Select("*");
            if(dt.Rows.Count > 0)
            {
                string contentlayanan = "";
                string description = langEn ? "description_en" : "description";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    
                    if(i % 2 == 0)
                    {
                        
                        string pcontent = all.paragraph(dt.Rows[i][description].ToString());
                        string img = "<img src=" + all.stripped(dt.Rows[i]["img_file"].ToString()) +" class=" + all.stripped("img-fluid") + " alt=" + all.stripped("...") + ">";
                        string boximage = all.div(img,"","box-image box-pis");
                        string order2 = all.div(boximage, "", "col-lg-6 col-md-6 col-12 align-self-center order-2 order-lg-1 order-md-1");
                        string order1 = all.div(pcontent, "", "col-lg-6 col-md-6 col-12 align-self-center order-1 order-lg-2 order-md-2");
                        string collg6 = order2+" "+order1;
                        string row = all.divRow(collg6);
                        string collg12 = all.div(row, "", "col-lg-12");
                        contentlayanan += collg12;
                    }
                    else
                    {
                        string pcontent = all.paragraph(dt.Rows[i][description].ToString());
                        string img = "<img src=" + all.stripped(dt.Rows[i]["img_file"].ToString()) + " class=" + all.stripped("img-fluid") + " alt=" + all.stripped("...") + ">";
                        string boximage = all.div(img, "", "box-image box-pis");
                        string col1 = all.div(pcontent, "", "col-lg-6 col-md-6 col-12 align-self-center");
                        string col2 = all.div(boximage, "", "col-lg-6 col-md-6 align-self-center");
                        string collg6 = col1 + " " + col2;
                        string row = all.divRow(collg6);
                        string collg12 = all.div(row, "", "col-lg-12");
                        contentlayanan += collg12;
                    }
                }
                content.InnerHtml = contentlayanan;
            }
           
        }
    }
}