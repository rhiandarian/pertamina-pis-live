﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Configuration;

namespace PIS_Backend
{
    public partial class loginadmin : System.Web.UI.Page
    {
        SqlConnection con = Connection.conn();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            
                recaptcha.Attributes["data-sitekey"] = ConfigurationManager.AppSettings["recaptcha-site-key"].ToString();
                

            }
        }
        protected bool AuthFirst()
        {
            Users u = new Users();
            u.Username = Username.Text;
            u.Password = Password.Text;

            
                return u.AuthUserLogin();
            
        }

        protected void BtnLogin_Click(object sender, EventArgs e)
        {
         
            if (!AuthFirst())
            {

                showValidationMessage("Username atau Password salah");

            }
            else
            {
                Users u = new Users();
                u.Username = Username.Text;
                u.Password = Password.Text;

                if (!u.validInputUsername(u.Username))
                {
                    showValidationMessage("Username tidak valid");
                }
                else
                {
                    if (ValidateRecaptcha())
                    {
                        
                        DataTable dt = u.LoginUser();
                        if (dt.Rows[0]["Password"].ToString() == u.EncryptPassword(Password.Text))
                        {
                            getip(Convert.ToInt32(dt.Rows[0]["Id"].ToString()));
                            Session.Add("UserID", dt.Rows[0]["Id"].ToString());
                            Session.Add("Username", dt.Rows[0]["User_name"].ToString());
                            Session.Add("Level", dt.Rows[0]["Level_Id"].ToString());
                            Session.Add("LevelType", dt.Rows[0]["Level"].ToString());

                            Response.Redirect("admin.aspx");
                        }
                        else
                        {
                            showValidationMessage("Password salah");
                        }

                        LblErrorMessage.Visible = false;
                    }
                    else
                    {
                        showValidationMessage("Recaptcha salah");
                    }
                    
                }

            }
        }
        public void showValidationMessage(string text)
        {
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = text;
        }
        public bool ValidateRecaptcha()
        {


            string Response = Request["g-recaptcha-response"];//Getting Response String Append to Post Method
            bool Valid = false;
            //Request to Google Server
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create
            (" https://www.google.com/recaptcha/api/siteverify?secret="+ ConfigurationManager.AppSettings["recaptcha-secret-key"].ToString() + "&response=" + Response);
            try
            {
                //Google recaptcha Response
                using (WebResponse wResponse = req.GetResponse())
                {

                    using (StreamReader readStream = new StreamReader(wResponse.GetResponseStream()))
                    {
                        string jsonResponse = readStream.ReadToEnd();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        MyObject data = js.Deserialize<MyObject>(jsonResponse);// Deserialize Json

                        Valid = Convert.ToBoolean(data.success);
                    }
                }

                return Valid;
            }
            catch (WebException ex)
            {
                throw ex;
            }
        }
        public class MyObject
        {
            public string success { get; set; }


        }
        public void getip(int user_id)
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            string mainIP = ipAddress[0];
            string action = "Login";

            ModelData Db = new ModelData();
            string query = $@"insert into current_log (ip_address,browser,created_at,action,user_id) values('{mainIP}','{GetBrowserDetails()}','{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}','{action}','{user_id}')";
            Db.setData(query); 
        }

        public static string DateNow()
        {
            TimeZoneInfo timeZoneInfo;
            DateTime dateTime;
            //Set the time zone information to US Mountain Standard Time 
            timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            //Get date and time in US Mountain Standard Time 
            dateTime = TimeZoneInfo.ConvertTime(DateTime.Now, timeZoneInfo);
            //Print out the date and time
            return dateTime.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
    }
}