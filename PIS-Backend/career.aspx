﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="career.aspx.cs" Inherits="PIS_Backend.career" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
					#footer-top-border{
						margin-top:-4px !important;
					}
    </style>
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			    <div class="row">
					    <nav aria-label="breadcrumb">
					      <ol class="breadcrumb">
					        <li class="breadcrumb-item"><a href="Index.aspx" id="li_1" runat="server">Home</a></li>
					        <li class="breadcrumb-item active" aria-current="page" id="li_2" runat="server">Karier</li>
					      </ol>
					    </nav>
					    <div class="col-12 page-title">
						    <h3 id="header_text" runat="server">Karier</h3>
					    </div>
			    </div>
  		    </div>
			<div class="col-lg-12 p-0 mt-4 banner-image"> 
                <img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section> 

    <section class="career">
  		<div class="container">
	  		<div class="row">
	  			<div class="col-12 mb-4 title">
	  				<div id="content" runat="server"  >
                 
	                </div> 
                  <div class="col-12 mb-4 desc">
                         <div id="content_disclaimer" runat="server" class="col disclaimer box-pis-right bg-gray p-4" >
                 
	                    </div>  
                        <a  href="mailto:HCPIS@pertamina.com" id="mail_lamaran" runat="server" class="btn btn-radius-pis bg-red mt-4">Kirim Lamaran Kerja</a>
	  		      </div> 

	  	    </div>
  	    </div>

    </section>
	<section class="internship career-section pb-5 pt-5" id="magang_section" runat="server">
      <div class="bg-overlay">
        <div class="container">
        <div class="row">
          <div class="col-12">
               <div id="content_magang" runat="server"  >
                 
	           </div>  
            
            <a  href="mailto:HCPIS@pertamina.com" id="mail_magang" runat="server" class="btn btn-radius-pis bg-red mt-4">Permohonan Magang</a>
          </div>
        </div>
      </div>
      </div>
      
    </section>
    
    <%--<div id="content" runat="server"  >--%>
                 
	<%--</div>--%>  
    <script>
        var urlName = '';
        if ($("#LbInd").attr("class") == "active") {
            urlName = 'karier';
        }
        if($("#LbEn").attr("class") == "active")
        {
            urlName = 'career';
        }
        history.pushState({}, null,urlName);
    </script>
</asp:Content>
