﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace PIS_Backend
{ // ini model presentasi
    public class Presentasi:ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName = "Presentasi";

        string viewName = "View_Presentasi";

        public int PageSize = 3;

        public int pageID = 24;

        General all = new General();

        public string folder = "Persentation";

        public string listPage = "AdminPresentasi.aspx";

        public string savePage = "SavePresentasi.aspx";

        public string title;

        public string title_en;

        public string url;

        public string cover;

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string TitleEn
        {
            get { return title_en; }
            set { title_en = value; }
        }


        public string Url
        {
            get { return url; }
            set { url = value; }
        }


        public string Cover
        {
            get { return cover; }
            set { cover = value; }
        }

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        public DataTable Select(string field, string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            string Query = Db.SelectQuery(field, viewName, conditions);

            DataTable dt = Db.getData(Query);

            return dt;
        }

        public PagesData getPage()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = " + pageID);

            PagesData p = new PagesData();
            p.Header = dt.Rows[0]["Header"].ToString();
            p.Content = dt.Rows[0]["content"].ToString();
            p.Content_En = dt.Rows[0]["content_en"].ToString();

            return p;

        }
        public DataTable LoadData(int PageIndex, bool langEn)
        {
            string lang = langEn ? "En" : "Ind";
            SqlCommand cmd = new SqlCommand("GetAllPresentasi", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PageIndex", PageIndex);
            cmd.Parameters.AddWithValue("@PageSize", PageSize);
            cmd.Parameters.AddWithValue("@Lang", lang);


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public string validate()

        {
            if (title == "")
            {
                return "Judul harus dalam bahasa Indonesia diisi";
            }
            if (title_en == "")
            {
                return "Judul dalam bahasa Inggris harus diisi";
            }
            if (url != "")
            {
                if (!all.PdfFileType(url))
                {
                    return "File Presentasi harus PDF";
                }
            }
            if (cover != "")
            {
                if (!all.ImageFileType(cover))
                {
                    return "Masukan file foto";
                }
            }
            return "Valid";
        }
        public string Insert(string mainIP, string browserDetail)
        {
            
            string validation = validate();
            if (validation != "Valid")
            {
                return validation;
            }
           if(url == "")
            {
                return "File Presentasi harus di Upload";
            }
            if (cover == "")
            {
                return "File Cover harus di Upload";
            }
            string field = "title, title_en, url, cover, ";
            string value = "'" + title + "', '" + title_en + "', '" + url + "', '"+folder+"/" + cover + "', ";
            Db.Insert(tableName, field, value, dateNow, user_id,mainIP,browserDetail);

            return "success";
        }
        public string Update(int Id,string mainIP, string browserDetail, string cond = null)
        {
            string validation = validate();
            if (validation != "Valid")
            {
                return validation;
            }
            string newRecord = " title = '" + title + "', ";
            newRecord += " title_en = '" + title_en + "', ";
            newRecord += url != "" ? " url = '" + url + "', " : "";
            newRecord += cover != "" ? " cover = '" + folder + "/" + cover + "', " : "";

            cond = cond == null ? "" : cond;
            Db.Update(tableName, newRecord, dateNow, user_id, cond,mainIP,browserDetail,Id);

            return "success";
        }
        public void Delete(string id, string mainIP, string browserDetail,string condition = null)
        {
            string conditions = condition != null ? condition : "";

            Db.Delete(tableName, dateNow, user_id, conditions,Convert.ToInt32(id),mainIP,browserDetail);
        }

    }
}