﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="UploadAlbum.aspx.cs" Inherits="PIS_Backend.UploadAlbum" EnableEventValidation="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid" style="background-color:ghostwhite">
        <div class="row pt-5">
             <div class="col text-center">
                 <h1>ALBUM</h1>
             </div>
         </div><hr />
         <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
        <div class="row pt-5">
             <div class="col">
                 <form>
                      <div class="form-group">
                        <label for="title"><b>title</b></label>
                        <asp:TextBox ID="title" cssClass="form-control" placeholder="Input Title Images" runat="server"></asp:TextBox>
                      </div>
                     <div class="form-group">
                         <label for="FileUpload1"><b>Image</b></label>
                         <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                      </div>
                      <div class="form-group">
                        <asp:Button ID="BtnSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="BtnSave_Click"     />
                    </div>
                      
                </form>
             </div>
             
         </div>
        <div class="text-right">
             <asp:LinkButton ID="LinkButtonHome" runat="server" CssClass="btn btn-info" OnClick="returnHome"><i class="fa fa-home"></i></asp:LinkButton>
         </div>
    </div>
</asp:Content>
