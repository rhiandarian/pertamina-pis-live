﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

namespace PIS_Backend
{
    public partial class EditVideos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadData();


            }
        }
        public void LoadData()
        {
            if (Request.QueryString["ID"] == null)
            {
                Response.Redirect("AdminVideos.aspx");
            }
            int Id = Convert.ToInt32(Request.QueryString["ID"]);
            string select = "title, content, video_url, img_url, category, embed_url";
            string WhereId = "ID = " + Id;
            Videos vd = new Videos();

            DataTable dt = vd.Select(select, WhereId);
            title.Text = dt.Rows[0]["title"].ToString();
            content.InnerHtml = dt.Rows[0]["content"].ToString();
            videoUrl.Text = dt.Rows[0]["video_url"].ToString();
            imageUrl.Text = dt.Rows[0]["img_url"].ToString();
            txtCategory.Text = dt.Rows[0]["category"].ToString();
            embedUrl.Text = dt.Rows[0]["embed_url"].ToString();
        }
        public void showErrorMessage(string txtMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = txtMessage;

            LoadData();
        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int Id = Convert.ToInt32(Request.QueryString["ID"]);

                Videos vd = new Videos();
                vd.Title = title.Text;
                vd.Content = content.InnerHtml;
                vd.Video_url = videoUrl.Text;
                vd.Img_url = imageUrl.Text;
                vd.Category = txtCategory.Text;
                vd.Embed_url = embedUrl.Text;

                string conditions = " ID = " + Id;

                string UpdateStatus = vd.Update(conditions);

                if (UpdateStatus != "success")
                {
                    showErrorMessage(UpdateStatus);
                }
                else
                {
                    Response.Redirect("AdminVideos.aspx");
                }
            }
            catch(Exception ex)
            {
                showErrorMessage(ex.Message.ToString());
            }
            
        }
    }
}