﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class Edit_Struktur_organisasi : System.Web.UI.Page
    {
        string folderHeader = "HeaderSO";
        string folderDownload = "documents";
        General all = new General();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "19"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                successdiv.Visible = false;
                LoadData();

            }
        }
        public void LoadData()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 12 ");
            content.InnerHtml = dt.Rows[0]["content"].ToString();
            content_en.InnerHtml = dt.Rows[0]["content_en"].ToString();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                PagesData pd = new PagesData();
                string validation = "";
                bool HeaderExsist = false;
                bool validheader = true;
                bool DownloadExsist = false;
                bool validDownload = true;
                if(FileUpload1.FileName != "")
                {
                    HeaderExsist = true;
                    pd.Header = folderHeader + "/" + FileUpload1.FileName;
                    if(!all.ImageFileType(FileUpload1.FileName))
                    {
                        validheader = false;
                    }

                }
                pd.Content = content.InnerText;
                pd.Content_En = content_en.InnerText;
                if(FileUpload2.FileName != "")
                {
                    DownloadExsist = true;
                    pd.Others = folderDownload + "/" + FileUpload2.FileName;
                    
                }
                if(validheader == false)
                {
                    validation = pd.validationHeaderImage;
                }
                if(validDownload == false)
                {
                    validation = pd.validationPdfMessage;
                }
                if(validation == "")
                {
                    if(HeaderExsist == true)
                    {
                        UploadHeader(FileUpload1.PostedFile.FileName);
                    }
                    if(DownloadExsist == true)
                    {
                        UploadPdf(FileUpload2.PostedFile.FileName);
                    }
                    string cond = " ID = 12";
                    pd.Save(getip(),GetBrowserDetails(),12,cond);
                    successMessage(pd.successMessage);
                }
                else
                {
                    showValidationMessage(validation);
                }

            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void showValidationMessage(string textMessage)
        {
            successdiv.Visible = false;
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void successMessage(string textMessage)
        {
            LblErrorMessage.Visible = false;
            successdiv.InnerHtml = textMessage;
            successdiv.Visible = true;

        }
        public void UploadHeader(string fileUpload)
        {
            
            string fn = System.IO.Path.GetFileName(fileUpload);
            string SaveLocation = Server.MapPath(folderHeader) + "\\" + fn;

            FileUpload1.PostedFile.SaveAs(SaveLocation);
        }
        public void UploadPdf(string fileUpload)
        {
            string fn = System.IO.Path.GetFileName(fileUpload);
            string SaveLocation = Server.MapPath(folderDownload) + "\\" + fn;

            FileUpload2.PostedFile.SaveAs(SaveLocation);
        }
    }
}