﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class Dewan_Komisaris1 : System.Web.UI.Page
    {
        int ptid = 1; int pageID = 13; string EnPage = "Board of Commissioners.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindKomisaris();
                bindPage();
                if (Session["Lang"] != null && Session["Lang"].ToString() == "En")
                {
                    Response.Redirect(EnPage);
                }
            }
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
        
        public void BindKomisaris()
        {
            string lang = "Ind";
            Person p = new Person();
            DataTable dt = p.allKomisaris();
            List<Person> komut = p.getUtama(dt, lang);
            ListViewKomut.DataSource = komut;
            ListViewKomut.DataBind();
            List<Person> allkomisaris = p.getAll(dt, lang);
            ListViewKomisaris.DataSource = allkomisaris;
            ListViewKomisaris.DataBind();
            ListViewModalKomisaris.DataSource = dt;
            ListViewModalKomisaris.DataBind();
        }
        public void bindPage()
        {
            editClassLang();

            PagesData pd = new PagesData();
            string cond = " ID = " + pageID;
            DataTable dt = pd.Select("*", cond);
            imgHeader.Src = dt.Rows[0]["Header"].ToString();
            content.InnerHtml = dt.Rows[0]["content"].ToString();
        }
    }
}