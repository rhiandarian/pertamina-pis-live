﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace PIS_Backend
{
    public class LaporanTahunan:ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName = "LaporanTahunan";

        string viewName = "ViewLaporanTahunan";

        public int PageSize = 8;

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        General all = new General();

        public string url;

        public int year;

        public string cover;

        public string orderBy;

        public string Url
        {
            get { return url; }
            set { url = value; }
        }
        public int  Year
        {
            get { return year; }
            set { year = value; }
        }

        public string Cover
        {
            get { return cover; }
            set { cover = value; }
        }
        public string OrderBy
        {
            get { return orderBy; }
            set { orderBy = value; }
        }
        public DataTable Select(string field, string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            if(orderBy != null)
            {
                Db.OrderBy = orderBy;
            }

            string query = Db.SelectQuery(field, viewName, conditions);


            DataTable dt = Db.getData(query);

            return dt;

        }
        public string validate()
        {
            if(year == 0)
            {
                return "tahun harus dipilih";
            }
            if(cover == null || cover == "")
            {
                return "cover harus dipilih";
            }
            else
            {
                if(!all.ImageFileType(cover))
                {
                    return "Masukan file foto";
                }
            }
            int yearNow = Convert.ToInt32(DateTime.Now.Year.ToString());
            if(year > yearNow)
            {
                return "Tahun tidak Valid";
            }
            if(url == null || url == "")
            {
                return "file harus di upload";
            }
            return "valid";
        }
        public string Insert(string mainIP,string browserDetail)
        {
            string valid = validate();
            if(valid != "valid")
            {
                return valid;
            }
            try
            {
                cover = "Laporan Tahunan/" + cover;
                SqlCommand cmd = new SqlCommand("SaveLaporanTahunan", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fileName", url);
                cmd.Parameters.AddWithValue("@year", year);
                cmd.Parameters.AddWithValue("@userID", user_id);
                cmd.Parameters.AddWithValue("@cover", cover);
                cmd.Parameters.AddWithValue("@mainIP", mainIP);
                cmd.Parameters.AddWithValue("@browserDetail", browserDetail); 
                cmd.Parameters.AddWithValue("@createdAt", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt.Rows[0]["Result"].ToString();
            }
            catch(Exception e)
            {
                //ErrorLogHistory(mainIP, browserDetail, "Insert", tableName, getUserName(user_id), user_id, e.Message.ToString());
                return e.ToString();
            }
            
        }
        public DataTable LoadlAll(int PageIndex,int PageSizes = 0, string Lang = "Ind")
        {
            PageSizes = PageSizes == 0 ? PageSize : PageSizes;

            SqlCommand cmd = new SqlCommand("GetAllLaporanTahunan", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PageIndex", PageIndex);
            cmd.Parameters.AddWithValue("@PageSize", PageSizes);
            cmd.Parameters.AddWithValue("@lang", Lang);


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public void Delete(string id,string mainIP,string browserDetail,string condition = null)
        {
            string conditions = condition != null ? condition : "";

            Db.Delete(tableName, dateNow, user_id, conditions,Convert.ToInt32(id),mainIP,browserDetail);
        }
    }
}