﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Pengguna</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                           <a href="UserList.aspx">Semua Pengguna</a>
                        </li>
                       
                        <li class="breadcrumb-item active">
                            <a href="Register.aspx"><strong>Registrasi Pengguna</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
            <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Registrasi Pengguna</h5>
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                            <form >
                      <div class="form-group">
                           <label for="emp_id"><b>No Pegawai</b></label>
                           <asp:TextBox ID="emp_id" cssClass="form-control" placeholder="Masukkan nomor pegawai" onkeypress="onlyNumberKey(event);" runat="server"></asp:TextBox>
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                            ControlToValidate="emp_id" runat="server"
                            ErrorMessage="Only Numbers allowed"
                            ValidationExpression="\d+$">
                        </asp:RegularExpressionValidator>
                       </div>
                     <div class="form-group">
                         <label for="username"><b>Nama Pengguna</b></label>
                         <asp:TextBox ID="username" cssClass="form-control" placeholder="Masukan Nama Pengguna" runat="server"></asp:TextBox>
                     </div>
                     <div class="form-group">
                        <label for="password"><b>Kata Sandi</b></label>
                        <asp:TextBox ID="password" TextMode="Password" cssClass="form-control" placeholder="Masukkan kata sandi pengguna" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                      </div>
                     <div class="form-group">
                        <label for="ConfirmPassword"><b>Konfirmasi Kata Sandi</b></label>
                        <asp:TextBox ID="ConfirmPassword" TextMode="Password" cssClass="form-control" placeholder="Ulang kata sandi pengguna" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                         
                      </div>
                      <div class="form-group">
                        <label for="email"><b>Alamat Email</b></label>
                        <asp:TextBox ID="email" cssClass="form-control" placeholder="Masukkan alamat email pengguna" runat="server"></asp:TextBox>
                         
                      </div>
                      
                      <div class="form-group">
                        <label for="ddlCategory"><b>Level Pengguna</b></label>
                          <asp:DropDownList ID="ddlLevel" runat="server" CssClass="form-control"></asp:DropDownList>
                      </div>
                     
                    
                    <div class="form-group">
                        <asp:Button ID="Save" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="BtnSave_Click"     />
                    </div>
                            </form>
                        </div>
                    </div>
                </div>
</asp:Content>
