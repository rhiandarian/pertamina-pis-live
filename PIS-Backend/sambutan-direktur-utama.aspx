﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="sambutan-direktur-utama.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.sambutan_direktur_utama" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a id="htitle" href="Index.aspx">Beranda</a></li>
					    <li class="breadcrumb-item"><a id="link1" runat="server" href="sambutan-direktur-utama.aspx">Manajemen</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="link2" runat="server">Sambutan Direktur Utama</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="title" runat="server">Sambutan Direktur Utama</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <section id="sambutan-direktur-utama" >
        <div class="container">
            <div id="content" runat="server" >

            </div>
        </div>
        
  		
  	</section>
    <script>
        var urlName = '', title = $("#htitle").text();
        
        if($("#LbEn").attr("class") == "active")
        {
            urlName = 'message-from-the-ceo';
            title = "Home";
        }
        if ($("#LbInd").attr("class") == "active") {
            urlName = 'sambutan-direktur-utama';
            title = "Beranda"
        }
        $("#htitle").text(title);
        history.pushState({}, null,urlName);
    </script>
</asp:Content>
