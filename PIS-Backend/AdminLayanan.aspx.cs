﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace PIS_Backend
{
    public partial class AdminLayanan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "9"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindGVLayanan();

            }
        }
        public void BindGVLayanan()
        {
            Layanan ly = new Layanan();
            DataTable dt = ly.Select("*");
            GridViewLayanan.DataSource = dt;
            GridViewLayanan.DataBind();
        }
        protected void Edit_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Layanan ly = new Layanan();
            Response.Redirect(ly.SavePage+"?ID="+id);
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void Delete_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Layanan ly = new Layanan();
            string cond = " ID = " + id;
            ly.Delete(id,getip(),GetBrowserDetails(),cond);
            BindGVLayanan();
        }

        protected void GridViewLayanan_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGVLayanan();
            GridViewLayanan.PageIndex = e.NewPageIndex;
            GridViewLayanan.DataBind();
        }
    }
}