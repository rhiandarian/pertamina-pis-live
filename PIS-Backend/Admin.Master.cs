﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;


namespace PIS_Backend
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        General all = new General();
        Gallery g = new Gallery();
        public int VideoCategoryID = Convert.ToInt32(ConfigurationManager.AppSettings["VideoCategoryID"]);
        public int FotoCategoryID = Convert.ToInt32(ConfigurationManager.AppSettings["FotoCategoryID"]);
        public int InfografisCategoryID = Convert.ToInt32(ConfigurationManager.AppSettings["InfografisCategoryID"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                idvideo.Value = VideoCategoryID.ToString();
                idfoto.Value = FotoCategoryID.ToString();
                idinfo.Value = InfografisCategoryID.ToString();
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                loginname.InnerText = Session["Username"].ToString();
                levelname.InnerText = Session["LevelType"].ToString();
                TxtLevel.Text = Session["Level"].ToString();
                 
                
            }
        }
        
        protected void LinkButtonLogout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Session.Clear();
            Response.Cookies.Clear();
            Response.Redirect("loginadmin.aspx");
        }

        protected void LinkBtnAddVideo_Click(object sender, EventArgs e)
        {
            Response.Redirect(g.adminSavePage + "?type=" + all.VideoCategoryID);
        }

        protected void LinkBtnAddFoto_Click(object sender, EventArgs e)
        {
            Response.Redirect(g.adminSavePage + "?type=" + all.FotoCategoryID);
        }

        protected void LinkBtnAddInfografis_Click(object sender, EventArgs e)
        {
            Response.Redirect(g.adminSavePage + "?type=" + all.InfografisCategoryID);
        }
    }
}