﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class KapalJson : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Response.Write(data());
            }
        }
        public string data()
        {
            General all = new General();
            Kapal k = new Kapal();
            DataTable dt = k.getLatest();
            PagesData pd = k.getPages();
            string print = "[";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string coma = i < dt.Rows.Count - 1 ? ", " : "";
                print += "{" + all.addJsonFieldValue("name", dt.Rows[i]["name"].ToString());
                print += all.addJsonFieldValue("Search", dt.Rows[i]["Search"].ToString());
                print += all.addJsonFieldValue("type", dt.Rows[i]["type"].ToString());
                print += all.addJsonFieldValue("built", dt.Rows[i]["built"].ToString());
                print += all.addJsonFieldValue("uuid", dt.Rows[i]["uuid"].ToString());
                print += all.addJsonFieldValue("img_file", dt.Rows[i]["img_file"].ToString());
                
                print += all.addJsonFieldValueInt("size",Convert.ToInt32(dt.Rows[i]["size"].ToString()));
                print += all.addJsonFieldValueInt("capacity", Convert.ToInt32(dt.Rows[i]["capacity"].ToString()),true)+"}"+coma;
            }
             print += "]";
            return print;
        }
    }
}