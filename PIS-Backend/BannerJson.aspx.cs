﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class BannerJson : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {//
            if(!IsPostBack)
            {
                Response.Write(data());
            }
        }

        public string data()
        {
            General all = new General();
            Banner b = new Banner();
            DataTable dt = b.Select("*");
            string print = "[";
            for(int i = 0; i < dt.Rows.Count; i++)
            {
                string coma = i < dt.Rows.Count - 1  ? ", " : "";
                print += "{" + all.addJsonFieldValue("active", dt.Rows[i]["active"].ToString());
                print += all.addJsonFieldValue("img_file", dt.Rows[i]["img_file"].ToString());
                print += all.addJsonFieldValue("title", dt.Rows[i]["title"].ToString());
                print += all.addJsonFieldValue("title_en", dt.Rows[i]["title_en"].ToString());
                print += all.addJsonFieldValue("description", dt.Rows[i]["description"].ToString());
                print += all.addJsonFieldValue("description_en", dt.Rows[i]["description_en"].ToString());
                print += all.addJsonFieldValue("buttonText", dt.Rows[i]["buttonText"].ToString());
                print += all.addJsonFieldValue("buttonTextEn", dt.Rows[i]["buttonTextEn"].ToString());
                print += all.addJsonFieldValue("url", dt.Rows[i]["url"].ToString(), true) + " }"+coma;
            }
            print += "]";
            return print;
        }
    }
}