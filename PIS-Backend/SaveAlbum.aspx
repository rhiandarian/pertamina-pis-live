﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="SaveAlbum.aspx.cs" Inherits="PIS_Backend.SaveAlbum" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Album</h2>
                    <ol class="breadcrumb">
                       <li class="breadcrumb-item">
                           <a href="ListFoto">Semua Foto</a> 
                        </li>
                        <li class="breadcrumb-item">
                            <asp:LinkButton ID="LinkButtonListAlbum" OnClick="LinkButtonListAlbum_Click" runat="server">Semua Album</asp:LinkButton>
                        </li>
                        <li class="breadcrumb-item active">
                           <a href="SaveAlbum.aspx"><strong>Unggah Album</strong></a> 
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Unggah Album.</small></h5>
                              <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                               
                                
                               
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <label for="title"><b>title</b></label>
                            <asp:TextBox ID="title" cssClass="form-control" placeholder="Masukkan judul foto dalam bahasa Indonesia" runat="server"></asp:TextBox>
                        </div>
                        <div class="col">
                            <label for="title_en"><b>title english</b></label>
                            <asp:TextBox ID="title_en" cssClass="form-control" placeholder="Masukkan judul foto dalam bahasa Inggris" runat="server"></asp:TextBox>
                        </div>
                    </div>
                        
                      </div>
                     <div class="form-group">
                         <label for="FileUpload1"><b>Image</b></label>
                         <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                      </div>
                      <div class="form-group">
                          <asp:Image ID="Image1" runat="server" />
                      </div>
                      <div class="form-group">
                        <asp:Button ID="Save" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="BtnSave_Click"     />
                          <asp:LinkButton ID="LinkButtonBack" CssClass="btn btn-info" OnClick="LinkButtonBack_Click" runat="server"> Kembali</asp:LinkButton>
                    </div>
            </div>
        </div>
    </div>
</asp:Content>
