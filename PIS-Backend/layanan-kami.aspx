﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="layanan-kami.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.layanan_kami" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
				<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a id="linkTitle" runat="server" href="Index.aspx">Beranda</a></li>
					    <li class="breadcrumb-item"><a href="" id="link1" runat="server">Aktivitas Bisnis</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="link2" runat="server">Layanan Kami</li>
					  </ol>
					</nav>
					<div class="col-lg-12 page-title">
						<h3 id="htitle" runat="server"></h3>
					</div>
				</div>
			</div>
      <div class="col-lg-12 p-0 mt-4 banner-image">
        <img src="" id="imgHeader" runat="server" class="img-fluid" alt="...">
      </div>
  	</section>
    <section id="layanan-kami">
  		<div id="contentPage" runat="server" class="container">
              <div  class="row mb-5 d-flex content">

              </div>
	  		

	  	</div>
        <div id="content" runat="server" >

	  			

	  			

	  		</div>
  	</section>
    <script>
        $("#ContentPlaceHolder1_content").hide();
        $("#ContentList").append($("#ContentPlaceHolder1_content").html());
        if ($("#ContentList").length == 0)
        {
            $("#ContentPlaceHolder1_content").show();
        }
        if($("#LbEn").attr("class") == "active")
        {
            history.pushState({}, null, "our-services");
        }
        else
        {
            history.pushState({}, null, "layanan-kami");
        }
    </script>
</asp:Content>
