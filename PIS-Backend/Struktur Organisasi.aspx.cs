﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class Struktur_Organisasi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Loadpage();
            }
        }
        public void editClassLang()
        {
            if(Session["Lang"] != null)
            {
                if(Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }
                
            }
            
            
        }
        public void Loadpage()
        {
            General all = new General();
            editClassLang();

            PagesData pd = new PagesData();
            
            DataTable dt = pd.Select("*", " ID = 12 ");
            imgHeader.Src = dt.Rows[0]["Header"].ToString();
            content.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content"].ToString();

            string filePdfText = "<span class=" + all.stripped("bi bi-file-pdf-fill") + "></span>"; //Lihat versi pdf
            filePdfText += Session["Lang"] != null && Session["Lang"] == "En" ? "See Image" : "Lihat Gambar";
            fileDownload.HRef = dt.Rows[0]["others"].ToString();
            fileDownload.InnerHtml = filePdfText;

            string tentang_pis = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "About PIS" : "Tentang PIS";
            string title = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Organization Structure and Company Group" : "Struktur Organisasi dan Kelompok Perusahaan";

            hTitle.InnerText = title;
            link1.InnerText = tentang_pis;
            link2.InnerText = title;

           

        }
    }
}