﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class admin_berkelanjutan : System.Web.UI.Page
    {
        string folderHeader = "Header";
        string folderDownload = "documents"; 
        public string PageUrl;
        public string Value { get; set; }
        General all = new General();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "28"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                successdiv.Visible = false;
                LoadData();

            }
        }
        public void LoadData()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 19 ");
            string decodecontent = Encoding.UTF8.GetString(Convert.FromBase64String(dt.Rows[0]["content"].ToString()));
            content.InnerHtml = decodecontent; 
            string content_ens = Encoding.UTF8.GetString(Convert.FromBase64String(dt.Rows[0]["content_en"].ToString()));
            content_en.InnerHtml = content_ens;
            PageUrl = dt.Rows[0]["url"].ToString();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                PagesData pd = new PagesData();
                string validation = "";
                bool HeaderExsist = false;
                bool validheader = true;
                bool DownloadExsist = false;
                bool validDownload = true;
                if(FileUpload1.FileName != "")
                {
                    HeaderExsist = true;
                    pd.Header = folderHeader + "/BERKELANJUTAN-" + FileUpload1.FileName;
                    if(!all.ImageFileType(FileUpload1.FileName))
                    {
                        validheader = false;
                    }

                }
                //pd.Content = content.InnerText;
                //if(FileUpload2.FileName != "")
                //{
                //    DownloadExsist = true;
                //    pd.Others = folderDownload + "/BERKELANJUTAN-" + FileUpload2.FileName;
                //    if(!all.PdfFileType(FileUpload2.FileName))
                //    {
                //        validDownload = false;
                //    }
                //}
                if(validheader == false)
                {
                    validation = pd.validationHeaderImage;
                }
                //if(validDownload == false)
                //{
                //    validation = "File must Pdf for Download";
                //}
                if(validation == "")
                {
                    if(HeaderExsist == true)
                    {
                        UploadFile(folderHeader, FileUpload1.PostedFile.FileName);
                    }
                    //if(DownloadExsist == true)
                    //{
                    //    UploadFile(folderDownload, FileUpload2.PostedFile.FileName);
                    //} 
                    pd.content = Convert.ToBase64String(Encoding.UTF8.GetBytes(content.Value));
                    pd.content_en = Convert.ToBase64String(Encoding.UTF8.GetBytes(content_en.Value));
                    string conditions = " ID = 19";
                    pd.Save(getip(),GetBrowserDetails(),19,conditions);
                    successMessage(pd.successMessage);
                }
                else
                {
                    showValidationMessage(validation);
                }

            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void showValidationMessage(string textMessage)
        {
            successdiv.Visible = false;
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void successMessage(string textMessage)
        {
            LblErrorMessage.Visible = false;
            successdiv.InnerHtml = textMessage;
            successdiv.Visible = true;

        }
        public void UploadFile(string folderName, string fileUpload)
        {
            
            string fn = System.IO.Path.GetFileName(fileUpload);
            string SaveLocation = Server.MapPath(folderName) + "\\" + "BERKELANJUTAN-" + fn;

            FileUpload1.PostedFile.SaveAs(SaveLocation);
        }
    }
}