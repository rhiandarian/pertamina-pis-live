﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class adminSaveKapal : System.Web.UI.Page
    {
        General all = new General();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "10"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadData();

            }
        }
        public void LoadData()
        {
            if (Request.QueryString["ID"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["ID"]);
                Kapal k = new Kapal();
                string conditions = " ID = " + Id;
                DataTable dt = k.Select("*", conditions);

                name.Text = dt.Rows[0]["name"].ToString();
                oil.Text = dt.Rows[0]["oil"].ToString();
                detail.InnerHtml = dt.Rows[0]["detail"].ToString();
                type.Text = dt.Rows[0]["type"].ToString();
                built.Text = dt.Rows[0]["built"].ToString();
                Image1.Attributes["src"] = dt.Rows[0]["img_file"].ToString();
                size.Text = dt.Rows[0]["size"].ToString();
                capacity.Text = dt.Rows[0]["capacity"].ToString();
                builtOn.Text = dt.Rows[0]["BuiltOn"].ToString();
                flag.Text = dt.Rows[0]["Flag"].ToString();
                callsign.Text = dt.Rows[0]["CallSign"].ToString();
                class_dnv_gl.Text = dt.Rows[0]["CLASS_DNV_GL"].ToString();
                particulars.Text = dt.Rows[0]["PARTICULARS"].ToString();
                loa.Text = dt.Rows[0]["LOA"].ToString();
                lbp.Text = dt.Rows[0]["LBP"].ToString();
                parallel_body_loaded.Text = dt.Rows[0]["PARALLEL_BODY_LOADED"].ToString();
                breadth_moulded.Text = dt.Rows[0]["BREADTH_MOULDED"].ToString();
                height_from_keel_to_mask.Text = dt.Rows[0]["HEIGHT_FROM_KEEL_TO_MASK"].ToString();
                depth_moulded.Text = dt.Rows[0]["DEPTH_MOULDED"].ToString();
                draft_summer.Text = dt.Rows[0]["DRAFT_SUMMER"].ToString();
                draft_ballast.Text = dt.Rows[0]["DRAFT_BALLAST"].ToString();
                tpc.Text = dt.Rows[0]["TPC"].ToString();
                airdraft_in_ballast__loaded_condition.Text = dt.Rows[0]["AIRDRAFT_IN_BALLAST__LOADED_CONDITION"].ToString();
                total_size_manifolds.Text = dt.Rows[0]["TOTAL_SIZE_MANIFOLDS"].ToString();
                distance_form_bow_to_centre_manifold.Text = dt.Rows[0]["DISTANCE_FORM_BOW_TO_CENTRE_MANIFOLD"].ToString();
                distance_form_stern_to_centre_manifold.Text = dt.Rows[0]["DISTANCE_FORM_STERN_TO_CENTRE_MANIFOLD"].ToString();
                distance_between_manifolds.Text = dt.Rows[0]["DISTANCE_BETWEEN_MANIFOLDS"].ToString();
                distance_form_ship_side_to_centre_manifold.Text = dt.Rows[0]["DISTANCE_FORM_SHIP_SIDE_TO_CENTRE_MANIFOLD"].ToString();
                distance_from_deck_tray_to_manifold.Text = dt.Rows[0]["DISTANCE_FROM_DECK_TRAY_TO_MANIFOLD"].ToString();
                cargo_tank_capacity.Text = dt.Rows[0]["CARGO_TANK_CAPACITY"].ToString();
                slop_tank_capacity.Text = dt.Rows[0]["SLOP_TANK_CAPACITY"].ToString();
                ballast_tank_capacity.Text = dt.Rows[0]["BALLAST_TANK_CAPACITY"].ToString();
                deadweight.Text = dt.Rows[0]["DEADWEIGHT"].ToString();
                tonnage_international.Text = dt.Rows[0]["TONNAGE_INTERNATIONAL"].ToString();
                coating.Text = dt.Rows[0]["COATING"].ToString();
                main_engine.Text = dt.Rows[0]["MAIN_ENGINE"].ToString();
                bow_thruster.Text = dt.Rows[0]["BOW_THRUSTER"].ToString();
                cranes.Text = dt.Rows[0]["CRANES"].ToString();
                cargo_pumps.Text = dt.Rows[0]["CARGO_PUMPS"].ToString();
                stripping_pump.Text = dt.Rows[0]["STRIPPING_PUMP"].ToString();
                ballast_pumps.Text = dt.Rows[0]["BALLAST_PUMPS"].ToString();
                max_load_rate_homogenous_cargo_per_manifold.Text = dt.Rows[0]["MAX_LOAD_RATE_HOMOGENOUS_CARGO_PER_MANIFOLD"].ToString();
                max_load_rate_homogenous_cargothrough_all_manifold.Text = dt.Rows[0]["MAX_LOAD_RATE_HOMOGENOUS_CARGOTHROUGH_ALL_MANIFOLD"].ToString();
                no_of_cargo_tanks.Text = dt.Rows[0]["NO_OF_CARGO_TANKS"].ToString();
                no_of_segregations.Text = dt.Rows[0]["NO_OF_SEGREGATIONS"].ToString();

            }
        }


        protected void BtnSave_Click(object sender, EventArgs e)
        {

            string randomtext = textrandom();
            try
            {
                Kapal kpl = new Kapal();
                kpl.NAME = name.Text;
                kpl.OIL = oil.Text;
                kpl.DETAIL = detail.InnerHtml;
                kpl.TYPE = type.Text;
                kpl.BUILT = built.Text;
                kpl.IMG_FILE = randomtext + FileUpload1.FileName;
                kpl.SIZE = Convert.ToInt32(size.Text);
                kpl.CAPACITY = Convert.ToInt32(capacity.Text);
                kpl.BUILTON = builtOn.Text;
                kpl.FLAG = flag.Text;
                kpl.CALLSIGN = callsign.Text;
                kpl.CLASS_DNV_GL = class_dnv_gl.Text;
                kpl.PARTICULARS = particulars.Text;
                kpl.LOA = loa.Text;
                kpl.LBP = lbp.Text;
                kpl.PARALLEL_BODY_LOADED = parallel_body_loaded.Text;
                kpl.BREADTH_MOULDED = breadth_moulded.Text;
                kpl.HEIGHT_FROM_KEEL_TO_MASK = height_from_keel_to_mask.Text;
                kpl.DEPTH_MOULDED = depth_moulded.Text;
                kpl.DRAFT_SUMMER = draft_summer.Text;
                kpl.DRAFT_BALLAST = draft_ballast.Text;
                kpl.TPC = tpc.Text;
                kpl.AIRDRAFT_IN_BALLAST__LOADED_CONDITION = airdraft_in_ballast__loaded_condition.Text;
                kpl.TOTAL_SIZE_MANIFOLDS = total_size_manifolds.Text;
                kpl.DISTANCE_FORM_BOW_TO_CENTRE_MANIFOLD = distance_form_bow_to_centre_manifold.Text;
                kpl.DISTANCE_FORM_STERN_TO_CENTRE_MANIFOLD = distance_form_stern_to_centre_manifold.Text;
                kpl.DISTANCE_BETWEEN_MANIFOLDS = distance_between_manifolds.Text;
                kpl.DISTANCE_FORM_SHIP_SIDE_TO_CENTRE_MANIFOLD = distance_form_ship_side_to_centre_manifold.Text;
                kpl.DISTANCE_FROM_DECK_TRAY_TO_MANIFOLD = distance_from_deck_tray_to_manifold.Text;
                kpl.CARGO_TANK_CAPACITY = cargo_tank_capacity.Text;
                kpl.SLOP_TANK_CAPACITY = slop_tank_capacity.Text;
                kpl.BALLAST_TANK_CAPACITY = ballast_tank_capacity.Text;
                kpl.DEADWEIGHT = deadweight.Text;
                kpl.TONNAGE_INTERNATIONAL = tonnage_international.Text;
                kpl.COATING = coating.Text;
                kpl.MAIN_ENGINE = main_engine.Text;
                kpl.BOW_THRUSTER = bow_thruster.Text;
                kpl.CRANES = cranes.Text;
                kpl.CARGO_PUMPS = cargo_pumps.Text;
                kpl.STRIPPING_PUMP = stripping_pump.Text;
                kpl.BALLAST_PUMPS = ballast_pumps.Text;
                kpl.MAX_LOAD_RATE_HOMOGENOUS_CARGO_PER_MANIFOLD = max_load_rate_homogenous_cargo_per_manifold.Text;
                kpl.MAX_LOAD_RATE_HOMOGENOUS_CARGOTHROUGH_ALL_MANIFOLD = max_load_rate_homogenous_cargothrough_all_manifold.Text;
                kpl.NO_OF_CARGO_TANKS = no_of_cargo_tanks.Text;
                kpl.NO_OF_SEGREGATIONS = no_of_segregations.Text;
                kpl.FileSizeInput = FileUpload1.PostedFile.ContentLength;


                if (Request.QueryString["ID"] == null)
                {
                  
                    string validationMessage = "";
                    if ((FileUpload1.PostedFile == null) && (FileUpload1.PostedFile.ContentLength == 0))
                    {
                        validationMessage = "File Harus di Upload";
                    }
                    if (!all.ImageFileType(FileUpload1.FileName))
                    {
                        validationMessage = "Masukan file foto";
                    }
                    
                    if (validationMessage == "")
                    {
                        string insertStatus = kpl.Insert(getip(),GetBrowserDetails());
                        if (insertStatus != "success")
                        {
                            showValidationMessage(insertStatus);
                        }
                        else
                        {
                            UploadImageKapal(randomtext);
                            Response.Redirect(kpl.listPage);
                        }
                    }
                    else
                    {
                        showValidationMessage(validationMessage);
                    }

                }
                else
                {
                    int Id = Convert.ToInt32(Request.QueryString["ID"]);
                    if(FileUpload1.FileName == "")
                    {
                        kpl.IMG_FILE = Image1.Attributes["src"].Replace("Kapal/","");
                    }
                    string UpdateStatus = kpl.Update(Id,getip(),GetBrowserDetails());
                    if (UpdateStatus != "success")
                    {
                        showValidationMessage(UpdateStatus);
                        LoadData();
                    }
                    else
                    {
                        if (FileUpload1.FileName != "")
                        {
                            UploadImageKapal(randomtext);
                        }
                        Response.Redirect(kpl.listPage);
                    }

                }
            }
            catch (Exception ex)
            {
                Kapal k = new Kapal();
                k.ErrorLogHistoryFull(getip(), GetBrowserDetails(), k.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString(), Request.QueryString["ID"]);
                showValidationMessage(ex.Message.ToString());
            }

        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void UploadImageKapal(string randomtext)
        {
            
            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string SetFolderBeforeResize = Server.MapPath("Kapal") + "\\" + "serize"+ randomtext + fn; //gambar yang akan diresize sesudah direze gambar akan dihapus
            string SetFolderAfterResize = Server.MapPath("Kapal") + "\\" + randomtext + fn;
            FileUpload1.PostedFile.SaveAs(SetFolderBeforeResize);
            ImageResize Iz = new ImageResize();
            Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "Armada-Kami", "Cut");
             
        }
        public string textrandom()
        {
            Random RNG = new Random();
            int length = 6;
            var rString = "";
            for (var i = 0; i < length; i++)
            {
                rString += ((char)(RNG.Next(1, 26) + 64)).ToString().ToLower();
            }
            return rString;
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
    }
}