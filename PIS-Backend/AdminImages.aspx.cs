﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class AdminImages : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindGVImages();
            }
        }
        public void BindGVImages(string txtSearch = null)
        {
            Image img = new Image();
            string field = "ID, title, Category_name";
            string conditions = txtSearch != null ? "title like '%" + txtSearch + "%' or Category_name like '%" + txtSearch + "%'" : "";
            DataTable dt = txtSearch != null ? img.Select(field, conditions) : img.Select(field);

            GridViewImages.DataSource = dt;
            GridViewImages.DataBind();
        }
        protected void btnSearchClick(object sender, EventArgs e)
        {
            BindGVImages(TxtSearch.Text);
        }
    }
}