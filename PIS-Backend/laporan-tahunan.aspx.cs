﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class laporan_tahunan : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                adminsec.Visible = false;
                BindLaporanKeuangan(1);
                LoadData();
            }
            LblIndex.Visible = true;
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
        public void LoadData()
        {
            editClassLang();

            link1.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Investor Relations" : link1.InnerText;
            link2.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Annual Report" : link2.InnerText;

            htitle.InnerText = link2.InnerText;
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 2 ");
            header.Src = dt.Rows[0]["Header"].ToString();
            content.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content"].ToString();
        }
        public void BindLaporanKeuangan(int PageIndex)
        {
            string lang = Session["Lang"] == null ? "Ind" : Session["Lang"].ToString();
            LblIndex.Text = PageIndex.ToString();
            LaporanTahunan lt = new LaporanTahunan();
            DataTable dt = lt.LoadlAll(PageIndex,lt.PageSize,lang);
            int totalRecord = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());
            ListViewLt.DataSource = dt;
            ListViewLt.DataBind();
            populatePaging(totalRecord, PageIndex);
          

        }
        public void Download(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string filename = lb.CommandArgument;
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.TransmitFile(Server.MapPath("~/Laporan Tahunan/" + filename));
            Response.End();
        }
        public void populatePaging(int TotalCount, int PageIndex)
        {
            string lang = Session["Lang"] == null ? "Ind" : Session["Lang"].ToString();
            LaporanTahunan lt = new LaporanTahunan();
            List<Paging> page = all.PopulatePaging(TotalCount,PageIndex,lt.PageSize);
            RptPaging.DataSource = page;
            RptPaging.DataBind();
            int totalpages = all.getTotalPaging(TotalCount, PageIndex, lt.PageSize);
            detailPaging.InnerHtml = all.getDetailPaging(lang, PageIndex, totalpages);
        }
        protected void Page_Changed(object sender, EventArgs e)
        {
            string commendArgument = (sender as LinkButton).CommandArgument.ToString();
            int pagesIndex = int.Parse(LblIndex.Text);
            if (commendArgument == "first")
            {
                if (pagesIndex > 1)
                {
                    pagesIndex--;
                    BindLaporanKeuangan(pagesIndex);
                }

            }
            else if (commendArgument == "last")
            {
                string lang = Session["Lang"] == null ? "Ind" : Session["Lang"].ToString();
                LaporanTahunan lt = new LaporanTahunan();
                DataTable dt = lt.LoadlAll(1, lt.PageSize, lang);


                int totalCount = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());

                List<Paging> pages = all.PopulatePaging(totalCount, 1, lt.PageSize);

                int limit = all.getTotalPaging(totalCount, 1, lt.PageSize);
                if (pagesIndex < limit)
                {
                    pagesIndex++;
                    BindLaporanKeuangan(pagesIndex);
                }

            }
            else
            {

                int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
                BindLaporanKeuangan(pageIndex);
            }

        }



    }
}