﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class komposisi_pemegang_saham : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindPage();
                Level l = new Level();
              
            }
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
        public void BindPage()
        {
            editClassLang();
            string currentPage = "komposisi-pemegang-saham.aspx";
            link1.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Stock Information" : link1.InnerText;
            link2.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Shareholders Composition" : link2.InnerText;
            title.InnerText = link2.InnerText.ToUpper();

            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 16");
            imgHeader.Src = dt.Rows[0]["Header"].ToString();
            content.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content"].ToString();
        }
    }
}