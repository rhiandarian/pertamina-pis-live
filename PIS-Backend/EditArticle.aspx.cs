﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

namespace PIS_Backend
{
    public partial class EditArticle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                BindCategory();
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LoadData();


            }
        }
        public void LoadData()
        {
            if(Request.QueryString["ID"] == null)
            {
                Response.Redirect("Articles.aspx");
            }
            int Id = Convert.ToInt32(Request.QueryString["ID"]);
            string select = "title, content, img_file, category_id, city";
            string conditions = "ID = " + Id;
            Article atc = new Article();
            DataTable dt = atc.Select(select,conditions);

            title.Text = dt.Rows[0]["title"].ToString();
            content.InnerHtml = dt.Rows[0]["content"].ToString();
            ddlCategory.SelectedValue = dt.Rows[0]["category_id"].ToString();
            city.Text = dt.Rows[0]["city"].ToString();
        }
        public void BindCategory()
        {
            Category ctg = new Category();

            ctg.Category_Type_Id = 1;

            string selectCategory = "ID, Category_name";

            DataSet dt = ctg.SelectList(selectCategory,"and");

            ddlCategory.DataTextField = dt.Tables[0].Columns["Category_name"].ToString();
            ddlCategory.DataValueField = dt.Tables[0].Columns["ID"].ToString();
            ddlCategory.DataSource = dt.Tables[0];
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, "Pilih Category");
            ddlCategory.SelectedIndex = 0;



        }
        public bool ImageFileType(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            switch (ext.ToLower())
            {
                case ".gif":
                    return true;
                case ".jpg":
                    return true;
                case ".jpeg":
                    return true;
                case ".png":
                    return true;
                default:
                    return false;
            }
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            bool exsist = false;
            bool validUpload = true;
            Article atc = new Article();
            if(title.Text != "")
            {
                atc.Title = title.Text;
                exsist = true;
            }  
            if(content.InnerHtml != "")
            {
                atc.Content = content.InnerHtml;
                exsist = true;
            }
            if(city.Text != "")
            {
                atc.City = city.Text;
                exsist = true;
            }
            if(FileUpload1.FileName != "")
            {
                atc.Img_File = "Article/" + FileUpload1.FileName;
                exsist = true;
                string SaveLocation = "";
                if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
                {
                    string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                    SaveLocation = Server.MapPath("Article") + "\\" + fn;
                    try
                    {
                        if (ImageFileType(FileUpload1.FileName))
                        {
                            FileUpload1.PostedFile.SaveAs(SaveLocation);
                        }
                        else
                        {
                            validUpload = false;
                            LblErrorMessage.Visible = true;
                            LblErrorMessage.Text = "File Must be Image";
                        }





                    }
                    catch (Exception ex)
                    {
                        validUpload = false;
                        LblErrorMessage.Visible = true;
                        LblErrorMessage.Text = ex.Message;
                    }
                }
            }
            if(ddlCategory.SelectedValue != "Pilih Category")
            {
                exsist = true;
                atc.Category_Id = Convert.ToInt32(ddlCategory.SelectedValue);
            }
            if(exsist)
            {
                if(validUpload)
                {
                    int Id = Convert.ToInt32(Request.QueryString["ID"]);
                    string conditions = "ID = " + Id;
                    atc.Update(conditions);

                    Response.Redirect("Articles.aspx");
                }
            } 
            else
            {
                Response.Redirect("Articles.aspx");
            }
        }
    }
}