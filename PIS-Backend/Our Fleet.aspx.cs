﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class Our_Fleet : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Kapal k = new Kapal();
                LoadKapal(1);
                LoadArmada(1);
                LoadPage();
                btnUpload.Visible = false;
                if (Session["Lang"] == null || Session["Lang"].ToString() == "Ind")
                {
                    Response.Redirect(k.IndPage);
                }
            }
            LblIndex.Visible = false;
        }
        protected void ChangeLang(object sender, EventArgs e)
        {
            Kapal k = new Kapal();
            if (Session["Lang"] == null)
            {
                Session.Add("Lang", "En");
            }
            else
            {
                Session["Lang"] = "En";
            }
            Response.Redirect(k.EnPage);

        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }
        }
        public void LoadPage()
        {
            editClassLang();
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 1");
            contentArmada.InnerHtml = dt.Rows[0]["content_en"].ToString();
        }
        public void LoadArmada(int PageIndex)
        {
            Armada amd = new Armada();
            DataTable dt = amd.GetAll(PageIndex);
            ListViewArmada.DataSource = dt;
            ListViewArmada.DataBind();
            int totalCount = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());
            populatePagingArmada(totalCount, PageIndex, amd.PageSizes);
        }
        public void LoadKapal(int PageIndex)
        {
            Kapal kpl = new Kapal();
            DataTable dt = kpl.GetAll(PageIndex);
            LblIndex.Text = PageIndex.ToString();
            ListViewKapal.DataSource = dt;
            ListViewKapal.DataBind();
            ListViewModalArmada.DataSource = dt;
            ListViewModalArmada.DataBind();
            int RecordCount = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());
            populatePagingKapal(RecordCount, PageIndex, kpl.PageSizes);
        }
        public void populatePagingArmada(int RecordCount, int PageIndex, int PageSize)
        {
            List<Paging> pg = all.PopulatePaging(RecordCount, PageIndex, PageSize);
            RptArmada.DataSource = pg;
            RptArmada.DataBind();
            int totalPages = all.getTotalPaging(RecordCount, PageIndex, PageSize);
            DetailPagingKapal.InnerHtml = all.getDetailPaging("En", PageIndex, totalPages);


        }
        public void populatePagingKapal(int RecordCount, int PageIndex, int PageSize)
        {

            List<Paging> pg = all.PopulatePaging(RecordCount, PageIndex, PageSize);
            RptKapal.DataSource = pg;
            RptKapal.DataBind();
            DetailPagingKapal.InnerHtml = all.getDetailPaging("En", PageIndex, pg.Count);
        }
        public void EditKapal(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Response.Redirect("SaveKapalMilik.aspx?ID=" + id);
        }
        public void EditArmada(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Response.Redirect("SaveArmada.aspx?ID=" + id);
        }
        public void DownloadArmada(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string fileName = lb.CommandArgument;
            string filePath = "Armada\\" + fileName;

        }
        public void page_changed_kapal(object sender, EventArgs e)
        {
            DataShow.Text = "Kapal";
            int pagesIndex = int.Parse(LblIndex.Text);
            string commendArgument = (sender as LinkButton).CommandArgument.ToString();
            if (commendArgument == "first")
            {
                if (pagesIndex > 1)
                {
                    pagesIndex--;
                    LoadKapal(pagesIndex);
                }
            }
            else if (commendArgument == "last")
            {
                Kapal kpl = new Kapal();
                DataTable dt = kpl.GetAll(1);


                int totalCount = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());

                int limit = all.getTotalPaging(totalCount, 1, kpl.PageSizes);
                if (pagesIndex < limit)
                {
                    pagesIndex++;
                    LoadKapal(pagesIndex);
                }
            }
            else
            {

                int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
                LoadKapal(pageIndex);
            }
        }
        public void page_changed_Armada(object sender, EventArgs e)
        {
            string commendArgument = (sender as LinkButton).CommandArgument.ToString();
            DataShow.Text = "Armada";
            if (commendArgument == "first")
            {
                LoadArmada(1);
            }
            else if (commendArgument == "last")
            {
                Armada amd = new Armada();
                DataTable dt = amd.GetAll(1);


                int totalCount = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString());

                List<Paging> pages = all.PopulatePaging(totalCount, 1, amd.PageSizes);

                int totalPaging = pages.Count;

                LoadArmada(totalPaging);

            }
            else
            {

                int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
                LoadArmada(pageIndex);
            }
        }

        protected void ListViewArmada_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                //if (Session["Level"].ToString() != "1")
                //{
                LinkButton l = (LinkButton)e.Item.FindControl("LbEditArmada");
                l.Visible = false;
                // }
            }
        }

        protected void ListViewKapal_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                //if (Session["Level"].ToString() != "1")
                //{
                LinkButton l = (LinkButton)e.Item.FindControl("LinkButtonEdit");
                l.Visible = false;
                //}
            }
        }
    }
}