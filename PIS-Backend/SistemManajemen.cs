﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace PIS_Backend
{
    public class SistemManajemen:ModelData
    {
        SqlConnection con = Connection.conn();

        public string tableName = "SistemManajemen";

        string viewName = "View_SistemManajemen";

        public int PageSize = 8;

        public int PageID = 15;

        public string listPage = "admin-sistem-manajemen.aspx";

       

        public string savePage = "unggah-sistem-manajemen.aspx";

        public string folder = "Standard";

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        General all = new General();

        public string nama;

        public string nama_en;

        public DateTime validasiFrom;

        public DateTime validasiUntil;

        public string pemberi;

        public string pemberi_en;

        public string img_file;

        public string Nama
        {
            get { return nama; }
            set { nama = value; }
        }
        public string Nama_En
        {
            get { return nama_en; }
            set { nama_en = value; }
        }

        public DateTime ValidasiFrom
        {
            get { return validasiFrom; }
            set { validasiFrom = value; }
        }
        public DateTime ValidasiUntil
        {
            get { return validasiUntil; }
            set { validasiUntil = value; }
        }

        public string Pemberi
        {
            get { return pemberi; }
            set { pemberi = value; }
        }

        public string Pemberi_En
        {
            get { return pemberi_en; }
            set { pemberi_en = value; }
        }

        public string Img_File
        {
            get { return img_file; }
            set { img_file = value; }
        }
        
        public DataTable Select(string field, string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            string Query = Db.SelectQuery(field, viewName, conditions);
            DataTable dt = Db.getData(Query+" Order by ValidasiFrom desc");

            return dt;
        }
        public string Validation()
        {
            if (nama == "")
            {
                return "Nama Sertifikat dalam bahasa Indonesia harus di isi";
            }
            if (nama_en == "")
            {
                return "Nama Sertifikat dalam bahasa Inggris harus di isi";
            }

            if (pemberi == "")
            {
                return "Pemberi Sertifikat dalam bahasa Indonesia harus di isi";
            }
            if (pemberi_en == "")
            {
                return "Pemberi Sertifikat dalam bahasa Inggris harus di isi";
            }
            if (img_file != "")
            {
                if (!all.ImageFileType(img_file))
                {
                    return "File harus Foto";
                }
            }



            return "Valid";
        }
        public string Insert(string mainIP, string browserDetail)
        {
            string validate = Validation();
            if (validate != "Valid")
            {
                return validate;
            }
            if (img_file == "")
            {
                return "File harus di Upload";
            }
            string field = "Nama, Nama_En, ValidasiFrom, ValidasiTo, Pemberi, Pemberi_En, img_file, ";
            string value = "'" + nama + "', '" + nama_en + "', '" + validasiFrom + "', '" + validasiUntil + "', '" + pemberi + "', '" + pemberi_en + "', '" + folder + "/" + img_file + "', ";


            Db.Insert(tableName, field, value, dateNow, user_id,mainIP,browserDetail);

            return "success";

        }
        public DataTable showSO()
        {
            SqlCommand cmd = new SqlCommand("ShowSO", con);
            cmd.CommandType = CommandType.StoredProcedure;


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
        public string Update(int id, string mainIP, string browserDetail, string conditions = null)
        {
            string validate = Validation();
            if (validate != "Valid")
            {
                return validate;
            }
            string newRecord = "";
            newRecord += " Nama = '" + nama + "', ";
            newRecord += " Nama_En = '" + nama_en + "', ";
            newRecord += " ValidasiFrom = '" + validasiFrom + "', ";
            newRecord += " ValidasiTo = '" + validasiUntil + "', ";
            newRecord += " Pemberi = '" + pemberi + "', ";
            newRecord += " Pemberi_En = '" + pemberi_en + "', ";
            newRecord += img_file != "" ? " img_file = '" + folder + "/" + img_file + "', " : "";

            conditions = conditions == null ? "" : conditions;

            Db.Updates(tableName, newRecord, conditions, dateNow, user_id,mainIP,browserDetail,id);

            return "success";
        }

        public void Delete(string id,string mainIP,string browserDetail,string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;
            Db.Delete(tableName, dateNow, user_id, conditions,Convert.ToInt32(id),mainIP,browserDetail);
        }
    }
}