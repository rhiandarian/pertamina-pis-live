﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="Search.aspx.cs" Inherits="PIS_Backend.Search" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <main id="main">

  	<!-- Top Main -->
  	<section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a id="linkTitle" runat="server" href="Index.aspx">Beranda</a></li>
					    <li class="breadcrumb-item active" id="link1" runat="server" aria-current="page">Pencarian</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="title" runat="server">Pencarian</h3>
					</div>
				</div>
  		</div>
  	</section>


  	<!-- -->
  	<section class="pencarian">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div class="col-12 content">
	  				<div class="row">
	  					<div class="col-12 mb-4">
                              <asp:TextBox ID="SearchValue" runat="server" CssClass="form-control"></asp:TextBox>
	  					</div>
	  					<div class="col-12">
	  						<p id="searchResult" runat="server" class="blue_pis"></p>
	  					</div>
                         <asp:ListView ID="ListViewSearch" runat="server">
                             <ItemTemplate>
                                 <div class="col-xs-12 col-sm-12 box-content">
			  				        <div class="row pt-3 pb-3">
			  					        <a href='<%# Eval("link") %>' class="col-12 content-list">
			  						        <p><%# Eval("Dates") %><strong class="ml-3"><%# Eval("Category") %></strong></p>
			  						        <p><%# Eval("title") %></p>
			  					        </a>
			  				        </div>
			  			        </div>
                             </ItemTemplate>
                         </asp:ListView>
	  					
			  			

	  				</div>

	  				<!---Pagination-->
				  	<div class="row mt-4">
		        	<div class="col-12 pagination-box">
		        		<div class="row">
		        			<div class="col-xs-12 col-sm-6">
				        				<p id="detailSearch" runat="server"></p>
				        			</div>
				        			<div class="col-xs-12 col-sm-6 text-right">
				        				<nav aria-label="...">
												  <ul class="pagination  justify-content-end">
												    <asp:Repeater ID="RptSrch" runat="server">
                                  <HeaderTemplate>
                      
										 <ul class="pagination justify-content-end">
                                               <li class="page-item">
                                                    <asp:LinkButton  runat="server" Text="<<" CommandArgument="first"
                                                        CssClass="page-link" OnClick="Page_Changed" ></asp:LinkButton>
                                                </li>
										    
                                  </HeaderTemplate>
                                  <ItemTemplate>
                                      <li class='<%#Eval("active")%>'>
                                        <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Index") %>'  CommandArgument='<%# Eval("Index") %>'
                                        CssClass="page-link" OnClick="Page_Changed" ></asp:LinkButton>
                                      </li>
                                  </ItemTemplate>
                                  <FooterTemplate>
                                      <li class="page-item">
                                              <asp:LinkButton  runat="server" Text=">>"  CommandArgument="last"
                                            CssClass="page-link" OnClick="Page_Changed" ></asp:LinkButton>
                                       </li>
                                     </ul>
                      
                                  </FooterTemplate>
                              </asp:Repeater>
												  </ul>
												</nav>
				        			</div>
		        		</div>
		        	</div>
		        </div>

	  			</div>
	  		</div>
	  	</div>
  	</section>

  </main>
    <script>
        $('#ContentPlaceHolder1_SearchValue').keyup(function (e) {
            if (e.keyCode == 13) {
                var keyword = $(this).val();
                window.location = "Search.aspx?search=" + keyword;
            }
        });
    </script>
</asp:Content>
