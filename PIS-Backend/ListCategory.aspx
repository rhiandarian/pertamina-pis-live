﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="ListCategory.aspx.cs" Inherits="PIS_Backend.ListCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function confirmDelete(btnDelete) {
            if (btnDelete.dataset.confirmed) {
                // The action was already confirmed by the user, proceed with server event
                btnDelete.dataset.confirmed = false;
                return true;
            } else {
                // Ask the user to confirm/cancel the action
                event.preventDefault();
                swal({
                    title: 'Are you sure?',
                    text: "This Category will be Removed!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                })
                .then(function (isConfirm) {
                    if (isConfirm.value) {
                        // Set data-confirmed attribute to indicate that the action was confirmed
                        btnDelete.dataset.confirmed = true;
                        // Trigger button click programmatically
                        btnDelete.click();
                    }
                    
                }).catch(function (reason) {
                    // The action was canceled by the user
                });
            }
        }
    </script>
    <style>
        

       #ContentPlaceHolder1_GVCategory tr:nth-child(even){background-color: white;}

       #ContentPlaceHolder1_GVCategory tr:nth-child(odd){background-color: gainsboro;}

       #ContentPlaceHolder1_GVCategory tr:hover {background-color: #ddd;}

       #ContentPlaceHolder1_GVCategory th {
          text-align: center;
          background-color: #d82f2f;
          color: black;
       }
       #ContentPlaceHolder1_GVCategory tbody {
          text-align: center;
          
       }
      
    </style>
    <div class="container-fluid" style="background-color:ghostwhite">
        <div class="row pt-5">
             <div class="col text-center">
                 <h1>List Category</h1>
             </div>
         </div><hr />
        <div class="row pb-3">
            <div class="col-4">
                <a class="btn btn-info" href="AddCategory.aspx"><i class="fa fa-plus"></i><span class="add_new_text">Buat Baru</span></a>
            </div>
            <div class="col-7">
             <asp:TextBox ID="TxtSearch" placeholder="Search Category" CssClass="form-control" runat="server"></asp:TextBox>  
               
            </div>     
           <div class="col">
             
             <asp:LinkButton ID="LinkButton4" CssClass="btn btn-dark"   runat="server"><i class="fa fa-search"></i></asp:LinkButton>     
            </div>       
         </div>
        
        <div class="row">
            <div class="col">
                
                <asp:GridView ID="GVCategory" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered" Width="100%"  AllowPaging="true" OnPageIndexChanging="GVCategory_PageIndexChanging"  >
                    <Columns>
                        <asp:TemplateField HeaderText="No" HeaderStyle-Width="10%" >   
                         <ItemTemplate>
                                <b> <%# Container.DataItemIndex + 1 %> </b>  
                         </ItemTemplate>
                     </asp:TemplateField>
                         <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <asp:Label ID="Label3" ForeColor="#3568ff" runat="server" Text='<%#Bind("Category_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category Type">
                            <ItemTemplate>
                                <asp:Label ID="Label3" ForeColor="#3568ff" runat="server" Text='<%#Bind("Category_type_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandArgument='<%#Bind("ID") %>' CssClass="btn btn-danger" OnClientClick="return confirmDelete(this);" OnClick="DeleteClick"  ><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                       </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <div align="center">No Category found.</div>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
    </div>
        <div class="text-right">
           <a href="News.aspx" class="btn btn-info"><i class="fa fa-home"></i></a>
        </div>
  </div>
</asp:Content>
