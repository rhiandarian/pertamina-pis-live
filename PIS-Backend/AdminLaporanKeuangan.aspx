﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminLaporanKeuangan.aspx.cs" Inherits="PIS_Backend.AdminLaporanKeuangan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Laporan Ikhtisar Keuangan</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Menu Lainnya 
                        </li>
                        <li class="breadcrumb-item">
                            Hubungan Investor
                        </li>
                         <li class="breadcrumb-item">
                            Laporan & Ikhtisar Keuangan 
                        </li>
                         
                        <li class="breadcrumb-item">
                           <a href="AdminLaporanKeuangan.aspx"><strong>Semua Laporan & Ikhtisar Keuangan</strong> </a> 
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row ml-2 pb-3">
            <a href="SaveLaporanKeuangan.aspx" class="btn btn-primary"><i class="fa fa-plus"></i><span class="add_new_text">Buat Baru</span></a>
        </div>
        <div class="row">
            
                <asp:ListView ID="ListViewFS" runat="server">
                    <ItemTemplate>
                        <div class="col-xs-12 col-sm-4 box-content">
                            <div class="ibox">
                                                            <div class="ibox-content product-box">
                                                                <div class="product-imitation">
                                                                    <h3><%#  Eval("title") %></h3>
                                                                    
                                                                </div>
                                                                <div class="product-desc">
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            <p class="text-center">
                                                                                <asp:LinkButton ID="LinkButton3" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="DeleteClick"  runat="server">Delete <i class="fa fa-trash"></i></asp:LinkButton>
                                                                            </p>
                                                                        </div>

                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
                                                        
            
        </div>
        <a href="laporan-ikhtisar-keuangan.aspx" target="_blank"><i class="fa fa-eye"></i> Lihat hasil website</a>
    </div>
</asp:Content>
