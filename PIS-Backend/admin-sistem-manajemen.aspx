﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="admin-sistem-manajemen.aspx.cs" Inherits="PIS_Backend.admin_sistem_manajemen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Sistem Manajemen</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="EditTataKelolaPerusahaan.aspx">Tata Kelola Perusahaan</a>
                        </li>
                        <li class="breadcrumb-item">
                           <a href="admin-sistem-manajemen.aspx"><strong>Sistem Manajemen</strong></a> 
                        </li>
                        
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row ml-2 pb-3">
            <a href="unggah-sistem-manajemen.aspx" class="btn btn-primary"><i class="fa fa-plus"></i><span class="add_new_text">Buat Baru</span></a>
        </div>
        <div class="row">
                    <asp:ListView ID="ListViewSO" runat="server">
                        <ItemTemplate>
                            <div class="col-xs-12 col-sm-3 box-content">
                                                        <div class="ibox">
                                                            <div class="ibox-content product-box">
                                                                <div class="">
                                                                    <img src='<%# Eval("img_file") %>' class="img-fluid" alt="...">
                                                                    
                                                                </div>
                                                                <div class="product-desc">
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            
                                                                            <p class="text-center">
                                                                               <b><%# Eval("Nama") %></b> <br>
                                                                               <asp:LinkButton ID="LinkButton1" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="Edit_Click" runat="server"><i class="fa fa-pencil"></i> Edit</asp:LinkButton>
                                                                               <asp:LinkButton ID="LinkButton2" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="Delete_Click" runat="server"><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                                                                                
                                                                                
                                                                            </p>
                                                                        </div>

                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                        </ItemTemplate>
                    </asp:ListView>
                    
                
                    
                
             
                 </div>
        <a href="EditTataKelolaPerusahaan.aspx" class="btn btn-info" ><i class="fa fa-home"></i> Kembali</a>
    </div>
</asp:Content>
