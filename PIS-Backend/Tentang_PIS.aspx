﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="Tentang_PIS.aspx.cs" Inherits="PIS_Backend.Tentang_PIS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx" id="home_text" runat="server">Home</a></li>
					    <li class="breadcrumb-item" id="company_profile_text" runat="server">Profil Perusahaan </li>
					    <li class="breadcrumb-item active" aria-current="page" id="about_pis_text" runat="server">Sekilas Tentang PIS</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="header_text" runat="server">SEKILAS TENTANG PIS</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <section id="sekilas-pis">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div id="content" runat="server" class="col-lg-12 content">
	  				

            
	  			</div>
	  		</div>
              <center class="mt-4">
              
                  <a id="fileDownload" runat="server" class="btn btn-default-pis">
                      <span class="bi bi-file-pdf-fill"></span> <span id="download_text" runat="server"> Download Company Profile PIS
                  </span>
                  </a>
            </center>
	  	</div>
  	</section>
  
    <script>
        var urlName = '';
        if ($("#LbInd").attr("class") == "active") {
            urlName = 'tentang-pis';
        }
        if($("#LbEn").attr("class") == "active")
        {
            urlName = 'company-overview';
        }
        history.pushState({}, null,urlName);
    </script>
</asp:Content>
