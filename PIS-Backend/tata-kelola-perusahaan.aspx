﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tata-kelola-perusahaan.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.tata_kelola_perusahaan" %>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Pertamina PIS</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="images/favicon.png" rel="icon">
  <link href="images/favicon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="vendor/bootstrap-4.5.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/animate/animate.css" rel="stylesheet" />
  <link href="vendor/venobox/venobox.css" rel="stylesheet">
  <link href="vendor/aos/aos.css" rel="stylesheet">
  <!--
    <link href="vendor/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
  -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

  <!-- Template Main CSS File -->
    <link href="css/v01/style.css" rel="stylesheet" />
</head>

<body>
  <form runat="server">

  
  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
          
        <asp:LinkButton ID="LinkButton1" CssClass="active" OnClick="LangInd_Click" runat="server">ID</asp:LinkButton> | <asp:LinkButton ID="LinkButton2" OnClick="LangEn_Click" runat="server">EN</asp:LinkButton>
      </div>
      <div class="contact-info float-right">
      	<a href="contacts.aspx"><i class="bi bi-envelope"></i></a>
      	<a href="#search"><i class="bi bi-search"></i></a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
      <a href="Index.aspx"><img src="images/Logo_Pertamina_PIS.svg" alt="" class="img-fluid"></a>
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
        <a href="" id="nav-right-option" class="nav-right-option float-right mobile-nav-toggle d-lg-none"><i class="bi bi-x"></i></a>
        <ul>
          
            <asp:ListView ID="ListViewMenu" runat="server">
                <ItemTemplate>
                    <li class='<%# Eval("classMenu") %>'>
                        <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                        <ul>
                            <asp:ListView ID="ListViewSubMenu" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                <ItemTemplate>
                                    <li class='<%# Eval("classMenu") %>'>
                                        <a href='<%# Eval("Link") %>'><%# System.Web.HttpUtility.HtmlEncode((string)Eval("Name")) %></a>
                                        <ul>
                                        <asp:ListView ID="ListViewSubMenu2" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                            </ul>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                        </ul>
                    </li>
                </ItemTemplate>
            </asp:ListView>
		      <asp:ListView ID="ListViewResponsive" runat="server">
                            <ItemTemplate>
                                <li class='<%# Eval("classMenu") %> d-lg-none'>
                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                    <ul>
                                        <asp:ListView ID="ListViewSubOthers" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li class='<%# Eval("classMenu") %>'>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                    <ul>
                                                        <asp:ListView ID="ListViewSub2Othes" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                                            <ItemTemplate>
                                                                <li>
                                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </ul>
                                                    
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </ul>
                                </li>
                            </ItemTemplate>
                        </asp:ListView>

          <li class="nav-right-option d-none d-lg-block" id="othermenu" runat="server"></li>
        </ul>
        <div id="langMobile" runat="server" class="text-center lang-mobile d-lg-none">
          
        </div>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- Nav Overlay -->
  <div id="navOverlay" class="nav-overlay d-none d-lg-block">
    <div class="bg-black-transparent">
      <div class="container">
        <div class="nav-overlay-content">
          <ul>
                        <asp:ListView ID="ListViewOthers" runat="server">
                            <ItemTemplate>
                                <li class='<%# Eval("classMenu") %>'>
                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                    <ul>
                                        <asp:ListView ID="ListViewSubOthers" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                            <ItemTemplate>
                                                <li class='<%# Eval("classMenu") %>'>
                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                    <ul>
                                                        <asp:ListView ID="ListViewSub2Othes" runat="server" DataSource='<%# Eval("SubMenu") %>'>
                                                            <ItemTemplate>
                                                                <li>
                                                                    <a href='<%# Eval("Link") %>'><%# Eval("Name") %></a>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </ul>
                                                    
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </ul>
                                </li>
                            </ItemTemplate>
                        </asp:ListView>
                        
		    		
		    	</ul>
        </div>
      </div>
    </div>
  </div><!-- End Nav Overlay -->

  <!-- Search -->
  <div id="search">
    <div class="bg-overlay">
      <button type="button" class="close"><i class="bi bi-x close"></i></button>
      <form action="pencarian.html">
      <input type="search" id="txtSearch" runat="server" value="" autocomplete="off" placeholder="Ketik kata kunci disini" />
            <asp:Button ID="BtnSearch" runat="server" OnClick="BtnSearch_Click" CssClass="btn bg-red" Text="Cari" />
      </form>
    </div>
  </div><!-- End Search -->

  <main id="main">

  	<!-- Top Main -->
  	<section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a id="htitle" href="Index.aspx"></a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="link1" runat="server">Komitmen & Keberlanjutan</li>
					    <li class="breadcrumb-item active" aria-current="page" id="link2" runat="server">Tata Kelola Perusahaan</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="title" runat="server">Tata Kelola Perusahaan</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>


  	<!-- Section -->
  	<section id="tata-kelola-perusahaan">
  		<div id="content" runat="server" class="container">
	  		
	  	</div>
  	</section>
      <div id="listSO">
          <div class="row row-eq-height">
            <asp:ListView ID="ListViewSO" runat="server">
                      <ItemTemplate>
                            <div class="col-md-4 col-xs-12 mb-4">
                                <div class="box-pis p-3 text-center eq-height">
                                    <img src='<%# Eval("img_file") %>' class="img-fluid mt-2 mb-2" alt="..." />
                                    <p><strong><%# Eval("Nama") %></strong></p>
                                </div>
                            </div>
                      </ItemTemplate>
                  </asp:ListView>
            </div>
      </div>
      
      
  </main>

 <!-- ======= Footer ======= -->
	<div id="footer-top-border">
		<div class="col-lg-12 col-12">
			<div class="row">
				<div class="col-lg-6 col-6 footer-border-1"></div>
				<div class="col-lg-3 col-3 footer-border-2"></div>
				<div class="col-lg-3 col-3 footer-border-3"></div>
			</div>
		</div>
	</div>
	<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-4 col-12 footer-info">
            <h6><strong id="kantor" runat="server">KANTOR PUSAT</strong></h6>
            <p class="mt-3" id="address" runat="server">
              Patra Jasa Office Tower Lantai 3 &amp; 14 
              <br>JJl. Jend. Gatot SubrotoKav 32–34
              <br>Setiabudi, Jakarta 12950 
              <br>Indonesia 
            </p>
            <div class="d-flex mt-2">
              <div class="align-self-center rounded-icon">
                <span class="bi bi-telephone-fill phone-icon"></span>
              </div>
              <div class="align-self-center">
                 &nbsp;<span id="phone" runat="server"></span>
              </div>
            </div>
            <div class="d-flex mt-2">
              <div class="align-self-center rounded-icon">
                <span class="bi bi-envelope-fill phone-icon"></span>
              </div>
              <div class="align-self-center">
                &nbsp; <span id="email" runat="server"></span>
              </div>
            </div>
            <div class="d-flex mt-2">
              <div class="align-self-center">
                <img src="images/logo-135.jpg" alt="" class="img-fluid" width="25px">
              </div>
              <div class="align-self-center">
                &nbsp; Call Center <strong>135</strong>
              </div>
            </div>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>


         <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong id="perusahaan" runat="server">TAUTAN </strong></h6>
            <ul class="mt-3">
              <li><i class="bx bx-chevron-right"></i> <a href="https://pertamina.com" id="pt1" runat="server">PT Pertamina (Persero)</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://bumn.go.id" id="pt2" runat="server">'Kementerian BUMN</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://www.esdm.go.id" id="pt3" runat="server">Kementerian ESDM RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="http://www.dephub.go.id" id="pt4" runat="server">Kementerian Perhubungan RI</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="https://www.bkpm.go.id" id="pt5" runat="server">BKPM</a></li>
            </ul>
          </div>
          <div class="d-md-none col-12 separate"><hr></div>

          <div class="col-lg-4 col-md-4 col-12 footer-links">
            <h6><strong id="findus" runat="server">TEMUKAN KAMI</strong></h6>
            <ul class="mt-3 social-links">
              <li>
                <a id="link_fb" runat="server" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-facebook facebook"></span>
                  </div>
                  <div class="align-self-center">Facebook<br><em>Pertamina International Shipping</em></div>
                </a>
              </li>
              <li>
                <a id="link_ig" runat="server"  class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-instagram instagram"></span>
                    </div>
                  <div class="align-self-center">Instagram<br><em>@pertamina_pis</em></div> 
                </a>
              </li>
              <li>
                <a id="link_tw" runat="server" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-twitter twitter"></span>
                    </div>
                  <div class="align-self-center">Twitter<br><em>@Pertamina_PIS</em></div> 
                </a>
              </li>
              <li>
                <a  id="link_yt" runat="server" class="d-flex">
                  <div class="align-self-center">
                    <span class="bi bi-youtube twitter"></span>
                    </div>
                  <div class="align-self-center">Youtube<br><em>PT Pertamina International Shipping</em></div> 
                </a>
              </li>
            </ul>
          </div>
          <div class="col-12 d-lg-none separate"><hr></div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="row d-flex">
        <div class="col-lg-8 col-12 align-self-center part-of">
          <span>PART OF</span>
          <img src="images/logo-bumn.svg" class="ml-3" alt="..." height="30px">
          <img src="images/logo-pertamina.svg" class="ml-3" alt="..." height="30px">
        </div>
        <div class="col-lg-4 col-12">
          <a href="" class="btn btn-whistle">
              <div class="row">
                <div class="col pr-0 d-flex mr-2">
                  <img src="images/icons/wbs.svg">
                </div>
                <div class="col text-left pl-0">
                  Whistle Blowing System
                  <br><em>https://pertaminaclean.tipoffs.info/</em>
                </div>
              </div>
            </a>
        </div>
        <div class="col-12 mt-3">
          © Copyright 2021 <strong><span>Pertamina International Shipping</span></strong>. All Rights Reserved
        </div>
      </div>
    </div>
  </footer><!-- End Footer -->
    
    

   

  <!-- Vendor JS Files -->
  <script src="vendor/jquery.min.js"></script>
  <script src="vendor/bootstrap-4.5.3-dist/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="vendor/venobox/venobox.min.js"></script>
  <script src="vendor/waypoints/lib/jquery.waypoints.min.js"></script>
  <script src="vendor/counterup/counterup.min.js"></script>
  <script src="vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="vendor/aos/aos.js"></script>
  <script src="vendor/fontawesome/js/all.min.js"></script>

  <!-- Template Main JS File -->
  <script src="js/main.js?v=0.1"></script>

  <script type="text/javascript">
      function changeLang(lang)
      {
           var button = lang == "En" ? document.getElementById("<%= LinkButton1.ClientID %>") : document.getElementById("<%= LinkButton2.ClientID %>");
           button.click();
      }
      var urlName = '', txtSearchText = '', btnSearchText = '', title = '';
        if ($("#LinkButton1").attr("class") == "active") {
            urlName = 'tata-kelola-perusahaan';
            txtSearchText = "Ketik kata kunci disini";
            btnSearchText = "Cari";
            title = "Beranda";
        }
        if ($("#LinkButton2").attr("class") == "active")
        {
            urlName = 'good-corporate-governance';
            txtSearchText = "Type keyword(s) here";
            btnSearchText = "Search";
            title = "Home";
        }
        $("#txtSearch").attr('placeholder', txtSearchText);
        $("#BtnSearch").text(btnSearchText);
        $("#htitle").text(title);
        history.pushState({}, null,urlName);
    
  	$("#tata-kelola, #tujuan-penerapan, #prinsip-pedoman, #penerapan, #whistleblowing, #site-manajemen, #performa-k3").bind('click', function(e){
  		e.preventDefault();
  		let id = $(this).prop('id');
  		$(".list-content li").removeClass("active");
  		$(this).parent().addClass("active");

  		$(".detail-content .row").children().addClass("d-none");
  		$(".c-"+id).removeClass("d-none");
      $(".c-"+id).children().children().removeClass("d-none");
  	});
  	$(".c-site-manajemen").append($("#listSO").html());
  	$("#listSO").hide();
  	var detailContent = ["tujuan-penerapan", "prinsip-pedoman", "penerapan", "whistleblowing", "site-manajemen", "performa-k3"];
  	$.each(detailContent, function (i,val) {
  	    $(".c-"+val).addClass("d-none");
  	});
  	$(".list-content li").removeClass("active");
  	$("#tata-kelola").parent().addClass("active");
  	$(".c-tata-kelola").removeClass("d-none");
  </script>
</form>
</body>

</html>