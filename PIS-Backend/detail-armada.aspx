﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="detail-armada.aspx.cs" Inherits="PIS_Backend.detail_armada" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a id="linkTitle" runat="server" href="Index.aspx" >Beranda</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="link2" runat="server">Kapal Milik</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="htitle" runat="server">Kapal Milik</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img src="images/Header/kapal_header_01.png" class="img-fluid" alt="...">
			</div>
  	</section>
    <section class="detail-armada">
  		<div class="container">
	  		<asp:ListView ID="ListViewDetailArmada" runat="server">
                  <ItemTemplate>
                      <div class="row mb-5 content">
          <div class="col-xs-12 col-sm-3">
            <div class="col box-image">
              <img src='<%#Eval("img_file")%>' class="img-fluid" alt="..." style="height:100%">
            </div>
          </div>
          <div class="col-xs-12 col-sm-9">
            <p class="mb-0"><strong><%#Eval("name")%></strong></p>
            <p><%#Eval("oil")%> </p>

            <p><%#Eval("detail")%></p>
          </div>

	  			<div class="col-12 pt-5">
            <p><strong id="spes" runat="server">Spesification</strong></p>
	  				<table class="table table-borderless table-striped">
                      <thead class="bg-green">
                        <tr>
                          <td colspan="2"></td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>BUILT</td>
                          <td><strong><%#Eval("BuiltOn")%></strong></td>
                        </tr>
                        <tr>
                          <td>FLAG</td>
                          <td><strong><%#Eval("Flag")%></strong></td>
                        </tr>
                        <tr>
                          <td>CALL SIGN </td>
                          <td><strong><%#Eval("CallSign")%></strong></td>
                        </tr>
                        <tr>
                          <td>CLASS DNV-GL </td>
                          <td><strong><%#Eval("CLASS_DNV_GL")%></strong></td>
                        </tr>
                        <tr>
                          <td>PARTICULARS</td>
                          <td><strong><%#Eval("PARTICULARS")%></strong></td>
                        </tr>
                        <tr>
                          <td>LOA</td>
                          <td><strong><%#Eval("LOA")%></strong></td>
                        </tr>
                        <tr>
                          <td>LBP</td>
                          <td><strong><%#Eval("LBP")%></strong></td>
                        </tr>
                        <tr>
                          <td>PARALLEL BODY LOADED </td>
                          <td><strong><%#Eval("PARALLEL_BODY_LOADED")%></strong></td>
                        </tr>
                        <tr>
                          <td>BREADTH MOULDED </td>
                          <td><strong><%#Eval("BREADTH_MOULDED")%></strong></td>
                        </tr>
                        <tr>
                          <td>HEIGHT FROM KEEL TO MASK </td>
                          <td><strong><%#Eval("HEIGHT_FROM_KEEL_TO_MASK")%></strong></td>
                        </tr>
                        <tr>
                          <td>DEPTH MOULDED </td>
                          <td><strong><%#Eval("DEPTH_MOULDED")%></strong></td>
                        </tr>
                        <tr>
                          <td>DRAFT SUMMER </td>
                          <td><strong><%#Eval("DRAFT_SUMMER")%></strong></td>
                        </tr>
                        <tr>
                          <td>DRAFT BALLAST </td>
                          <td><strong><%#Eval("DRAFT_BALLAST")%></strong></td>
                        </tr>
                        <tr>
                          <td>TPC</td>
                          <td><strong><%#Eval("TPC")%></strong></td>
                        </tr>
                        <tr>
                          <td>AIRDRAFT IN BALLAST / LOADED CONDITION </td>
                          <td><strong><%#Eval("AIRDRAFT_IN_BALLAST__LOADED_CONDITION")%>/strong></td>
                        </tr>
                        <tr>
                          <td>TOTAL / SIZE MANIFOLDS </td>
                          <td><strong><%#Eval("TOTAL_SIZE_MANIFOLDS")%></strong></td>
                        </tr>
                        <tr>
                          <td>DISTANCE FORM BOW TO CENTRE MANIFOLD </td>
                          <td><strong><%#Eval("DISTANCE_FORM_BOW_TO_CENTRE_MANIFOLD")%></strong></td>
                        </tr>
                        <tr>
                          <td>DISTANCE FORM STERN TO CENTRE MANIFOLD </td>
                          <td><strong><%#Eval("DISTANCE_FORM_STERN_TO_CENTRE_MANIFOLD")%></strong></td>
                        </tr>
                        <tr>
                          <td>DISTANCE BETWEEN MANIFOLDS</td>
                          <td><strong><%#Eval("DISTANCE_BETWEEN_MANIFOLDS")%></strong></td>
                        </tr>
                      </tbody>
                    </table>

                    <table class="table table-borderless table-striped mt-5">
                      <thead class="bg-green">
                        <tr>
                          <td colspan="2"></td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>DISTANCE FORM SHIP SIDE TO CENTRE MANIFOLD </td>
                          <td><strong><%#Eval("DISTANCE_FORM_SHIP_SIDE_TO_CENTRE_MANIFOLD")%></strong></td>
                        </tr>
                        <tr>
                          <td>DISTANCE FROM DECK/TRAY TO MANIFOLD </td>
                          <td><strong><%#Eval("DISTANCE_FROM_DECK_TRAY_TO_MANIFOLD")%></strong></td>
                        </tr>
                        <tr>
                          <td>CARGO TANK CAPACITY </td>
                          <td><strong><%#Eval("CARGO_TANK_CAPACITY")%></strong></td>
                        </tr>
                        <tr>
                          <td>SLOP TANK CAPACITY </td>
                          <td><strong><%#Eval("SLOP_TANK_CAPACITY")%></strong></td>
                        </tr>
                        <tr>
                          <td>BALLAST TANK CAPACITY </td>
                          <td><strong><%#Eval("BALLAST_TANK_CAPACITY")%></strong></td>
                        </tr>
                        <tr>
                          <td>DEADWEIGHT</td>
                          <td><strong><%#Eval("DEADWEIGHT")%></strong></td>
                        </tr>
                        <tr>
                          <td>TONNAGE INTERNATIONAL </td>
                          <td><strong><%#Eval("TONNAGE_INTERNATIONAL")%></strong></td>
                        </tr>
                        <tr>
                          <td>COATING</td>
                          <td><strong><%#Eval("COATING")%></strong></td>
                        </tr>
                        <tr>
                          <td>MAIN ENGINE </td>
                          <td><strong><%#Eval("MAIN_ENGINE")%></strong></td>
                        </tr>
                        <tr>
                          <td>BOW THRUSTER </td>
                          <td><strong><%#Eval("TONNAGE_INTERNATIONAL")%></strong></td>
                        </tr>
                        <tr>
                          <td>CRANES</td>
                          <td><strong><%#Eval("CRANES")%></strong></td>
                        </tr>
                        <tr>
                          <td>CARGO PUMPS </td>
                          <td><strong><%#Eval("CARGO_PUMPS")%></strong></td>
                        </tr>
                        <tr>
                          <td>STRIPPING PUMP </td>
                          <td><strong><%#Eval("STRIPPING_PUMP")%></strong></td>
                        </tr>
                        <tr>
                          <td>BALLAST PUMPS </td>
                          <td><strong><%#Eval("BALLAST_PUMPS")%></strong></strong></td>
                        </tr>
                        <tr>
                          <td>MAX LOAD RATE HOMOGENOUS CARGO PER MANIFOLD </td>
                          <td><strong><%#Eval("MAX_LOAD_RATE_HOMOGENOUS_CARGO_PER_MANIFOLD")%></strong></strong></td>
                        </tr>
                        <tr>
                          <td>MAX LOAD RATE HOMOGENOUS CARGOTHROUGH ALL MANIFOLD </td>
                          <td><strong><%#Eval("MAX_LOAD_RATE_HOMOGENOUS_CARGOTHROUGH_ALL_MANIFOLD")%></strong></strong></td>
                        </tr>
                        <tr>
                          <td>NO. OF CARGO TANKS </td>
                          <td><strong><%#Eval("NO_OF_CARGO_TANKS")%></strong></strong></td>
                        </tr>
                        <tr>
                          <td>NO. OF SEGREGATIONS</td>
                          <td><strong><%#Eval("NO_OF_SEGREGATIONS")%></strong></strong></td>
                        </tr>
                      </tbody>
                    </table>
	  			</div>
	  			
	  		</div>
                  </ItemTemplate>
	  		</asp:ListView>
        

	  	</div>
  	</section>
    <script>
        if($("#LbInd").attr("class") == "active")
        {
            $("#ContentPlaceHolder1_ListViewDetailArmada_spes_0").text("Spesifikasi");
        }
        else
        {
            $("#ContentPlaceHolder1_ListViewDetailArmada_spes_0").text($("#ContentPlaceHolder1_ListViewDetailArmada_spes_0").text());
        }
    </script>
</asp:Content>
