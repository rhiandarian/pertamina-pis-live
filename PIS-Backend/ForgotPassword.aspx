﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="PIS_Backend.ForgotPassword" %>
 

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head><meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0" /><link rel="shortcut icon" href="/image/pertamina-7oziq.jpg" type="image/png" /><title>
	 Pertamina International Shipping | Login
</title><link href="css/bootstrap.min.css" rel="stylesheet" /><link href="font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="css/animate.css" rel="stylesheet" /><link href="css/style.css" rel="stylesheet" /></head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown" style="background-color: white;width: 400px;padding: 40px;margin-top: 100px;">
        <div>
            
            <div class="form-group">

                 <img src="image/logo-PIS.jpg" width="100%" />
            </div> 
              <p>Input Your Registered Email.</p>
            <asp:Label ID="LblErrorMessage" runat="server" ></asp:Label>
            <form id="form2" class="m-t" role="form" runat="server">
                <div class="form-group">
                    <asp:TextBox ID="Email" CssClass="form-control" placeholder="Email Address" runat="server"></asp:TextBox>
                </div>
                
                
                 <asp:Button ID="BtnSendEmail" CssClass="btn btn-danger block full-width m-b" runat="server" Text="Send Email" OnClick="BtnSendEmail_Click"  />
                  
            </form>
            
            
        </div>
    </div>

    <!-- Mainly scripts -->
    


</body>

</html>
