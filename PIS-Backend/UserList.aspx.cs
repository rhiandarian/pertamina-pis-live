﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class UserList : System.Web.UI.Page
    {
        ModelData Db = new ModelData();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "35"))
                {
                    Response.Redirect("admin.aspx");
                }
                LoadLevelUser();
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void LoadLevelUser()
        {
            ModelData Db = new ModelData();

            string query = Db.SelectQuery("Id,User_name,Email,Level", "View_UsersFull", ""); 
            DataTable dt = Db.getData(query);
            GridViewGallery.DataSource = dt;
            GridViewGallery.DataBind();
        }
        protected void EditClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            //Response.Redirect("admin-edit-keberlanjutan.aspx?ID=" + id);
            Response.Redirect("UserEdit.aspx?ID=" + id);
        }
        
        public string queryloghistory(string id)
        {
            ModelData db = new ModelData();
            return db.logHistoryQuery(getip(), GetBrowserDetails(), Session["Username"].ToString() + " Delete User with Id = " + id, Convert.ToInt32(Session["UserID"].ToString()), "Success");
        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Db.setData("update Users set isdeleted = 1 where Id="+id+" "+queryloghistory(id));
            LoadLevelUser();
        }
        public void GVGalleryPageindexChanging(object sender, GridViewPageEventArgs e)
        {
            LoadLevelUser();
            GridViewGallery.PageIndex = e.NewPageIndex;
            GridViewGallery.DataBind();
        }
    }
}