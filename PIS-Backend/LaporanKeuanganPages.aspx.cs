﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace PIS_Backend
{
    public partial class LaporanKeuanganPages : System.Web.UI.Page
    {
        string folder = "Header";
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginpages.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "22"))
                {
                    Response.Redirect("admin.aspx");
                }

                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                successdiv.Visible = false;
                LoadData();

            }
        }
        public void LoadData()
        {
            FinancialStatement fs = new FinancialStatement();
            PagesData pd = fs.getPage();
            content.InnerHtml = pd.content;
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                PagesData pd = new PagesData();
                FinancialStatement fs = new FinancialStatement();
                bool fileExsist = false;
                string validation = "";
                if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
                {
                    fileExsist = true;
                    pd.Header =  folder+"/" + FileUpload1.FileName;
                    if(!all.ImageFileType(FileUpload1.FileName))
                    {
                        validation = pd.validationHeaderImage;
                    }
                }
                pd.Content = content.InnerText;
                pd.Content_En = content_en.InnerText;
                if(validation == "")
                {
                    string cond = " ID = " + fs.PageID;
                    pd.Save(getip(),GetBrowserDetails(),fs.PageID,cond);
                    if(fileExsist)
                    {
                        UploadFile();
                    }
                    successMessage(pd.successMessage);
                }
                else
                {
                    showValidationMessage(validation);
                }
            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void UploadFile()
        {
            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string SaveLocation = Server.MapPath(folder) + "\\" + fn;

            FileUpload1.PostedFile.SaveAs(SaveLocation);
        }
        public void showValidationMessage(string textMessage)
        {
            successdiv.Visible = false;
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void successMessage(string textMessage)
        {
            LblErrorMessage.Visible = false;
            successdiv.InnerHtml = textMessage;
            successdiv.Visible = true;

        }
    }
}