﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace PIS_Backend
{
    public class RUPSModel
    {
        SqlConnection con = Connection.conn();

        string tableName = "RUPS";

        string viewName = "ViewRUPS";

        public int PageSize = 8;

        DateTime dateNow = DateTime.Now;

        string user_id = HttpContext.Current.Session["UserID"] != null ? HttpContext.Current.Session["UserID"].ToString() : "";

        ModelData Db = new ModelData();

        public string url;

        public int year;

        public string Url
        {
            get { return url; }
            set { url = value; }
        }
        public int  Year
        {
            get { return year; }
            set { year = value; }
        }
        public DataTable Select(string field, string conditions = null)
        {
            conditions = conditions == null ? "" : conditions;

            string query = Db.SelectQuery(field, viewName, conditions);


            DataTable dt = Db.getData(query);

            return dt;

        }
        public string validate()
        {
            if(year == 0)
            {
                return "year must be choose";
            }
            int yearNow = Convert.ToInt32(DateTime.Now.Year.ToString());
            if(year > yearNow)
            {
                return "Year not Valid";
            }
            if(url == null || url == "")
            {
                return "file must be exsist";
            }
            return "valid";
        }
        public string Insert()
        {
            string valid = validate();
            if(valid != "valid")
            {
                return valid;
            }
            try
            {
                SqlCommand cmd = new SqlCommand("SaveRUPS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@fileName", url);
                cmd.Parameters.AddWithValue("@year", year);
                cmd.Parameters.AddWithValue("@userID", user_id);

                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt.Rows[0]["Result"].ToString();
            }
            catch(Exception e)
            {
                return e.ToString();
            }
            
        }
        public DataTable LoadlAll(int PageIndex,int PageSizes = 0)
        {
            PageSizes = PageSizes == 0 ? PageSize : PageSizes;

            SqlCommand cmd = new SqlCommand("GetAllRUPS", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PageIndex", PageIndex);
            cmd.Parameters.AddWithValue("@PageSize", PageSizes);

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt); 

            return dt;
        }
        public void Delete(string condition = null)
        {
            string conditions = condition != null ? condition : "";

            Db.Delete(tableName, dateNow, user_id, conditions);
        }
    }
}