﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace PIS_Backend
{
    public partial class AdminPresentasi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if(!l.isValidAccessPage(Session["Level"].ToString(),"8"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindPersentasi();



            }
        }
        public void BindPersentasi()
        {
            Presentasi p = new Presentasi();
            DataTable dt = p.Select("*");
            ListViewPersentasi.DataSource = dt;
            ListViewPersentasi.DataBind();
        }
        protected void EditClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Presentasi p = new Presentasi();
            Response.Redirect(p.savePage+"?ID="+id);
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Presentasi p = new Presentasi();
            string cond = " ID = " + id;
            try
            {
                p.Delete(id,getip(),GetBrowserDetails(),cond);
                BindPersentasi();
            }
            catch(Exception ex)
            {
                General all = new General();
                p.ErrorLogHistory(getip(), GetBrowserDetails(), "Delete", p.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                Response.Redirect(all.alert("Error with "+ex.Message.ToString()));
            }
            
        }

        protected void ListViewPersentasi_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (ListViewPersentasi.FindControl("DataPager1") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindPersentasi();
        }
    }
}