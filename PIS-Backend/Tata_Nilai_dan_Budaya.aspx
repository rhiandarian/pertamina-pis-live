﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="Tata_Nilai_dan_Budaya.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.Tata_Nilai_dan_Budaya" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx" id="li_1" runat="server">Home</a></li>
					    <li class="breadcrumb-item " id="li_2" runat="server"> Tentang PIS</li>
					    <li class="breadcrumb-item active" aria-current="page" id="li_3" runat="server">Tata Nilai</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="header_text"  runat="server">Tata Nilai &amp; Budaya Perusahaan</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <section id="tata-nilai">
  		<div class="container">
	  		<div id="content" runat="server" class="row"> 
				
	  		</div>

	  	</div>
  	</section>
    <script>
        var urlName = '';
        if ($("#LbInd").attr("class") == "active") {
            urlName = 'tata-nilai-budaya';
        }
        if($("#LbEn").attr("class") == "active")
        {
            urlName = 'corporate-value';
        }
        history.pushState({}, null,urlName);
    </script>
</asp:Content>
