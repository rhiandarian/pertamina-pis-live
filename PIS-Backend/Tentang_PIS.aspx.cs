﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PIS_Backend
{
    public partial class Tentang_PIS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             
            LoadData();

        }

        public void LoadData()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 4 ");
            imgHeader.Src = dt.Rows[0]["Header"].ToString();
            content.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content"].ToString();
            fileDownload.HRef = dt.Rows[0]["others"].ToString();
            home_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Home" : "Beranda";
            company_profile_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Company Profile" : "Profil Perusahaan";
            about_pis_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Company Overview" : "Sekilas Tentang PIS";
            download_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Download PIS Company Profile" : "Download Profil Perusahaan PIS";
            header_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Company Overview" : about_pis_text.InnerHtml;
            editClassLang();
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
    }
}