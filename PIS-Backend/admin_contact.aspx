﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="admin_contact.aspx.cs" Inherits="PIS_Backend.admin_contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Kontak</h2>
                    <ol class="breadcrumb"> 
                        <li class="breadcrumb-item ">
                            <span>Menu Lainnya</span>
                        </li>
                        <li class="breadcrumb-item ">
                            <span><strong>Kontak</strong></span>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Halaman Kontak</small></h5> 
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                <div id="successdiv" class="alert alert-success" runat="server"></div>
                <div class="row">
                    
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="Email"><b>Email</b></label>
                            <asp:TextBox ID="Email" cssClass="form-control" placeholder="Masukkan alamat email perusahaan" runat="server"></asp:TextBox>
                         </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="company"><b>Telepon</b></label>
                            <asp:TextBox ID="phone" cssClass="form-control" placeholder="Masukkan nomor telepon perusahaan" runat="server"></asp:TextBox>
                      </div>
                    </div>
                    <div class="col-lg-6">
                         <div class="form-group">
                            <label for="address"><b>Alamat (Bahasa Indonesia)</b></label>
                           <textarea class="summernote" id="address" runat="server"  rows="10"></textarea>
                        </div>
                    </div>
                    <div class="col-lg-6">
                         <div class="form-group">
                            <label for="address_en"><b>Alamat (Bahasa Inggris)</b></label>
                           <textarea class="summernote" id="address_en" runat="server"  rows="10"></textarea>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="fb"><b>Tautan Facebook</b></label>
                            <asp:TextBox ID="link_fb" cssClass="form-control" placeholder="Masukkan tautan Facebook" runat="server"></asp:TextBox>
                         </div>
                    </div> 
                     <div class="col-lg-6">
                        <div class="form-group">
                            <label for="ig"><b>Tautan Instagram</b></label>
                            <asp:TextBox ID="link_ig" cssClass="form-control" placeholder="Masukkan tautan Instagram" runat="server"></asp:TextBox>
                         </div>
                    </div> 
                     <div class="col-lg-6">
                        <div class="form-group">
                            <label for="yt"><b>Tautan Youtube</b></label>
                            <asp:TextBox ID="link_yt" cssClass="form-control" placeholder="Masukkan tautan Youtube" runat="server"></asp:TextBox>
                         </div>
                    </div> 
                     <div class="col-lg-6">
                        <div class="form-group">
                            <label for="tw"><b>Tautan Twiter</b></label>
                            <asp:TextBox ID="link_tw" cssClass="form-control" placeholder="Masukkan tautan Twiter" runat="server"></asp:TextBox>
                         </div>
                    </div>   
                </div>
                <div class="form-group">
                        <asp:Button ID="Save" CssClass="btn btn-primary btn-sm" runat="server" Text="Save changes" OnClick="Save_Click" />  
                 </div>
            </div>
            
        </div>
    </div>
        </div>
</asp:Content>
