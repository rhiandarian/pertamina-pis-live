﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PIS_Backend
{
    public partial class SaveAlbum : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                Level l = new Level();
                if(!l.isValidAccessPage(Session["Level"].ToString(),"6"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                if (Request.QueryString["GalleryID"] == null)
                {
                    Response.Redirect("admin-list-gallery.aspx");
                }
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string randomText = textrandom();
                int GalleryID = Convert.ToInt32(Request.QueryString["GalleryID"]);
                Image img = new Image();
                img.Gallery_Id = GalleryID;
                img.Title = title.Text;
                img.TitleEn = title_en.Text;
                img.Img_File = randomText + FileUpload1.FileName;
                img.FileInputSize = FileUpload1.PostedFile.ContentLength;
                string insertStatus = img.Insert(getip(),GetBrowserDetails());
                if(insertStatus == "success")
                {
                    Upload(randomText);
                    Response.Redirect("admin-admin-list-album?GalleryID="+ Request.QueryString["GalleryID"].ToString());

                }
                else
                {
                    showValidationMessage(insertStatus);
                }
            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public string textrandom()
        {
            Random RNG = new Random();
            int length = 6;
            var rString = "";
            for (var i = 0; i < length; i++)
            {
                rString += ((char)(RNG.Next(1, 26) + 64)).ToString().ToLower();
            }
            return rString;
        }
        public void Upload(string randomtext)
        {
       
            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            string SetFolderBeforeResize = Server.MapPath("Gallerry") + "\\" + "serize" + randomtext + fn; //gambar yang akan diresize sesudah direze gambar akan dihapus
            string SetFolderAfterResize = Server.MapPath("Gallerry") + "\\" + randomtext + fn;
            FileUpload1.PostedFile.SaveAs(SetFolderBeforeResize);
            ImageResize Iz = new ImageResize();
            Iz.ResizeImage(SetFolderBeforeResize, SetFolderAfterResize, "Gallery", "Cut");

        }

        protected void LinkButtonListAlbum_Click(object sender, EventArgs e)
        {
            Response.Redirect("admin-admin-list-album?GalleryID=" + Request.QueryString["GalleryID"].ToString());
        }

        protected void LinkButtonBack_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Request.QueryString["GalleryID"].ToString());
            Response.Redirect("EditFoto?ID=" + id); 
        }
    }
}