﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="LevelUserEdit.aspx.cs" Inherits="PIS_Backend.LevelUserEdit" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
                        <h2>Level Pengguna</h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                Pengaturan Pengguna
                            </li>
                            <li class="breadcrumb-item">
                                <a>Level Pengguna</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="LevelUserEdit.aspx">Buat Baru</a>
                            </li>
                             
                        </ol>
                    </div>
                    <div class="col-lg-2">

                    </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Buat Baru Level Pengguna</small></h5>
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i> 
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                 <form>
                      <div class="form-group">
                        <label for="levelName"><b>Nama Level Pengguna</b></label>
                        <asp:TextBox ID="levelName" cssClass="form-control" placeholder="Masukkan level pengguna" runat="server"></asp:TextBox>
                      </div>  
                     <div class="form-group">
                        <label for="levelName"><b>Pilih Halaman</b></label>
                         <asp:DropDownList ID="DdlHalaman" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                      </div>
                     <asp:GridView ID="GVListPage" runat="server" AutoGenerateColumns="false" AllowPaging="true" OnPageIndexChanging="GVListPage_PageIndexChanging"  CssClass="table table-bordered table-striped">
                         <Columns>
                             <asp:TemplateField HeaderText="No">   
                                         <ItemTemplate>
                                               <b>  <%# Container.DataItemIndex + 1 %>  </b>  
                                         </ItemTemplate>
                                    </asp:TemplateField>
                             <asp:TemplateField HeaderText="List Halaman">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" ForeColor="#3568ff" runat="server" Text='<%#Bind("namapages") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                             <asp:TemplateField HeaderText="Hapus">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandArgument='<%#Bind("LevelPagesId") %>' OnClick="Delete_Click"   CssClass="btn btn-danger"><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                         </Columns>
                         <EmptyDataTemplate>
                                    <div align="center">Halaman tidak ada.</div>
                                </EmptyDataTemplate>
                     </asp:GridView>
                    <div class="form-group">
                        <asp:Button ID="Save" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="BtnSave_Click"      />
                        <a href="LevelList.aspx" class="btn btn-info">Kembali</a>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</asp:Content>
