﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminPengadaan.aspx.cs" Inherits="PIS_Backend.AdminPengadaan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Informasi Pengadaan</h2>
                    <ol class="breadcrumb">
                         <li class="breadcrumb-item">
                            Menu Lainnya 
                        </li>
                        <li class="breadcrumb-item">
                            Pengadaan
                        </li> 
                        <li class="breadcrumb-item">
                           <a href="AdminPengadaan.aspx"><strong>Semua Pengadaan</strong> </a> 
                        </li>
                        
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
     <div class="wrapper wrapper-content animated fadeInRight">
         <div class="pb-3">
             <a href="SavePengadaan.aspx" class="btn btn-primary"><i class="fa fa-plus"></i><span class="add_new_text">Buat Baru</span></a>
         </div>
        
                                        <div class="row">
                                            <asp:ListView ID="ListViewPengadaan" ItemPlaceholderID="itemPlaceHolder1" OnPagePropertiesChanging="ListViewPengadaan_PagePropertiesChanging" runat="server" GroupPlaceholderID="groupPlaceHolder1">
                                                <GroupTemplate>
                                                    <tr>
                                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                                                    </tr>
                                                </GroupTemplate>
                                                <ItemTemplate>
                                                    <div class="col-xs-12 col-sm-4 box-content">
                                                        <div class="ibox">
                                                            <div class="ibox-content product-box">
                                                                <div class="product-imitation">
                                                                    <img src='<%#  Eval("cover") %>' class="img-fluid mt-2 mb-2" alt="..." height="10%">
                                                                </div>
                                                                <div class="product-desc">
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            <p class="text-center"><%#  Eval("title") %></p>
                                                                            <p class="text-center">
                                                                                <asp:LinkButton ID="LinkButton3" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="EditClick" runat="server">Edit <i class="fa fa-pencil"></i></asp:LinkButton>
                                                                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="DeleteClick" runat="server">Delete <i class="fa fa-trash"></i></asp:LinkButton>
                                                                            </p>
                                                                        </div>

                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
                                                    <asp:DataPager ID="DataPager1" runat="server" PagedControlID="ListViewPengadaan" PageSize="3">
                                                        <Fields>
                                                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
                                                                ShowNextPageButton="false" />
                                                            <asp:NumericPagerField ButtonType="Link" />
                                                            <asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton = "false" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </LayoutTemplate>
                                            </asp:ListView>
                                            <%--<asp:ListView ID="ListViewPersentasi" runat="server" ItemPlaceholderID="itemPlaceHolder1" OnPagePropertiesChanging="ListViewPersentasi_PagePropertiesChanging" GroupPlaceholderID="groupPlaceHolder1">
                                                
                                                
                                                
                                            </asp:ListView>--%>
                                            <%--<asp:ListView ID="ListViewArmada" runat="server" ItemPlaceholderID="itemPlaceHolder1" OnPagePropertiesChanging="OnPagePropertiesChanging" GroupPlaceholderID="groupPlaceHolder1">
                                                
                                                
                                                
                                            </asp:ListView>--%>
                                                
                                         </div>
        <a href="informasi-pengadaan.aspx"><i class="fa fa-eye"> Lihat hasil website</i></a>
                                     </div>
</asp:Content>
