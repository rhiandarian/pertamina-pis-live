﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="admin-edit-keberlanjutan.aspx.cs" Inherits="PIS_Backend.admin_edit_keberlanjutan" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
                        <h2>Keberlanjutan</h2>
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item">
                                <a>Menu Lainnya</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="admin_listkeberlanjutan.aspx">Keberlanjutan</a>
                            </li>
                            <li class="breadcrumb-item active">
                                 <strong>Ubah keberlanjutan</strong> 
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">

                    </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Keberlanjutan  Ubah Data.</small></h5>
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                 <form>
                      <div class="form-group">
                        <label for="title"><b>title</b></label>
                        <asp:TextBox ID="title" cssClass="form-control" placeholder="Input Title Gallery" runat="server"></asp:TextBox>
                      </div>
                     <div class="form-group">
                        <label for="title_en"><b>title En</b></label>
                        <asp:TextBox ID="title_en" cssClass="form-control" placeholder="Input Title En Gallery" runat="server"></asp:TextBox>
                      </div>
                        <div class="form-group">
                        <label for="date"><b>add date</b></label>
                        <asp:TextBox ID="date" cssClass="form-control" placeholder="Input Date Gallery" runat="server"></asp:TextBox>
                      </div>
                     <asp:Panel ID="PanelImage" runat="server">
                         <div class="form-group">
                            <label for="FileUpload1"><b>Image</b></label>
                            <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                         </div>
                         <div class="form-group">
                             <asp:LinkButton ID="LinkButtonUploadAlbum" runat="server" OnClick="LinkButton1_Click">Lihat Album</asp:LinkButton>
                         </div>
                     </asp:Panel>
                     
                    <div class="form-group">
                        <asp:Button ID="Save" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="BtnSave_Click"      />
                    </div>
                      
                </form>
            </div>
        </div>
    </div>
</asp:Content>
