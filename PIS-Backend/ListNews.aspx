﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ListNews.aspx.cs" Inherits="PIS_Backend.ListNews" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Berita</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Media dan Informasi
                        </li>
                        <li class="breadcrumb-item">
                            <a>Berita</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="ListNews.aspx"><strong>Semua Berita</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Semua Berita </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        
                        <div class="ibox-content">
                            <div class="row">
                                <a href="admin-add-news.aspx" class="btn btn-primary"><i class="fa fa-plus"></i> <span class="add_new_text">Buat Baru</span></a>
                            </div>
                            <div class="row pt-3">
                                <asp:GridView ID="GVArticles" runat="server" AutoGenerateColumns="false" AllowPaging="true" OnPageIndexChanging="GVArticles_PageIndexChanging" CssClass="table table-bordered table-striped" >
                                    <Columns>
                                        <asp:TemplateField HeaderText="No" >   
                                         <ItemTemplate>
                                                <b> <%# Container.DataItemIndex + 1 %> </b>  
                                         </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Judul">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" ForeColor="#3568ff" runat="server" Text='<%#Bind("title") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Content">
                                            <ItemTemplate>
                                                <%# Eval("content") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Kategori">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" ForeColor="#3568ff" runat="server" Text='<%#Bind("Category_name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" ForeColor="#3568ff" runat="server" Text='<%#Bind("status") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tanggal Terbit">
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" ForeColor="#3568ff" runat="server" Text='<%#Bind("Publish_Date") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Diterbitkan Oleh">
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" ForeColor="#3568ff" runat="server" Text='<%#Bind("Publish_User") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ubah">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Bind("ID") %>' OnClick="EditClick" CssClass="btn btn-success"    ><i class="fa fa-edit"></i></asp:LinkButton>
                                            </ItemTemplate>
                                       </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Hapus">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandArgument='<%#Bind("ID") %>' OnClick="DeleteClick" CssClass="btn btn-danger"   ><i class="fa fa-trash"></i></asp:LinkButton>
                                            </ItemTemplate>
                                       </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <div align="center">Tidak Ada Berita</div>
                                    </EmptyDataTemplate>
                            </asp:GridView>
                            </div>
                            <a href="AllNews.aspx" target="_blank"><i class="fa fa-eye"></i> Lihat hasil website </a>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
</asp:Content>
