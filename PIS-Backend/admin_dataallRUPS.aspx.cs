﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace PIS_Backend
{
    public partial class admin_dataallRUPS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "25"))
                {
                    Response.Redirect("admin.aspx");
                }

                BindRUPS();
            }
        }
        public void BindRUPS()
        {
            modelRUPS lt = new modelRUPS();
            DataTable dt = lt.Select("*");
            ListViewRUPS.DataSource = dt;
            ListViewRUPS.DataBind();
        }
        protected void EditClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument; 
            Response.Redirect("admin_addRUPS.aspx?ID=" + id);
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            modelRUPS lt = new modelRUPS();
            General all = new General();
            string cond = " ID = " + id;
            try
            {
                lt.Delete(id,getip(),GetBrowserDetails(),cond);
                BindRUPS();
            }
            catch(Exception ex)
            {
                lt.ErrorLogHistory(getip(), GetBrowserDetails(), "Delete", lt.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                Response.Write(all.alert("Error with " + ex.Message.ToString()));
            }
            
        }

        protected void ListViewRUPS_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (ListViewRUPS.FindControl("DataPager1") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindRUPS();
        }
    }
}