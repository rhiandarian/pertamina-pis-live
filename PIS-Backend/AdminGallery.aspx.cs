﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class AdminGallery : System.Web.UI.Page
    {
        General all = new General();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                //if (Session["Level"].ToString() != "1")
                //{
                //    Response.Redirect("admin.aspx");
                //}
                Gallery g = new Gallery();
                string type = Request.QueryString["type"].ToString();
                int typeInp = 0;


                if (type == "Video")
                {
                    typeInp = all.VideoCategoryID;
                }
                if (type == "Foto")
                {
                    typeInp = all.FotoCategoryID;
                }
                if (type == "Infografis")
                {
                    typeInp = all.InfografisCategoryID;
                }
                Response.Redirect(g.adminListPage + "?type=" + typeInp);

            }
        }
        
    }
}