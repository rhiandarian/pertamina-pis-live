﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PIS_Backend
{
    public partial class keberlanjutan_old : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadPage();
            }
        }
        public void loadPage()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 19 ");
            imgHeader.Src = dt.Rows[0]["Header"].ToString(); 
            string decodecontent = Encoding.UTF8.GetString(Convert.FromBase64String(Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content"].ToString()));
            content.InnerHtml = decodecontent;

            li_1.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Home" : "Beranda";
            li_2.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Commitment & Sustainibility" : "Komitmen & Keberlanjutan";
            li_3.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Sustainability" : "Keberlanjutan";
            header_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Sustainability" : "Keberlanjutan";
            header_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Sustainability" : "Keberlanjutan";
            editClassLang();
        }

        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
    }
}