﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.IO;

namespace PIS_Backend
{
    public partial class news_detail : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if(Session["Lang"] == null || Session["Lang"].ToString() == "Ind")
                {
                    
                }
                BindNews();
                GetLatestNews();
                LoadLang();
            }
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
        public void LoadLang()
        {
            editClassLang();//
            link1.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Media & Information" : link1.InnerText;
           

            htitle.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "News" : htitle.InnerText;

            ctgr.InnerText = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "OTHER NEWS" : ctgr.InnerText;

            if (Session["Lang"] != null && Session["Lang"].ToString() == "En")
            {
                LinkButton2.Visible = true;
                LinkButton1.Visible = false;
            }
            else
            {
                LinkButton1.Visible = true;
                LinkButton2.Visible = false;
            }

        }
        public void BindNews()
        {
            if (Request.QueryString["title"] == null)
            {
                Response.Redirect("News.aspx");
            }
            string Id = Request.QueryString["title"].ToString();
            News nws = new News();

            string title = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "title_en as title" : "title";
            string key = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "key_en as keys" : "[key] as keys";
            string content = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "content_en as content" : "content";
            string category = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Category_En" : "Category_name";
            string link = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "news-detail" : "berita-detail";

            string field = "top 1 "+title+", "+key+", uuid,CASE WHEN Dates IS NULL THEN CONVERT(varchar,addDate,106) ELSE CONVERT(varchar,Dates,106) END as 'news_date', UPPER(" + category+") as Category_name, img_file, city, "+content;
            string conditions =  " [key] LIKE  '%" + Id + "%' or key_en LIKE  '%" + Id + "%'";
            DataTable dt = nws.Select(field,conditions);
            bool exsist = false;
            if(dt.Rows.Count > 0)
            {
                exsist = true;
                ttl.InnerText = dt.Rows[0]["title"].ToString();
                LblkeyWord.Text = dt.Rows[0]["keys"].ToString();
                LinkButton1.CommandArgument = dt.Rows[0]["uuid"].ToString();
                LinkButton2.CommandArgument = dt.Rows[0]["uuid"].ToString();
                ListViewNews.DataSource = dt;
                ListViewNews.DataBind();
            }
            string titleQS = exsist ? dt.Rows[0]["title"].ToString() : Id;
        


        }
        public void GetLatestNews()
        {
            string lang = Session["Lang"] == null ? "Ind" : Session["Lang"].ToString();
            News nws = new News();
            DataTable dt = nws.getLatestNews(lang);
            ListViewLatestNews.DataSource = dt;
            ListViewLatestNews.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                News n = new News();
                LinkButton lb = (LinkButton)sender;
                string uuid = lb.CommandArgument;
                string cond = " uuid = '"+uuid+"'";
                DataTable dt = n.Select("ID, title, title_en, content, content_en", cond);


                //pdfDoc.Open(); 
                int ID = Convert.ToInt32(dt.Rows[0]["ID"].ToString());
                string title = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["title_en"].ToString() : dt.Rows[0]["title"].ToString();
                string content = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content"].ToString();
                StringBuilder sb = new StringBuilder();
                sb.Append("<p><b>" + title + "</b></p>");
                sb.Append("<p>" + content + "</p>");
                StringReader sr = new StringReader(sb.ToString());

                Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
                HTMLWorker htmlParser = new HTMLWorker(pdfDoc);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                    pdfDoc.Open();

                    htmlParser.Parse(sr);
                    pdfDoc.Close();

                    byte[] bytes = memoryStream.ToArray();
                    memoryStream.Close();

                    Response.Clear();
                    // Gets or sets the HTTP MIME type of the output stream.
                    Response.ContentType = "application/pdf";
                    // Adds an HTTP header to the output stream
                    Response.AddHeader("Content-Disposition", "attachment; filename=Pertamina-PIS-News #" + ID + ".pdf");

                    //Gets or sets a value indicating whether to buffer output and send it after
                    // the complete response is finished processing.
                    Response.Buffer = true;
                    // Sets the Cache-Control header to one of the values of System.Web.HttpCacheability.
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    // Writes a string of binary characters to the HTTP output stream. it write the generated bytes .
                    Response.BinaryWrite(bytes);
                    // Sends all currently buffered output to the client, stops execution of the
                    // page, and raises the System.Web.HttpApplication.EndRequest event.

                    Response.End();
                    // Closes the socket connection to a client. it is a necessary step as you must close the response after doing work.its best approach.
                    Response.Close();
                }


            }
            catch (Exception ex)
            {
                Response.Write(ex.Message.ToString());
            }

        }
    }
}