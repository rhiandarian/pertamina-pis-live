﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls; 
using System.Data;

namespace PIS_Backend
{
    public partial class admin_addRUPS : System.Web.UI.Page
    {
        General all = new General();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "25"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                year.Text = "0";
                LoadData();
            }
        }
        public void LoadData()
        {
            if (Request.QueryString["ID"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                string cond = " ID = " + Id;
                modelRUPS p = new modelRUPS();
                DataTable dt = p.Select("*", cond);
                title.Text = dt.Rows[0]["title"].ToString();
                title_en.Text = dt.Rows[0]["title_en"].ToString();
                year.Text = dt.Rows[0]["year"].ToString();
            }
        }
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                modelRUPS lt = new modelRUPS();
                lt.Year = Convert.ToInt32(year.Text);
                lt.Title_En = title_en.Text;
                lt.Title = title.Text;
                bool file = false;
                bool validfile = true;
                string validation = "";

                if (FileUpload1.FileName != "")
                {
                    if (!all.PdfFileType(FileUpload1.FileName))
                    {
                        validfile = false;
                    }
                    file = true;
                    lt.Url = "ImageRUPS/"+FileUpload1.FileName;
                    lt.Cover = FileUpload1.FileName;

                }
                else
                {
                    lt.Url = "#";
                    lt.Cover = "#"; 

                }

                if (validfile == false)
                {
                    validation = "File harus Pdf ";
                }

                if (validation == "")
                {
                    string insertStatus = "";
                    if (Request.QueryString["ID"] == null)
                    {
                         insertStatus = lt.Insert(getip(),GetBrowserDetails());
                    }
                    else
                    {
                        int Id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                        string cond = " ID = " + Id;
                        //showValidationMessage(cond);
                        insertStatus = lt.Update(Id,getip(),GetBrowserDetails(),cond);
                    }

                    if (insertStatus == "Success")
                    {
                        if (file == true)
                        {
                            string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                            string SaveLocation = Server.MapPath("ImageRUPS") + "\\" + fn;

                            FileUpload1.PostedFile.SaveAs(SaveLocation);
                        }
                        Response.Redirect("admin_dataallRUPS.aspx");
                    }
                    else
                    {
                        showValidationMessage(insertStatus);
                    }
                }
                else
                {
                    showValidationMessage(validation);
                }

            }
            catch (Exception ex)
            {
                modelRUPS lt = new modelRUPS();
                string action = Request.QueryString["ID"] == null ? "Insert" : "Update";
                lt.ErrorLogHistory(getip(), GetBrowserDetails(), action, lt.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
                showValidationMessage(ex.ToString());
            }
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        public void showValidationMessage(string textMessage)
        {
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
    }
}