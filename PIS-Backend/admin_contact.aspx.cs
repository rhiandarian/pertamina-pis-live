﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class admin_contact : System.Web.UI.Page
    {  
        public string PageUrl;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "31"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                successdiv.Visible = false;
                LoadData();

            }
        }
        public void LoadData()
        {
            ModelData Db = new ModelData();
            string query = "Select * from  Contact  where ID = 1";   
            DataTable dt = Db.getData(query);
            //company.Text = dt.Rows[0]["name_company"].ToString();
            address.InnerHtml = dt.Rows[0]["address"].ToString();
            address_en.InnerHtml = dt.Rows[0]["address_en"].ToString();
            phone.Text = dt.Rows[0]["phone"].ToString();
            Email.Text = dt.Rows[0]["email"].ToString();
            link_yt.Text = dt.Rows[0]["link_yt"].ToString();
            link_fb.Text = dt.Rows[0]["link_fb"].ToString();
            link_tw.Text = dt.Rows[0]["link_tw"].ToString();
            link_ig.Text = dt.Rows[0]["link_ig"].ToString();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            { 
                ModelData Db = new ModelData();
                string field = "address = '" + address.InnerText + "',address_en = '" + address_en.InnerText + "', phone = '" + phone.Text + "', email = '" + Email.Text + "', link_ig = '" + link_ig.Text + "', link_yt = '" + link_yt.Text + "', link_tw = '" + link_tw.Text + "', link_fb = '" + link_fb.Text + "'";
                string query = "update Contact set "+ field +" where ID = 1";
                Db.setData(query);

                successMessage("Data kontak telah tersimpan");

            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
        }
        public void showValidationMessage(string textMessage)
        {
            successdiv.Visible = false;
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void successMessage(string textMessage)
        {
            LblErrorMessage.Visible = false;
            successdiv.InnerHtml = textMessage;
            successdiv.Visible = true;

        }
         
    }
}