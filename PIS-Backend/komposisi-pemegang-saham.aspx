﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="komposisi-pemegang-saham.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.komposisi_pemegang_saham" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a id="ttl" href="Index.aspx"></a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="link1" runat="server">Informasi Saham</li>
					    <li class="breadcrumb-item active" aria-current="page" id="link2" runat="server">Komposisi Pemegang Saham</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="title" runat="server">Komposisi Pemegang Saham</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  			<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <section id="komposisi-pemegang-saham">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div id="content" class="col-lg-12 content" runat="server">
	  				
	  			</div>
	  		</div>
	  	</div>
  	</section>
    <script>
        var urlName = '', ttl = "";
        if ($("#LbInd").attr("class") == "active") {
            urlName = 'komposisi-pemegang-saham';
            ttl = "Beranda";
        }
        if($("#LbEn").attr("class") == "active")
        {
            urlName = 'shareholders-composition';
            ttl = "Home";
        }
        $("#ttl").text(ttl);
        history.pushState({}, null,urlName);
    </script>
</asp:Content>
