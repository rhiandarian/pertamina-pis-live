﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class R_U_P_S : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadData();
            BindLaporanRUPS(1);
            
        }

        public void LoadData()
        {
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = 22 ");
            header.Src = dt.Rows[0]["Header"].ToString();
            content.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content"].ToString();
            li_1.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Home" : "Beranda";
            li_2.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "Investor Relations" : "Hubungan Investor";
            li_3.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "AGMS" : "RUPS";
            header_text.InnerHtml = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? "AGMS" : "RUPS";
            editClassLang();
        }
        public void BindLaporanRUPS(int PageIndex)
        {
            bool langen = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? true : false;
            modelRUPS lt = new modelRUPS();
            //DataTable dt = lt.LoadlAll(PageIndex);
            //int totalRecord = Convert.ToInt32(dt.Rows[0]["Total Record"].ToString()); 
            string field = langen ? "title_en as 'title', " : "title, ";
            field += "url,cover";
            DataTable dt = lt.Select(field);
            ListViewLt.DataSource = dt;
            ListViewLt.DataBind();  
        }

        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
    }
}