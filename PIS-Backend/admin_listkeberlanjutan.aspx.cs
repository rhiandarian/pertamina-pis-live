﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class admin_listkeberlanjutan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "28"))
                {
                    Response.Redirect("admin.aspx");
                }
                BindGallery();
            }
        }
        public void BindGallery()
        {
            General gn = new General();
            Gallery g = new Gallery();
            string field = "ID, title, Category_name";
            string conditions = "category_id = "+ gn.CategoryKeberlanjutanID;
            DataTable dt = conditions != "" ? g.Select(field, conditions) : g.Select(field);

            GridViewGallery.DataSource = dt;
            GridViewGallery.DataBind();
        }
        protected void EditClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Response.Redirect("admin-edit-keberlanjutan.aspx?ID=" + id);
        }
        public string GetBrowserDetails()
        {
            string BrowserDetails = HttpContext.Current.Request.Browser.Browser + " - " + HttpContext.Current.Request.Browser.Version + "  |  Operating System : " + HttpContext.Current.Request.Browser.Platform;
            return BrowserDetails;
        }
        public string getip()
        {
            string url = "http://checkip.dyndns.org";
            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            string response = sr.ReadToEnd().Trim();
            string[] ipAddressWithText = response.Split(':');
            string ipAddressWithHTMLEnd = ipAddressWithText[1].Substring(1);
            string[] ipAddress = ipAddressWithHTMLEnd.Split('<');
            return ipAddress[0];

        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            Gallery g = new Gallery();
            General all = new General();
            try
            {
                g.Delete(id,getip(),GetBrowserDetails()," ID = " + id);
                BindGallery();
            }
            catch(Exception ex)
            {
                g.ErrorLogHistory(getip(), GetBrowserDetails(), "Delete", g.tableName, Session["Username"].ToString(), Session["UserID"].ToString(), ex.Message.ToString());
            }
            
            
        }
        public void GVGalleryPageindexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGallery();
            GridViewGallery.PageIndex = e.NewPageIndex;
            GridViewGallery.DataBind();
        }
    }
}