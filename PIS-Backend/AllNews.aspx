﻿    <%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="AllNews.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.AllNews" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="https://bins-git.github.io/pertamina-pis/vendor/animate.css/animate.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="https://bins-git.github.io/pertamina-pis/vendor/owl-carousel/dist/assets/owl.carousel.min.css">
    <link href="https://bins-git.github.io/pertamina-pis/vendor/owl-carousel/dist/assets/owl.theme.default.css" rel="stylesheet" />
    <link href="https://bins-git.github.io/pertamina-pis/css/style.css?v=0.1" rel="stylesheet" />
      <main id="main">
          <asp:Label ID="LblIndex" runat="server" Text="Label"></asp:Label>
  	<!-- Top Main -->
  	<section id="breadcrumbs" class="pt-3">
			<div class="container">
				<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a id="htitle" href="Index.aspx">Beranda</a></li>
					    <li class="breadcrumb-item"><a id="link1" runat="server" href="">Media & Informasi</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="link2" runat="server">Berita</li>
					  </ol>
					</nav>
					<div class="col-lg-12 page-title">
						<h3 id="hTitle" runat="server">Berita</h3> 
					</div>
				</div>
					
				</div>
  	</section>


  	<!-- ======= Media Information Section ======= -->
    <section class="media-information section-bg">
      <div class="container">
				<div class="row no-gutters">
          <div class="col-lg-12 col-12">
          	<div class="row">
        <asp:ListView ID="ListViewNews1" runat="server">
            <ItemTemplate>
                <div class="col-lg-7 col-12 mb-4 media-information-box-left">
			    	  	<div class="col pl-0 pr-0">
				  	    	<div class="card text-white overflow-hidden media-information-box card-block">

                    <a href="news-detail.aspx?title=<%#Eval("Key") %>">
                        <div class="box-image">
  										<img src='<%#Eval("Img_File") %>' class="card-img" alt="" height="100%" width="100%">
                        </div>
  										<div class="card-img-overlay bg-black-transparent">
  											<p class="card-content p-4 ">
  												<em><%#Eval("Dates") %></em> &nbsp;<span class="badge badge-danger"><%#Eval("Categories") %></span>
  												<br><%#Eval("Title") %>
  											</p>
  										</div>
                    </a>
									</div>
								</div>
			    	  </div>
            </ItemTemplate>
        </asp:ListView>
        			
                   
			    	  <div class="col-lg-5 col-12 mb-4">
			    	  	<div class="row">
                              <asp:ListView ID="ListViewNews2" runat="server">
                                  <ItemTemplate>
                                      <div class="col-12 pl-0 pr-0 mb-3 media-information-box-right">
					  	    	<div class="card text-white overflow-hidden media-information-box card-block">
                      <a href="news-detail.aspx?title=<%#Eval("Key") %>">
  											<img src='<%#Eval("Img_File") %>' class="card-img" alt="..." height="100%" width="100%">
  											<div class="card-img-overlay bg-black-transparent">
  												<p class="card-content pl-4 pb-1">
  													<em><%#Eval("Dates") %></em> &nbsp;<span class="badge badge-danger"><%#Eval("Categories") %></span>
  													<br><%#Eval("Title") %>
  												</p>
  											</div>
                      </a>
										</div>
									</div>
                                  </ItemTemplate>
                              </asp:ListView>
				    	  	
                              <asp:ListView ID="ListViewNews3" runat="server">
                                  <ItemTemplate>
                                      <div class="col-12 pl-0 pr-0 mb-4 media-information-box-right">
					  	    	<div class="card text-white overflow-hidden media-information-box card-block">
                      <a href="news-detail.aspx?title=<%#Eval("Key") %>">
  											<img src='<%#Eval("Img_File") %>' class="card-img" alt="..." height="100%" width="100%">
  											<div class="card-img-overlay bg-black-transparent">
  												<p class="card-content pl-4 pb-1">
  													<em><%#Eval("Dates") %></em> &nbsp;<span class="badge badge-danger"><%#Eval("Categories") %></span>
  													<br><%#Eval("Title") %>
  												</p>
  											</div>
                      </a>
										</div>
									</div>
                                  </ItemTemplate>
                              </asp:ListView>
									
			    	  	</div>
			    	  </div>
          	</div>
          </div>
        </div>
      </div>
    </section><!-- Media Information Section -->

    <!-- ======= Berita Section ======= -->
    <section class="berita">
      <div class="container">
				<div class="row no-gutters">
          <div class="col-lg-9 col-12 box-left">
              <asp:ListView ID="ListViewNews" runat="server">
                  <ItemTemplate>
                      <div class="row mb-5">
        			    <div class="col-lg-6">
        				    <div class="box-image">
			  					    <img src='<%# Eval("img_file") %>' class="" alt="..."  width="100%" height="100%">
			  				    </div>
        			    </div>
        			    <div class="col-lg-6">
        				    <div class="row">
        					    <div class="col-lg-12 berita-info">
        						    <p class="pb-0 mb-0"><em><%# Eval("news_date") %></em> &nbsp;<span class="badge badge-danger"><%# Eval("Category_name") %></span></p>
        						    <p class="title"><strong><%# Eval("title") %></strong></p>
        						    <p><%# Eval("content") %></p>
        						    <p><u><a id="readNews" href="news-detail.aspx?title=<%# Eval("keys") %>">Selengkapnya</a></u></p>
        					    </div>
        				    </div>
        			    </div>
          	        </div>
                  </ItemTemplate>
              </asp:ListView>
          	
          	
          	
          	<div class="row mb-5">
		        	<div class="col-12 pagination-box">
		        		<div class="row">
		        			<div class="col-xs-12 col-sm-6">
		        				<p id="detailPagings" runat="server"></p>
		        			</div>
		        			<div class="col-xs-12 col-sm-6 text-right">
		        				<nav aria-label="...">
              <asp:Repeater ID="RptPaging" runat="server">
                  <HeaderTemplate>
                      
										  <ul class="pagination justify-content-end">
                                              <li class="page-item">
                                                <asp:LinkButton  runat="server" Text="<<" CommandArgument="first"
                                                    CssClass="page-link" OnClick="Page_Changed"></asp:LinkButton>
                                              </li>
										    
                  </HeaderTemplate>
                  <ItemTemplate>
                      <li class='<%#Eval("active")%>'>
                        <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Index") %>' CommandArgument='<%# Eval("Index") %>'
                            CssClass="page-link" OnClick="Page_Changed"></asp:LinkButton>
                      </li>
                  </ItemTemplate>
                  <FooterTemplate>
                      <li class="page-item">
                              <asp:LinkButton  runat="server" Text=">>" CommandArgument="last"
                            CssClass="page-link" OnClick="Page_Changed"></asp:LinkButton>
                          </li>
                          </ul>
                      
                  </FooterTemplate>
              </asp:Repeater>
                                    </nav>
                      </div>
                      </div>
                        
                        
		        	</div>
		        </div>

          	<!-- Pagination -->
          	
										    
										    
										    
										  
										
		        			
                            
		        		

          </div>
          <div class="col-lg-3 box-right">
          	<div class="row mb-5">
          		<div class="col-lg-12 category">
          			<div class="col title">
          				<h5 id="ctgr" runat="server">Kategori</h5>
          			</div>
          			<div class="col desc">
                         <ul>
                            <asp:ListView ID="ListViewCategory" runat="server">
                              <ItemTemplate>
                                  <li><a href='<%#Eval("Link")%>'><strong><%#Eval("Category_name")%></strong> (<%#Eval("Total_Category")%>)</a></li>
                              </ItemTemplate>
                            </asp:ListView>
          					
          					
          				</ul>
          			</div>
          		</div>
          	</div>
          	<%--<div class="row mb-5">
          		<div class="col-lg-12 arsip">
          			<div class="col title">
          				<h5 id="arsip" runat="server">Arsip</h5>
          			</div>
          			<div class="col desc">
          				<ul>
          					<li><a href=""><strong>2021</strong> (100)</a></li>
          					<li><a href=""><strong>2020</strong> (32)</a></li>
          					<li><a href=""><strong>2019</strong> (15)</a></li>
          				</ul>
          			</div>
          		</div>
          	</div>--%>
            <div class="row mb-5">
              <div class="col-lg-12 energia-persero">
                <div class="col title">
                  <h5 ">Energia Persero</h5>
                </div>
                <div class="col desc pl-0 pr-0 pt-0">
                  <div class="row">
                    <div class="col-12 mt-3 energia-carousel owl-carousel owl-theme">
                      <div class="box-content">
                          <a href="https://pertamina.com/id/digital-media/energia-weekly/">
                              <img src="https://pertamina.com/Media/File/thumbs/20210703164020044_0f87deef774c407d8fc798277b008546.PNG" class="img-fluid mt-2 mb-2" alt="...">
                          </a>
                      </div>
                      <div class="box-content">
                          <a href="https://pertamina.com/id/digital-media/energia-weekly/">
                              <img src="https://pertamina.com/Media/File/thumbs/20210627091106055_80e771dea6ed4b3e86195c199fa05bda.jpg" class="img-fluid mt-2 mb-2" alt="...">
                          </a>
                      </div>
                      <div class="box-content">
                          <a href="https://pertamina.com/id/digital-media/energia-weekly/">
                            <img src="https://pertamina.com/Media/File/thumbs/20210619143148197_9ca152b63eac438ea74544687bd0f11d.png" class="img-fluid mt-2 mb-2" alt="...">
                          </a>
                      </div>
                    </div>
                    <div class="col-12 text-center">
                      <a href="https://pertamina.com/id/digital-media/energia-weekly/" target="_blank" id="allshow" runat="server">Lihat Semua</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
             
              <div class="row mb-5">
                   <a  id="link_ig" runat="server">
              <div class="col-lg-12 instagram-feed">
                <div class="col title">
                  <h5>Instagram</h5>
                </div>
                <div class="col desc pl-0 pr-0">
                  <img src="images/feed/Instagram_horizontal.png" class="img-fluid" width="100%" alt="...">
                </div>
              </div>
                        </a>
            </div>
             
              <%--<div class="row mb-5">
                   <a  id="link_fb" runat="server">   
              <div class="col-lg-12 facebook-feed">
                <div class="col title">
                  <h5>Facebook</h5>
                </div>
                <div class="col desc pl-0 pr-0">
                  <img src="images/feed/Facebook_Feed.png" class="img-fluid" width="100%" alt="...">
                </div>
              </div>
                        </a>
            </div>--%>
          </div>
        </div>
        
      </div>
    </section><!-- Media Information Content Section -->

  </main>
    <script src="https://bins-git.github.io/pertamina-pis/vendor/jquery.min.js"></script>
    <script src="https://bins-git.github.io/pertamina-pis/vendor/owl-carousel/dist/owl.carousel.min.js"></script>
    <script type="text/javascript">
        var title = $("#htitle").text();
        if ($("#LbEn").attr("class") == "active")
        {
            history.pushState({}, null, "news");
            title = "Home";
            $("#readNews").text('Read More');
        }
        else
        {
            history.pushState({}, null, "berita");
            title = "Beranda";
        }
        $("#htitle").text(title);
        
    $(document).ready(function(){

      let pengadaan = $('.energia-carousel');
      pengadaan.owlCarousel({
        margin: 15,
        nav: false,
        navText:["<div class='nav-btn prev-slide justify-content-center bg-green'><i class='bi bi-chevron-left'></i></div>","<div class='nav-btn next-slide bg-green'><i class='bi bi-chevron-right'></div>"],
        loop: false,
        autoplayTimeout:3000,
        autoplayHoverPause:false,
        responsive: {
          0: {
            items: 1
          }
        }
      });

    });

    //let imgHighlighLeft = $(".media-information-box-left").find('img');
    //$.each(imgHighlighLeft, function(key, value){
    //  console.log(value);
    //  let widthImage = value.width;
    //  let heightImage = value.height;
    //  if(heightImage<425){
    //    $(this).css({'height': '100%', 'width': 'auto'});
    //  } else {
    //    $(this).css({'width': '100%', 'height': 'auto'});
    //  }
    //});

    //let imgHighlighRight = $(".media-information-box-right").find('img');
    //$.each(imgHighlighRight, function(key, value){
    //  let widthImage = value.width;
    //  let heightImage = value.height;
    //  console.log(heightImage);
    //  if(heightImage<201){
    //    $(this).css({'height': '100%', 'width': 'auto'});
    //  } else {
    //    $(this).css({'width': '100%', 'height': 'auto'});
    //  }
    //});

    //let imgBox = $(".box-image").find('img');
    //$.each(imgBox, function(key, value){
    //  let widthImage = value.width;
    //  let heightImage = value.height;
    //  if(heightImage<251){
    //    $(this).css({'height': '100%', 'width': 'auto'});
    //  } else {
    //    $(this).css({'width': '100%', 'height': 'auto'});
    //  }
    //});
  </script>
</asp:Content>
