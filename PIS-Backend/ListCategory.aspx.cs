﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class ListCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["Level"].ToString() != "1")
                {
                    Response.Redirect("Index.aspx");
                }
                BindGVCategory();


            }
        }
        public void BindGVCategory(string txtSearch = null)
        {
            Category ctg = new Category();
            string fieldSelect = "ID, Category_name, Category_type_name";
            string conditions = txtSearch != null ? "Category_name like '%" + txtSearch + "%'" : "";
            DataTable dt = txtSearch == null ? ctg.Select(fieldSelect) : ctg.Select(fieldSelect, conditions);

            GVCategory.DataSource = dt;
            GVCategory.DataBind();
        }
        protected void DeleteClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string id = lb.CommandArgument;
            string WhereId = "ID = " + id;
            Category Db = new Category();
            Db.Delete(WhereId);
            BindGVCategory();
        }
        protected void GVCategory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGVCategory();
            GVCategory.PageIndex = e.NewPageIndex;
            GVCategory.DataBind();
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {

        }
    }
}