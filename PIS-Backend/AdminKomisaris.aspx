﻿    <%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminKomisaris.aspx.cs" Inherits="PIS_Backend.AdminKomisaris" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Komisaris Data</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Tentang PIS
                        </li>
                        <li class="breadcrumb-item">
                            <a>Manajemen</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Dewan komisaris</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="AdminKomisaris.aspx"><strong> Semua </strong></a>
                        </li>
                        
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row ml-2 pb-3">
            <a href="SaveKomisaris.aspx" class="btn btn-primary"><i class="fa fa-plus"></i><span class="add_new_text">Buat Baru</span></a>
        </div>
        <div class="row">
            <asp:ListView ID="ListViewKomisaris" runat="server">
                <ItemTemplate>
                    <div class="col-xs-12 col-sm-3 box-content">
                                                        <div class="ibox">
                                                            <div class="ibox-content product-box">

                                                                <div class="">
                                                                     <img src='<%# Eval("img_file") %>' class="img-fluid" alt="...">
                                                                </div>
                                                                <div class="product-desc text-center">
                                                                     
                                                                    <small class="text-muted"><%# Eval("division") %></small>
                                                                    <a href="#" class="product-name"> <%# Eval("name") %></a> 
                                                                    <div class="m-t text-righ">
                                                                        <asp:LinkButton ID="LinkButton3" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="EditClick" runat="server"><i class="fa fa-pencil"></i> Edit</asp:LinkButton>
                                                                        <asp:LinkButton ID="LinkButton4" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="DeleteClick" runat="server"><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                                                                                
                                                                        <%--<a href="#" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>--%>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <%--<div class="ibox-content product-box">
                                                                <div class="product-imitation">
                                                                    <img src='<%# Eval("img_file") %>' class="img-fluid" alt="...">
                                                                    
                                                                </div>
                                                                <div class="product-desc">
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            
                                                                            <p class="text-center">
                                                                               <b><%# Eval("name") %></b> <br />
                                                                                <%# Eval("division") %><br />
                                                                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="EditClick" runat="server"><i class="fa fa-pencil"></i> Edit</asp:LinkButton>
                                                                                <asp:LinkButton ID="LinkButton2" CssClass="btn btn-xs btn-outline btn-primary" CommandArgument='<%# Eval("ID") %>' OnClick="DeleteClick" runat="server"><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                                                                                
                                                                            </p>
                                                                        </div>

                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>--%>
                                                        </div>
                                                    </div>
                </ItemTemplate>
            </asp:ListView>
             
                 </div>
        <a href="Dewan_Komisaris.aspx" target="_blank"><i class="fa fa-eye"></i> Lihat hasil website</a>
    </div>
</asp:Content>
