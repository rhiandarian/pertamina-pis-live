﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class PengadaanJson : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Response.Write(data());
            }
        }
        public string data()
        {
            InfoPengadaan ip = new InfoPengadaan();
            ip.OrderBy = " Dates Desc ";
            DataTable dt = ip.Select("top 8 SUBSTRING(title,1,24) as title, SUBSTRING(title_en,1,24) as title_en, url, cover, CONVERT(varchar,Dates,106) as 'date' ");
            General all = new General();
            string print = "[";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string coma = i < dt.Rows.Count - 1 ? ", " : "";
                print += "{" + all.addJsonFieldValue("title", dt.Rows[i]["title"].ToString());
                print += all.addJsonFieldValue("title_en", dt.Rows[i]["title_en"].ToString());
                print += all.addJsonFieldValue("url", dt.Rows[i]["url"].ToString());
                print += all.addJsonFieldValue("cover", dt.Rows[i]["cover"].ToString());
                print += all.addJsonFieldValue("date", dt.Rows[i]["date"].ToString(),true)+"}"+coma;
            }
            print += "]";
            return print;
        }
    }
}