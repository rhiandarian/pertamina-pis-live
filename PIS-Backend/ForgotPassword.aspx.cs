﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace PIS_Backend
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        SqlConnection con = Connection.conn();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                LblErrorMessage.Visible = false;
            }

        }

        protected void BtnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                string email = Email.Text;
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(email);
                if (match.Success)
                {
                    SqlCommand cmd = new SqlCommand("ResetPasswordUser", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@email", Email.Text);
                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    if (dt.Rows[0]["Result"].ToString() != "Error")
                    {
                        Email emails = new Email();
                        emails.sendEmail("Reset Password", Email.Text, "<b>Kode Aktivasi Anda adalah " + dt.Rows[0]["Result"].ToString() + " </b>");
                        LblErrorMessage.Visible = false;
                        Session.Add("KeyCode", dt.Rows[0]["Result"].ToString());
                        Response.Redirect("ResetPassword.aspx");
                    }
                    else
                    {
                        LblErrorMessage.Visible = true;
                        LblErrorMessage.Text = "Email tidak terdaftar";
                    }
                }
                else
                {
                    LblErrorMessage.Visible = true;
                    LblErrorMessage.Text = "Email tidak terdaftar";
                }

            }
            catch(Exception ex)
            {
                LblErrorMessage.Visible = true;
                LblErrorMessage.Text = ex.Message;
            }
            
        }
    }
}