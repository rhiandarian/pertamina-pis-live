﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="informasi-dividen.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.informasi_dividen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="breadcrumbs" class="pt-3">
			<div class="container">
  			<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx" id="li_1" runat="server">Home</a></li>
					    <li class="breadcrumb-item active" aria-current="page" id="li_2" runat="server">Informasi Saham</li>
					    <li class="breadcrumb-item active" aria-current="page" id="li_3" runat="server">Informasi Dividen</li>
					  </ol>
					</nav>
					<div class="col-12 page-title">
						<h3 id="header_text" runat="server">Informasi Dividen</h3>
					</div>
				</div>
  		</div>
			<div class="col-lg-12 p-0 mt-4 banner-image">
  				<img id="imgHeader" runat="server" class="img-fluid" alt="...">
			</div>
  	</section>
    <section id="komposisi-pemegang-saham">
  		<div class="container">
	  		<div class="row mb-5">
	  			<div class="col-lg-12 content">
	  				<div class="row">
					  <div id="content" runat="server" class="col-md-12">
                
					  </div>
						  <%--<center class="mt-4">
						<a id="fileDownload" runat="server"  target="_blank" class="btn btn-default-pis">
							<span class="bi bi-file-pdf-fill"></span> Download pdf</a>

					</center>--%>
					</div>
	  			</div>
	  		</div>
	  	</div>
  	</section>
    <script>
        var urlName = '';
        if ($("#LbInd").attr("class") == "active") {
            urlName = 'informasi-dividen';
        }
        if($("#LbEn").attr("class") == "active")
        {
            urlName = 'dividend-information';
        }
        history.pushState({}, null,urlName);
    </script>
</asp:Content>
