﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data; 
using System.Text; 

namespace PIS_Backend
{
    public partial class admin_career : System.Web.UI.Page
    {
        public string PageUrl;

        string folderHeader = "Header";
        string folderDownload = "documents";
        
        General all = new General();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                Level l = new Level();
                if (!l.isValidAccessPage(Session["Level"].ToString(), "30"))
                {
                    Response.Redirect("admin.aspx");
                }
                LblErrorMessage.Visible = false;
                LblErrorMessage.ForeColor = System.Drawing.Color.Red;
                successdiv.Visible = false;
                LoadData();

            }
        }
        public void LoadData()
        {
            Carier pd = new Carier();
            DataTable dt = pd.Select("*", " ID = 1 ");
            //string decodecontent = Encoding.UTF8.GetString(Convert.FromBase64String(dt.Rows[0]["content"].ToString()));
            content.InnerHtml = dt.Rows[0]["content"].ToString();
            //string decodecontent2 = Encoding.UTF8.GetString(Convert.FromBase64String(dt.Rows[0]["content_en"].ToString()));
            content_en.InnerHtml = dt.Rows[0]["content_en"].ToString(); 
            disclaimer.InnerHtml = dt.Rows[0]["disclaimer"].ToString();
            disclaimer_en.InnerHtml = dt.Rows[0]["disclaimer_en"].ToString(); 
            magang.InnerHtml = dt.Rows[0]["magang"].ToString();
            magang_en.InnerHtml = dt.Rows[0]["magang_en"].ToString();
            magang_en.InnerHtml = dt.Rows[0]["magang_en"].ToString();
            maillamaran.Text = dt.Rows[0]["mail_lamaran_kerja"].ToString();
            mailmagang.Text = dt.Rows[0]["mail_permohonan_magang"].ToString();
            PageUrl = dt.Rows[0]["name"].ToString();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                Carier pd = new Carier();
                string validation = "";
                string validationKarier = "";
                string filenameheader = "";
                string filenamekarier = "";
                bool HeaderExsist = false;
                bool ImageKarier = false;
                bool validheader = true;
                bool validImageKarier = true;
                //bool DownloadExsist = false;
                //bool validDownload = true;
                if (image_karier.FileName != "")
                {
                    ImageKarier = true;
                    pd.Others = folderHeader + "/Karier_" + image_karier.FileName;
                    filenamekarier = "/Karier_" + image_karier.FileName;
                    if (!all.ImageFileType(image_karier.FileName))
                    {
                        validImageKarier = false;
                    }
                    else
                    {
                        UploadFileKarier(folderHeader, image_karier.PostedFile.FileName, filenamekarier);
                    }

                }

                if (FileUpload1.FileName != "")
                {
                    HeaderExsist = true;
                    pd.Header = folderHeader + "/CAREER-" + FileUpload1.FileName;
                    filenameheader =  "/CAREER-" + FileUpload1.FileName; 
                    if(!all.ImageFileType(FileUpload1.FileName))
                    {
                        validheader = false;
                    }
                   

                }
                
                pd.Content = content.InnerText;/*Convert.ToBase64String(Encoding.UTF8.GetBytes(content.Value));*/
                pd.Content_En = content_en.InnerText;/*Convert.ToBase64String(Encoding.UTF8.GetBytes(content_en.Value));*/

                pd.Disclaimer = disclaimer.InnerText; /*Convert.ToBase64String(Encoding.UTF8.GetBytes(disclaimer.Value));*/
                pd.Disclaimer_en = disclaimer_en.InnerText;/*Convert.ToBase64String(Encoding.UTF8.GetBytes(disclaimer_en.Value));*/

                pd.Magang = magang.InnerText;/*Convert.ToBase64String(Encoding.UTF8.GetBytes(magang.Value));*/
                pd.Magang_en = magang_en.InnerText;/*Convert.ToBase64String(Encoding.UTF8.GetBytes(magang_en.Value)); */

                pd.Mail_lamaran_kerja = maillamaran.Text;
                pd.Mail_permohonan_magang = mailmagang.Text;

                if (validheader == false)
                {
                    validation = pd.validationHeaderImage;
                }
                 
                if(validation == "")
                {
                    if(HeaderExsist == true)
                    {
                        UploadFile(folderHeader, FileUpload1.PostedFile.FileName, filenameheader); 

                    } 

                    pd.Save(" ID = 1");
                    successMessage(pd.successMessage);
                }
                else
                {
                    showValidationMessage(validation);
                }

            }
            catch(Exception ex)
            {
                showValidationMessage(ex.Message.ToString());
            }
        }
        public void showValidationMessage(string textMessage)
        {
            successdiv.Visible = false;
            LblErrorMessage.Visible = true;
            LblErrorMessage.ForeColor = System.Drawing.Color.Red;
            LblErrorMessage.Text = textMessage;
        }
        public void successMessage(string textMessage)
        {
            LblErrorMessage.Visible = false;
            successdiv.InnerHtml = textMessage;
            successdiv.Visible = true;

        }
        public void UploadFile(string folderName, string fileUpload,string filename)
        { 
            string fn = System.IO.Path.GetFileName(fileUpload);
            string SaveLocation = Server.MapPath(folderName) + "\\" + filename; 
            FileUpload1.PostedFile.SaveAs(SaveLocation);

        }

        public void UploadFileKarier(string folderName, string fileUpload, string filename)
        {
            string fn = System.IO.Path.GetFileName(fileUpload);
            string SaveLocation = Server.MapPath(folderName) + "\\" + filename;
            image_karier.PostedFile.SaveAs(SaveLocation);

        }
        protected void uploadFile_Click(object sender, EventArgs e)
        {
            if (image_karier.HasFiles)
            {
                foreach (HttpPostedFile uploadedFile in image_karier.PostedFiles)
                {
                    uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath(folderHeader), "/Karier_" + uploadedFile.FileName));
                    //listofuploadedfiles.Text += String.Format("{0}<br />", uploadedFile.FileName);
                }
            }
        }
    }
}