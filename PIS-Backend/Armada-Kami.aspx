﻿<%@ Page Language="C#" MasterPageFile="~/PIS-Master.Master" AutoEventWireup="true" CodeBehind="Armada-Kami.aspx.cs" EnableEventValidation="false" Inherits="PIS_Backend.Armada_Kami" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style>
        .Datashiw
    </style>
  	<!-- Top Main -->
    <asp:Label ID="LblIndex" runat="server" Text="Label"></asp:Label>
  	<section id="breadcrumbs" class="pt-3">
			<div class="container">
				<div class="row">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="Index.aspx">Beranda</a></li>
					    <li class="breadcrumb-item"><a>Aktivitas Bisnis</a> </li>
					    <li class="breadcrumb-item active" aria-current="page">Armada Kami</li>
					  </ol>
					</nav>
					<div class="col-lg-12 page-title">
						<h3>ARMADA KAMI</h3>
                        <a id="btnUpload" href="SaveKapalMilik.aspx" class="btn btn-primary" runat="server" ><i class="fa fa-file"></i> Upload</a>
					</div>
				</div>
					
				</div>
  	</section>


  	<!-- =======  ======= -->
    <section class="armada-kami">
      <div class="container">
				<div class="row no-gutters">
					<div class="col-12 mb-4" id="contentArmada" runat="server">

					</div>
					<div class="col-12 mb-3">
						<ul class="nav nav-tabs row-eq-height" id="myTab" role="tablist">
							<li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5 active" id="video-tab" data-toggle="tab" href="#kapal-milik" role="tab" aria-controls="video" aria-selected="true">
							   Kapal Milik
							  </a>
						  </li>
						  <%--<li class="nav-item pl-0 pr-0">
						    <a class="nav-link pl-0 pr-0 mr-5" id="photo-tab" data-toggle="tab" href="#profil-armada" role="tab" aria-controls="photo" aria-selected="false">
						    	Profil Armada
						    </a>
						  </li>--%>
						</ul>
						<div class="tab-content bg-white pt-5 pb-5" id="myTabContent">
						  <div class="tab-pane fade show active" id="kapal-milik" role="tabpanel" aria-labelledby="video-tab">
						  	<div class="row"> 
                                         <asp:ListView ID="ListViewKapal" runat="server" OnItemDataBound="ListViewKapal_ItemDataBound">
                                                  <ItemTemplate>
                                                      <div class="col-xs-12 col-sm-4 box-content">
                                                          <div class="col box-pis pt-3">
					  					                        <a href="#modal-armada-<%# Eval("ID") %>" class="popup-armada">
						  					                        <div class="box-image">
						  						                        <img src="<%# Eval("img_file") %>" class="" alt="..." style="height: 100%; width: auto;">
					  						                        </div>
						  					                        <p class="text-left">
						  						                         <strong><%# Eval("name") %></strong>
						  					                         </p>
                                                                     <table class="table table-borderless pl-0">
						  						                        <tr>
					                                                      <td>TYPE</td>
					                                                      <td><strong><%# Eval("type") %></strong></td>
					                                                    </tr>
					                                                    <tr>
					                                                      <td>BUILT</td>
					                                                      <td><strong><%# Eval("built") %></strong></td>
					                                                    </tr>
					                                                    <tr>
					                                                      <td>SIZE (DWT)</td>
					                                                      <td><strong><%# Eval("size") %></strong></td>
					                                                    </tr>
					                                                    <tr>
					                                                      <td>CAPACITY (BBLS)</td>
					                                                      <td><strong><%# Eval("capacity") %></strong></td>
					                                                    </tr>
					                                               </table>
				                                                 </a>
                                                                  <asp:LinkButton ID="LinkButtonEdit" CommandArgument='<%# Eval("ID") %>' OnClick="EditKapal" runat="server" CssClass="btn btn-success"><i class="fa fa-edit"></i></asp:LinkButton>

					  				                         </div>
                                                      </div>
                                                  </ItemTemplate>
                                              </asp:ListView>
                                 </div>

						  	<!---Pagination-->
                            <div class="row mt-4">
				        	        <div class="col-12 pagination-box">
				        		        <div class="row">
				        			        <div class="col-xs-12 col-sm-6">
				        				        <p id="DetailPagingKapal" runat="server">Pages: 1 of 52</p>
				        			        </div>
                                            <div class="col-xs-12 col-sm-6 text-right">
				        				        <nav aria-label="...">
                                                    <asp:Repeater ID="RptKapal" runat="server">
                                                        <HeaderTemplate>
                                                            <ul class="pagination  justify-content-end">
											                   <li class="page-item">
                                                                    <asp:LinkButton  runat="server" Text="<<" CommandArgument="first"
                                                                    CssClass="page-link" OnClick="page_changed_kapal" ></asp:LinkButton>
                                                              </li>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <li class='<%#Eval("active")%>'>
                                                                <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Index") %>' CommandArgument='<%# Eval("Index") %>'
                                                                CssClass="page-link" OnClick="page_changed_kapal" ></asp:LinkButton>
                                                            </li>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <li class="page-item">
											                    <asp:LinkButton  runat="server" Text=">>" CommandArgument="last"
                                                                CssClass="page-link" OnClick="page_changed_kapal" ></asp:LinkButton>
										                    </li>
									                     </ul>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
				        			            </nav>
				        			        </div>
				        		        </div>
				        	        </div>
				                </div>
						  	</div>

						  <div class="tab-pane fade" id="profil-armada" role="tabpanel" aria-labelledby="photo-tab">
						  	<div class="row pt-3 pb-2">
                                  <div class="row"> 
                                          <asp:ListView ID="ListViewArmada" runat="server" OnItemDataBound="ListViewArmada_ItemDataBound">
                                              <ItemTemplate>
                                                    <div class="col-xs-12 col-sm-4 box-content">
                                                      <div class="card">
                                                          <div class="card-body">
                                                              <div class="row">
                                                                  <div class="col">
                                                                      <a class="image-popup-vertical-fit" href="Armada/<%#Eval("img_file")%>">
					  						                            <img src="Armada/<%#Eval("img_file")%>" class="img-fluid mt-2 mb-2" alt="...">
					  					                              </a>
                                                                  </div>
                                                              </div>
                                                              <div class="row pb-3">
                                                                  <div class="col">
                                                                      <p class="text-center">
					  						                            <strong><%#Eval("title")%></strong>
					  					                            </p>
					  					                            <p class="text-center">
					  						                           <asp:LinkButton ID="LbDownloadArmada" CommandArgument='<%#Eval("img_file")%>' OnClick="DownloadArmada" CssClass="red_pis" runat="server">Download</asp:LinkButton>
					  					                            </p>
                                                                      <p class="text-center">
                                                                          <asp:LinkButton ID="LbEditArmada"  runat="server" CommandArgument='<%#Eval("ID")%>' CssClas="btn btn-primary" OnClick="EditArmada"><i class="fa fa-edit"></i></asp:LinkButton>
					  					                            </p>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  
                                              </ItemTemplate>
                                      </asp:ListView>
                                      </div> 
                                  
                                  
                                  
						  		
					  			
						  		
						  	</div>

						  	<!---Pagination-->
                              <div class="row mt-4">
				        	    <div class="col-12 pagination-box">
				        		    <div class="row">
				        			    <div class="col-xs-12 col-sm-6">
				        				    <p id="DetailPagingArmada" runat="server"></p>
				        			    </div>
				        			    <div class="col-xs-12 col-sm-6 text-right">
				        				    <nav aria-label="...">
                                                <asp:Repeater ID="RptArmada" runat="server">
                                                    <HeaderTemplate>
                                                        <ul class="pagination  justify-content-end">
												            <li class="page-item">
                                                                    <asp:LinkButton  runat="server" Text="<<" CommandArgument="first"
                                                                    CssClass="page-link" OnClick="page_changed_Armada"  ></asp:LinkButton>
                                                              </li>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <li class='<%#Eval("active")%>'>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text='<%#Eval("Index") %>' CommandArgument='<%# Eval("Index") %>'
                                                            CssClass="page-link" OnClick="page_changed_Armada" ></asp:LinkButton>
                                                         </li>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <li class="page-item">
											               <asp:LinkButton  runat="server" Text=">>" CommandArgument="last"
                                                           CssClass="page-link" OnClick="page_changed_Armada" ></asp:LinkButton>
										                </li>
												      </ul>
                                                    </FooterTemplate>
                                                </asp:Repeater>
												    </nav>
				        			    </div>
				        		    </div>
				        	    </div>
				            </div>
                              
						  	
						  </div>
						  
						</div>

					</div>
        </div>
        
      </div>
    </section><!-- Media Information Section -->

    <asp:ListView ID="ListViewModalArmada" runat="server">
        <ItemTemplate>
            <div id="modal-armada-<%#Eval("ID")%>" class="zoom-anim-dialog mfp-hide modal-armada container pb-4 pt-4">
		<a class="btn-close-modal"><span class="bi bi-x"></a>
	  	<div class="row mb-5 content">
          <div class="col-xs-12 col-sm-3">
            <div class="col box-image">
              <img src='<%#Eval("img_file")%>' class="img-fluid" alt="...">
            </div>
          </div>
          <div class="col-xs-12 col-sm-9">
            <p class="mb-0"><strong><%#Eval("name")%></strong></p>
            <p><%#Eval("oil")%> </p>

            <p><%#Eval("detail")%></p>
          </div>

	  			<div class="col-12 pt-5">
            <p><strong>Spesifikasi</strong></p>
	  				<table class="table table-borderless table-striped">
                      <thead class="bg-green">
                        <tr>
                          <td colspan="2"></td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>BUILT</td>
                          <td><strong><%#Eval("BuiltOn")%></strong></td>
                        </tr>
                        <tr>
                          <td>FLAG</td>
                          <td><strong><%#Eval("Flag")%></strong></td>
                        </tr>
                        <tr>
                          <td>CALL SIGN </td>
                          <td><strong><%#Eval("CallSign")%></strong></td>
                        </tr>
                        <tr>
                          <td>CLASS DNV-GL </td>
                          <td><strong><%#Eval("CLASS_DNV_GL")%></strong></td>
                        </tr>
                        <tr>
                          <td>PARTICULARS</td>
                          <td><strong><%#Eval("PARTICULARS")%></strong></td>
                        </tr>
                        <tr>
                          <td>LOA</td>
                          <td><strong><%#Eval("LOA")%></strong></td>
                        </tr>
                        <tr>
                          <td>LBP</td>
                          <td><strong><%#Eval("LBP")%></strong></td>
                        </tr>
                        <tr>
                          <td>PARALLEL BODY LOADED </td>
                          <td><strong><%#Eval("PARALLEL_BODY_LOADED")%></strong></td>
                        </tr>
                        <tr>
                          <td>BREADTH MOULDED </td>
                          <td><strong><%#Eval("BREADTH_MOULDED")%></strong></td>
                        </tr>
                        <tr>
                          <td>HEIGHT FROM KEEL TO MASK </td>
                          <td><strong><%#Eval("HEIGHT_FROM_KEEL_TO_MASK")%></strong></td>
                        </tr>
                        <tr>
                          <td>DEPTH MOULDED </td>
                          <td><strong><%#Eval("DEPTH_MOULDED")%></strong></td>
                        </tr>
                        <tr>
                          <td>DRAFT SUMMER </td>
                          <td><strong><%#Eval("DRAFT_SUMMER")%></strong></td>
                        </tr>
                        <tr>
                          <td>DRAFT BALLAST </td>
                          <td><strong><%#Eval("DRAFT_BALLAST")%></strong></td>
                        </tr>
                        <tr>
                          <td>TPC</td>
                          <td><strong><%#Eval("TPC")%></strong></td>
                        </tr>
                        <tr>
                          <td>AIRDRAFT IN BALLAST / LOADED CONDITION </td>
                          <td><strong><%#Eval("AIRDRAFT_IN_BALLAST__LOADED_CONDITION")%></strong></td>
                        </tr>
                        <tr>
                          <td>TOTAL / SIZE MANIFOLDS </td>
                          <td><strong><%#Eval("TOTAL_SIZE_MANIFOLDS")%></strong></td>
                        </tr>
                        <tr>
                          <td>DISTANCE FORM BOW TO CENTRE MANIFOLD </td>
                          <td><strong><%#Eval("DISTANCE_FORM_BOW_TO_CENTRE_MANIFOLD")%></strong></td>
                        </tr>
                        <tr>
                          <td>DISTANCE FORM STERN TO CENTRE MANIFOLD </td>
                          <td><strong><%#Eval("DISTANCE_FORM_STERN_TO_CENTRE_MANIFOLD")%></strong></td>
                        </tr>
                        <tr>
                          <td>DISTANCE BETWEEN MANIFOLDS</td>
                          <td><strong><%#Eval("DISTANCE_BETWEEN_MANIFOLDS")%></strong></td>
                        </tr>
                      </tbody>
                    </table>

                    <table class="table table-borderless table-striped mt-5">
                      <thead class="bg-green">
                        <tr>
                          <td colspan="2"></td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>DISTANCE FORM SHIP SIDE TO CENTRE MANIFOLD </td>
                          <td><strong><%#Eval("DISTANCE_FORM_SHIP_SIDE_TO_CENTRE_MANIFOLD")%></strong></td>
                        </tr>
                        <tr>
                          <td>DISTANCE FROM DECK/TRAY TO MANIFOLD </td>
                          <td><strong><%#Eval("DISTANCE_FROM_DECK_TRAY_TO_MANIFOLD")%></strong></td>
                        </tr>
                        <tr>
                          <td>CARGO TANK CAPACITY </td>
                          <td><strong><%#Eval("CARGO_TANK_CAPACITY")%></strong></td>
                        </tr>
                        <tr>
                          <td>SLOP TANK CAPACITY </td>
                          <td><strong><%#Eval("SLOP_TANK_CAPACITY")%></strong></td>
                        </tr>
                        <tr>
                          <td>BALLAST TANK CAPACITY </td>
                          <td><strong><%#Eval("BALLAST_TANK_CAPACITY")%></strong></td>
                        </tr>
                        <tr>
                          <td>DEADWEIGHT</td>
                          <td><strong><%#Eval("DEADWEIGHT")%></strong></td>
                        </tr>
                        <tr>
                          <td>TONNAGE INTERNATIONAL </td>
                          <td><strong><%#Eval("TONNAGE_INTERNATIONAL")%></strong></td>
                        </tr>
                        <tr>
                          <td>COATING</td>
                          <td><strong><%#Eval("COATING")%></strong></td>
                        </tr>
                        <tr>
                          <td>MAIN ENGINE </td>
                          <td><strong><%#Eval("MAIN_ENGINE")%></strong></td>
                        </tr>
                        <tr>
                          <td>BOW THRUSTER </td>
                          <td><strong><%#Eval("TONNAGE_INTERNATIONAL")%></strong></td>
                        </tr>
                        <tr>
                          <td>CRANES</td>
                          <td><strong><%#Eval("CRANES")%></strong></td>
                        </tr>
                        <tr>
                          <td>CARGO PUMPS </td>
                          <td><strong><%#Eval("CARGO_PUMPS")%></strong></td>
                        </tr>
                        <tr>
                          <td>STRIPPING PUMP </td>
                          <td><strong><%#Eval("STRIPPING_PUMP")%></strong></td>
                        </tr>
                        <tr>
                          <td>BALLAST PUMPS </td>
                          <td><strong><%#Eval("BALLAST_PUMPS")%></strong></strong></td>
                        </tr>
                        <tr>
                          <td>MAX LOAD RATE HOMOGENOUS CARGO PER MANIFOLD </td>
                          <td><strong><%#Eval("MAX_LOAD_RATE_HOMOGENOUS_CARGO_PER_MANIFOLD")%></strong></strong></td>
                        </tr>
                        <tr>
                          <td>MAX LOAD RATE HOMOGENOUS CARGOTHROUGH ALL MANIFOLD </td>
                          <td><strong><%#Eval("MAX_LOAD_RATE_HOMOGENOUS_CARGOTHROUGH_ALL_MANIFOLD")%></strong></strong></td>
                        </tr>
                        <tr>
                          <td>NO. OF CARGO TANKS </td>
                          <td><strong><%#Eval("NO_OF_CARGO_TANKS")%></strong></strong></td>
                        </tr>
                        <tr>
                          <td>NO. OF SEGREGATIONS</td>
                          <td><strong><%#Eval("NO_OF_SEGREGATIONS")%></strong></strong></td>
                        </tr>
                      </tbody>
                    </table>
	  			</div>
	  			
	  		</div>
		</div>
        </ItemTemplate>
    </asp:ListView>
    <asp:TextBox ID="DataShow" runat="server" CssClass="datashow"></asp:TextBox>  
   <!-- Vendor JS Files -->
  <script src="vendor/jquery.min.js"></script>
  <script src="vendor/bootstrap-4.5.3-dist/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="vendor/venobox/venobox.min.js"></script>
  <script src="vendor/waypoints/lib/jquery.waypoints.min.js"></script>
  <script src="vendor/counterup/counterup.min.js"></script>
  <script src="vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="vendor/aos/aos.js"></script>
  <script src="vendor/fontawesome/js/all.min.js"></script>
  <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Template Main JS File -->
  <%--<script src="js/main.js?v=0.1"></script>--%>

  <script type="text/javascript">
      history.pushState({}, null, "armada-kami");
      $(document).ready(function () {
          $(".datashow").fadeOut();
          if ($(".datashow").val() == 'Armada') {
              $('.nav-link').removeClass('active');
              $('.tab-pane').removeClass('active show');
              $('#photo-tab').addClass('active');
              $('#profil-armada').addClass('active show');
              console.log("armada");
          }
          $('.image-popup-vertical-fit').magnificPopup({
              type: 'image',
              closeOnContentClick: true,
              mainClass: 'mfp-img-mobile',
              image: {
                  verticalFit: true
              },
              zoom: {
                  enabled: true,
                  duration: 300
              }
          });

          $('.popup-youtube').magnificPopup({
              disableOn: 700,
              type: 'iframe',
              mainClass: 'mfp-fade',
              removalDelay: 160,
              preloader: false,
              fixedContentPos: false,
          });

          let countGallery = $(".popup-gallery").length;
          for (i = 0; i < countGallery; i++) {
              $(".gallery-" + i).magnificPopup({
                  delegate: 'a',
                  type: 'image',
                  tLoading: 'Loading image #%curr%...',
                  mainClass: 'mfp-img-mobile',
                  gallery: {
                      enabled: true,
                      navigateByImgClick: true,
                      preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                  },
                  image: {
                      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                      titleSrc: function (item) {
                          return item.el.attr('title') + '';
                      }
                  }
              });
          }

          $('.popup-armada').magnificPopup({
              type: 'inline',
              preloader: false,
              modal: true,
              mainClass: 'my-mfp-zoom-in',
          });

          $(".btn-close-modal").bind('click', function (e) {
              e.preventDefault();
              $.magnificPopup.close();
          });

      });

      $(document).click(function (event) {
          var className = $(event.target).prop('class');
          if (className == 'mfp-content') {
              $.magnificPopup.close();
          }
      });

      let shipImg = $(".box-image").find('img');
      $.each(shipImg, function (key, value) {
          let widthImage = value.width;
          let heightImage = value.height;
          console.log(value);
          console.log(heightImage);
          if (heightImage > 200) {
              $(this).css({ 'width': '100%', 'height': '100%' });
          } else {
              $(this).css({ 'height': '100%', 'width': '100%' });
          }
      });
  </script>
</asp:Content>
