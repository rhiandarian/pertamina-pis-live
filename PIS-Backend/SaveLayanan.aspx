﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="SaveLayanan.aspx.cs" Inherits="PIS_Backend.SaveLayanan" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Layanan Data</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Aktivitas Bisnis
                        </li>
                        <li class="breadcrumb-item">
                            <a>Layanan</a>
                        </li> 
                        <li class="breadcrumb-item active">
                            <a href="SaveLayanan.aspx"><strong>Tambah Layanan</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Simpan Layanan</small></h5>
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
            </div>
            <div class="ibox-content">
                <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                 <form>
                      <div class="form-group">
                          <div class="row">
                              <div class="col">
                                  <label for="title"><b>judul</b></label>
                                  <asp:TextBox ID="title" cssClass="form-control" placeholder="Masukkan judul layanan dalam bahasa Indonesia" runat="server"></asp:TextBox>
                              </div>
                              <div class="col">
                                  <label for="title_en"><b>judul (bahasa inggris)</b></label>
                                  <asp:TextBox ID="title_en" cssClass="form-control" placeholder="Masukkan judul layanan dalam bahasa Inggris" runat="server"></asp:TextBox>
                              </div>
                          </div>
                        
                      </div>
                     <div class="form-group">
                        <label for="description"><b>Deskripsi</b></label>
                        <textarea id="description" runat="server" class="summernote" ></textarea>
                     </div>
                     <div class="form-group">
                        <label for="description_en"><b>Deskripsi bahasa inggris</b></label>
                         <textarea id="description_en" runat="server" class="summernote" ></textarea>
                     </div>
                      <div class="form-group">
                        <label for="FileUpload1"><b>Foto</b></label>
                        <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                     </div>
                     <div class="form-group">
                        <asp:Button ID="BtnSave" runat="server" CssClass="btn btn-primary" Text="Simpan" OnClick="BtnSave_Click"      />
                    </div>
                      
                </form>
            </div>
        </div>
    </div>
</asp:Content>
