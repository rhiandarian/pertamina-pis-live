﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Configuration;


namespace PIS_Backend
{
    public class Email
    {
        private string subject;

        private string to;


        public void sendEmail(string subject,string to, string body)
        {
            string fromEmail = ConfigurationManager.AppSettings["emailsend"].ToString();
            string frompass = ConfigurationManager.AppSettings["pwsend"].ToString();
            string emlpss = "from " + fromEmail + ' ' + frompass;


            MailMessage message = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            message.From = new MailAddress(fromEmail);
            message.To.Add(new MailAddress(to));
            message.Subject = subject;
            message.IsBodyHtml = true; //to make message body as html  
            message.Body = body;

            using (SmtpClient client = new SmtpClient())
            {
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(fromEmail, frompass);
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                client.Send(message);
                    
                
                
            }
          
        }
    }
}