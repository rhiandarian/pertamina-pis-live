﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;


namespace PIS_Backend
{
    public class Article
    {
        SqlConnection con = Connection.conn();

        string tableName = "Article";

        DateTime dateNow = DateTime.Now;

        string user_id =  HttpContext.Current.Session["UserID"].ToString();

        ModelData Db = new ModelData();

        private int id;

        private string title;

        private string content;

        private string img_file;

        private int category_id;

        private string city;


        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }


        public string Content
        {
            get { return content; }
            set { content = value; }
        }
        public string Img_File
        {
            get { return img_file; }
            set { img_file = value; }
        }
        public int Category_Id
        {
            get { return category_id; }
            set { category_id = value; }
        }
        public string City
        {
            get { return city; }
            set { city = value; }
        }

       
        public void Insert()
        {
            string field = "";
            string value = "";

            if (title != null)
            {
                field += "title, ";
                value += "'" + title + "', ";
            }
            if (content != null)
            {
                field += "content, ";
                value += "'" + content + "', ";
            }
            if (img_file != null)
            {
                field += "img_file, ";
                value += "'" + img_file + "', ";
            }
            if(category_id != null)
            {
                field += "category_id, ";
                value += "'" + category_id + "', ";
            }
            if(city != null)
            {
                field += "city, ";
                value += "'" + city + "', ";
            }
            field += "addDate, addUser";


            value += "'" + dateNow + "', " + user_id;

            string query = "INSERT INTO " + tableName + " (" + field + ") VALUES (" + value + ")";

            Db.setData(query);
        }
        public void Update(string conditions)
        {
            string newRecord = "set";

            newRecord += title != null ? " title = '" + title + "',  " : "";
            newRecord += content != null ? " content = '" + content + "',  " : "";
            newRecord += img_file != null ? " img_file = '" + img_file + "',  " : "";
            newRecord += category_id != null ? " category_id = '" + category_id + "',  " : "";
            newRecord += city != null ? " city = '" + city + "', " : "";

            string flagUpdate = " editDate = '"+dateNow+"', ";
            flagUpdate += "editUser = " + user_id;

           

            string Query = "Update " + tableName + " " + newRecord + "" + flagUpdate;
            Query += conditions != "" ? "Where " + conditions : "";

            Db.setData(Query);
        }
        public DataTable Select(string field, string conditions = null)
        {
           
            string and = conditions != null ? " and" : "";
            string flagConditions = and+" (fl_deleted != 1 or fl_deleted IS NULL)";

            string query = "Select " + field + " from " + tableName + " Where " + conditions + "" + flagConditions;
            
            DataTable dt = Db.getData(query);

            return dt;
        }
        public void Delete(string conditions = null)
        {
            string query = "Update " + tableName + " set fl_deleted = 1, deleteDate = '" + dateNow + "', deleteUser = " + user_id;
            query += conditions != null ? "Where " + conditions : ""; 
            Db.setData(query);

        }
        public DataTable LoadData(string txtSearch = null)
        {
            

            SqlCommand cmd = new SqlCommand("LoadArticle", con);
            cmd.CommandType = CommandType.StoredProcedure;
            if(txtSearch != null)
            {
                cmd.Parameters.AddWithValue("@txtSearch", txtSearch);
            }
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            return dt;
        }
    }
}