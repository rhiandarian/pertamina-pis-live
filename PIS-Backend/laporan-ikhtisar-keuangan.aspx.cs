﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PIS_Backend
{
    public partial class laporan_ikhtisar_keuangan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindKeuangan();
                bindpage();
            }
        }
        public void editClassLang()
        {
            if (Session["Lang"] != null)
            {
                if (Session["Lang"] == "En")
                {
                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = lbInd.CssClass.Replace("active", "");

                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = "active";
                }
                else
                {
                    LinkButton lbEnd = (LinkButton)Master.FindControl("LbEn");
                    lbEnd.CssClass = lbEnd.CssClass.Replace("active", "");

                    LinkButton lbInd = (LinkButton)Master.FindControl("LbInd");
                    lbInd.CssClass = "active";
                }

            }


        }
        public void bindpage()
        {
            bool langen = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? true : false;
            link1.InnerText = langen ? "Investor Relations" : link1.InnerText;
            link2.InnerText = langen ? "Financial Statement & Highlight" : link2.InnerText;
            ttl.InnerText = link2.InnerText.ToUpper();
            FinancialStatement fs = new FinancialStatement();
            PagesData pd = new PagesData();
            DataTable dt = pd.Select("*", " ID = " + fs.PageID);

            PagesData p = new PagesData();
            imgHeader.Src = dt.Rows[0]["Header"].ToString();
            content.InnerHtml = langen ? dt.Rows[0]["content_en"].ToString() : dt.Rows[0]["content"].ToString();
            editClassLang();

        }
        public void BindKeuangan()
        {
            bool langen = Session["Lang"] != null && Session["Lang"].ToString() == "En" ? true : false;
            FinancialStatement lk = new FinancialStatement();
            string field = langen ? "year as 'title', " : "title, ";
            field += "url";
            lk.OrderBy = "Year desc";
            DataTable dt = lk.Select(field);
            ListViewKeuangan.DataSource = dt;
            ListViewKeuangan.DataBind();
        }
    }
}