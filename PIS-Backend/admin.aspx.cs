﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


namespace PIS_Backend
{
    public partial class admin : System.Web.UI.Page
    {
        SqlConnection con = Connection.conn();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("loginadmin.aspx");
                }
                General all = new General();
                Level l = new Level();
                if(!l.isValidAccessPage(Session["Level"].ToString(),"1"))
                {
                    Response.Redirect("loginadmin.aspx");
                }
                LoadSummary();
                
            }
            
        }
        public void LoadSummary()
        {
            SqlCommand cmd = new SqlCommand("SummaryAll", con);
            cmd.CommandType = CommandType.StoredProcedure;
            


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            ListViewSummary.DataSource = dt;
            ListViewSummary.DataBind();
        }
    }
}