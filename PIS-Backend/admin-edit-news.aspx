﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="admin-edit-news.aspx.cs" Inherits="PIS_Backend.admin_edit_news" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Data Berita</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            Media dan Informasi
                        </li>
                        <li class="breadcrumb-item">
                            <a>Berita</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="ListNews.aspx">Semua Berita</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="admin-edit-news.aspx"><strong>Ubah Berita</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
            <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Ubah Berita.</small></h5>
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <asp:Label ID="LblErrorMessage" runat="server" Text="Label"></asp:Label>
                            <form >
                       <div class="form-group">
                        <div class="row">
                              <div class="col">
                                  <label for="title"><b>judul</b></label>
                                  <asp:TextBox ID="title" cssClass="form-control" placeholder="Masukkan judul berita dalam bahasa Indonesia" runat="server"></asp:TextBox>
                              </div>
                              <div class="col">
                                  <label for="title_en"><b>judul bahasa inggris</b></label>
                                  <asp:TextBox ID="title_en" cssClass="form-control" placeholder="Masukkan judul berita dalam bahasa Inggris" runat="server"></asp:TextBox>
                              </div>
                          </div>
                      </div>
                      <div class="form-group">
                        <div class="row">
                              <div class="col">
                                  <label for="key"><b>kata Kunci</b></label>
                                  <asp:TextBox ID="key" cssClass="form-control" placeholder="Masukkan kata kunci untuk tautan dalam bahasa Indonesia" runat="server"></asp:TextBox>
                              </div>
                              <div class="col">
                                  <label for="key_en"><b>kata Kunci bahasa inggris</b></label>
                                  <asp:TextBox ID="key_en" cssClass="form-control" placeholder="Masukkan kata kunci untuk tautan dalam bahasa Inggris" runat="server"></asp:TextBox>
                              </div>
                          </div>
                      </div>
                     <div class="form-group">
                        <label for="date"><b>tanggal</b></label>
                        <asp:TextBox ID="date" cssClass="form-control" placeholder="Masukkan tanggal berita" runat="server"></asp:TextBox>
                      </div>
                     <div class="form-group">
                        <label for="content"><b>konten</b></label>
                        <textarea class="summernote" id="content" runat="server" placeholder="Input Content News" rows="10"></textarea>
                         
                      </div>
                      <div class="form-group">
                        <label for="content_en"><b>kontent bahasa inggris</b></label>
                        <textarea class="summernote" id="content_en" runat="server" placeholder="Input Content News" rows="10"></textarea>
                         
                      </div>
                      <div class="form-group">
                        <label for="ddlCategory"><b>Kategori</b></label>
                          <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control"></asp:DropDownList>
                      </div>
                     <div class="form-group">
                        <label for="FileUpload1"><b>Foto</b></label>
                        <asp:FileUpload ID="FileUpload1" cssClass="form-control-file" runat="server" />
                         
                     </div>
                                <div class="form-group">
                                    <asp:Image ID="Image1" runat="server" />
                                </div>
                    <div class="form-group">
                        <asp:Button ID="BtnSave" runat="server" CssClass="btn btn-primary" Text="Simpan Sebagai Draf" OnClick="BtnSave_Click"     />
                        <asp:Button ID="BtnPublished" runat="server" CssClass="btn btn-success" Text="Terbitkan" OnClick="BtnPublished_Click" />
                    </div>
                            </form>
                        </div>
                    </div>
                </div>
</asp:Content>
